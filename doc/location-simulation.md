## TamraSDK : Location Simulation

이 문서는 Tamra SDK의 `Location Simulation` 기능에 대한 사용법을 다룬다. 
이 기능을 제공하는 기본 목적은 *TamraSDK*를 통한 앱을 개발함에 있어서 비콘이 실제로 설치된 환경으로 이동하여 테스트가 어려운 점을 감안하여 개발 과정에서 약간의 편의를 제공하기 위함이다.

### 주의사항

`Location Simulation` 기능은 *TamraSDK* `v0.9.0` 이후 버전에서만 지원하며 해당 기능에서 제공하는 비콘 신호는 물리적인 비콘 신호의 패턴과 다를 수 있으므로 앱의 특성에 따라 개발 후 실제 환경에서 별도로 확인하는 것을 권장한다.
**개발에 편의성을 제공하는 기능으로 테스트용 환경에서만 작동이 됩니다.**

### Location Simulation 기능의 설정

*TamraSDK* 초기화 과정에서 `Config` 설정 시 **`onSimulation()`** 호출을 통해 활성화할 수 있다.
> 이 경우 기기상의 물리적인 비콘 센서는 활성화되지 않는다.

- 기본 모드 (기기의 센서 이용)

```Java
    ...

    Config config = Config.forRelease(
            getApplicationContext(),
            "APPKEY");
    Tamra.init(config);
    
    ...
```

- `Location Simulation` 기능 활성화 모드 (기본 기기명으로 지정)

```Java
    ...

    Config config = Config.forRelease(
            getApplicationContext(),
            "APPKEY")
            .onSimulation();
    Tamra.init(config);
    
    ...
```

- `Location Simulation` 모드의 커스텀 기기명 지원

    - 가상 비콘 신호 생성 도구에 보여지는 이름을 아래와 같이 지정할 수 있다.
    - 여러 개발자가 동시에 테스트를 할 경우, 각각의 기기에 대한 구분을 제공하며 기본 값은 `Android`의 `Build.MODEL`로 설정된다. 

```Java
    ...

    Config config = Config.forRelease(
            getApplicationContext(),
            "APPKEY")
            .onSimulation("My Device");
    Tamra.init(config);
    
    ...
```

### 가상 비콘 신호의 생성

- `Location Simulation` 기능이 활성화된 *TamraSDK*에 **가상 비콘 신호**를 보내려면 *Oreum Zigi*를 이용해야 한다.

- *Oreum Zigi*의 **설치지역** 탭 우측상단에는 *Oreum Zigi*를 `Location Simulation` 모드로 전환할 수 있는 버튼이 있다.

    ![](images/location-simulation-1.png)
    
- 이 버튼을 클릭하면 다음과 같은 컨트롤바가 나타난다.

    ![](images/location-simulation-2.png)
    
    1. **지속시간** : 발생시킬 신호에 대한 지속시간을 나타내며, `하루` 혹은 `15초`를 선택할 수 있다. 이 설정은 선택된 기기에 발생하는 모든 신호에 적용된다.
    2. **정확도** : 발생시킬 신호에 대한 정확도를 나타내며 `Immediately`(1m 이내), `Near`(1~3m 이내), `Far`(3m 이상)을 선택할 수 있다.
    3. **앱** : 앱 키를 발급받은 앱 목록을 보여주며, 신호를 전달할 앱을 선택할 수 있다.
    4. **기기** : 지정한 앱을 실행한 기기 중 `Location Simulation` 모드로 동작중인 기기 목록을 보여주며, *TamraSDK* 초기화시 지정한 기기명으로 표시된다. 기기가 없을 경우 **'대기중인 기기가 없습니다.'** 라는 문구가 출력된다.
    5. **발생중인 신호 제거** : 지정된 기기를 대상으로 발생중인 모든 신호를 제거한다.
    6. **Location Simulation Mode OFF** : `Location Simulation` 모드를 끈다. 모드를 끄더라도 **발생중인 신호가 제거되는 것은 아니다**.
    
- 기기까지 선택된 상태에서 지도상의 `POI`를 클릭하거나 우측 비콘 목록의 `가상신호 생성`버튼을 클릭하면 가상신호가 생성되어 *TamraSDK*로 보내진다.

- 발생중인 가상신호는 다음과 같이 지도상에서 시각적으로 표시된다.

    ![](images/location-simulation-3.png)
