## 개요

**Tamra**는 다양한 기관 및 단체가 소유 및 관리하고 있는 *Beacon*들을 관광 플랫폼 인프라스트럭쳐로써 활용할 수 있도록 다수의 *Beacon Region*을 통합하며, Beacon을 활용한 **모바일 App** 개발을 지원하기 위한 *Mobile SDK* 제공을 목표로 하고 있다.

### 용어

- **Oreum Platform**
    - Kakao에서 개발하는 제주 스마트 관광 플랫폼.
    - 비콘 관리 시스템(`OreumZigi`), 비콘 SDK(`Tamra`), 비콘 기반 CMS, 데이터 분석 서비스 등을 포함함
- **Tamra SDK**
    - **Oreum Platform**의 모바일용 Beacon SDK.
    - `iOS`, `Android` 용 SDK가 제공 (iOS용은 2016 하반기 릴리즈 예정)

### Role

**Oreum Platform**은 Beacon 관련 서브시스템에 대하여 아래와 같은 3가지 Role을 가정한다.

- **Beacon Region Provider**
    - *Beacon* 소유자로써 `Tamra`에 자신의 `Beacon Region`을 공공 인프라스트럭처로써 **Oreum platfrom** 제공하는 자.
    - 2016년 3월 현재, 제주창조경체혁신센터, Kakao, 제주공항, 제주도청 등의 *Beacon Region Provider*역할을 하는 플랫폼 파트너가 있음.
- **Platform Consumer**
    - Beacon을 활용한 모바일App을 개발하는 개발자 혹은 회사.
    - `Tamra SDK`의 직접적인 사용자.
- **Platform Owner**
    - 플랫폼을 개발하며 관리하는 주체 (제주창조경제혁신센터/Kakao)
    - 현재 Kakao에서 개발 중.

![](images/CFDDBB04EB1B22C5E238CC2D851798F4.jpg)

### Values

각 Role Player에게 **Oreum Platform** 아래와 같은 가치를 제공한다.

#### for Beacon Region Provider

제주창조경제혁신 센터에서 추진하는 사업인 만큼 제주 스마트 관광을 위한 통합 플랫폼의 공공성을 확보하여 Beacon을 활용한 다양한 App이 활성화하는데 기여하게 될 것이다.

또한 자기 소유의 Beacon을 가진 회사에게는 비콘관리기능과 비콘의 접근통계를 통해 저비용 고효율의 비콘 인프라 관리 효율을 제공하게 되며,
`Tamra SDK`를 통해 모든 Beacon Region에 대해 통합된 단일 인터페이스를 얻을 수 있다.

#### for Platform Consumer

`proximityUUID`, `major`, `minor`의 복합키 형태를 갖는 Beacon 식별자로 프로그래밍해야 하는 일반적인 Beacon API와는 달리,
`Tamra SDK`는 비콘을 활용한 위치기반의 App을 개발이 용이하도록 아래와 같은 기능을 제공한다.

- 비콘의 설치 위치와 방향이 표기된 실내/실내 지도를 제공하며, platform 내부의 단일 식별자 제공(`long` 타입)
- owner가 다른 다수의 Beacon Region을 자동으로 감지하고 효율적으로 monitoring & ranging을 수행하여 비콘에 대한 정보(`Spot`)를 제공
- 위치 정보에 맞게 추상화된 Fluent API. (e.g. 동선 tracking, 방문 history)
- `iOS`, `Android` 모두 동일한 interface 제공. (iOS는 하반기 예정)
    - android의 경우 국내 주요 스마트폰 모델들에 대한 beacon 신호의 조정이 포함된.
- 지점 정보에 App사의 Custom Data를 매핑하고 Online으로 관리(Online CMS)할 수 있음(하반기 예정)

## Architecture

`Tamra SDK`는 **Oreum Plafform**의 모듈로써 아래와 같이 구성된다.

![](images/89ACC65CDA9168DD56C99138DB185E5F.jpg)
