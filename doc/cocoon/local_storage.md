## Local Storage

### 왜 필요한가
- Beacon·Region 정보 증가에 대한 Network Traffic 최소화 준비
- Network 불가시 대응??

### Android에서의 구현 방법
1. Key-Value
    - ex) Preferences
2. SQLite
    - Relational DB

### 기존 메모리 방식에서 변경했을때 예상되는 이슈
1. API와의 동적 데이터 연동
2. 삭제 또는 숨김되는 비콘이 있을 경우 처리

### SQLite를 사용하게 된 이유
1. 데이터베이스 형태로 쓸 수 있어서 편하다.
2. Android 기본 제공 라이브러리이다.
   (Realm 같은 편리한 외부 라이브러리가 있지만 SDK라는 특성상 외부 Library에 대한 의존도를 낮게 유지할 필요가 있다.) 

### 연동
1. 관련 클래스가 초기화될때 변경분 데이터를 가져와 sync를 맞춘다.


### 참고문서
- https://developer.android.com/training/basics/data-storage/databases.html?hl=ko