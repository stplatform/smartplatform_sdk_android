
*android-library* 로 빌드되는 **Tamra SDK** 특성상 외부 의존 library를 가지는 것이 부담스러운 관계로 내부에서 별도 `Logger` 클래스를 만들어서 사용하고 있다. 
 
본 문서에서는 내부 `Logger` 클래스와 기본 logging정책에 대해 설명한다. 

## Why? 

`android.util.Log`를 직접 사용할 경우 제어되지 않는 아래와 같은 문제가 있다. 

- unit test에서 console 출력을 해주지 않음.
- 출력 문자열에 CR(`\r`: 캐리지리턴)이 있는 경우 정상적으로 로그 출력이 안됨.
- SDK를 사용하는 app에는 내부 debug log를 감추고 싶음. 
- 그러나 **모름지기** 에서는 로그 레벨을 조정할 수 있으면 좋겠음.

## Logger 사용

사용방법은 `slf4j` logger와 동일하며, 기본적인 argument 처리를 지원한다. 

```java
import com.kakao.oreum.common.logger.Logger;
import com.kakao.oreum.common.logger.LoggerFactory;


public class MyClass {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    
    public void aMethod() {
        logger.debug("hi, {}", "there");
    }
}
```


## On Android Device

기본 `Logger`구현체는 `AndroidLogger`이고 이 클래스는 `android.util.log`를 wrapping한다. 
 
단, 아래와 같은 차이가 있다.
 
- **debug** 빌드에서는 `INFO` 레벨 이상의 로그만 출력한다.
- **release** 빌드에서는 `ERROR` 레벨 이상의 로그만 출력한다.
- *TAG* 값으로는 `OREUM`이 사용된다. 

출력되는 Log Format은 다음과 같다. 

```
05-26 16:05:47.311 31581-31701/com.kakao.oreum.oreumzigi I/OREUM: HttpCall - HTTP Request:
                          GET /spots/243 HTTP/1.1
                          Host: test-api-cocoon.kakao.com
                          User-Agent: Tamra/0.7.0 (SM-N920S; samsung) Android/23 (REL; 6.0.1; ko_KR)
                          Content-Type: application/json
                          Oreum-CID: android/com.kakao.oreum.oreumzigi/6e8ae9fb-c3c3-4890-9fea-cc4ee546511a
                          Oreum-Token: uxEsxkullcOGX7g7j1LTFO9GyoQTWOpdYSKWOZ9WxGynoR3xRt5VGjm0sGqUD15I
```

## On JUnitTest

JUnit Test 환경을 위해서 `UnitTestLogger`를 제공한다. 

```java
// 아래 코드가 수행된 이후에 LoggerFactory는 logger는 `UnitTestLogger` 구현체를 반환한다. 
LoggerFactory.onUnitTest(true);
```

`UnitTestLogger`는 unitTest에서 사용하기 위한 용도이므로 소스코드 참조가 가능하도록 아래와 같이 포맷된다. 

```
05-26 17:35:10.316 [main] DEBUG c.k.o.d.h.HttpCall.call(HttpCall.java:56) - HTTP Request:
                         GET /spots/243 HTTP/1.1
                         Host: test-api-cocoon.kakao.com
                         User-Agent: Tamra/0.7.0 (SM-N920S; samsung) Android/23 (REL; 6.0.1; ko_KR)
                         Content-Type: application/json
                         Oreum-CID: android/com.kakao.oreum.oreumzigi/6e8ae9fb-c3c3-4890-9fea-cc4ee546511a
                         Oreum-Token: uxEsxkullcOGX7g7j1LTFO9GyoQTWOpdYSKWOZ9WxGynoR3xRt5VGjm0sGqUD15I
```

## On 3rt-party App

Tamra SDK를 사용하는 3rd-party app에서 `Logger`는 해당 app의 빌드 설정을 따라 작동한다.
  
- app의 `BuildConfig.DEBUG=true`라면
    - `INFO` 레벨 이상의 Tamra 로그만 `android.util.Log`를 통해 출력된다.
    - *Tag* 값은 `OREUM`으로 출력되므로 *Tamra* 내부 log만 필터링 하려는 경우 *tag* 값으로 필터링할 수 있다. 
- app의 `BuildConfig.DEBUG=false`라면
    - `ERROR` 레벨 이상의 Tamra 로그만 출력된다.
    - `ERROR` 레벨의 로그를 남기는 이유는 app의 오류추적을 위함이다. 
    
## On MOreumZigi 

`MOreumzigi`는 Tamra의 `debug` 빌드를 사용하므로 `INFO` 레벨 이상의 로그가 출력된다.

단, `MOreumzigi`는 ProGuard가 적용되지 않은 빌드를 사용하므로 아래와 같은 방법으로 Log Level을 제어할 수 있다.
 
```java
    // tamra 초기화 전에 수행되어야, 로그 레벨이 적용된다.
    LoggerFactory.setLogLevel(Level.TRACE);
    Tamra.init(...);
```

