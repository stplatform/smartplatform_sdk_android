
이 문서는 *고치TF(cocoon.tf)* 내부 개발자의 잡다한 정보, 정책, 이슈들을 다루기 위한 문서입니다.

## index

- [Proguard 적용](./proguard.md)
- [SDK Release 가이드](./release.md)
- [예외 처리 방법](./handle-exception.md)
- [Logger 사용](./logger.md)
- [UnitTest tips](./unit-test-tips.md)
- [SDK 문서 배포](./document-deploy.md)
- [v0.8.0 이후 계획](./after-v0.8.0.md)

