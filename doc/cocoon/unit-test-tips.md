
## UnitTest tips

### `OxTasks`를 사용하는 코드에 대한 테스트
 
*SUT(system under test)*에서 `OxTasks`를 사용하여 비동기 처리를 하는 경우 아래와 같이 한다. 

```java
public class SUT {
    public void method1(Runnable runnable) {
        OxTasks.runOn(RunOn.BACKGROUND, () -> runnable.run());
    }
}
```

```java

public class SUTTest extends AndroidContextTest {


    public void test_case1() {
        // Given
        SUT sut = new SUT(..);
        Runnable runnable = mock(Runnable.class);
        
        // When 
        sut.method1(runnable);
        
        // Then  
        backgroundLooper().idle(); // looper의 queue를 모두 처리함. 
        verify(runnable, times(1)).run();
    }
}
```

`AndroidContextTest` 클래스에는 `RobolectricTestRunner`로 테스트가 실행되며 
`OxTasks`의 `OxTasks.Loopers.MAIN_LOOPER`와 `OxTasks.Loopers.BACKGROUND_LOOPER`에 대한 
`ShadowLooper`를 제공하는 `#backgroundLooper()`, `#mainLooper()` 메소드가 정의되어 있다.
 
따라서 위의 예제 코드와 같이 `backgroundLooper().idle()`을 수행하면 looper의 queue가 모두 처리되며 
별도의 `Thread.sleep()`이 필요하지 않다. 