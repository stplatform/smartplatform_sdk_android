
## Release Guide.

gradle-plugin으로 작성된 `gradle-release-plugin`을 사용하여 version 등을 관리한다.
 

### 버전정책

- 버전형식 : `major.minor.patch` (e.g. `1.2.1`)
- `major` 버전 
    - SDK의 인터페이스 상의 큰 변화를 야기하는 변경이 있을 경우 올림. 
    - SDK의 로드맵의 영향을 받는다.  
- `minor` 버전
    - SDK의 인터페이스 상의 소소한 변경이 있을 수 있다.
    - 전체 컨셉의 변경 없이 기능이 추가될 때 올라간다.
- `patch` 버전
    - 인터페이스나 기능 상의 변경 없이 내부 로직 개선 및 버그픽스 등의 소소한 변경에 대한 버전

당분간 정식 릴리즈는 `minor` 버전을 올리고 핫픽스 릴리즈의 경우 `patch`버전을 올린다.
 
아직 추후 로드맵에 대해서는 논의된 바 없으므로 `v0.x.y` 버전을 유지하되, 
SDK의 컨셉 및 인터페이스가 최종적으로 안정화 되는 시점에 `v1.0.0`을 릴리즈한다.
(잠정적으로 iOS 버전의 안정화 버전 출시 시점에 `v1.0.0`을 릴리즈하는 것을 계획으로 한다.)
 
### Branch 정책 

기본적으로 `git-flow`의 branch 정책을 따른다. 

하지만 현재 개발인력이 1~2명 수준이고 별도의 QA프로세스가 존재하지 않으므로 `release`브랜치에서의 commit은 없고, 
`releaese` 브랜치는 자동 tagging 용도로만 사용한다.

- **master** branch 
    - 항상 마지막으로 릴리즈된 최종 소스와 동일함을 보장한다.
    - `master` 브랜치는 `RELEASE`용 binary를 생성하게 된다.  
- **develop** branch 
    - 개발작업이 이루어지는 브랜치이다. 
    - 별도의 병렬 브랜치가 없다면 `develop` branch는 현재 개발버전의 `SNAPSHOT` binary를 생성한다.


### Build Pipeline

빌드 파이프라인의 세부 설정은 [Go Server](http://go.r2d2.devel.kakao.com)을 참고하라. 

#### `RELEASE` 빌드 파이프라인

![](img/build-pipeline-RELEASE.png)

- `RELEASE.BuildTamra`
    - `develop` 브랜치의 소스를 테스트하고 문서를 생성한다. (문서 생성은 [문서 배포방식](./document-deploy.md)을 참고)
    - 생성된 문서 파일들은 go server에 artifact로 upload된다. 
- `RELEASE.UploadTamra`
    - `RELEASE.BuildTamra`가 성공하면 자동으로 수행되며 생성된 binary(pom artifact)를 maven repository에 업로드한다.
    - repository 정책상 동일 버전의 binary를 다시 upload할 수 없다. 이 과정에서 문제가 발생하면 버전을 올려서 다시 빌드해야 한다. 
- `RELEASE.DeployTamraDocs`
    - `RELEASE.BuildTamra`가 성공한 후에 실행가능하며 **수동**으로 수행해야 한다. 
    - 문서 배포가 수동인 이유는 *문서의 배포*가 변경에 대한 Notification이 이루어지는 **최종 릴리즈 단계**이기 때문이다.
        - (문서를 배포하지 않으면 어느 누구도 새로운 버전이 릴리즈되었음을 알 수 없다)
    - `RELEASE.UploadTamra`와 `RELEASE.BuildSampleApp` 가 성공한 후에 수행하도록 한다.  
- `RELEASE.BuildSampleApp`
    - 릴리즈 된 binary로 SampleApp 소스를 빌드 및 테스트하는 단계이다.
    - 버전별 소스 호환성 등을 검증하기 위한 단계이다. 
    - 현재 이 단계는 기능 검증용으로 구성되지는 않았다.

#### `SNAPSHOT` 빌드 파이프라인 

![](img/build-pipeline-DEV.png)

`SNAPSHOT` 빌드 파이프라인은 `RELEASE` 빌드 파이프라인과 거의 동일하지만 아래와 같은 부분이 다르다.

- `DEV.AnalysisTamra`
    - 소스 코드에 대한 정적/동적 분석을 수행하는 단계이며 매 빌드마다 수행된다.
    - sonarQube를 사용하며 [SonarQube - Tamra](http://sonar.r2d2.devel.kakao.com/dashboard/index/1827)에서 분석 레포트를 볼 수 있다. 
- `DEV.DeployTamraDocs`
    - `RELEASE` 빌드와는 달리 `SNAPSHOT`빌드의 문서는 외부로 publication 되지 않으므로 항상 **자동**으로 수행된다. 
    - 문서는 [개발서버의 문서 페이지](http://oreum.devel.kakao.com/#/documents/tamra/getting-start)에서 확인할 수 있다. 

### 개발 및 릴리즈 절차
 
특정 버전의 릴리즈 버전을 위한 *개발시작*부터 *릴리즈*, *확인* 단계를 모두 서술한다. 

#### 개발 시작 

- `${PROJECT.ROOT}/build.gradle`의 `tamraVersion`을 다음 릴리즈 버전으로 수정한다. 
- Android Studio의 `Editor > File and Code Templates > Include > File Header`에서 `@since` 부분의 버전을 다음 릴리즈 버전으로 수정한다.
- **SampleApp**의 의존성을 다름 릴리즈 스냅샷 버전으로 수정한다. 

#### 개발 단계 

개발이 진행되는 단계에서는 항상 하기 사항들이 준수되어야 한다.

- 빌드 파이프라인이 항상 성공적으로 수행되고 있는가?
- 기술부채가 적절히 통제되고 있는가?
    - 일정 및 상황에 따라 기술부채는 늘어날 수 있다. 중요한 것은 의도치 않게 기술부채가 늘어나는 상황이다.  
- `SampleApp` 빌드가 성공적으로 수행되는가?

#### 코드 프리징 준비

`git flow`의 `release` 브랜치를 빌드 단계에서 활용하지 않으므로 아래의 모든 체크사항은 `develop` 브랜치에서 진행한다. 만약 아래의 체크 항목을 pass하면 코드 프리징을 한다. 
  
코드가 프리징되면 문서 변경 외의 커밋은 할 수 없으며 다시 코드 상의 변경이 발생하면 아래 체크항목을 다시 점검하도록 한다. 

- `/build.gradle` `tamraVersion`이 릴리즈할 버전과 동일한가?
- `SampleApp` 빌드가 성공적으로 수행되는가?
    - (주의) `gradle`이 `SNAPSHOT` 버전의 artifact를 매번 다운로드 하지 않을 수 있음에 유의 (`change=true`를 설정해도 마찬가지)
- **MOreumZigi**의 빌드가 디바이스에서 정상적으로 동작하는가?
    - beacon 목록이 표시되는가?
    - spot 목록이 표시되는가?
- **SampleApp**의 빌드가 디바이스에서 정상적으로 동작하는가?
- 지난 버전과 비교한 [코드 분석 지표](http://sonar.r2d2.devel.kakao.com/dashboard/index/1827?did=1&period=3)가 충분히 납득할만 한가?
    - 그렇지 않다면 **Refactoring**을 수행하여 코드 품질을 개선하거나 릴리즈 일정을 조율할 것.
    - **Test Coverage**는 `>80%`을 유지하거나 이전 버전보다 높은 수준으로 유지한다. 
    - **기술부채(Technical Debt)**는 일정상의 이슈가 있는 경우가 아니면 증가하지 않도록 한다.
    - **Blocker**, **Critical** 이슈는 항상 **0**로 유지한다.
- [Maven Repository](http://devrepo.kakao.com:8088/nexus/content/groups/public/com/kakao/oreum/tamra/)에 릴리즈할 버전에 대한 `SNAPSHOT` binary가 존재하는가?
    - `SNAPSHOT` 버전의 binary가 빌드 파이프라인의 최종 수행시각의 일치하는가?
    - `javadoc` qualifier의 artifact도 존재하는가?

#### 릴리즈 변경사항 점검

- `ReleaseNote.md`에 변경사항의 누락이 없는가?
    - [diff: master - develop](https://github.daumkakao.com/cocoon/tamra-android/compare/master...develop?expand=1)
    - [jira release](http://jira.daumkakao.com/projects/COCOON?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page)
    - 위 두 자료를 기준으로 누락된 변경사항이이 없도록 작성되어야 한다. 
- SDK 사용예에 대한 변경이 `GettingStart.md`에 반영되었는가?
    - 문서의 베이스 **버전이 변경되었는가?**
    - gradle dependency에 대한 code quot에 **SDK 버전이 변경되었는가?**
    - 이번에 *deprecated* 되거나 사라진 인터페이스에 대한 노트가 있는가? 
    - 새로 추가된 인터페이스 및 기능에 대한 설명이 포함되었는가?
- `javadoc` 체크사항 ([Tamra SDK Javadoc](http://oreum.devel.kakao.com/tamrasdk/javadoc/)) 
    - 한글 텍스트가 정상적으로 보이는가?
    - `Tamra` class의 javadoc에 포함된 예제 코드가 현 버전에서도 유효한가?
    - `NearbySpots` class의 javadoc에 포함된 예제 코드가 현 버전에서도 유효한가?
    - 난독화되는 패키지의 클래스가 javadoc에 포함되어 있지 않은가?

#### 릴리즈 

릴리즈 준비 단계에서 확인 과정을 거쳤으므로 릴리즈 단계는 아래와 같이 수행한다.

```
$(develop) TAMRA_VERSION=v0.7.0  # set version
$(develop) git flow release start $TAMRA_VERSION
$(develop) git flow release finish $TAMRA_VERSION
$(master) git push 
$(master) git push --tags
$(develop) git checkout develop
```

#### 최종 확인 및 문서 배포

- **RELEASE 빌드 파이프라인**이 모두 수행될 때까지 기다린다.  
- `SampleApp` 빌드 및 검증이 성공적으로 완료되었다면 문서를 배포한다.(`RELEASE.DeployTamraDocs`)
- 문서배포가 완료되면 **test**와 **production** 서버에 문서가 정상적으로 배포되었는지 확인한다. 
    - 문서의 SDK 베이스 버전이 변경되었는가?
    - javadoc 페이지의 title에 버전이 정상적으로 표시되는가?
    - (문서의 상세 내용에 대한 검증은 릴리즈 준비단계에서 수행되었다고 가정하고 여기에서는 별도 체크항목으로 표기하지 않는다)


### 패치 버전의 릴리즈 

상기의 릴리즈 절차와 동일하다.

단, 패치 버전의 경우 `git flow`의 `hotfix` 브랜치를 사용하여 작업하고, 
`hotfix` 작업을 하는 동안은 `DEV.BuildTamra` 파이프라인의 material을 해당 *hotfix* 브랜치를 가리키게 한 후 **코드 프리징 준비** 단계부터 수행한다.  

### release-plugin

[gradel-release-plugin](https://github.daumkakao.com/cocoon/gradle-release-plugin) 참고.

