
## ProGuard 적용

`tamra` 모듈에는 ProGuard를 적용하여 난독화 및 최적화된 빌드를 릴리즈 한다. 

현재 SDK 외부에 노출되는 `com.kakao.oreum.tamra`와 `com.kakao.oreum.common` 패키지를 제외한 모든 클래스를 난독화한다. 

```
-keep public class com.kakao.oreum.tamra.*
-keep public class com.kakao.oreum.common.**


-keepclassmembers class com.kakao.oreum.tamra.* {
   public *;
   protected *;
}
-keepclassmembers class com.kakao.oreum.common.** {
   public *;
}
```

난독화 결과는 `build/outputs/mapping/mapping.txt` 파일에서 확인할 수 있다.

단, `MOreumZigi`에서는 `BeaconService` 등의 내부 객체를 사용을 위해서 ProGuard가 적용되지 않은 빌드를 사용하도록 한다. ('debug')
관련 설정은 아래와 같다. 

```gradle
// in MOreumZigi 

dependencies {
    compile project(path: ':tamra', configuration: 'debug') // proguard가 적용되지 않은 것을 사용.
}
```

```gradle
// in Tamra
android {
    // oreumzigi에서 'debug' build를 참조할 수 있게 publishNonDefault를 true로 설정한다. 
    publishNonDefault publishConfig()
    buildTypes {
            debug { // debug 결과물은 oreumzigi에서 사용.
                debuggable true
                minifyEnabled false
            }
            release {
                minifyEnabled true
                proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            }
        }
    }
    ...
}
configurations {
    debugCompile
    releaseCompile
}

// 기본은 'debug'
// `release`로 설정하면 proguard가 적용된다.
// deploy할 때는 'tamra.publish.config'를 'release'로 설정하자.
// e.g. gradle clean deploySnapshot -Ptamra.publish.config=release
def publishConfig() {
    project.hasProperty('tamra.publish.config') ? project.getProperty('tamra.publish.config') : 'debug'
}
```

난독화된 버전으로 릴리즈하려면 아래와 같이 추가 속성을 준다. 

```
# for snapshot 
gradle clean deploySnapshot -Ptamra.publish.config=release
# for release
gradle clean deployRelease -Ptamra.publish.config=release
```
