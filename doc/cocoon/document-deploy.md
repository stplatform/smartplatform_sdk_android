
## SDK 문서 배포 방식

Tamra SDK 문서는 두 가지 종류로 구성된다. 

- Markdown 문서
    - `.md` 확장자를 가진 파일로 OreumZigi에서 markdown rendering 통해 서비스되는 문서다. 
- javadoc 문서
    - 난독화되지 않는 공개 클래스에 대한 javadoc 문서. 
    - 빌드 단계에서 자동 생성된다.
    
### 문서 배포의 문제 

기존에는 SDK사용자가 보게 되는 문서가 OreumZigi 서버의 소스 내에 있었기 때문에 
SDK 배포과정과 서버 배포의 과정이 상호간의 의존성을 가지고 있었으며, 배포의 순서 역시 중요했다. 

이러한 배포과정의 복잡성을 줄이기 위해서 TamraSDK 배포 파이프라인과 서버(OreumZigi)의 배포 파이프라인을 분리한다. 
 
- 서버 측(`OreumZigi`)
    - 웹서버에서 파일 호스팅되는 특정 경로(`/data/servcies/tamrasdk/doc`) 밑에 있는 `.md` 파일을 rendering하도록 한다. 
    - 문서 목록 역시 동적으로 읽어서 *TOC*를 구성한다 (`/data/services/tamrasdk/doc/toc.json`)
    - javadoc은 별도 창으로 뜨도록 구성. 
    
- Tamra 빌드 : 문서 패키징 
    - 소스 상의 `/doc` 디렉토리 내용 전체를 `/build/sdkdoc/doc`으로 복사한다. (`toc.json` 포함)
    - 생성된 생성된 javadoc (`tamra/build/docs/javadoc`)디렉토리를 `/build/sdkdoc/javadoc`으로 복사한다. 
    - 배포를 위한 스크립트(`script/deploy-doc.sh`)을 `/build/sdkdoc`으로 복사한다.
    - `/build/sdkdoc` 디렉토리를 go pipeline artifact로 지정하여 go server 업로드한다.
    - (주의) 문서 복사 후에 아래와 같은 부가적인 작업이 이루어진다.
        - markdown rendering시에 문제가 있는 `<`를 `&lt;`로 치환한다. 
        - markdown 문서간의 inter-link 처리를 위해 상대경로를 OreumZigi 문서 호스팅에 맞게 변경한다. 
            -e.g. `[hello](./hello-world.md)` -> `[hello](/#/documents/hello-world)`

- Tamra 문서 배포
    - 문서 배포는 외부에 SDK를 publishing하는 과정이므로 pipeline을 manual로 구성함. 
        - 단, 개발 서버로 배포시에는 자동으로 배포됨. 
 
### 잠재적 이슈

- `.md` 파일 내에 image를 포함하는 경우에 대한 경로 치환로직은 없는 상태임.
- `.md` 파일들을 subdirectory를 가진 형태로 구성할 수 없음. 
- `/doc/toc.json`의 형식을 바꾸는 경우 서버 측(OreumZigi) 소스도 함께 변경되어야 함. 

