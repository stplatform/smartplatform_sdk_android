## Next Plan 

`v0.8.0` 릴리즈 이후 개발 계획. 
(iOS 버전은 android의 `v0.8.0` 버전을 목표로 9월까지 구현)

## Location Simulator. 

서버(OreumZigi) 기반의 비콘 신호 시뮬레이터. 
 
전략. 

- `BeaconService` 인터페이스를 구현하여 교체. 
    - 주기적(수 초)으로 서버에 active beacon 질의.
    - 모니터링 중인 region에 속하는 active beacon 들에 대해서 주기적(200~500msec)으로 ranged 이벤트 제공. 
        - region entry 이벤트는 최초 ranged beacon에 대해서 발생. 
        - region exit는 ranged beacon이 없을 때 발생
        - range 신호 강도는 서버에 기준 치를 받고 약간의 delta를 양념으로 쓰는 형태??
- `Config.forSimulation()` 같은 메소드로 별도의 설정 객체 factory 제공

**question**

SDK 구현에서 신경쓸 건 아니지만... 

- 서버에 동일 계정으로 2명 이상이 접속하여 가상신호를 시뮬레이션 할 경우 어떻게?
    - simulation session 개념을 쓸 건가? 

## Local Storage

History 기능 및 Region, Beacon 데이터의 효율적 관리를 위해 비휘발성 storage를 구성한다. (e.g. `sqlite`)

- App이 종료된 후에도 데이터 유지 
- 대량의 데이터 관리 (그래봐야 몇 천 건이겠지만) 
- History data 관리 

## Region, Beacon 데이터 관리 

cache 전략 수립 

e.g.

- region은 1hr expire
- beacon은 region 이탈시 expire. 

## Spot 데이터 관리 

- ranging 시 매번 조회하는 건 비효율적이므로 
- region exit시에 clear. 
- 짧은 시간(5~10분) 동안만 cache. 


## Beacon Ranging와 Spot data 접근의 분리

- beacon ranging
    - beacon 자체의 access 빈도. 
    - access가 없었던 beacon은 죽었거나 유실됐을 가능성 있음
    - access 빈도가 높은 beacon은 노출도가 높은 녀석
- spot data access
    - 특정 app에서의 beacon 활용도.
    - ranging 되는 N개의 비콘 중 M(`M<=N`)개의 비콘에 대해서만 데이터 접근이 이루어 질거임.
    - spot data access가 높은 비콘은 app사에서 활용도가 높은 비콘인거

즉 beacon ranging 통계는 beacon 관리관점에서 유용하고, spot data access는 beacon 활용도 관점을 드러낼 수 있음.

**issue**
- beacon ranging은 현재 별도로 통계 수집이 안되고 있음
- spot data access가 cache되면 beacon ranging은 더 알 수 없게 됨. 
- SDK 내부적으로 beacon ranging 정보를 관리하다가 서버에 전송할 필요가 있음 (아래서 설명할 history data와의 연계 가능성 있음)
- beacon raging 정보(각 beacon 별)
    - ranging 시각 (혹은 시간대)
    - ranging 횟수
    - app 정보 (appKey) (서버에 올릴때 header에 실려가겠지?)

- spot data access는 cache된다고 하더라도 sampling 수가 줄어든걸로 볼 수 있으므로 beacon 활용도 관점을 유지하는데 문제는 없을 듯. 

## History API

조낸 중요한 건지는 의문.

이쁘게 잘 만들면 edge는 있을 듯. 

### Concept Develop

short term에 대한 history 사용과 long term에 대한 history 사용의 need와 용법이 다를 것 같음. 

**short-term**
- e.g. *"정문으로 들어와서 hall을 지나서 여기에 왔니?"*
- e.g. *"헬로키티 박물관에 들렸다가 우리 카페에 왔지?"*
- short-term의 경우 디테일한 **동선 트래킹**의 관점이 필요할 것 같음

**long-term**
- e.g. _"너 3개월 전에 여기 왔었지?"_
- e.g. _"너 지난 1년 동안 우리 카페에 5번 왔었네"_
- long-term에 대한 정보 질의 패턴은 방문이력 형태가 많을 듯 

위 가정을 base로 생각해보면 location history의 데이터 설계는 최신일 수록 granual하게 오래된 것일 수록 coarse한게 관리될 수 있으면 좋겠음. 

예를 들면, SNS 등에서 포스팅 시각을 time unit을 기준으로 세부정보를 날려버리는 것 처럼....(e.g. "12분전", "2시간전", "3일전", "7개월 전", "2년 전")

### Interface

`v1.0.0`이 되기 전까지 릴리즈 때마다 확확 갈아버린 다른 생각으로 unstable interface로 개발할 필요가 있음.

초기 접근
- short-term interface와 long-term interface를 아예 분리해서 시작. (Tracking interface, VisitHistory interface)
- 각 interface에 맞게 내부 데이터 설계. 
- short-term -> long-term으로 넘어가는 데이터의 처리. (이럴때 beacon ranging 정보를 서버에 올린다던가) 
- short-term은 SDK 내부에서, long-term은 서버 질의로 한다던가하는 전략도 가능할 듯 


뭐 이러다가 개념과 interface가 하나로 묶을 수 있으면 일반화하는거고.... 혹  mid-term에 대한 니즈가 생기면 interface를 또 하나 만드는 거고...

## beacon 신호를 iBeacon 기준으로 튜닝 

iOS 버전의 tamra를 개발하면서 측정데이터를 만들고, 
그 데이터를 기준으로 해보자. (How?)

## AltBeacon 교체 

이거 교체하면 의존성 zero!