
## Tamra 내부 예외 처리 방법 

Tamra는 background에서 동작하기 때문에 callback(or observer)를 사용한 비동기 방식으로 사용하게 된다.
그래서 *functional interface*(`com.kakao.oreum.common.function`)를 많이 사용한다. 
(`com.kakao.oreum.ox` 패키지의 utility들도 이를 지원하기 위한 클래스들이다)
   
   
### Functional Interface에서 예외의 문제

그런데 기본 *functional interface* 사용시에 아래와 같이 예외처리의 난점이 있다. 

**functional interface는 예외를 던지지 않음**

```java

Runnable runnable = () -> {
    URL url = new URL(serverURL); // compile error! MalformedURLException를 처리해야 한다.
    ... do something.
};

```

위와 같은 경우 결국 예외 처리를 위한 코드 블럭이 필요하고, 때로는 `RunntimeException`으로 wrapping해야 할 수도 있다.

```java

Runnable runnable = () -> {
    try {
        URL url = new URL(serverURL);
        ... do something
    } catch (MalformedURLException e) {
        throw new RunntimeException(e);
    }
}
```

결국 java에서 functional interface를 사용할 때 얻을 수 있는 장점 중 하나인 **코드의 간결함**은 사라진다.
 
**function에서 발생할 수 있는 unchecked exception은 제어구조를 분산시킨다**
 
위 코드와 같이 `RunntimeException`으로 wrapping하는 경우에도 예외 처리 제어로직의 분산을 야기할 수 있는데 아래의 예를 보자. 
  
```java
OxTasks.runOnUi(() -> {
    try {
        ...
    } catch (Exception e) {
        throw new RunntimeException(e);
    }
});
```

*funtional interface*는 *lazy*하게 실행될 수 있고, function 객체가 다른 객체로 넘겨져서 실행되는 경우 
`RuntimeException`이 발생할 때 function 객체가 정의되는 곳에서 예외처리를 수행하기 힘들다.
 
### Solution

오류 처리를 위한 2-layer로 접근하는데, 
`com.kakao.oreum.ox.error` 패키지에서 funtional interface에 대한 기본 예외 처리 전략을 제공하고 
`com.kakao.oreum.tamra`에서 Tamra SDK의 예외 처리를 구현한다. 

#### `com.kakao.oreum.ox.error`

여기에서 제공하는 기본 예외처리 전략은 *Observer Pattern*을 이용해 예외를 observer들에게 전달하는 것이다.

- `OxErrorObserver`
    - 예외를 notify 받을 observer 인터페이스.
- `OxErrors`
    - static member, method로 구현되는 클래스이며 `OxErrorObserver`들에게 발생하는 예외를 notify하는 역할을 하는 클래스이다.
    - `OxErrors.NOTIFIER`는 `OxErrorObserver` 타입이며 여기에 전달된 예외를 등록된 모든 observer들에게 전달하는 역할을 한다. 
    - 즉, 예외 메시지에 대한 허브 역할을 하는 정적 구현 클래스이다. 
- `ExceptionFree`
    - *functional interface*에 대한 예외 처리를 위한 helper 클래스이다.
    - 기존 functional interface에 예외 처리(observing, or notify)를 추가할 수 있다. 
    - 자세한 사용은 아래 예를 보자. 
    
```java
// checked exception을 던지는 코드를 담을 수 있는 runnable
Runnable runnable = ExceptionFree.runnable(() -> {
    URL url = new URL(serverURL); // checked exception도 던질 수 있다. 
});

// unchecked exception 핸들링을 하는 executable를 생성. 
// aRunnable이 예외를 발생시킨다고 해도 wrappedd는 안그런다. 대신 TamraError에 예외를 담는다. 
Executable wrapped = ExceptionFree.of(() -> {
    throw new NullPointerException();
});

// 아래 코드에서는 절대 예외가 발생하지 않는다.
// 대신 발생하는 예외는 모두 OxErrors를 통해 observer들에게 전달된다. 
ExceptionFree.exec(() -> {
    throw new IOException();
}).run();

```

`ExceptionFree`는 별도의 `OxErrorObserver`을 지정하지 않는 경우 예외는 `OxErrors` 통해 모든 observers에게 전달된다.
아래 처럼 observer를 지정하면 별도로 예외를 핸들링 할 수 있다.

```java
// 직접 예외처리 : TamraErrors로 오류를 안보낸다. 
ExceptionFree.exec(() -> {
    doSomething();
}, (e) -> { // 예외는 죄다 씹어먹는다.
    sendToServer(e);
});

// OxErrors를 통해 observer에게 통보한 후 추가 처리를 하는 경우
ExceptionFree.exec(() -> {
    doSomething();
}, OxErrors.notifyAndExec(() -> {
    // 예외를 OxErrors로 보내고 아래 작업을 수행한다.
    sendToServer(e);
});
```

#### `com.kakao.oreum.tamra`

Tamra 내부에서는 위의 `OxErrors`, `ExceptionFree`를 통해 예외 발생을 observing 하는 전략을 취한다. 
즉, Tamra 내에서 구현한 `OxErrorObserver`를 `OxErrors`에 등록하고 `ExceptionFree`를 통한 로직 구현으로 예외 핸들링을 한 곳에서 한다. 
 
- `TamraErrorHolder`
    - `TamraError`타입으로 오류를 보관하고 `RecentTamraErrors`를 제공하는 클래스이다.
    - 이 클래스의 `errorHandler()` 메소드로 `OxErrorObserver`를 객체를 제공하는데, 이 객체를 `OxErrors`에 등록하여 모든 에러를 이 객체 내에 수집한다. 
- `Tamra`
    - static 멤버로 `TamraErrorHolder` 객체를 가지고 있으며, 
    - 초기화 과정에서 `OxErrors`에 `TamraErrorHolder#errorHandler()`를 등록한다.
    - 내부 오류는 `TamraErrorHolder#recentErrors()` 메소드로 `RecentTamraErrors` 객체로 외부에 제공하게 된다. 

