
## Android Beacon SDK : Getting Start

이 문서는 비콘 기반의 위치서비스 앱 개발을 위한 Android버전의 Tamra SDK의 기본 사용법을 다룬다.

### 주의사항

이 문서는 *Android Beacon SDK* `v0.10.1`을 기준으로 작성되었다.
현재 활발히 개발이 진행되는 SDK이므로 버전에 따라 API의 변화가 클 수 있다. 따라서 항상 최신 문서를 참고하기 바란다.

(버전별 변경사항은 [Change History](./change-history.md)를 참고할 것)

### 요구사항

- Android SDK : version 18 이상
- Java : JDK7

그리고 [Android Studio](http://developer.android.com/intl/ko/tools/studio)와 `gradle` 빌드툴을 사용하는 것을 가정한다.

### 권장사항 

`NearbySpots` 등의 클래스에서 `functional interface` 기반의 메소드를 제공하므로 
[retrolambda](https://github.com/evant/gradle-retrolambda)를 사용하여 간결한 코드를 작성할 수 있다. 
(필수 요구사항은 아니다)

### 빌드 의존성

`build.gradle`에 아래와 같은 내용을 추가한다.

```
repositories {
    ...
    maven {
        url 'http://220.124.222.193:8080/nexus/repository/public/'
    }
}

dependencies {
    compile('com.kakao.oreum:tamra:0.11.0') { changing = true }
}
```

### Android Beacon SDK 초기화

Tamra SDK는 백그라운드에서 동작하므로 `Android Beacon SDK`를 사용하는 App은 App이 구동되는 시점에 `Android Beacon SDK`를 초기화하고 시작하는 로직이 필요하다. `Android Beacon SDK` 초기화를 위한 코드는 아래와 같다.

```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreaet(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView(); // initialized your own view.
        initTamra();
    }

    private void initTamra() {
        String appkey = "my-app-key-for-this-app";
        Config config = Config.forRelease(
            getApplicationContext(),
            appkey);
        Tamra.init(config);

        // monitor region
        Tamra.startMonitoring(Regions.제주국제공항);
        Tamra.startMonitoring(Region.of(3));

        // if you want to monitor all regions.
        for (Region region : Regions.all()) {
            Tamra.startMonitoring(region);
        }

        // check errors
        Tamra.recentErrors()
            .filter(TamraErrorFilters.causedBy(TamraInitException.class))
            .foreach(tamraError -> LOG.d(TAG, tamraError.toString());
    }
}
```

`Android Beacon SDK`를 초기화할 때 `Config` 객체를 argument로 받는다.

- `appkey`는 [비콘관리시스템](http://oreum.kakao.com/)를 통해 app 별로 발급받을 수 있다.
- `Config.forRelease()`는 실서비스용 설정을 생성하고, `Config.forTesting()`는 테스트용 설정을 생성한다.
    - **Android Beacon SDK**는 Production 환경과 Testing 환경이 분리되어 있다.
- 초기화시 `Tamra`는 필요한 서비스를 백그라운드에서 동작시키며 초기화 과정에서 발생한 오류는 `Tamra.errors()`를 통해 제공한다.
- 초기화시 필요한 서비스들을 점검한다. 만약 아래와 같은 서비스들이 enable되어 있지 않다면 `Tamra`는 정상동작하지 않는다.
    - Bluetooth
    - Network

- `observerSensitivity()`는 비콘 신호에 대한 Observer 이벤트의 감도를 설정할 수 있다.
    - 내부적으로 비콘 신호에 대한 안정화 작업을 거치고 있으며 안정화 정책에 따라 비콘 신호 응답이 빨리 오느냐 또는 정확한 비콘 신호를 받느냐의 차이를 만들 수 있다.
    - 감도(`ObserverSensitivity`)는 아래의 3가지로 구분되며 절대적이 아닌 상대적 관점으로 사용되어야 한다.
        - `ObserverSensitivity.Balanced` : 기본 모드로써 기존에 사용되던 패턴으로 이벤트를 발생시킨다.
        - `ObserverSensitivity.Responsive` : 응답성 우선 모드로써 비콘 신호에 좀더 즉각적으로 이벤트를 발생시킨다.
        - `ObserverSensitivity.Accurate` : 좀 더 확실한 상황에 놓인 비콘 신호들만 이벤트를 발생시킨다.
    - 감도 설정을 하지 않을 경우, 기본 모드는 `ObserverSensitivity.Balanced` 이며, 별도로 세팅하고 싶은 경우 아래와 같이 할 수 있다.
    
```java
Config config = Config.forRelease(
    getApplicationContext(),
    appkey)
    .observerSensitivity(ObserverSensitivity.Responsive);
```
     
(참고) Android Beacon SDK 내부의 예외 조회 방식이 `v0.8.0`에서 변경되었음. 이 문서의 하단을 참고할 것. 

### Region

`Region`은 *iBeacon*의 `BeaconRegion`과 개념적으로는 유사하지만,
Oreum Platform에서 소유자가 다른 `BeaconRegion`들을 식별하고 관리하기 위한 용도로 사용된다.

`Region`은 `id`와 `name` 속성을 가지며, `name` 속성은 *Human Readable Information*으로만 사용된다.

`Android Beacon SDK`에서 모니터링할 지역(Region)을 다음과 같은 방식으로 선택할 수 있다.

```java
// id로 직접 지정.
Region region1 = Region.of(4);
// 사전에 정의된 region이름으로 지정.
Region region2 = Regions.제주국제공항;

Tamra.startMonitoring(region1);
Tamra.startMonitoring(region2);
```

(참고)
`Regions.all()`을 사용하여 사전 등록된 region 전체를 얻을 수 있다. 

(참고)
SDK가 배포된 후에 지역(Region)이 추가된 경우 *OreumZigi관리툴*을 참고하여 직접 해당 Region의 id를 사용하여
해당 Region을 모니터링할 수 있다. (use `Region.of(id)`)

### Spot

`Tamra`는 비콘이 설치된 *어떤 지점*을 `Spot`이라는 모델 객체로 제공한다.

기본적으로 `Spot`은 `proximityUUID`, `major`, `minor`와 같은 beacon 식별자를 캡슐화하기 위한 모델이므로 beacon의 기본 속성들을 거의 대부분 가지고 있다.

`Spot`은 `long`타입의 단일 식별자를 사용하며, *어떤 지점*에 대한 부가적인 속성들을 가지고 있다.

- `id`
    + `long`타입의 식별자.
- `description`
    + 설명
- `region`
    + `Oreum Platform`에서 관리하는 지역.
    + 위에서 설명한 `Region`객체.
- `proximity`
    + `iBeacon`에서 정의하는 것과 동일한 근접성의 개념.
    + `Immediate`(1m 이내), `Near`(1~3m), `Far` 중 하나.
- `accuracy`
    + 정확도 (meter 단위)
    + **주의** Apple의 `iBeacon` 문서에서 언급한 대로 거리개념보다는 정확도로 보는 것이 좋다.
        * 특히, `Near` 이상에서는 신호 간섭 때문에 정확도가 많이 떨어지기 때문에 실내측위용으로 사용하는 것을 권장하지 않는다.
- `indoor`
    + 실내 지점인지 여부.
- `movable`
    + 이동형인지 여부.
    + 시설물 등의 고정된 위치에 부착되지 않고 차량 같은 이동가능한 물체에 붙은 beacon에 대한 지점이라면 `true`값을 갖는다.
    + 현재 이동형 비콘은 없다.
- `latitude`, `longitude`, `geohash`
    + 지리적 위치
    + `indoor=true`인 경우 해당 건물 자체의 지리적 위치좌표 값을 갖는다. (실내의 경우 개별 spot의 좌표가 아님에 유의)
    + `movable=true`인 경우에 유효성을 보장하지 않는다.
- `data`
    + spot에 지정한 사용자 정의 data.
    + **OreumZigi**를 통해 온라인으로 데이터를 등록 및 관리할 수 있으며, 등록한 값은 `string`타입으로 제공된다.
    + since `0.7.0`
        - `spot.data()`는 deprecated 됨. (항상 `Optional.empty()`를 리턴하도록 변경됨)
        - 비동기 메소드인 `spot.data(callback)`을 사용할 것.

#### Spot 데이터 접근

**Oreum Platform**에서 **Production** 환경과 **Testing** 환경의 데이터는 완전히 분리되어 있으며, 
동일 사용자 계정에 등록된 App들은 그 계정에 등록된 데이터를 공유하도록 되어 있다. 

예를 들어, `john.doe`라는 사용자 계정(*OreumZigi*에 등록된)으로 *Beacon{id=N}*에 `{'myName':'john.doe'}`를 등록했다면, 
`john.doe` 계정으로 생성한 `myapp.android`, `myapp.ios` 2개 app에서 *Spot{id=N}*에 대해 동일한 데이터를 얻을 수 있다. 
하지만 **Testing** 환경에서 등록한 데이터는 **Production**환경에서 조회할 수 없다. 
 
**중요**

`v0.7.0` 부터 Spot 데이터 조회가 동기방식에서 **비동기방식**으로 변경되었다.
따라서 기존의 `spot.data()`는 *deprecated* 되었으며 더이상 이 메소드로 데이터를 얻을 수 없다.
대신 비동기 방식의 메소드인 `spot.data(callback)`를 사용하여 데이터를 조회할 수 있다.

```java
// 0.6.0 
Optional<String> data = spot.data();
textView.setText(data.orElse("N/A")); // 0.7.0 부터는 항상 Optional.empty()를 리턴한다. 

// 0.7.0 이후 
spot.data(new Consumer<Optional<String>>() {
    @Override
    public void accept(Optional<String> data) {
        textView.setText(data.orElse("N/A"));
    } 
});
//// lambda표기로 쓰면 아래처럼 쓸 수 있다 (using retrolambda)
spot.data(data -> textView.setText(data.orElse("N/A")));
```

`spot.data(callback)`을 main thread에서 호출한다면 *callback*은 main thread에서 수행되므로, 
위의 예제 코드와 같이 직접 UI를 수정할 수 있다. 

#### Tamra로 현재 주변의 Spot을 얻기

현재 모바일 디바이스의 주변의 Spot 목록을 보여주는 화면을 만든다고 가정하자.

아래와 같은 코드로 현재 주변의 Spot 목록을 해당 Activity의 listView에 디스플레이할 수 있다.

```java
public class SpotListActivity extends AppCompatActivity {
    private SpotListAdapter spotListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView(); // initialize view.

        for (Spot spot : Tamra.spots()) {
            spotListAdapter.addSpot(spot);
        }
    }
}
```

#### Spot에 대한 이벤트

디바이스 주변에 감지되는 spot들을 이벤트 형태로 제공받기 위해서는 `TamraObserver` 인터페이스를 구현해야 한다.

```java
public interface TamraObserver {

    void didEnter(Region region);

    void didExit(Region region);

    void ranged(NearbySpots spots);

}
```
(참고) `TamraObserver`에 대한 adapter 추상 클래스인 `TamraObserverAdapter` 클래스를 제공한다.

`TamraObserver`를 아래와 갈이 등록하면 지역(Region)의 출입/이탈, 감지되는 지점(Spot) 정보를 이벤트로 받을 수 있다.

```java
public class SpotListActivity extends AppCompatActivity {
    private SpotListAdapter spotListAdapter;

    private final TamraObserver tamraObserver = new TamraObserverAdapter() {
        @Override
        public void ranged(final NearbySpots spots) {
            spotListAdapter.replaceSpots(spots);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        Tamra.addObserver(tamraObserver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Tamra.removeObserver(tamraObserver);
    }
    ...
}


```

어떤 지역(Region)의 범위 내에 들어가거나 이탈했을 때 `#didEnter(Region)`, `#didExit(Region)` *observer*의 메소드가 호출되며,
주변에 감지되는 지점(spot)이 있으면 `#ranged(NearbySpots)`가 호출된다.
(`NearbySpots`에 대해서는 아래 설명하겠다)

**참고**
`v0.6.0`부터는 main 스레드에서 등록한 `TamraObserver`는 main Thread에서 수행되는 것을 보장한다. 
따라서 `TamraObserver`에서 UI를 변경하는 경우에 명시적으로 `runOnUiThread()`를 사용할 필요가 없다. 

```
// v0.6.0 미만 
Tamra.addObserver(new TamraObserver() {
    ...(중략)...
    @Override
    void ranged(NearbySpots spots) {
        spotListAdaptor.changeSpots(spots);
        // tamraObserver가 background에서 수행되므로 ui스레드에서 ui를 변경이 수행되도록 해야 함.
        runOnUiThread(() -> redraw()); // 간결한 예시를 위해 lambda 표현을 사용.
    }
}

// v0.6.0 이상
// main thread에서 observer를 등록했다고 가정.
Tamra.addObserver(new TamraObserver() {
    ...(중략)...
    @Override
    void ranged(NearbySpots spots) {
        spotListAdaptor.changeSpots(spots);
        // main thread에서 observer를 등록했으므로 observer는 main Thread에서 수행됨. 
        // 따라서 직접 ui 변경이 가능함
        redraw(); 
    }
}
```


#### `NearbySpots`

`Tamra`는 `Tamra.spots()`와 `TamraObserver#ranged(spots)`를 통해 `NearbySpots` 클래스 객체를 제공한다.

`NearbySpots`는 `Iterable<Spots>` 구현클래스이며 지점 정보들을 변환 혹은 필터링하거나 처리하기 위한 funtional programming 스타일의 편의 메소드들을 제공한다.


**filtering**

filtering을 위해서 `Predicate<Spot>` 객체를 argument로 받는데, `SpotFilters` 클래스에는 다수의 filter들이 미리 정의되어 있다.

```java
// 제주공항의 spot중 proximity가 Near이하인 것들만 조회
List<Spot> nearSpots = Tamra.spots()
        .filter(SpotFilters.belongsTo(Regions.제주국제공항))
        .filter(SpotFilters.maxProximity(Proximity.Near))
        .toList();

// 이전에 감지된 spots과 비교하여 새로 감지된 spot들만 필터링.
NearbySpots prevSensed = ...;
NearbySpots newSensed = Tamra.spots()
    .filter(SpotFilters.oneOf(prevSensed).not());
```

**sorting**

`#orderBy(Comparator)`를 통해 정렬 기능을 제공하며 `Spot` 클래스에 주요 속성에 대한 comparator들이 static 필드로 정의되어 있다.

```java
// id순 정렬후 list변환
List<Spot> spots = Tamra.spots()
        .orderBy(Spot.ID_ORDER)
        .toList();

// accuracy 기준으로 5개만 조회
List<Spot> nearSpots = Tamra.spots()
        .orderBy(Spot.ACCURACY_ORDER)
        .limit(5)
        .toList();

// 가장 멀리있는 spot 조회
Optional<Spot> farSpot =  Tamra.spots()
        .orderBy(Spot.PROXIMITY_ORDER.reversed())
        .first();

```

**predicate**

`#all(Predicate<Spot>)`, `#any(Predicate<Spot>)`을 제공한다.

```java
// 모두 제주국제공항의 spot인가?
boolean bool1 = Tamra.spots()
        .all(SpotFilters.belongsTo(Regions.제주국제공항));

// Immediate 수준의 spot이 하나라도 있는가?
boolean bool2 = Tamra.spots()
        .any(SpotFilters.maxProximity(Proximity.Immediate));
```

**transform**


```java
// spot에서 geohash 추출
Collection<String> geohashs = Tamra.spots()
        .transform(new Function<Spot, String>() {
            @Override
            public String apply(Spot spot) {
                return spot.geohash();
            }
        });
// lambda 표현을 쓰면 아래처럼 쓸수도 있다.
Collection<String> geohashs2 = Tamra.spots()
        .transform(Spot::geohash);

```
(참고)
[retrolambda](https://github.com/evant/gradle-retrolambda)를 사용하면 `lambda`표현식으로 코드를 간결하게 만들 수 있다.


**toMap**

`#toMap()`, `#index()`를 통해 `NearbySpots`를 map으로 변환할 수 있다.

```java
// region 별로 그루핑.
Map<Region, Collection<Spot>> grouped = Tamra.spots()
        .index(Spot::region);

// spot -> proximity map
Map<Spot, Proximity> proxMap = Tamra.spots()
        .toMap(Spot::proximity);
```


#### 내부 에러 확인


(참고) `v0.8.0` 부터 *Tamra* 예외처리 인터페이스가 변경되었다.

기존의 `Tamra.errors()`는 **deprecate** 되었으니 `Tamra.recentErrors()`를 사용 할 것(아래 내용 참고) 

**Tamra SDK** background에서 동작하고 App의 life-cycle에 직접 개입하지 못하므로 *Tamra* 내부에서 발생하는 예외들을 별도의 interface로 제공한다(`Tamra.recentErrors()`).

`Tamra.recentErrors()`는 `Iterable<TamraError>`를 구현하는 `RecentTamraErrors` 인터페이스 객체를 반환하는데, `NearbySpots`와 유사하게 `filter`, `orderBy` 등의 functional style로 사용할 수 있는 메소드들을 제공한다.

```java
// 직접 오류를 navigate 
for (TamraError error : Tamra.recentErrors()) {
    doSomethingWithError(error);
}
// list 변환 후 사용
List<TamraError> errors = Tamra.recentErrors().toList();

// Tamra 초기화 오류가 있는지 체크
boolean initSuccess = Tamra.recentErrors()
        .filter(TamraErrorFilters.causedBy(TamraInitException.class))
        .isEmpty();
        
// 모든 오류를 출력 
Tamra.recentErrors()
    .foreach(te -> LOG.d(TAG, te.toString()));
```

`TamraError` 클래스는 발생한 예외(`#causedBy()`) 객체와 예외가 발생한 시각(`#occurredAt()`) 속성을 가진 오류 정보 객체이며, 
최대 `RecentTamraErrors.MAX_RETAIN_COUNT`(`v0.8.0` 기준 100개) 갯수 만큼만 오류 정보를 보관한다.
따라서 짧은 시간에 많은 수의 예외가 발생한 경우 모든 오류정보를 얻지 못할 수 있음에 유의하라. 

또한 SDK 사용자가 등록한 `TamraObserver`를 수행하는 중에 오류가 발생한 경우 역시 `Tamra.recentErrors()`에서 조회된다.