package com.kakao.oreum.oreumzigi.scan.supports;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kakao.oreum.oreumzigi.scan.models.ScannedBeacon;
import com.kakao.oreum.oreumzigi.scan.models.ScannedHistory;
import com.kakao.oreum.oreumzigi.supports.OreumHttpRequest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class ReportUploader {

    public static boolean upload(String tag) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ScannedBeacon> results = realm.where(ScannedBeacon.class)
                .equalTo("tag", tag)
                .findAll();

        List<ScannedBeaconForJson> list = new ArrayList<>();

        for (ScannedBeacon beacon : results) {
            list.add(ScannedBeaconForJson.from(beacon));
        }

        realm.close();

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        String json = gson.toJson(list);

        File file;

        try {
            file = File.createTempFile("_report.json", null);

            Writer writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        HttpRequest request = OreumHttpRequest.post("/scanreport");
        request.part("tag", tag);
        request.part("file", file);

        return request.ok();
    }

    private static class ScannedBeaconForJson {

        private String uuid;
        private Integer major;
        private Integer minor;
        private Date createdAt;
        private List<ScannedSignalForJson> signals;

        private static ScannedBeaconForJson from(ScannedBeacon scannedBeacon) {
            ScannedBeaconForJson forJson = new ScannedBeaconForJson();
            forJson.uuid = scannedBeacon.uuid;
            forJson.major = scannedBeacon.major;
            forJson.minor = scannedBeacon.minor;
            forJson.createdAt = scannedBeacon.createdAt;
            forJson.signals = new ArrayList<>();

            for (ScannedHistory history : scannedBeacon.histories) {
                forJson.signals.add(ScannedSignalForJson.from(history));
            }

            return forJson;
        }
    }

    private static class ScannedSignalForJson {

        private String proximity;
        private Double accuracy;
        private Integer rssi;
        private Date createdAt;

        private static ScannedSignalForJson from(ScannedHistory scannedHistory) {
            ScannedSignalForJson forJson = new ScannedSignalForJson();
            forJson.proximity = scannedHistory.proximity;
            forJson.accuracy = scannedHistory.accuracy;
            forJson.rssi = scannedHistory.rssi;
            forJson.createdAt = scannedHistory.createdAt;

            return forJson;
        }
    }

}
