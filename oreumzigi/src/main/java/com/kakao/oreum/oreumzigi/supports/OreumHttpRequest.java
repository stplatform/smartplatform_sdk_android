package com.kakao.oreum.oreumzigi.supports;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.internal.fixture.ClientId;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class OreumHttpRequest {

    private static final String format = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static DateFormat dateFormat = new SimpleDateFormat(format, Locale.KOREA);
    private static Gson gson = new GsonBuilder().setDateFormat(format).create();
    private static Config config;
    private static Token token;

    public static void init(Config config) {
        OreumHttpRequest.config = config;
    }

    public static HttpRequest get(String path) {
        return wrapRequest(HttpRequest.get(url(path)));
    }

    public static HttpRequest post(String path) {
        return wrapRequest(HttpRequest.post(url(path)));
    }

    public static HttpRequest put(String path) {
        return wrapRequest(HttpRequest.put(url(path)));
    }

    public static HttpRequest delete(String path) {
        return wrapRequest(HttpRequest.delete(url(path)));
    }

    private static HttpRequest wrapRequest(HttpRequest request) {
        return request.headers(oreumHeaders());
    }

    private static Map<String, String> oreumHeaders() {
        assureToken();

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Oreum-Token", token.token());
        headers.put("Oreum-CID", ClientId.get(config.context()));

        return headers;
    }

    private static String url(String path) {
        String host;

        switch (config.profile()) {
            case DEVELOP:
                host = "http://oreum-dev-api.krane.iwilab.com";
                break;
            case TEST:
                host = "https://test-api-cocoon.kakao.com";
                break;
            case PRODUCTION:
                host = "https://api-cocoon.kakao.com";
                break;
            case LOCAL:
            default:
                throw new RuntimeException("Unsupported Profile.");
        }

        return host + path;
    }

    private static void assureToken() {
        if (token != null && !token.isExpired()) {
            return;
        }

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Oreum-CID", ClientId.get(config.context()));

        Map<String, String> data = new HashMap<>();
        data.put("appId", config.appId());
        data.put("appKey", config.appkey());

        HttpRequest request = HttpRequest.post(url("/auth/authenticate"))
                .headers(headers)
                .send(gson.toJson(data));

        if (!request.ok()) {
            throw new RuntimeException("Authentication is failed.");
        }

        String json = request.body();
        token = Token.from(json);
    }

    private static class Token {

        private String token;
        private Date expireAt;

        private Token() {

        }

        static Token from(String json) {
            Token token = new Token();
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            Map<String, String> map = gson.fromJson(json, type);
            token.token = map.get("token");

            try {
                token.expireAt = dateFormat.parse(map.get("expireAt"));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return token;
        }

        String token() {
            return token;
        }

        boolean isExpired() {
            return expireAt.compareTo(new Date()) < 0;
        }

    }
}
