package com.kakao.oreum.oreumzigi.errors;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.supports.Utils;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class ErrorsActivity extends AppCompatActivity {

    private TextView headerTextView;
    private ListView listView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Errors");

        ErrorListAdapter listAdapter = new ErrorListAdapter(this);

        String message = Utils.countingHeader(listAdapter.getCount());
        headerTextView.setText(message);

        listView.setAdapter(listAdapter);
    }

    private void findingViews() {
        setContentView(R.layout.activity_errors);

        headerTextView = (TextView) findViewById(R.id.center);
        listView = (ListView) findViewById(R.id.listView);
    }

}
