package com.kakao.oreum.oreumzigi.customviews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.tamra.base.Proximity;

public class ProximityView extends LinearLayout {

    private TextView proximityView;

    public ProximityView(Context context) {
        super(context);
        initialize();
    }

    public ProximityView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public ProximityView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    private void initialize() {
        LayoutInflater inflater  = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_proximity_view, this, true);

        proximityView = (TextView) findViewById(R.id.text);
    }

    public void setProximity(Proximity proximity) {
        proximityView.setText(proximity.toString());

        String backgroundColor;

        switch (proximity) {
            case Immediate:
                backgroundColor = "#77DD77";
                break;
            case Near:
                backgroundColor = "#DDDD77";
                break;
            case Far:
            default:
                backgroundColor = "#999999";
        }

        GradientDrawable drawable = (GradientDrawable) proximityView.getBackground();
        drawable.setColor(Color.parseColor(backgroundColor));
    }

    public void setProximity(com.kakao.oreum.infra.beacon.Proximity proximity) {
        setProximity(Proximity.valueOf(proximity.toString()));
    }

}
