package com.kakao.oreum.oreumzigi.errors;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.supports.Utils;
import com.kakao.oreum.tamra.error.TamraError;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class ErrorView extends LinearLayout {

    private TextView exceptionName;
    private TextView exceptionMessage;
    private TextView occurredAt;
    private TamraError tamraError;

    public ErrorView(Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_error, this, true);

        exceptionName = (TextView) findViewById(R.id.exceptionName);
        exceptionMessage = (TextView) findViewById(R.id.exceptionMessage);
        occurredAt = (TextView) findViewById(R.id.occurredAt);
    }


    public void setData(TamraError tamraError) {
        this.tamraError = tamraError;
        exceptionName.setText(tamraError.causedBy().getClass().getName());
        exceptionMessage.setText(String.valueOf(tamraError.causedBy().getMessage()));
        occurredAt.setText(Utils.format(tamraError.occurredAt()));

        setOnClickListener((v) -> moveToDetail());
    }

    private void moveToDetail() {
        if (tamraError == null) {
            return;
        }

        Intent intent = new Intent(getContext(), ErrorDetailActivity.class);
        intent.putExtra("exception", tamraError.causedBy());
        intent.putExtra("occurredAt", tamraError.occurredAt());
        getContext().startActivity(intent);
    }

}
