package com.kakao.oreum.oreumzigi.scan.models;

import com.kakao.oreum.infra.beacon.Beacon;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class ScannedHistory extends RealmObject {

    @Required
    public String proximity;

    @Required
    public Double accuracy;

    @Required
    public Integer rssi;

    @Required
    public Date createdAt;

    public ScannedHistory() {
        super();
        createdAt = new Date();
    }

    public ScannedHistory(Beacon beacon) {
        this();
        proximity = beacon.proximity().name();
        accuracy = beacon.accuracy();
        rssi = beacon.rssi();
    }

}
