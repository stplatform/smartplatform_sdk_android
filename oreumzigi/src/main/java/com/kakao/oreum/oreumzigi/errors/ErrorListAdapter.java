package com.kakao.oreum.oreumzigi.errors;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.tamra.Tamra;
import com.kakao.oreum.tamra.error.RecentTamraErrors;
import com.kakao.oreum.tamra.error.TamraError;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class ErrorListAdapter extends BaseAdapter {

    private final Activity activity;
    private RecentTamraErrors tamraErrors = null;

    public ErrorListAdapter(Activity activity) {
        this.activity = activity;
        reloadData();
    }

    @Override
    public int getCount() {
        return tamraErrors == null ? 0 : tamraErrors.retainCount();
    }

    @Override
    public Object getItem(int position) {
        return tamraErrors == null ?
                null :
                OxIterable.from(tamraErrors)
                        .get(position)
                        .orElse(null);
    }

    @Override
    public long getItemId(int position) {
        Object item = getItem(position);
        return item == null ? -1 : ((TamraError) item).occurredAt().getTime();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ErrorView errorView;

        if (convertView == null) {
            errorView = new ErrorView(activity);
        } else {
            errorView = (ErrorView) convertView;
        }

        errorView.setData((TamraError) getItem(position));
        return errorView;
    }

    public void reloadData() {
        this.tamraErrors = Tamra.recentErrors();
    }

}
