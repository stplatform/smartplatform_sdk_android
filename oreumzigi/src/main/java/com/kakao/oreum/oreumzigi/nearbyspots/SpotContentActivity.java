package com.kakao.oreum.oreumzigi.nearbyspots;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kakao.oreum.oreumzigi.R;

import java.util.Locale;
import java.util.Map;

public class SpotContentActivity extends AppCompatActivity {

    private TextView headerTextView;
    private TextView contentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Nearby Spots > Spot Content");

        Long spotId = getIntent().getLongExtra("spotId", -1L);
        String description = getIntent().getStringExtra("description");

        String headerText = String.format(Locale.KOREA, "%d - %s", spotId, description);
        headerTextView.setText(headerText);

        String content = getIntent().getStringExtra("content");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        contentView.setText(gson.toJson(gson.fromJson(content, Map.class)));
    }

    private void findingViews() {
        setContentView(R.layout.activity_spot_content);

        headerTextView = (TextView) findViewById(R.id.center);
        contentView = (TextView) findViewById(R.id.content);
    }
}
