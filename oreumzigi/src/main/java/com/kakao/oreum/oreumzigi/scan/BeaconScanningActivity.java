package com.kakao.oreum.oreumzigi.scan;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.infra.beacon.altbeacon.BeaconServiceFactory;
import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.scan.models.ScannedBeacon;
import com.kakao.oreum.oreumzigi.scan.models.ScannedHistory;
import com.kakao.oreum.oreumzigi.supports.Utils;
import com.kakao.oreum.oreumzigi.scan.views.ScannedItemView;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.RegionUtil;

import java.util.Date;
import java.util.Locale;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class BeaconScanningActivity extends AppCompatActivity {

    private TextView headerTextView;
    private ListView scanListView;
    private Button resetButton;
    private ToggleButton startButton;
    private Button saveButton;

    private BeaconService beaconService;
    private ScanListAdapter scanListAdapter;
    private Realm realm;

    final BeaconServiceObserver beaconObserver = new BeaconServiceObserver() {

        @Override
        public void didEnterRegion(BeaconRegion beaconRegion) {
            Region region = RegionUtil.toRegion(beaconRegion);
            String message = String.format(Locale.KOREA, "%s 지역에 진입했습니다.", region.name());
            runOnUiThread(() -> {
                Toast.makeText(BeaconScanningActivity.this, message, Toast.LENGTH_SHORT).show();
            });
        }

        @Override
        public void didExitRegion(BeaconRegion beaconRegion) {
            Region region = RegionUtil.toRegion(beaconRegion);
            String message = String.format(Locale.KOREA, "%s 지역을 이탈했습니다.", region.name());
            runOnUiThread(() -> {
                Toast.makeText(BeaconScanningActivity.this, message, Toast.LENGTH_SHORT).show();
            });
        }

        @Override
        public void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion) {
            if (startButton.isChecked()) {
                for (Beacon beacon : beacons) {
                    scanListAdapter.addBeacon(beacon);
                }

                refreshResults();
            }
        }

    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.scan, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_bar_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                runOnUiThread(() -> {
                    if (newText == null || newText.isEmpty()) {
                        scanListAdapter.clearSearchResults();
                    } else {
                        scanListAdapter.search(newText);
                    }

                    refreshResults();
                });

                return false;
            }
        });

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Beacon Scanning");

        realm = Realm.getDefaultInstance();

        scanListAdapter = new ScanListAdapter();
        assert scanListView != null;
        scanListView.setAdapter(scanListAdapter);

        beaconService = BeaconServiceFactory.create(getApplicationContext());
        beaconService.addObserver(beaconObserver);

        resetButton.setOnClickListener((view) -> {
            boolean savedStatus = startButton.isChecked();
            startButton.setChecked(false);

            new AlertDialog.Builder(BeaconScanningActivity.this)
                    .setTitle("스캔결과 삭제")
                    .setMessage("스캔결과를 모두 삭제하시겠습니까?")
                    .setPositiveButton("네", (dialog, which) -> {
                        clearResults();
                    })
                    .setNegativeButton("아니오", (dialog, which) -> {
                        startButton.setChecked(savedStatus);
                    })
                    .show();
        });

        saveButton.setOnClickListener((view) -> {
            boolean savedStatus = startButton.isChecked();
            startButton.setChecked(false);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("스캔결과 저장");
            builder.setMessage("저장을 하면 현재 스캔 목록은 초기화됩니다.\n저장을 위해 태그를 입력해주세요.");

            EditText tagEdit = new EditText(this);
            int padding = Float.valueOf(10 * getBaseContext().getResources().getDisplayMetrics().density).intValue();
            builder.setView(tagEdit, padding, 0, padding, 0);

            tagEdit.setText(Utils.formatShortly(new Date()) + "_");
            tagEdit.setSelection(tagEdit.getText().length());

            builder.setPositiveButton("저장", (dialog, which) -> {
                String tag = tagEdit.getText().toString();
                scanListAdapter.save(tag);
                refreshResults();

                String message = String.format(Locale.KOREA, "%s 로 저장되었습니다.", tag);
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show();
            });

            builder.setNegativeButton("취소", (dialog, which) -> {
                startButton.setChecked(savedStatus);
            });

            builder.show();
        });

        realm.addChangeListener(element -> {
            refreshResults();
        });

        refreshResults();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconService.removeObserver(beaconObserver);
        realm.close();
    }

    private void refreshResults() {
        runOnUiThread(() -> {
            scanListAdapter.notifyDataSetChanged();
            int count = scanListAdapter.getCount();
            String message = Utils.countingHeader(count);
            headerTextView.setText(message);
        });
    }

    private void clearResults() {
        runOnUiThread(() -> {
            startButton.setChecked(false);
            scanListAdapter.clearAll();
            refreshResults();
        });
    }

    private void findingViews() {
        setContentView(R.layout.activity_beacon_scanning);
        LinearLayout headerView = (LinearLayout) findViewById(R.id.header);
        assert headerView != null;
        headerTextView = (TextView) headerView.findViewById(R.id.center);
        scanListView = (ListView) findViewById(R.id.scanList);
        startButton = (ToggleButton) findViewById(R.id.btnStart);
        resetButton = (Button) findViewById(R.id.btnReset);
        saveButton = (Button) findViewById(R.id.btnSave);
    }

    class ScanListAdapter extends BaseAdapter {

        private final String TAG = getClass().getName();
        private RealmQuery<ScannedBeacon> baseQuery = realm.where(ScannedBeacon.class)
                .equalTo("tag", ScannedBeacon.UNASSIGNED_TAG);
        private RealmResults<ScannedBeacon> results = baseQuery.findAllSorted("createdAt");
        private RealmResults<ScannedBeacon> searchResults = null;

        @Override
        public int getCount() {
            if (searchResults != null) {
                return Long.valueOf(searchResults.size()).intValue();
            }

            return Long.valueOf(results.size()).intValue();
        }

        @Override
        public Object getItem(int position) {
            if (searchResults != null) {
                return searchResults.get(position);
            }

            return results.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ScannedItemView scannedItemView;

            if (convertView == null) {
                scannedItemView = new ScannedItemView(BeaconScanningActivity.this);
            } else {
                scannedItemView = (ScannedItemView) convertView;
            }

            ScannedBeacon item;

            if (searchResults != null) {
                item = searchResults.get(position);
            } else {
                item = results.get(position);
            }

            scannedItemView.setItem(item);

            return scannedItemView;
        }

        public void search(String query) {
            Integer numberQuery;
            try {
                numberQuery = Integer.valueOf(query);
            } catch (NumberFormatException e) {
                numberQuery = null;
            }

            // TODO 쿼리 앞 부분을 재사용 할 순 없을까?
            RealmQuery<ScannedBeacon> preQuery = realm.where(ScannedBeacon.class)
                    .equalTo("tag", ScannedBeacon.UNASSIGNED_TAG).beginGroup()
                    .contains("uuid", query);

            if (numberQuery != null) {
                preQuery.or().equalTo("major", numberQuery)
                        .or().equalTo("minor", numberQuery);
            }

            searchResults = preQuery.endGroup()
                    .findAllSorted("createdAt");
        }

        public void clearSearchResults() {
            searchResults = null;
        }

        void addBeacon(Beacon beacon) {
            runOnUiThread(() -> {
                for (ScannedBeacon item : results) {
                    if (item.isSame(beacon)) {
                        realm.executeTransaction((realm) -> {
                            item.histories.add(new ScannedHistory(beacon));
                        });
                        return;
                    }
                }

                realm.executeTransaction((realm) -> {
                    realm.copyToRealm(new ScannedBeacon(beacon))
                            .histories
                            .add(realm.copyToRealm(new ScannedHistory(beacon)));
                });
            });
        }

        public void clearAll() {
            realm.executeTransaction((realm) -> {
                results.deleteAllFromRealm();
            });
        }

        public void save(String tag) {
            runOnUiThread(() -> {
                realm.executeTransaction((realm) -> {
                    for (ScannedBeacon scannedBeacon : results) {
                        scannedBeacon.tag = tag;
                        scannedBeacon.taggedAt = new Date();
                    }
                });
            });
        }
    }

}
