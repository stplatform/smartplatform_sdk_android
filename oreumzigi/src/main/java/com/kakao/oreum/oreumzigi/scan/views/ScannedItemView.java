package com.kakao.oreum.oreumzigi.scan.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.scan.HistoryDialog;
import com.kakao.oreum.oreumzigi.scan.models.ScannedBeacon;
import com.kakao.oreum.oreumzigi.supports.Utils;

public class ScannedItemView extends LinearLayout {

    private TextView uuidView;
    private TextView majorView;
    private TextView minorView;
    private TextView updatedAtView;
    private TextView historiesView;

    private ScannedBeacon scannedBeacon;

    public ScannedItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_scan, this, true);

        uuidView = (TextView) findViewById(R.id.uuid);
        majorView = (TextView) findViewById(R.id.major);
        minorView = (TextView) findViewById(R.id.minor);
        updatedAtView = (TextView) findViewById(R.id.updatedAt);
        historiesView = (TextView) findViewById(R.id.histories);

        setOnClickListener((view) -> {
            HistoryDialog dialog = new HistoryDialog(getContext(), scannedBeacon);
            dialog.show();
        });
    }

    public void setItem(ScannedBeacon item) {
        scannedBeacon = item;

        uuidView.setText(item.uuid);
        majorView.setText(String.valueOf(item.major));
        minorView.setText(String.valueOf(item.minor));
        updatedAtView.setText(Utils.format(item.createdAt));
        historiesView.setText(String.valueOf(item.histories.size()));
    }

}