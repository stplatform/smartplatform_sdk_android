package com.kakao.oreum.oreumzigi.nearbyspots;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.kakao.oreum.oreumzigi.beaconranging.BeaconOrdering;
import com.kakao.oreum.tamra.base.Spot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class SpotListAdapter extends BaseAdapter {

    private final Activity activity;

    private final List<Spot> currentSpots = new ArrayList<>();
    private final Map<Spot, Date> cameAtTimeMap = new HashMap<>();
    private BeaconOrdering ordering = BeaconOrdering.ByProximity;

    public SpotListAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return currentSpots.size();
    }

    @Override
    public Object getItem(int position) {
        return currentSpots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return currentSpots.get(position).id();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SpotView spotView;

        if (convertView == null) {
            spotView = new SpotView(activity);
        } else {
            spotView = (SpotView) convertView;
        }

        if (currentSpots.size() > position && cameAtTimeMap.size() > position) {
            Spot spot = currentSpots.get(position);
            Date cameAt = cameAtTimeMap.get(spot);
            spotView.setData(spot, cameAt);
        }

        return spotView;
    }

    public void addSpot(Spot spot) {
        currentSpots.add(spot);
        cameAtTimeMap.put(spot, new Date());
        sort();
    }

    public void removeSpot(Spot spot) {
        currentSpots.remove(spot);
        cameAtTimeMap.remove(spot);
        sort();
    }

    public void replaceSpot(Spot spot) {
        currentSpots.remove(spot);
        currentSpots.add(spot);
        sort();
    }

    public void setOrdering(BeaconOrdering ordering) {
        this.ordering = ordering;
        sort();
        notifyDataSetChanged();
    }

    public void sort() {
        switch (ordering) {
            case ById:
                Collections.sort(currentSpots, Spot.ID_ORDER);
                break;
            case ByProximity:
                Collections.sort(currentSpots, Spot.PROXIMITY_ORDER
                        .thenComparing(Spot.ACCURACY_ORDER));
                break;
            case ByRangedAt:
                Collections.sort(currentSpots, (lhs, rhs) -> {
                    Date ldate = cameAtTimeMap.get(lhs);
                    Date rdate = cameAtTimeMap.get(rhs);
                    return ldate.compareTo(rdate);
                });
                break;
            default:
                break;
        }
    }

}
