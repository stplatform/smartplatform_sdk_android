package com.kakao.oreum.oreumzigi;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.beaconranging.BeaconRangingActivity;
import com.kakao.oreum.oreumzigi.errors.ErrorsActivity;
import com.kakao.oreum.oreumzigi.scan.BeaconScanningActivity;
import com.kakao.oreum.oreumzigi.scan.BeaconScanningResultsActivity;
import com.kakao.oreum.oreumzigi.nearbyspots.NearbySpotsActivity;
import com.kakao.oreum.oreumzigi.supports.OreumHttpRequest;
import com.kakao.oreum.tamra.Tamra;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.base.Profile;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.error.TamraErrorFilters;
import com.kakao.oreum.tamra.error.TamraInitException;
import com.kakao.oreum.tamra.internal.fixture.Version;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class MainActivity extends AppCompatActivity {

    private final int MY_PERMISSIONS_REQUEST_ACCESS_LOCATION = 1;
    private final int REQUEST_ENABLE_BT = 2;

    private TextView appVersionView;
    private TextView sdkVersionView;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Main Menu");

        checkBluetooth();

        //checkPermission();

        String appVersion = String.format(Locale.KOREA,
                "APP VERSION - %s",
                BuildConfig.VERSION_NAME);

        String sdkVersion = String.format(Locale.KOREA,
                "SDK VERSION - %s",
                Version.asString());

        appVersionView.setText(appVersion);
        sdkVersionView.setText(sdkVersion);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        List<MenuItem> items = new ArrayList<>();
        items.add(new MenuItem("Beacon Ranging", "주변 비콘 신호를 확인합니다.", BeaconRangingActivity.class));
        items.add(new MenuItem("Nearby Spots", "주변 비콘에 대한 Spot 정보를 확인합니다.", NearbySpotsActivity.class));
        items.add(new MenuItem("Beacon Scanning", "주변 비콘 신호를 누적하여 기록합니다.", BeaconScanningActivity.class));
        items.add(new MenuItem("Beacon Scanning Results", "Beacon Scanning에서 저장된 내역을 확인합니다.", BeaconScanningResultsActivity.class));
        items.add(new MenuItem("Errors", "Tamra SDK에서 발생한 에러내역을 확인합니다.", ErrorsActivity.class));
        MenuAdapter adapter = new MenuAdapter(this, items);
        recyclerView.setAdapter(adapter);
    }

    private void findingViews() {
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayout headerView = (LinearLayout) findViewById(R.id.header);
        assert headerView != null;
        appVersionView = (TextView) headerView.findViewById(R.id.left);
        sdkVersionView = (TextView) headerView.findViewById(R.id.right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for (Region region : Tamra.monitoringRegions()) {
            Tamra.stopMonitoring(region);
        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_LOCATION);
            }
        } else {
            initTamraForDefault();
        }
    }

    public void checkBluetooth() {

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initTamraForDefault();
                }
            }
            case REQUEST_ENABLE_BT: {
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermission();
                }
            }
        }
    }

    private void initTamraForDefault() {
        initTamra(Profile.PRODUCTION);
    }

    private void initTamra(Profile profile) {
        String appKey;
        switch (profile) {
            case DEVELOP:
                appKey = "f8312695ce1e4cbfaa4ac82caf7be720";
                break;
            case TEST:
                appKey = "b5296e7022054c8284aeb6e9935ccb30";
                break;
            case PRODUCTION:
                appKey = "5f0e6ae2f407467bbf7ca895167511a4";
                break;
            default:
                throw new RuntimeException("Unsupported profile.");
        }

        Config config = Config.createFor(
                getApplicationContext(),
                appKey,
                profile);
        Tamra.init(config);
        OreumHttpRequest.init(config);

        // 초기화 오류가 있는지 체크
        boolean initOk = Tamra.recentErrors()
                .filter(TamraErrorFilters.causedBy(TamraInitException.class))
                .isEmpty();

        // monitor all known regions.
        if (initOk) {
            for (Region region : Regions.all()) {
                Tamra.startMonitoring(region);
            }
        }
    }

    private class MenuItem {

        String title;
        String subTitle;
        Class<? extends Activity> activityClass;

        MenuItem(String title, String subTitle, Class<? extends Activity> activityClass) {
            this.title = title;
            this.subTitle = subTitle;
            this.activityClass = activityClass;
        }

    }

    private class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

        private Context context;
        private List<MenuItem> items;

        MenuAdapter(Context context, List<MenuItem> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mainmenu, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MenuAdapter.ViewHolder holder, int position) {
            MenuItem item = items.get(position);

            holder.titleView.setText(item.title);
            holder.subTitleView.setText(item.subTitle);
            holder.cardView.setOnClickListener(v -> {
                Intent intent = new Intent(context, item.activityClass);
                startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CardView cardView;
            TextView titleView;
            TextView subTitleView;

            public ViewHolder(View itemView) {
                super(itemView);

                cardView = (CardView) itemView.findViewById(R.id.card);
                titleView = (TextView) cardView.findViewById(R.id.title);
                subTitleView = (TextView) cardView.findViewById(R.id.subTitle);
            }

        }
    }
}
