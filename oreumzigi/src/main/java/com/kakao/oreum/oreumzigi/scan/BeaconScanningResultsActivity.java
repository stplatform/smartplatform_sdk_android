package com.kakao.oreum.oreumzigi.scan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.scan.models.ScannedBeacon;
import com.kakao.oreum.oreumzigi.supports.Utils;

import io.realm.Realm;
import io.realm.RealmResults;

public class BeaconScanningResultsActivity extends AppCompatActivity {

    private TextView headerTextView;
    private ListView tagListView;
    private Button clearAllButton;

    private TagListAdapter tagListAdapter;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Beacon Scanning Results");

        realm = Realm.getDefaultInstance();
        tagListAdapter = new TagListAdapter();

        tagListView.setAdapter(tagListAdapter);
        clearAllButton.setOnClickListener(v -> {
            new AlertDialog.Builder(BeaconScanningResultsActivity.this)
                    .setTitle("스캔결과 모두 삭제")
                    .setMessage("저장된 스캔결과를 모두 삭제하시겠습니까?")
                    .setPositiveButton("모두 삭제", (dialog, which) -> {
                        realm.executeTransaction(realm -> {
                            realm.where(ScannedBeacon.class)
                                    .notEqualTo("tag", ScannedBeacon.UNASSIGNED_TAG)
                                    .findAll()
                                    .deleteAllFromRealm();
                        });

                        runOnUiThread(() -> {
                            String message = "저장된 스캔결과가 모두 삭제되었습니다.";
                            Toast.makeText(BeaconScanningResultsActivity.this, message, Toast.LENGTH_SHORT).show();
                        });
                    })
                    .setNegativeButton("취소", (dialog, which) -> {
                        // do nothing
                    })
                    .show();
        });
        realm.addChangeListener(element -> {
            refreshResults();
        });

        refreshResults();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshResults();
    }

    private void refreshResults() {
        runOnUiThread(() -> {
            tagListAdapter.notifyDataSetChanged();
            String header = Utils.countingHeader(tagListAdapter.getCount());
            headerTextView.setText(header);
        });
    }

    private void findingViews() {
        setContentView(R.layout.activity_beacon_scanning_results);

        LinearLayout headerView = (LinearLayout) findViewById(R.id.header);
        assert headerView != null;
        headerTextView = (TextView) headerView.findViewById(R.id.center);
        tagListView = (ListView) findViewById(R.id.tagList);
        clearAllButton = (Button) findViewById(R.id.btnClearAll);
    }

    class TagListAdapter extends BaseAdapter {

        RealmResults<ScannedBeacon> results = realm.where(ScannedBeacon.class)
                .notEqualTo("tag", ScannedBeacon.UNASSIGNED_TAG)
                .distinct("tag").sort("taggedAt");

        @Override
        public int getCount() {
            return results.size();
        }

        @Override
        public Object getItem(int position) {
            return results.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TagView tagView;

            if (convertView == null) {
                tagView = new TagView(BeaconScanningResultsActivity.this);
            } else {
                tagView = (TagView) convertView;
            }

            tagView.setItem(results.get(position));

            return tagView;
        }

    }

    private class TagView extends LinearLayout {

        private TextView tagView;
        private TextView savedAtView;

        private ScannedBeacon scannedBeacon;

        public TagView(Context context) {
            super(context);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.item_scan_tag, this, true);

            tagView = (TextView) findViewById(R.id.tag);
            savedAtView = (TextView) findViewById(R.id.savedAt);

            setOnClickListener(v -> {
                Intent intent = new Intent(BeaconScanningResultsActivity.this, BeaconScanningResultsDetailActivity.class);
                intent.putExtra("tag", scannedBeacon.tag);
                intent.putExtra("taggedAt", scannedBeacon.taggedAt);
                startActivity(intent);
            });
        }

        public void setItem(ScannedBeacon scannedBeacon) {
            this.scannedBeacon = scannedBeacon;

            tagView.setText(scannedBeacon.tag);
            savedAtView.setText(Utils.format(scannedBeacon.taggedAt));
        }

    }
}
