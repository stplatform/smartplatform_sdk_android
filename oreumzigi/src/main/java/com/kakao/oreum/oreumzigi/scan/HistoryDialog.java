package com.kakao.oreum.oreumzigi.scan;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.scan.models.ScannedBeacon;
import com.kakao.oreum.oreumzigi.scan.models.ScannedHistory;
import com.kakao.oreum.oreumzigi.supports.Utils;

import java.util.List;
import java.util.Locale;

public class HistoryDialog extends Dialog {

    private ScannedBeacon scannedBeacon;
    private ScannedHistoryListAdapter listAdapter;

    public HistoryDialog(Context context, ScannedBeacon scannedBeacon) {
        super(context);
        this.scannedBeacon = scannedBeacon;
        listAdapter = new ScannedHistoryListAdapter(scannedBeacon.histories);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_history);

        TextView uuidView = (TextView) findViewById(R.id.uuid);
        TextView majorView = (TextView) findViewById(R.id.major);
        TextView minorView = (TextView) findViewById(R.id.minor);
        LinearLayout headerView = (LinearLayout) findViewById(R.id.header);
        assert headerView != null;
        TextView headerTextView = (TextView) headerView.findViewById(R.id.center);
        ListView historyListView = (ListView) findViewById(R.id.historyList);
        Button dismissButton = (Button) findViewById(R.id.btnDismiss);

        uuidView.setText(scannedBeacon.uuid);
        majorView.setText(String.valueOf(scannedBeacon.major));
        minorView.setText(String.valueOf(scannedBeacon.minor));
        headerTextView.setText(Utils.countingHeader(scannedBeacon.histories.size()));
        historyListView.setAdapter(listAdapter);
        dismissButton.setOnClickListener((view) -> dismiss());
    }

    private class ScannedHistoryListAdapter extends BaseAdapter {

        private List<ScannedHistory> scannedHistories;

        ScannedHistoryListAdapter(List<ScannedHistory> scannedHistories) {
            this.scannedHistories = scannedHistories;
        }

        @Override
        public int getCount() {
            return scannedHistories.size();
        }

        @Override
        public Object getItem(int position) {
            return scannedHistories.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ScannedHistoryView view;

            if (convertView == null) {
                view = new ScannedHistoryView(getContext());
            } else {
                view = (ScannedHistoryView) convertView;
            }

            ScannedHistory history = scannedHistories.get(position);
            view.setItem(history);

            return view;
        }

    }

    private class ScannedHistoryView extends LinearLayout {

        TextView proximityView;
        TextView accuracyView;
        TextView rssiView;
        TextView createdAtView;

        ScannedHistoryView(Context context) {
            super(context);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.item_scan_history, this, true);

            proximityView = (TextView) findViewById(R.id.proximity);
            accuracyView = (TextView) findViewById(R.id.accuracy);
            rssiView = (TextView) findViewById(R.id.rssi);
            createdAtView = (TextView) findViewById(R.id.createdAt);
        }

        public void setItem(ScannedHistory item) {
            proximityView.setText(item.proximity);
            accuracyView.setText(String.format(Locale.KOREA, "%.3f", item.accuracy));
            rssiView.setText(String.valueOf(item.rssi));
            createdAtView.setText(Utils.format(item.createdAt));
        }

    }

}
