package com.kakao.oreum.oreumzigi.supports;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.KOREA);
    private static final DateFormat dateFormatShortly = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);

    public static String format(Date date) {
        if (date == null) {
            return "";
        }

        return dateFormat.format(date);
    }

    public static String formatShortly(Date date) {
        if (date == null) {
            return "";
        }

        return dateFormatShortly.format(date);
    }

    public static String countingHeader(int count) {
        return String.format(Locale.KOREA, "총 %d 건", count);
    }

}
