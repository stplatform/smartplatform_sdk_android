package com.kakao.oreum.oreumzigi;

import android.app.Application;

//import com.tsengvn.typekit.Typekit;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class OreumZigi extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Typekit
        //Typekit.getInstance().addNormal(Typekit.createFromAsset(this, "fonts/NotoSansKR-Thin.otf"));

        // Realm
        //RealmConfiguration config = new RealmConfiguration.Builder(this)
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("oreumzigi.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

}
