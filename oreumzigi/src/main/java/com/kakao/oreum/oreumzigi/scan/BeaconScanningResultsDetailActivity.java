package com.kakao.oreum.oreumzigi.scan;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.scan.models.ScannedBeacon;
import com.kakao.oreum.oreumzigi.scan.supports.ReportUploader;
import com.kakao.oreum.oreumzigi.scan.views.ScannedItemView;
import com.kakao.oreum.oreumzigi.supports.Utils;

import java.util.Date;
import java.util.concurrent.Executors;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class BeaconScanningResultsDetailActivity extends AppCompatActivity {

    private TextView tagView;
    private TextView taggedAtView;
    private TextView headerTextView;
    private ListView scanListView;
    private Button removeButton;
    private Button sendToServerButton;

    private ScanListAdapter scanListAdapter;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Beacon Scanning Results > Detail");

        realm = Realm.getDefaultInstance();

        String tag = getIntent().getStringExtra("tag");
        Date taggedAt = (Date) getIntent().getSerializableExtra("taggedAt");

        tagView.setText(tag);
        taggedAtView.setText(Utils.format(taggedAt));

        scanListAdapter = new ScanListAdapter(tag);
        scanListView.setAdapter(scanListAdapter);

        removeButton.setOnClickListener(v -> {
            new AlertDialog.Builder(BeaconScanningResultsDetailActivity.this)
                    .setTitle("스캔기록 삭제")
                    .setMessage("현재 스캔기록을 삭제하시겠습니까?")
                    .setPositiveButton("삭제", (dialog, which) -> {
                        scanListAdapter.clearAll();
                        finish();
                    })
                    .setNegativeButton("취소", (dialog, which) -> {
                        // do nothing
                    })
                    .show();
        });

        sendToServerButton.setOnClickListener(v -> {
            new AlertDialog.Builder(BeaconScanningResultsDetailActivity.this)
                    .setTitle("스캔기록 전송")
                    .setMessage("현재 스캔기록을 서버로 전송하시겠습니까?")
                    .setPositiveButton("전송", (dialog, which) -> {
                        Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                String message;

                                if (msg.what == 0) {
                                    message = "전송 성공";
                                } else {
                                    message = "전송 실패";
                                }

                                Toast.makeText(BeaconScanningResultsDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        };

                        Executors.newSingleThreadExecutor().execute(() -> {
                            if (ReportUploader.upload(tag)) {
                                handler.sendEmptyMessage(0);
                            } else {
                                handler.sendEmptyMessage(-1);
                            }
                        });
                    })
                    .setNegativeButton("취소", (dialog, which) -> {
                        // do nothing
                    })
                    .show();
        });

        refreshResults();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.scan, menu);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_bar_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                runOnUiThread(() -> {
                    if (newText == null || newText.isEmpty()) {
                        scanListAdapter.clearSearchResults();
                    } else {
                        scanListAdapter.search(newText);
                    }

                    refreshResults();
                });

                return false;
            }
        });

        return true;
    }

    public void refreshResults() {
        runOnUiThread(() -> {
            scanListAdapter.notifyDataSetChanged();
            int count = scanListAdapter.getCount();
            headerTextView.setText(Utils.countingHeader(count));
        });
    }

    private void findingViews() {
        setContentView(R.layout.activity_beacon_scanning_results_detail);
        tagView = (TextView) findViewById(R.id.tag);
        taggedAtView = (TextView) findViewById(R.id.taggedAt);
        LinearLayout headerView = (LinearLayout) findViewById(R.id.header);
        assert headerView != null;
        headerTextView = (TextView) headerView.findViewById(R.id.center);
        scanListView = (ListView) findViewById(R.id.scanList);
        removeButton = (Button) findViewById(R.id.btnRemove);
        sendToServerButton = (Button) findViewById(R.id.btnSendToServer);
    }

    private class ScanListAdapter extends BaseAdapter {

        private String tag;
        private RealmResults<ScannedBeacon> results;
        private RealmResults<ScannedBeacon> searchResults = null;

        public ScanListAdapter(String tag) {
            this.tag = tag;
            results = realm.where(ScannedBeacon.class)
                    .equalTo("tag", tag)
                    .findAll();
        }

        @Override
        public int getCount() {
            if (searchResults != null) {
                return Long.valueOf(searchResults.size()).intValue();
            }

            return results.size();
        }

        @Override
        public Object getItem(int position) {
            if (searchResults != null) {
                return searchResults.get(position);
            }

            return results.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ScannedItemView view;

            if (convertView == null) {
                view = new ScannedItemView(BeaconScanningResultsDetailActivity.this);
            } else {
                view = (ScannedItemView) convertView;
            }

            ScannedBeacon item;

            if (searchResults != null) {
                item = searchResults.get(position);
            } else {
                item = results.get(position);
            }

            view.setItem(item);

            return view;
        }

        public void search(String query) {
            Integer numberQuery;
            try {
                numberQuery = Integer.valueOf(query);
            } catch (NumberFormatException e) {
                numberQuery = null;
            }

            // TODO 쿼리 앞 부분을 재사용 할 순 없을까?
            RealmQuery<ScannedBeacon> preQuery = realm.where(ScannedBeacon.class)
                    .equalTo("tag", tag).beginGroup()
                    .contains("uuid", query);

            if (numberQuery != null) {
                preQuery.or().equalTo("major", numberQuery)
                        .or().equalTo("minor", numberQuery);
            }

            searchResults = preQuery.endGroup()
                    .findAllSorted("createdAt");
        }

        public void clearSearchResults() {
            searchResults = null;
        }

        public void clearAll() {
            realm.executeTransaction(realm -> {
                results.deleteAllFromRealm();
            });
        }

    }

}
