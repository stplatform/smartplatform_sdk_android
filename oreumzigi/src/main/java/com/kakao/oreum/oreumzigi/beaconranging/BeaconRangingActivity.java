package com.kakao.oreum.oreumzigi.beaconranging;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.infra.beacon.altbeacon.BeaconServiceFactory;
import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.supports.Utils;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.RegionUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class BeaconRangingActivity extends AppCompatActivity {

    private static final String EMPTY_REGION_MESSAGE = "진입한 지역이 없습니다.";

    private BeaconService beaconService;
    private BeaconListAdapter listAdapter;

    private TextView regionView;
    private TextView countView;
    private ListView listView;
    private Button orderByIdButton;
    private Button orderByProximityButton;
    private Button orderByRangedAtButton;

    private final BeaconServiceObserver beaconObserver = new BeaconServiceObserver() {
        private final Map<BeaconRegion, Set<Beacon>> ranged = new HashMap<>();

        @Override
        public void didEnterRegion(BeaconRegion beaconRegion) {
            Region region = RegionUtil.toRegion(beaconRegion);
            showToastMessage(region.name() + "에 진입했습니다.");
            setRegion(region);
        }

        @Override
        public void didExitRegion(BeaconRegion beaconRegion) {
            ranged.remove(beaconRegion);
            Region region = RegionUtil.toRegion(beaconRegion);
            showToastMessage(region.name() + "에서 이탈했습니다.");
            setRegion(null);
        }

        @Override
        public void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion) {
            ranged.put(beaconRegion, beacons);

            if (beacons.size() > 0) {
                setRegion(RegionUtil.toRegion(beaconRegion));
            }

            List<Beacon> beaconList = new ArrayList<>();

            for (Set<Beacon> c : ranged.values()) {
                beaconList.addAll(c);
            }

            listAdapter.resetData(beaconList);
            setCount(beaconList.size());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Beacon Ranging");

        beaconService = BeaconServiceFactory.create(getApplicationContext());
        beaconService.addObserver(beaconObserver);

        regionView.setText(EMPTY_REGION_MESSAGE);

        listAdapter = new BeaconListAdapter(this);
        listView.setAdapter(listAdapter);

        orderByIdButton.setOnClickListener(v -> listAdapter.setOrdering(BeaconOrdering.ById));
        orderByProximityButton.setOnClickListener(v -> listAdapter.setOrdering(BeaconOrdering.ByProximity));
        orderByRangedAtButton.setOnClickListener(v -> listAdapter.setOrdering(BeaconOrdering.ByRangedAt));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        beaconService.removeObserver(beaconObserver);
    }

    private void findingViews() {
        setContentView(R.layout.activity_beacon_ranging);

        RelativeLayout header = (RelativeLayout) findViewById(R.id.header);
        assert header != null;
        regionView = (TextView) header.findViewById(R.id.left);
        countView = (TextView) header.findViewById(R.id.right);

        listView = (ListView) findViewById(R.id.listView);

        orderByIdButton = (Button) findViewById(R.id.btnOrderById);
        orderByProximityButton = (Button) findViewById(R.id.btnOrderByProximity);
        orderByRangedAtButton = (Button) findViewById(R.id.btnOrderByRangedAt);
    }

    private void showToastMessage(String message) {
        runOnUiThread(() -> Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT).show());

    }

    public void setRegion(Region region) {
        runOnUiThread(() -> {
            if (region == null) {
                regionView.setText(EMPTY_REGION_MESSAGE);
            } else {
                regionView.setText(region.name());
            }
        });
    }

    public void setCount(int count) {
        runOnUiThread(() -> countView.setText(Utils.countingHeader(count)));
    }

}

