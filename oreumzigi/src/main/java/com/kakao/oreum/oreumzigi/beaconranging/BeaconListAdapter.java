package com.kakao.oreum.oreumzigi.beaconranging;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.google.common.collect.Sets;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.oreumzigi.beaconranging.BeaconOrdering;
import com.kakao.oreum.oreumzigi.beaconranging.BeaconView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconListAdapter extends BaseAdapter {

    private final Activity activity;

    private final List<Beacon> currentBeacons = new ArrayList<>();
    private final Map<Beacon, Date> rangedTimeMap = new HashMap<>();
    private BeaconOrdering ordering = BeaconOrdering.ByProximity;

    public BeaconListAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return currentBeacons.size();
    }

    @Override
    public Object getItem(int position) {
        return currentBeacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return idForBeacon(currentBeacons.get(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BeaconView beaconView;
        if (convertView == null) {
            beaconView = new BeaconView(activity);
        } else {
            beaconView = (BeaconView) convertView;
        }
        if (currentBeacons.size() > position && rangedTimeMap.size() > position) {
            Beacon beacon = currentBeacons.get(position);
            Date rangedAt = rangedTimeMap.get(beacon);
            beaconView.setData(beacon, rangedAt);
        }
        return beaconView;
    }

    public void resetData(Collection<Beacon> beacons) {
        activity.runOnUiThread(() -> {
            Set<Beacon> prevBeaconSet = Sets.newHashSet(currentBeacons);
            Set<Beacon> newBeaconSet = Sets.newHashSet(beacons);

            for (Beacon toRemove : Sets.difference(prevBeaconSet, newBeaconSet)) {
                rangedTimeMap.remove(toRemove);
            }

            for (Beacon toAdd : Sets.difference(newBeaconSet, prevBeaconSet)) {
                rangedTimeMap.put(toAdd, new Date());
            }

            this.currentBeacons.clear();
            this.currentBeacons.addAll(beacons);
            sort();
            notifyDataSetChanged();
        });
    }

    public void setOrdering(BeaconOrdering ordering) {
        this.ordering = ordering;
        sort();
        notifyDataSetChanged();
    }

    public void sort() {
        switch (ordering) {
            case ById:
                Collections.sort(currentBeacons, (lhs, rhs) -> beaconId(lhs).compareTo(beaconId(rhs)));
                break;
            case ByProximity:
                Collections.sort(currentBeacons, (lhs, rhs) -> Double.compare(lhs.accuracy(), rhs.accuracy()));
                break;
            case ByRangedAt:
                Collections.sort(currentBeacons, (lhs, rhs) -> {
                    Date ldate = rangedTimeMap.get(lhs);
                    Date rdate = rangedTimeMap.get(rhs);
                    return ldate.compareTo(rdate);
                });
                break;
        }
    }

    private String beaconId(Beacon beacon) {
        return String.format(Locale.KOREA, "%s-%03d-%03d", beacon.proximityUUID(), beacon.major(), beacon.minor());
    }

    private long idForBeacon(Beacon beacon) {
        return (beacon.proximityUUID().hashCode() << 32)
                | (beacon.major() << 16)
                | beacon.minor();
    }
}
