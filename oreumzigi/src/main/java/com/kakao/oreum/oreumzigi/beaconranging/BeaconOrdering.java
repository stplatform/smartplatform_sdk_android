package com.kakao.oreum.oreumzigi.beaconranging;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public enum BeaconOrdering {
    ById,
    ByProximity,
    ByRangedAt
}
