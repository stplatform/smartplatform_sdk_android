package com.kakao.oreum.oreumzigi.nearbyspots;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.customviews.ProximityView;
import com.kakao.oreum.oreumzigi.supports.Utils;
import com.kakao.oreum.tamra.base.Spot;

import java.util.Date;
import java.util.Locale;

import io.realm.annotations.Index;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class SpotView extends LinearLayout {

    private TextView idView;
    private TextView regionView;
    private TextView descriptionView;
    private TextView dataSizeView;
    private ProximityView proximityView;
    private TextView accuracyView;
    private TextView cameAtView;

    public SpotView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_spot, this, true);

        idView = (TextView) findViewById(R.id.id);
        regionView = (TextView) findViewById(R.id.region);
        descriptionView = (TextView) findViewById(R.id.description);
        dataSizeView = (TextView) findViewById(R.id.dataSize);
        proximityView = (ProximityView) findViewById(R.id.proximity);
        accuracyView = (TextView) findViewById(R.id.accuracy);
        cameAtView = (TextView) findViewById(R.id.cameAt);
    }

    public void setData(Spot spot, Date cameAt) {
        Log.w("Spot", spot.toString());
        String idString = String.valueOf(spot.id());

        idView.setText(idString);
        regionView.setText(spot.region().name());
        descriptionView.setText(spot.description());
        proximityView.setProximity(spot.proximity());
        accuracyView.setText(String.format(Locale.KOREAN, "%.3f", spot.accuracy()));
        cameAtView.setText(Utils.format(cameAt));

        spot.data(optionalValue -> {
            String content = optionalValue.orElse("");
            int contentSize = 0;

            if (!"\"\"".equals(content.trim())) {
                contentSize = content.length();
                setOnClickListener(v -> {
                    Intent intent = new Intent(getContext(), SpotContentActivity.class);
                    intent.putExtra("spotId", spot.id());
                    intent.putExtra("description", spot.description());
                    intent.putExtra("content", content);
                    getContext().startActivity(intent);
                });
            }

            dataSizeView.setText(String.valueOf(contentSize));
        });
    }

}
