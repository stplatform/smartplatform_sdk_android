package com.kakao.oreum.oreumzigi.scan.models;

import com.kakao.oreum.infra.beacon.Beacon;

import java.util.Date;
import java.util.Locale;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.Required;

public class ScannedBeacon extends RealmObject {

    public static final String UNASSIGNED_TAG = "unassigned";

    @Required
    public String uuid;

    @Required
    public Integer major;

    @Required
    public Integer minor;

    @Index
    public String tag;

    @Required
    public Date createdAt;

    public Date taggedAt;

    public RealmList<ScannedHistory> histories;

    public ScannedBeacon() {
        super();
        tag = UNASSIGNED_TAG;
        createdAt = new Date();
    }

    public ScannedBeacon(Beacon beacon) {
        this();
        uuid = beacon.proximityUUID().toString();
        major = beacon.major();
        minor = beacon.minor();
    }

    public boolean isSame(Beacon beacon) {
        return uuid.equals(beacon.proximityUUID().toString())
                && major.equals(beacon.major())
                && minor.equals(beacon.minor());
    }

    public String minifiedUuid() {
        return String.format(Locale.KOREA,
                "%s..%s",
                uuid.substring(0, 4).toUpperCase(),
                uuid.substring(uuid.length() - 4).toUpperCase());
    }

}
