package com.kakao.oreum.oreumzigi.nearbyspots;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.beaconranging.BeaconOrdering;
import com.kakao.oreum.oreumzigi.supports.Utils;
import com.kakao.oreum.tamra.Tamra;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.SpotFilters;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.tamra.base.TamraObserverAdapter;
import com.kakao.oreum.tamra.internal.spots.NearbySpotsImpl;

import java.util.Locale;

public class NearbySpotsActivity extends AppCompatActivity {

    private TextView headerTextView;
    private ListView listView;
    private RadioButton orderByIdButton;
    private RadioButton orderByProximityButton;
    private RadioButton orderByRangedAtButton;

    private SpotListAdapter listAdapter;

    private final TamraObserver tamraObserver = new TamraObserverAdapter() {
        private NearbySpots prev = NearbySpotsImpl.empty();

        @Override
        public void didExit(Region region) {
            prev = prev.filter(SpotFilters.belongsTo(region));
            prev.foreach(s -> listAdapter.removeSpot(s));
            listAdapter.notifyDataSetChanged();
            updateHeader(listAdapter.getCount());
        }

        @Override
        public void ranged(final NearbySpots spots) {
            prev.filter(SpotFilters.oneOf(spots).negate())
                    .foreach(s -> {
                        listAdapter.removeSpot(s);
                    });

            spots.filter(SpotFilters.oneOf(prev).negate())
                    .foreach(s -> {
                        listAdapter.addSpot(s);
                    });

            spots.filter(SpotFilters.oneOf(prev))
                    .foreach(s -> {
                        listAdapter.replaceSpot(s);
                    });

            prev = spots;
            listAdapter.notifyDataSetChanged();
            updateHeader(listAdapter.getCount());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Nearby Spots");

        Tamra.addObserver(tamraObserver);

        orderByIdButton.setOnClickListener(v -> listAdapter.setOrdering(BeaconOrdering.ById));
        orderByProximityButton.setOnClickListener(v -> listAdapter.setOrdering(BeaconOrdering.ByProximity));
        orderByRangedAtButton.setOnClickListener(v -> listAdapter.setOrdering(BeaconOrdering.ByRangedAt));

        listAdapter = new SpotListAdapter(this);
        listView.setAdapter(listAdapter);

        updateHeader(0);
    }

    private void findingViews() {
        setContentView(R.layout.activity_nearby_spots);

        LinearLayout header = (LinearLayout) findViewById(R.id.header);
        assert header != null;
        headerTextView = (TextView) header.findViewById(R.id.center);
        listView = (ListView) findViewById(R.id.listView);
        orderByIdButton = (RadioButton) findViewById(R.id.btnOrderById);
        orderByProximityButton = (RadioButton) findViewById(R.id.btnOrderByProximity);
        orderByRangedAtButton = (RadioButton) findViewById(R.id.btnOrderByRangedAt);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Tamra.removeObserver(tamraObserver);
    }

    private void updateHeader(int count) {
        headerTextView.setText(Utils.countingHeader(count));
    }
}
