package com.kakao.oreum.oreumzigi.errors;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.supports.Utils;
import com.kakao.oreum.tamra.error.TamraError;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class ErrorDetailActivity extends AppCompatActivity {

    private TextView exceptionName;
    private TextView exceptionMessage;
    private TextView occurredAt;
    private TextView stackTrace;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findingViews();

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setSubtitle("Errors > Error Detail");

        readIntentExtra().ifPresent(this::setTamraError);
    }

    private void findingViews() {
        setContentView(R.layout.activity_error_detail);

        exceptionName = (TextView) findViewById(R.id.exceptionName);
        exceptionMessage = (TextView) findViewById(R.id.exceptionMessage);
        occurredAt = (TextView) findViewById(R.id.occurredAt);
        stackTrace = (TextView) findViewById(R.id.stackTrace);
    }

    private Optional<TamraError> readIntentExtra() {
        Intent intent = getIntent();
        Exception exception = (Exception) intent.getSerializableExtra("exception");
        Date occurredAt = (Date) intent.getSerializableExtra("occurredAt");
        if (exception != null && occurredAt != null) {
            return Optional.of(new TamraError(exception, occurredAt));
        } else {
            return Optional.empty();
        }
    }

    private void setTamraError(TamraError tamraError) {
        exceptionName.setText(tamraError.causedBy().getClass().getName());
        exceptionMessage.setText(String.valueOf(tamraError.causedBy().getMessage()));
        occurredAt.setText(Utils.format(tamraError.occurredAt()));

        StringBuilder builder = new StringBuilder();
        for (StackTraceElement e : tamraError.causedBy().getStackTrace()) {
            builder.append(e.toString()).append("\n");
        }
        stackTrace.setMovementMethod(new ScrollingMovementMethod());
        stackTrace.setText(formatThrowable(tamraError.causedBy()));
    }


    private String formatThrowable(Throwable throwable) {
        StringBuilder sb = new StringBuilder();
        sb.append(throwable.getClass().getName()).append(": ")
                .append(throwable.getMessage())
                .append("\n");

        for (StackTraceElement ste : throwable.getStackTrace()) {
            sb.append("   at ").append(ste).append("\n");
        }

        if (throwable.getCause() != null) {
            sb.append("Caused by: ").append(formatThrowable(throwable.getCause()));
        }

        return sb.toString();
    }

}
