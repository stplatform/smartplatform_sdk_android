package com.kakao.oreum.oreumzigi.beaconranging;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.oreumzigi.R;
import com.kakao.oreum.oreumzigi.customviews.ProximityView;
import com.kakao.oreum.oreumzigi.supports.Utils;

import java.util.Date;
import java.util.Locale;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconView extends LinearLayout {

    private TextView uuidView;
    private TextView majorView;
    private TextView minorView;
    private ProximityView proximityView;
    private TextView accuracyView;
    private TextView rssiView;
    private TextView txPowerView;
    private TextView rangedAtView;

    public BeaconView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.item_ranged_beacon, this, true);

        uuidView = (TextView) findViewById(R.id.uuid);
        majorView = (TextView) findViewById(R.id.major);
        minorView = (TextView) findViewById(R.id.minor);
        proximityView = (ProximityView) findViewById(R.id.proximity);
        accuracyView = (TextView) findViewById(R.id.accuracy);
        rssiView = (TextView) findViewById(R.id.rssi);
        txPowerView = (TextView) findViewById(R.id.txPower);
        rangedAtView = (TextView) findViewById(R.id.rangedAt);
    }

    public void setData(Beacon beacon, Date rangedAt) {
        uuidView.setText(beacon.proximityUUID().toString());
        majorView.setText(String.valueOf(beacon.major()));
        minorView.setText(String.valueOf(beacon.minor()));
        proximityView.setProximity(beacon.proximity());
        accuracyView.setText(String.format(Locale.KOREA, "%.3f", beacon.accuracy()));
        rssiView.setText(String.valueOf(beacon.rssi()));
        txPowerView.setText(String.valueOf(beacon.txPower()));
        rangedAtView.setText(Utils.format(rangedAt));
    }

}
