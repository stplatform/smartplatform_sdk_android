# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/oddpoet/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep public class com.kakao.oreum.tamra.*
-keep public class com.kakao.oreum.tamra.base.*
-keep public class com.kakao.oreum.tamra.error.*
-keep public class com.kakao.oreum.common.**


-keepclassmembers class com.kakao.oreum.tamra.* { public protected *;}
-keepclassmembers class com.kakao.oreum.tamra.base.* { public protected *;}
-keepclassmembers class com.kakao.oreum.tamra.error.* { public protected *;}
-keepclassmembers class com.kakao.oreum.common.** { public protected *;}


# keep generictype parameter
-keepattributes Signature, InnerClasses, Exceptions

-dontwarn sun.misc.Unsafe
-dontwarn com.google.common.collect.MinMaxPriorityQueue

# retrolambda
-dontwarn java.lang.invoke.*