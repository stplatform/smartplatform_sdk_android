package com.kakao.oreum.tamra.internal.data.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public abstract class JsonDataParser<T> {
    protected abstract T map(JSONObject jsonObject) throws JSONException;

    public List<? extends T> parseArray(String jsonArray) throws JSONException {
        List<T> list = new ArrayList<>();
        JSONArray array = new JSONArray(jsonArray);

        for (int i = 0; i < array.length(); i++) {
            list.add(map(array.getJSONObject(i)));
        }
        return list;
    }

    public T parseObject(String jsonObject) throws JSONException {
        JSONObject obj = new JSONObject(jsonObject);
        return map(obj);
    }
}
