package com.kakao.oreum.tamra.internal.history;

import com.kakao.oreum.tamra.base.Spot;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface HistoryLogger {
    void log(Spot spot);
}
