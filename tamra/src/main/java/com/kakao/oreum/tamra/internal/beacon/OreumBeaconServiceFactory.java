package com.kakao.oreum.tamra.internal.beacon;

import android.content.Context;
import android.location.LocationManager;

import com.kakao.oreum.infra.beacon.altbeacon.BeaconServiceFactory;
import com.kakao.oreum.infra.beacon.simulator.SimulatorFactory;
import com.kakao.oreum.infra.geofence.GeofenceServiceFactory;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLogger;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLoggerSyncFactory;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.tamra.base.ObserverSensitivity;
import com.kakao.oreum.tamra.internal.data.BeaconRepositoryFactory;
import com.kakao.oreum.tamra.internal.data.RegionRepositoryFactory;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class OreumBeaconServiceFactory {
    private OreumBeaconServiceFactory() { /* DO NOT CREATE INSTANCE */ }

    /**
     * BeaconService 생성
     *
     * @param config
     * @param httpClient
     * @param sdkLogger
     * @return
     */
    public static AbstractBeaconService create(Config config, OxHttpClient httpClient, SDKLogger sdkLogger) {

        AbstractBeaconService beaconService;
        if (config.automaticMonitoring()) {
            beaconService = createForAutomatic(config.context(),
                    httpClient,
                    config.observerSensitivity(),
                    sdkLogger,
                    config.simulation(),
                    config.deviceName());

            beaconService.startMonitoring();
        } else {
            beaconService = create(config.context(),
                    httpClient,
                    config.observerSensitivity(),
                    sdkLogger,
                    config.simulation(),
                    config.deviceName());
        }

        return beaconService;
    }

    /**
     * 수동 모니터링 기능의 BeaconService
     *
     * @param context
     * @param httpClient
     * @param observerSensitivity Beacon 반응 감도
     * @param onSimulation Location Simulation Mode 여부
     * @param deviceName Location Simulation Mode 활성화 시 기기명 지정
     * @return
     * @since 0.5.0
     */
    private static OreumBeaconService create(Context context,
                                             OxHttpClient httpClient,
                                             ObserverSensitivity observerSensitivity,
                                             SDKLogger sdkLogger,
                                             boolean onSimulation,
                                             String deviceName) {
        return new OreumBeaconService(
                onSimulation ? SimulatorFactory.create(httpClient, deviceName) : BeaconServiceFactory.create(context),
                RegionRepositoryFactory.create(context, httpClient, true),
                BeaconRepositoryFactory.create(context, httpClient, true),
                getRangedBeaconStabilizer(observerSensitivity),
                sdkLogger
        );
    }

    /**
     * 자동 모니터링 기능의 BeaconService
     *
     * @param context
     * @param httpClient
     * @param observerSensitivity
     * @param onSimulation Location Simulation Mode 여부
     * @param deviceName Location Simulation Mode 활성화 시 기기명 지정
     * @return
     * @since 0.11.0
     */
    private static AutomaticBeaconService createForAutomatic(Context context,
                                                             OxHttpClient httpClient,
                                                             ObserverSensitivity observerSensitivity,
                                                             SDKLogger sdkLogger,
                                                             boolean onSimulation,
                                                             String deviceName) {
        return new AutomaticBeaconService(
                onSimulation ? SimulatorFactory.create(httpClient, deviceName) : BeaconServiceFactory.create(context),
                GeofenceServiceFactory.create(context),
                (LocationManager) context.getSystemService(Context.LOCATION_SERVICE),
                RegionRepositoryFactory.create(context, httpClient, true),
                BeaconRepositoryFactory.create(context, httpClient, true),
                getRangedBeaconStabilizer(observerSensitivity),
                sdkLogger
        );
    }

    private static RangedBeaconStabilizer getRangedBeaconStabilizer(ObserverSensitivity observerSensitivity) {
        RangedBeaconStabilizer rangedBeaconStabilizer;
        switch (observerSensitivity) {
            case Responsive:
                rangedBeaconStabilizer = new TTLAvgRangedBeaconStabilizer(new SystemTimeService(), 2000, 1);
                break;
            case Accurate:
                rangedBeaconStabilizer = new TTLAvgRangedBeaconStabilizer(new SystemTimeService(), 7000, 4);
                break;
            case Balanced:
            default:
                rangedBeaconStabilizer = new TTLAvgRangedBeaconStabilizer();
                break;
        }
        return rangedBeaconStabilizer;
    }

    /**
     * 서버와 연동된 Simulator 모드의 BeaconService
     *
     * @param httpClient
     * @param deviceName Location Simulation 도구에 표시하고 싶은 기기명
     * @since 0.9.0
     * @return Simulator 인터페이스의 BeaconService
     * @deprecated create 로 통합
     */
    @Deprecated
    public static OreumBeaconService createForSimulation(Context context, OxHttpClient httpClient, String deviceName) {
        return new OreumBeaconService(
                SimulatorFactory.create(httpClient, deviceName),
                RegionRepositoryFactory.create(context, httpClient, true),
                BeaconRepositoryFactory.create(context, httpClient, true),
                new TTLAvgRangedBeaconStabilizer(),
                SDKLoggerSyncFactory.getDummySDKLogger()
        );
    }

}