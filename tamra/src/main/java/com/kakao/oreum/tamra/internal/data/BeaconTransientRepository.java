package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * BeaconData를 위한 Repository.
 *
 * 오류시 빈 Collection이나 빈 Optional을 리턴하며 외부로 예외를 던지지 않는다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconTransientRepository extends ApiConnectedBeaconRepository {
    private static final Logger LOG = getLogger(BeaconTransientRepository.class);
    private final List<BeaconData> prefetched = new ArrayList<>();

    public BeaconTransientRepository(OxHttpClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<BeaconData> find(long id) {
        fetchAll();
        return OxIterable.from(prefetched)
                .find(b -> b.id() == id);
    }

    @Override
    public Collection<BeaconData> findBy(Region region) {
        fetchAll();
        return OxIterable.from(prefetched)
                .filter(b -> b.regionId() == region.id())
                .toList();
    }

    @Override
    public Optional<BeaconData> findBy(Beacon beacon) {
        fetchAll();
        return OxIterable.from(prefetched)
                .find(b -> b.uuid().equals(beacon.proximityUUID())
                        && b.major() == beacon.major()
                        && b.minor() == beacon.minor());
    }

    // 일단 전체 로딩하자
    public Collection<BeaconData> all() {
        fetchAll();
        return prefetched;
    }

    private void fetchAll() {
        if (!prefetched.isEmpty()) {
            return;
        }
        ExceptionFree.exec(() -> {
            String json = getJsonForAllBeacons();
            prefetched.addAll(parser.parseArray(json));
            LOG.debug("Successfully fetch Beacon data");
        }, OxErrors.wrapAndNotify(e ->
                new TamraException("Fail to fetch beacon data", e)
        ));
    }

}
