package com.kakao.oreum.tamra.error;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * Tamra 초기화 예외.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
@Accessibility.Public
public class TamraNotInitializedException extends TamraException {
}
