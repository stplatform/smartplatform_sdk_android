package com.kakao.oreum.tamra.internal.data.data;

import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.tamra.base.Proximity;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class HistoryData {
    private final long spotId;
    private final String description;
    private final Proximity proximity;
    private final Double accuracy;
    private final Double latitude;
    private final Double longitude;
    private final Date issuedAt;

    private HistoryData(long spotId,
                        String description,
                        Proximity proximity,
                        Double accuracy,
                        Double latitude,
                        Double longitude,
                        Date issuedAt) {
        this.spotId = spotId;
        this.description = description;
        this.proximity = proximity;
        this.accuracy = accuracy;
        this.latitude = latitude;
        this.longitude = longitude;
        this.issuedAt = issuedAt;
    }

    public long spotId() {
        return spotId;
    }

    public String description() {
        return description;
    }

    public Proximity proximity() {
        return proximity;
    }

    public double accuracy() {
        return accuracy;
    }

    public double latitude() {
        return latitude;
    }

    public double longitude() {
        return longitude;
    }

    public Date issuedAt() {
        return issuedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof HistoryData))
            return false;

        HistoryData that = (HistoryData) o;

        return spotId == that.spotId && issuedAt.equals(that.issuedAt());
    }

    @Override
    public int hashCode() {
        return (int) ((long)super.hashCode() ^ ((long)super.hashCode() >>> 32));
    }

    @Override
    public String toString() {
        return "HistoryData{" +
                "spotId=" + spotId +
                ", description='" + description + "'" +
                ", proximity=" + proximity.name() +
                ", accuracy=" + accuracy +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", issuedAt='" + issuedAt + "'" +
                "}";
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long spotId;
        private String description;
        private Proximity proximity;
        private Double accuracy;
        private Double latitude;
        private Double longitude;
        private Date issuedAt;

        private Builder() {}

        public Builder spotId(long spodId) {
            this.spotId = spodId;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder proximity(Proximity proximity) {
            this.proximity = proximity;
            return this;
        }

        public Builder accuracy(double accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public Builder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder issuedAt(Date issuedAt) {
            this.issuedAt = issuedAt;
            return this;
        }

        public HistoryData build() {
            Asserts.notNull(spotId);
            Asserts.notNull(proximity);
            Asserts.notNull(description);
            Asserts.notNull(issuedAt);
            return new HistoryData(spotId,
                    description,
                    proximity,
                    accuracy,
                    latitude,
                    longitude,
                    issuedAt);
        }
    }
}
