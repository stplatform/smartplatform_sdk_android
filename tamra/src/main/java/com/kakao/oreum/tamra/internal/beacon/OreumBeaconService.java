package com.kakao.oreum.tamra.internal.beacon;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.tamra.base.SDKLogParams;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLogger;
import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.ox.task.Period;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.error.TamraNotSupportedException;
import com.kakao.oreum.tamra.internal.data.BeaconRepository;
import com.kakao.oreum.tamra.internal.data.RegionRepository;
import com.kakao.oreum.tamra.internal.data.data.RegionUtil;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class OreumBeaconService extends AbstractBeaconService {
    private static final Logger LOG = getLogger(OreumBeaconService.class);

    public OreumBeaconService(BeaconService beaconService,
                              RegionRepository regionRepository,
                              BeaconRepository beaconRepository,
                              RangedBeaconStabilizer rangedBeaconStabilizer,
                              SDKLogger sdkLogger) {
        super(beaconService, regionRepository, beaconRepository, rangedBeaconStabilizer, sdkLogger);
        this.beaconService.addObserver(new AutoRangingBeaconObserver());

        // 주기적으로 수행
        OxTasks.execRepeatly(
                Period.milliseconds(500), // 500msec 주기
                ExceptionFree.executable(this::notifyNearbySpots));
    }

    /**
     * @since 0.11.0
     */
    @Override
    public void startMonitoring() {
        OxErrors.notifyAll(new TamraNotSupportedException("use startMonitoring(Region);"));
    }

    /**
     * @since 0.11.0
     */
    @Override
    public void stopMonitoring() {
        OxErrors.notifyAll(new TamraNotSupportedException("use stopMonitoring(Region);"));
    }

    @Override
    public void startMonitoring(Region region) {
        LOG.info("TRACE> OreumBeaconService#startMonitoring");
        OxTasks.runOnBackground(
                ExceptionFree.runnable(() -> regionRepository.find(region)
                        .map(RegionUtil::toBeaconRegion)
                        .ifPresent(beaconService::startMonitoringFor))
        );
        monitoringRegions.add(region);
        sdkLogger.log("startMonitoring", SDKLogParams.of("regionId", String.valueOf(region.id())));
    }

    @Override
    public void stopMonitoring(Region region) {
        OxTasks.runOnBackground(
                ExceptionFree.runnable(() -> regionRepository.find(region)
                        .map(RegionUtil::toBeaconRegion)
                        .ifPresent(beaconService::stopMonitoringFor)
                )
        );
        monitoringRegions.remove(region);
        sdkLogger.log("stopMonitoring", SDKLogParams.of("regionId", String.valueOf(region.id())));
    }

    @Override
    public Collection<Region> monitoringRegions() {
        return ImmutableSet.copyOf(monitoringRegions);
    }

    private class AutoRangingBeaconObserver implements BeaconServiceObserver {

        @Override
        public void didEnterRegion(BeaconRegion beaconRegion) {
            LOG.debug("didEnterRegion > {}", beaconRegion);
            ExceptionFree.exec(() -> {
                beaconRepository.findBy(RegionUtil.toRegion(beaconRegion));// prefetch
                OxTasks.runOnBackground(() -> beaconService.startRangingBeaconsIn(beaconRegion));
                // noti region enter.
                regionRepository.find(RegionUtil.toRegion(beaconRegion))
                        .ifPresent(r -> {
                            observer.didEnter(r.region());
                            sdkLogger.log("didEnterRegion", SDKLogParams.of("regionId", String.valueOf(r.id())));
                        });
            });
        }

        @Override
        public void didExitRegion(BeaconRegion beaconRegion) {
            LOG.debug("didExitRegion > {}", beaconRegion);
            ExceptionFree.exec(() -> {
                OxTasks.runOnBackground(() -> beaconService.stopRangingBeaconsIn(beaconRegion));
                // noti region exit
                regionRepository.find(RegionUtil.toRegion(beaconRegion))
                        .ifPresent(r -> {
                            observer.didExit(r.region());
                            sdkLogger.log("didExitRegion", SDKLogParams.of("regionId", String.valueOf(r.id())));
                        });
            });

        }

        @Override
        public void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion) {
            LOG.debug("didRangeBeacons (Region) > {}", beaconRegion);
            LOG.debug("didRangeBeacons (Beacons) > {}", beacons);
            // add to stabilizer.
            ExceptionFree.exec(() -> {
                Date now = timeService.now();
                rangedBeaconStabilizer.ranged(
                        OxIterable.from(beacons)
                                .map(beacon -> new RangedBeacon(beaconRegion, beacon, now))
                                .toList());
            });
        }
    }
}
