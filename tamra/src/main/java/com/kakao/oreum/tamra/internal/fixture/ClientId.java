package com.kakao.oreum.tamra.internal.fixture;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class ClientId {
    private final static String PLATFORM_ID = "android";
    private final String value;
    private final DeviceUUIDHolder uuidHolder;

    private ClientId(Context context) {
        this.uuidHolder = new DeviceUUIDHolder(context);
        this.value = PLATFORM_ID + "/" + context.getPackageName() + "/" + uuidHolder.value;
    }

    public static String get(Context context) {
        return new ClientId(context).value;
    }

    private static class DeviceUUIDHolder {
        private final static String KEY = "DEVICE.UUID";
        private final UUID value;

        private DeviceUUIDHolder(Context context) {
            value = getUUID(context);
        }

        private UUID getUUID(Context context) {
            SharedPreferences preferences = TamraPreferences.get(context);
            if (!preferences.contains(KEY)) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(KEY, UUID.randomUUID().toString());
                editor.apply();
            }
            String uuidAsString = preferences.getString(KEY, null);
            return UUID.fromString(uuidAsString);
        }
    }

}
