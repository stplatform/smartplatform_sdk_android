package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * {@link TamraObserver}에 대한 adaptor역할을 하는 abstract class.
 *
 * <p>
 * {@link TamraObserver}에서 특정 이벤트만 관심이 있는 경우 이 클래스를 상속받아서 해당 이벤트에 대한 메소드만 override하여 사용하라.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public abstract class TamraObserverAdapter implements TamraObserver {
    /**
     * {@inheritDoc}
     */
    @Override
    public void didEnter(Region region) { /* optional implement */ }

    /**
     * {@inheritDoc}
     */
    @Override
    public void didExit(Region region) { /* optional implement */ }

    /**
     * {@inheritDoc}
     */
    @Override
    public void ranged(NearbySpots spots) { /* optional implement */ }
}
