package com.kakao.oreum.tamra.error;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class TamraNotSupportedException extends TamraException {
    public TamraNotSupportedException(String message) { super(message); }
}
