package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;

import java.util.Collection;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public interface BeaconRepository {
    Optional<BeaconData> find(long id);

    Collection<BeaconData> findBy(Region region);

    Optional<BeaconData> findBy(Beacon beacon);

    Optional<String> getSpotData(long id);
}
