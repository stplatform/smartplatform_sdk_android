package com.kakao.oreum.tamra.internal.beacon;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.infra.geofence.CircularRegion;
import com.kakao.oreum.infra.geofence.GeofenceObserver;
import com.kakao.oreum.infra.geofence.GeofenceService;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.collection.Sets;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.SDKLogParams;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.error.TamraNotSupportedException;
import com.kakao.oreum.tamra.internal.data.BeaconRepository;
import com.kakao.oreum.tamra.internal.data.RegionRepository;
import com.kakao.oreum.tamra.internal.data.data.RegionData;
import com.kakao.oreum.tamra.internal.data.data.RegionUtil;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLogger;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@Accessibility.Private
public class AutomaticBeaconService extends AbstractBeaconService {
    private static final Logger LOG = getLogger(AutomaticBeaconService.class);
    private final GeofenceService geofenceService;
    private final LocationManager locationManager;

    private Set<BeaconRegion> currentIssuedBeaconRegion = Sets.hashSet();
    private Set<CircularRegion> currentIssuedCircularRegion = Sets.hashSet();

    private final Region metaRegion = Region.of(1);
    private BeaconRegion metaBeaconRegion;
    private volatile boolean listenLocation = false;

    AutomaticBeaconService(BeaconService beaconService,
                           GeofenceService geofenceService,
                           LocationManager locationManager,
                           RegionRepository regionRepository,
                           BeaconRepository beaconRepository,
                           RangedBeaconStabilizer rangedBeaconStabilizer,
                           SDKLogger sdkLogger
    ) {
        super(beaconService, regionRepository, beaconRepository, rangedBeaconStabilizer, sdkLogger);
        this.geofenceService = geofenceService;
        this.locationManager = locationManager;

        beaconService.addObserver(new AutomaticBeaconObserver());
        geofenceService.addObserver(new AutomaticGeofenceObserver());
    }

    @Override
    public void startMonitoring() {
        LOG.info("AutomaticBeaconService#startMonitoring");
        OxTasks.runOnBeacon(() -> {
            if (metaBeaconRegion == null) {
                regionRepository.find(metaRegion)
                        .map(RegionUtil::toBeaconRegion)
                        .ifPresent(beaconRegion -> {
                            metaBeaconRegion = beaconRegion;
                            beaconService.startMonitoringFor(metaBeaconRegion);
                            // 외부에서 미리 체크한 것으로 간주한다.

                            // 위치 기반 지역 진입 체크 시작
                            startMonitoringLocation();
                        });
            }
        });
        sdkLogger.log("startMonitoring", SDKLogParams.of("regionId", String.valueOf(metaRegion.id())));
    }

    @Override
    public void stopMonitoring() {
        LOG.debug("AutomaticBeaconService#stopMonitoring");
        OxTasks.runOnBeacon(() -> {
            beaconService.stopMonitoringFor(metaBeaconRegion);
            OxIterable.from(currentIssuedBeaconRegion).doEach(beaconService::stopRangingBeaconsIn);
            OxIterable.from(currentIssuedCircularRegion).doEach(geofenceService::stopMonitoring);
        });

        // 위치 기반 지역 진입 체크 중지
        stopMonitoringLocation();
    }

    @Override
    public void startMonitoring(Region region) {
        OxErrors.notifyAll(new TamraNotSupportedException("use startMonitoring();"));
    }

    @Override
    public void stopMonitoring(Region region) {
        OxErrors.notifyAll(new TamraNotSupportedException("use stopMonitoring();"));
    }

    @Override
    public Collection<Region> monitoringRegions() {
        return Sets.hashSet(metaRegion);
    }

    private class AutomaticBeaconObserver implements BeaconServiceObserver {

        @Override
        public void didEnterRegion(BeaconRegion beaconRegion) {
            LOG.debug("didEnterRegion > {}", beaconRegion);
            ExceptionFree.exec(() -> {
                Region region = RegionUtil.toRegion(beaconRegion);
                regionRepository.find(region)
                        .ifPresent(r -> {
                            // Meta 지역에 진입한 경우
                            // 해당 지역을 인식하기 위해 Meta 비콘에 대한 Ranging을 시작한다.
                            if (isMetaRegion(r.region())) {
                                beaconService.startRangingBeaconsIn(beaconRegion);

                                stopMonitoringLocation();
                            } else {
                                // 일반 Region에 진입한 경우
                                // 이미 진입되어 있는지 확인하고
                                if (!currentIssuedBeaconRegion.contains(beaconRegion)) {
                                    // 외부에 Region 진입 이벤트를 날리고
                                    observer.didEnter(region);
                                    // 해당 Region에 속한 비콘에 대한 Ranging을 시작한다.
                                    beaconService.startRangingBeaconsIn(beaconRegion);

                                    // 해당 Region 이탈은 Geofence를 이용하기 위해서 Geofence 모니터링을 요청한다.
                                    CircularRegion circularRegion = createCircularRegion(r);
                                    geofenceService.startMonitoring(circularRegion);

                                    // 모니터링 되고 있는 Region을 관리하여 중복 요청이 되지 않도록 한다.
                                    currentIssuedBeaconRegion.add(beaconRegion);
                                    currentIssuedCircularRegion.add(circularRegion);
                                }
                            }
                        });
            });
        }

        @Override
        public void didExitRegion(BeaconRegion beaconRegion) {
            LOG.debug("didExitRegion > {}", beaconRegion);
            // Beacon의 개개별
        }

        @Override
        public void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion) {
            ExceptionFree.exec(() -> {
                // Meta Region에서 인식된 경우
                // 1. meta region의 ranging을 끄고
                // 2. 인식된 meta 비콘에 속한 Region들에 대해서 모니터링을 시작함
                if (isMetaRegion(RegionUtil.toRegion(beaconRegion))) {
                    beaconService.stopRangingBeaconsIn(beaconRegion);
                    Set<BeaconRegion> beaconRegions = OxIterable.from(beacons)
                            .map(beaconRepository::findBy)
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .map(beaconData -> regionRepository.find(beaconData.regionId()))
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .map(RegionUtil::toBeaconRegion)
                            .reduce((set, region) -> { set.add(region); return set; }, new HashSet<BeaconRegion>());

                    OxIterable.from(beaconRegions).doEach(beaconService::startMonitoringFor);
                } else {
                    // add to stabilizer.
                    Date now = timeService.now();
                    rangedBeaconStabilizer.ranged(
                            OxIterable.from(beacons)
                                    .map(beacon -> new RangedBeacon(beaconRegion, beacon, now))
                                    .toList());

                    notifyNearbySpots();
                }
            });
        }
    }

    /**
     * 위치 모니터링 요청
     */
    private void startMonitoringLocation() {
        LOG.info("startMonitoringLocation");
        if (!listenLocation) {
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000 /* 10 seconds */ , 100 /* meters */, automaticLocationListener);
                listenLocation = true;
            } catch (SecurityException ex) {
                LOG.error("Can't get Location");
                OxErrors.wrapAndNotify(TamraException::new);
            }
        }
    }

    /**
     * 위치 모니터링 중단
     */
    private void stopMonitoringLocation() {
        LOG.info("stopMonitoringLocation");
        if (listenLocation) {
            try {
                locationManager.removeUpdates(automaticLocationListener);
                listenLocation = false;
            } catch (SecurityException ex) {
                LOG.error("Can't remove Location Listener");
                OxErrors.wrapAndNotify(TamraException::new);
            }
        }
    }

    private class AutomaticGeofenceObserver implements GeofenceObserver {
        @Override
        public void didEnterRegion(CircularRegion region) {
            // Meta가 아닌 BeaconRegion에 인입한 경우, 관련 Geofence가 켜진후 해당 지역의 Range 시작
            convertCircularToBeacon(region).ifPresent(beaconRegion -> {
                // beacon region monitoring을 끄고
                beaconService.stopMonitoringFor(beaconRegion);
                // beacon region의 ranging 시작
                beaconService.startRangingBeaconsIn(beaconRegion);
                // 위치 기반 지역 진입 중지
                stopMonitoringLocation();
            });
        }

        @Override
        public void didExitRegion(CircularRegion region) {
            // 해당 region의 beacon ranging 을 종료
            convertCircularToBeacon(region).ifPresent(beaconRegion -> {
                // Geofence 영역을 벗어났을때 exit 이벤트를 발생시킨다
                observer.didExit(RegionUtil.toRegion(beaconRegion));
                beaconService.stopRangingBeaconsIn(beaconRegion);
                beaconService.stopMonitoringFor(beaconRegion);
                currentIssuedBeaconRegion.remove(beaconRegion);
            });

            // 해당 region의 geofence monitoring을 종료
            geofenceService.stopMonitoring(region);
            currentIssuedCircularRegion.remove(region);

            // 아무 Region에도 진입하지 않았을 경우 메타 및 위치 기반 지역 모니터링 시작
            if (currentIssuedBeaconRegion.isEmpty()) {
                beaconService.startMonitoringFor(metaBeaconRegion);
                startMonitoringLocation();
            }
        }
    }

    private LocationListener automaticLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // 현재 위치와 Region의 중심 위치와의 거리 비교 후 Region에 속하면 start

            OxIterable.from(regionRepository.all()).doEach(regionData -> {
                Location regionLocation = new Location(regionData.name());

                regionLocation.setLatitude(regionData.latitude());
                regionLocation.setLongitude(regionData.longitude());

                if (location.distanceTo(regionLocation) <= regionData.radius()) {
                    LOG.info("Entered region by Location => {}", regionData);
                    beaconService.startMonitoringFor(RegionUtil.toBeaconRegion(regionData));

                    // 위치 기반 지역 진입 중지
                    stopMonitoringLocation();
                }
            });
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        // 사용자가 GPS 켰을때
        public void onProviderEnabled(String provider) {
            startMonitoringLocation();
        }

        // 사용자가 GPS 껐을때
        public void onProviderDisabled(String provider) {
            stopMonitoringLocation();
        }
    };

    private boolean isMetaRegion(Region region) {
        return region.equals(metaRegion);
    }

    private CircularRegion createCircularRegion(RegionData regionData) {
        return CircularRegion.builder()
                .id(regionData.id())
                .latitude(regionData.latitude())
                .longitude(regionData.longitude())
                .radius(regionData.radius())
                .build()
                ;
    }

    private Optional<BeaconRegion> convertCircularToBeacon(CircularRegion region) {
        return regionRepository.find(region.id())
                .flatMap(regionData -> Optional.of(RegionUtil.toBeaconRegion(regionData)));
    }
}
