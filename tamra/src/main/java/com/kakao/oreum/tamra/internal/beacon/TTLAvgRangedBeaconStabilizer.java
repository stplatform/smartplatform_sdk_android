package com.kakao.oreum.tamra.internal.beacon;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconFactory;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.OxIterable;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

/**
 * TTL 내에 특정 횟수 이상 range된 비콘들의 avg값을 제공하는 stabilizer.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class TTLAvgRangedBeaconStabilizer implements RangedBeaconStabilizer {
    private final static long DEFAULT_TTL_MSEC = 5000;
    private final static int DEFAULT_MIN_RANGE_COUNT = 3;

    private final LinkedList<RangedBeacon> beacons = new LinkedList<>();
    private final TimeService timeService;
    private final long ttlMsec;
    private final int minRangeCount;

    public TTLAvgRangedBeaconStabilizer(TimeService timeService, long ttlMsec, int minRangeCount) {
        this.timeService = timeService;
        this.ttlMsec = ttlMsec;
        this.minRangeCount = minRangeCount;
    }

    public TTLAvgRangedBeaconStabilizer() {
        this(new SystemTimeService(), DEFAULT_TTL_MSEC, DEFAULT_MIN_RANGE_COUNT);
    }

    @Override
    public void ranged(Collection<RangedBeacon> rangedBeacons) {
        synchronized (beacons) {
            beacons.addAll(rangedBeacons);
            removeExpired();
        }
    }

    private void removeExpired() {
        Date baseTime = new Date(timeService.now().getTime() - ttlMsec);
        beacons.removeAll(OxIterable.from(beacons)
                .filter(b -> b.rangedAt().before(baseTime))
                .toList());
    }

    @Override
    public Collection<RangedBeacon> stabledBeacons() {
        synchronized (beacons) {
            removeExpired();
            Map<Beacon, Collection<RangedBeacon>> groupByBeacon = OxIterable.from(beacons)
                    .index(RangedBeacon::beacon);
            return OxIterable.from(groupByBeacon.values())
                    .filter(c -> c.size() >= minRangeCount)
                    .map(this::averageMeasurement)
                    .toList();
        }
    }

    private RangedBeacon averageMeasurement(Collection<RangedBeacon> beacons) {
        if (beacons == null || beacons.isEmpty()) {
            return null;
        }
        int count = beacons.size();
        int avgTxPower = OxIterable.from(beacons)
                .map(b -> b.beacon())
                .reduce((s, b) -> s + b.txPower(), 0) / count;
        int avgRssi = OxIterable.from(beacons)
                .map(b -> b.beacon())
                .reduce((s, b) -> s + b.rssi(), 0) / count;
        Date firstRangedAt = OxIterable.from(beacons)
                .map(b -> b.rangedAt())
                .reduce((s, t) -> s == null ? t : (t.before(s) ? t : s), null);
        return OxIterable.from(beacons)
                .first()
                .map(base -> new RangedBeacon(
                        base.region(),
                        BeaconFactory.getDefault()
                                .create(base.beacon().proximityUUID(),
                                        base.beacon().major(),
                                        base.beacon().minor(),
                                        avgTxPower,
                                        avgRssi),
                        firstRangedAt)
                ).orElse(null);
    }
}
