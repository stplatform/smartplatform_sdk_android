package com.kakao.oreum.tamra.internal.http;

import com.kakao.oreum.common.function.Supplier;
import com.kakao.oreum.ox.http.Header;
import com.kakao.oreum.ox.http.HttpOpts;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.internal.http.auth.AuthHeaderSupplier;
import com.kakao.oreum.tamra.internal.http.common.CommonHeaders;
import com.kakao.oreum.tamra.internal.props.PropName;
import com.kakao.oreum.tamra.internal.props.TamraProperties;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class HttpClientFactory {

    private HttpClientFactory() {
    }

    public static OxHttpClient create(Config config) throws MalformedURLException {
        URL baseURL = getBaseURL(config);
        return OxHttpClient.forBaseUrl(baseURL)
                .defaultHeaders(CommonHeaders.get(config.context()))
                .defaultHeader(authTokenHeader(config))
                .defaultOpts(HttpOpts.create()
                        .connectTimeout(5000)
                        .readTimeout(5000)
                        .followRedirects(true) // api url 변경시 3XX 응답으로 대응할 수 있게 on 시켜놓는다
                );
    }

    private static Supplier<Header> authTokenHeader(Config config) throws MalformedURLException {
        return new AuthHeaderSupplier(
                getAuthenticationURL(config),
                config.appId(),
                config.appkey(),
                CommonHeaders.get(config.context())
        );
    }

    private static URL getBaseURL(Config config) throws MalformedURLException {
        TamraProperties props = TamraProperties.load(config.profile());
        String baseUrl = props.get(PropName.API_BASEURL).get();
        return new URL(baseUrl);
    }

    private static URL getAuthenticationURL(Config config) throws MalformedURLException {
        TamraProperties props = TamraProperties.load(config.profile());
        String baseUrl = props.get(PropName.API_BASEURL).get();
        return new URL(baseUrl.replaceAll("/$", "") + "/auth/authenticate");
    }
}
