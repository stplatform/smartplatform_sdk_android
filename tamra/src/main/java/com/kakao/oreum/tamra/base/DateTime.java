package com.kakao.oreum.tamra.base;

import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class DateTime {
    private Date date;
    private TimeService timeService = new SystemTimeService();

    public DateTime() {
        date = timeService.now();
    }

    public DateTime(Date date) {
        this.date = (Date) date.clone();
    }

    public Date getTime() {
        return date;
    }

    public DateTime agoInMilliseconds(int amount) {
        date = timeService.addMilliseconds(date, -amount);
        return this;
    }

    public DateTime agoInSeconds(int amount) {
        return agoInMilliseconds(amount * 1000);
    }

    public DateTime agoInMinutes(int amount) {
        return agoInSeconds(amount * 60);
    }

    public DateTime agoInHours(int amount) {
        return agoInMinutes(amount * 60);
    }

    public DateTime agoInDays(int amount) {
        return agoInHours(amount * 24);
    }

    public DateTime agoInMonths(int amount) {
        date = timeService.addMonth(date, -amount);
        return this;
    }

    public DateTime agoInYears(int amount) {
        date = timeService.addYear(date, -amount);
        return this;
    }
}
