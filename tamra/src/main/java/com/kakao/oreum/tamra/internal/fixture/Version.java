package com.kakao.oreum.tamra.internal.fixture;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.helper.ClasspathFiles;

import java.io.IOException;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Version {
    private static final Logger LOG = getLogger(Version.class);
    private static final String value = readVersion();

    private Version() {
        /* DO NOT CREATE INSTANCE */
    }

    public static String asString() {
        return value;
    }

    private static String readVersion() {
        try {
            return ClasspathFiles.read("tamra/VERSION");
        } catch (IOException e) {
            LOG.error("Cannot read 'VERSION' info.", e);
            return "0.0.0";
        }
    }
}
