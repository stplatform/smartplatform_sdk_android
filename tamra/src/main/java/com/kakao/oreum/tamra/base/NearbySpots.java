package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 특정 시점에 감지된 주변의 spot 목록.
 *
 * <p>
 * {@link Spot}에 대한 {@link Iterable} 구현이며 functional style coding을 위해 설계되었다.
 * </p>
 *
 * <pre><code>
 *     NearbySpots spots = Tamra.spots();
 *
 *     // as iterable.
 *     for (Spot spot : spots) {
 *         doWithSpot(spot);
 *     }
 *
 *     // filter and order
 *     List&lt;Spot&gt; indoor = spots.filter(SpotFilters.indoor(true))
 *          .orderBy(Spot.ACCURACY_ORDER)
 *          .limit(3)
 *          .toList();
 *
 *     // do for each
 *     spots.orderBy(Spot.ACCURACY_ORDER)
 *          .foreach(s-&gt; doWithSpot(s));
 * </code></pre>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public interface NearbySpots extends Iterable<Spot> {
    /**
     * 현재 정렬 기준으로 max 개수만큼 spot을 포함하는 nearbySpots.
     *
     * @param max 최대 개수.
     * @return nearbySpots.
     */
    NearbySpots limit(int max);

    /**
     * 특정 조건으로 spot을 filter.
     *
     * @param filter 필터 조건.
     * @return filter에 포함된 spot들만 포함하는 nearbySpots.
     */
    NearbySpots filter(Predicate<? super Spot> filter);

    /**
     * 모든 spot이 predicate에 만족하는지 검사.
     *
     * @param predicate 조건.
     * @return 모든 spot이 predicate을 만족하면 true, 아니면 false.
     */
    boolean all(Predicate<? super Spot> predicate);

    /**
     * 어떤 spot이라도 predicate에 만족하는지 검사.
     *
     * @param predicate 조건
     * @return spot 중 하나라도 predicate을 만족하면 true, 아니면 false.
     */
    boolean any(Predicate<? super Spot> predicate);

    /**
     * 현재 정렬 기준으로 첫 spot.
     *
     * @return
     */
    Optional<Spot> first();

    /**
     * 현재 정렬 기준으로 마지막 spot.
     *
     * @return
     */
    Optional<Spot> last();

    Optional<Spot> get(int position);

    /**
     * iterable을 정렬한다.
     *
     * @param comparator 정렬시 사용할 comparator.
     * @return comparator로 정렬된 nearbySpots.
     */
    NearbySpots orderBy(Comparator<? super Spot> comparator);

    /**
     * spot list.
     *
     * @return spot list.
     */
    List<Spot> toList();

    /**
     * set으로 변환.
     *
     * @return spot 집합.
     */
    Set<Spot> toSet();

    /**
     * spot을 index 기준으로 분류하는 map 생성.
     *
     * @param indexFunction index 함수
     * @param <T>           index 객체 클래스.
     * @return index를 key로, spot collection 을 value로 하는 map.
     */
    <T> Map<T, Collection<Spot>> index(Function<? super Spot, ? extends T> indexFunction);

    /**
     * {@link Spot} 객체를 key로 하는 map으로 변환한다.
     *
     * @param valueFunction value 함수.
     * @param <T>           value 객체 클래스.
     * @return spot을 key로하는 map 객체.
     */
    @Nonnull
    <T> Map<Spot, T> toMap(Function<? super Spot, ? extends T> valueFunction);

    /**
     * {@link Spot}을 다른 객체의 {@link Collection}으로 변환한다.
     *
     * @param mapFunction 변환 함수
     * @param <T>         변환결과 객체 클래스
     * @return 변환객체의 collection.
     */
    @Nonnull
    <T> Collection<T> transform(Function<? super Spot, ? extends T> mapFunction);

    /**
     * 각 {@link Spot}에 대해 작업을 수행한다.
     *
     * @param action 수행할 작업.
     */
    void foreach(Consumer<? super Spot> action);

    /**
     * spot 갯수.
     *
     * @return size
     */
    int size();

    /**
     * @return empty 여부
     */
    boolean isEmpty();

    /**
     * {@link Spot}이 감지된 시각을 제공한다.
     *
     * @return sensedAt.
     */
    Date sensedAt();

}
