package com.kakao.oreum.tamra.internal.data;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class RemoteApiException extends RuntimeException {
    public RemoteApiException(Throwable throwable) {
        super(throwable);
    }
}
