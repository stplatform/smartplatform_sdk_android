package com.kakao.oreum.tamra.internal.history;

import android.content.Context;

import com.kakao.oreum.infra.persistence.sqlite.HistoryDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class HistoryLoggerFactory {
    public static HistoryLogger logger;

    public static HistoryLogger getLogger(Context context) {
        if (logger == null) {
            SystemTimeService timeService = new SystemTimeService();
            logger = new HistoryLoggerImpl(
                    new HistoryDBRepository(
                            new TamraDBHelper(context), new ISO8601DateFormat()),
                    timeService
                    );
        }
        return logger;
    }
}
