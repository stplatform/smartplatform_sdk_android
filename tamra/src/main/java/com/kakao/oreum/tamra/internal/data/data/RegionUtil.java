package com.kakao.oreum.tamra.internal.data.data;

import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.tamra.base.Region;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class RegionUtil {
    private static final Pattern REGION_ID_PATTERN = Pattern.compile("Oreum#([0-9]+)/(.+)");

    private RegionUtil() { /* DO NOT CREATE INSTANCE */}

    public static BeaconRegion toBeaconRegion(RegionData regionData) {
        return BeaconRegion.initWith("Oreum#" + regionData.id() + "/" + regionData.name(), regionData.uuid());
    }

    public static Region toRegion(BeaconRegion beaconRegion) {
        String identifier = beaconRegion.identifier();
        Matcher matcher = REGION_ID_PATTERN.matcher(identifier);
        if (matcher.matches()) {
            long id = Long.parseLong(matcher.group(1));
            String name = String.valueOf(matcher.group(2));
            return Region.of(id, name);
        }
        return Region.UNKNOWN;
    }


}
