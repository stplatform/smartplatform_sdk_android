package com.kakao.oreum.tamra.internal.spots;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Spot;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * {@link NearbySpots}에 대한 구현 클래스.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public class NearbySpotsImpl implements NearbySpots {
    private static final TimeService timeService = new SystemTimeService();
    private final Iterable<Spot> spots;
    private final Date sensedAt;

    private NearbySpotsImpl(Iterable<Spot> spots, Date sensedAt) {
        Asserts.notNull(spots);
        Asserts.notNull(sensedAt);
        this.spots = spots;
        this.sensedAt = sensedAt;
    }

    public static NearbySpots copyOf(Iterable<Spot> spots) {
        return new NearbySpotsImpl(spots, timeService.now());
    }

    public static NearbySpots of(Spot... spots) {
        return copyOf(OxIterable.from(spots));
    }

    public static NearbySpots empty() {
        return of();
    }

    @Override
    public NearbySpots limit(int max) {
        return copyOf(OxIterable.from(spots).limit(max));
    }

    @Override
    public NearbySpots filter(Predicate<? super Spot> filter) {
        return copyOf(OxIterable.from(spots).filter(filter));
    }

    @Override
    public boolean all(Predicate<? super Spot> predicate) {
        return OxIterable.from(spots).all(predicate);
    }

    @Override
    public boolean any(Predicate<? super Spot> predicate) {
        return OxIterable.from(spots).any(predicate);
    }

    @Override
    public Optional<Spot> first() {
        return OxIterable.from(spots).first();
    }

    @Override
    public Optional<Spot> last() {
        return OxIterable.from(spots).last();
    }

    @Override
    public Optional<Spot> get(int position) {
        return OxIterable.from(spots).get(position);
    }

    @Override
    public NearbySpots orderBy(Comparator<? super Spot> comparator) {
        return copyOf(OxIterable.from(spots).sort(comparator));
    }

    @Override
    public List<Spot> toList() {
        return OxIterable.from(spots).toList();
    }

    @Override
    public Set<Spot> toSet() {
        return OxIterable.from(spots).toSet();
    }

    @Override
    public <T> Map<T, Collection<Spot>> index(Function<? super Spot, ? extends T> indexFunction) {
        return OxIterable.from(spots).index(indexFunction);
    }

    @Override
    public <T> Map<Spot, T> toMap(Function<? super Spot, ? extends T> valueFunction) {
        return OxIterable.from(spots).toMap(valueFunction);
    }

    @Override
    public <T> Collection<T> transform(Function<? super Spot, ? extends T> mapFunction) {
        return OxIterable.from(spots).map(mapFunction).toList();
    }

    @Override
    public void foreach(Consumer<? super Spot> action) {
        OxIterable.from(spots)
                .doEach(action);
    }

    @Override
    public int size() {
        return OxIterable.from(spots).size();
    }

    @Override
    public boolean isEmpty() {
        return !OxIterable.from(spots).first().isPresent();
    }

    @Override
    public Date sensedAt() {
        return sensedAt;
    }

    @Override
    public Iterator<Spot> iterator() {
        return spots.iterator();
    }
}
