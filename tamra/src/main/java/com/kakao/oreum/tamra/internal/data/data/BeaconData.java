package com.kakao.oreum.tamra.internal.data.data;

import com.kakao.oreum.ox.helper.Asserts;

import java.util.UUID;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconData {
    private final long id;
    private final long regionId;
    private final UUID uuid;
    private final int major;
    private final int minor;
    private final boolean indoor;
    private final boolean movable;
    private final int txPower;
    private final double latitude;
    private final double longitude;
    private final String description;
    private final String beaconType;
    private final String geohash;

    private BeaconData(long id,
                       long regionId,
                       UUID uuid,
                       int major,
                       int minor,
                       boolean indoor,
                       boolean movable,
                       int txPower,
                       double latitude,
                       double longitude,
                       String description,
                       String beaconType,
                       String geohash) {
        this.id = id;
        this.regionId = regionId;
        this.uuid = uuid;
        this.major = major;
        this.minor = minor;
        this.indoor = indoor;
        this.movable = movable;
        this.txPower = txPower;
        this.latitude = latitude;
        this.longitude = longitude;

        this.description = description;
        this.beaconType = beaconType;
        this.geohash = geohash;
    }

    public static Builder builder() {
        return new Builder();
    }


    public long id() {
        return id;
    }

    public long regionId() {
        return regionId;
    }

    public UUID uuid() {
        return uuid;
    }

    public int major() {
        return major;
    }

    public int minor() {
        return minor;
    }

    public boolean isIndoor() {
        return indoor;
    }

    public boolean isMovable() {
        return movable;
    }

    public int txPower() {
        return txPower;
    }

    public double latitude() {
        return latitude;
    }

    public double longitude() {
        return longitude;
    }

    public String description() {
        return description;
    }

    public String beaconType() {
        return beaconType;
    }

    public String geohash() {
        return geohash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BeaconData)) return false;

        BeaconData that = (BeaconData) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "BeaconData{" +
                "id=" + id +
                ", regionId=" + regionId +
                ", uuid='" + uuid + '\'' +
                ", major=" + major +
                ", minor=" + minor +
                ", indoor=" + indoor +
                ", movable=" + movable +
                ", txPower=" + txPower +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", description='" + description + '\'' +
                ", beaconType='" + beaconType + '\'' +
                ", geohash='" + geohash + '\'' +
                '}';
    }

    public boolean isMeta() {
        return "Meta".equalsIgnoreCase(beaconType);
    }

    public static class Builder {
        private Long id;
        private Long regionId;
        private String uuid;
        private Integer major;
        private Integer minor;
        private Boolean indoor = true;
        private Boolean movable = false;
        private Integer txPower = 0;
        private Double latitude = 0.0;
        private Double longitude = 0.0;
        private String description;
        private String beaconType;
        private String geohash;

        private Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder regionId(Long regionId) {
            this.regionId = regionId;
            return this;
        }

        public Builder uuid(String uuid) {
            this.uuid = uuid;
            return this;
        }

        public Builder major(Integer major) {
            this.major = major;
            return this;
        }

        public Builder minor(Integer minor) {
            this.minor = minor;
            return this;
        }

        public Builder indoor(Boolean indoor) {
            this.indoor = indoor;
            return this;
        }

        public Builder movable(Boolean movable) {
            this.movable = movable;
            return this;
        }

        public Builder txPower(Integer txPower) {
            this.txPower = txPower;
            return this;
        }

        public Builder latitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder beaconType(String beaconType) {
            this.beaconType = beaconType;
            return this;
        }

        public Builder geohash(String geohash) {
            this.geohash = geohash;
            return this;
        }

        public BeaconData build() {
            Asserts.notNull(id);
            Asserts.notNull(regionId);
            Asserts.notNull(uuid);
            Asserts.notNull(major);
            Asserts.notNull(minor);
            return new BeaconData(id,
                    regionId,
                    UUID.fromString(uuid),
                    major,
                    minor,
                    indoor,
                    movable,
                    txPower,
                    latitude,
                    longitude,
                    description,
                    beaconType,
                    geohash);
        }
    }
}
