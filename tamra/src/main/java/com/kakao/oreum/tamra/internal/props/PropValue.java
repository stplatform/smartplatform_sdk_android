package com.kakao.oreum.tamra.internal.props;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.helper.Asserts;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class PropValue {
    private final String value;
    private static final Logger LOG = getLogger(PropValue.class);

    private PropValue(String value) {
        this.value = value;
    }

    public static PropValue of(String value) {
        return new PropValue(value);
    }

    public boolean isPresent() {
        return value != null;
    }

    public String get() {
        Asserts.notNull(value);
        return value;
    }

    public String or(String defaultValue) {
        return value == null ? defaultValue : value;
    }

    public int asInt() {
        return Integer.valueOf(get());
    }

    public int asIntOr(int defaultValue) {
        try {
            return asInt();
        } catch (NumberFormatException e) {
            LOG.debug("Fail to read value as Integer.", e);
            return defaultValue;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        String otherValue = null;
        if (o instanceof PropValue) {
            otherValue = ((PropValue) o).value;
        } else if (o instanceof String) {
            otherValue = (String) o;
        } else {
            return false;
        }
        return value != null ? value.equals(otherValue) : otherValue == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "PropValue{" +
                "value='" + value + '\'' +
                '}';
    }
}
