package com.kakao.oreum.tamra;

import android.app.Application;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.infra.device.DeviceStateChecker;
import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.ox.task.RunOn;
import com.kakao.oreum.tamra.base.Config;
import com.kakao.oreum.tamra.base.MyHistories;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Period;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.SDKLogParams;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.tamra.error.RecentTamraErrors;
import com.kakao.oreum.tamra.error.TamraError;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.error.TamraInitException;
import com.kakao.oreum.tamra.error.TamraNotInitializedException;
import com.kakao.oreum.tamra.internal.beacon.AbstractBeaconService;
import com.kakao.oreum.tamra.internal.beacon.OreumBeaconServiceFactory;
import com.kakao.oreum.tamra.internal.error.TamraErrorHolder;
import com.kakao.oreum.tamra.internal.fixture.Version;
import com.kakao.oreum.tamra.internal.history.HistoryService;
import com.kakao.oreum.tamra.internal.history.HistoryServiceFactory;
import com.kakao.oreum.tamra.internal.http.HttpClientFactory;
import com.kakao.oreum.tamra.internal.observer.TamraObservers;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLogSync;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLogger;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLoggerSyncFactory;
import com.kakao.oreum.tamra.internal.spots.RangedSpotsHolder;

import java.util.Collection;
import java.util.List;

/**
 * Tamra SDK.
 *
 * <p>
 * Tamra SDK에 대한 entry point 역할을 하는 클래스이며 Oreum Platform에 등록된 beacon들에 대한 정보를 제공한다.
 * </p>
 *
 * <p>
 * Android 환경에서 사용이 용이하도록 <code>static method</code> 형태로 기능을 제공하며,
 * 현재 주변의 beacon을 {@link com.kakao.oreum.tamra.base.Spot} 객체로 제공한다.
 *
 * </p>
 * <p>
 * Android Applicatiion이 구동된 후에 초기화 한 후에 사용할 수 있다. 초기화하지 않으면 동작하지 않는다.
 * 아래와 같이 {@link android.content.Context}와 <code>appkey</code>를 사용하여 초기화한다.
 * </p>
 *
 *
 * <pre><code>
 *     // init Tamra
 *     String appkey = ...; // OreumZigi에서 발급받은 appkey
 *     Config config = Config.forRelease(getApplicationContext(), appkey);
 *     Tamra.init(config);
 *
 *     // monitor regions.
 *     Tamra.startMonitoring(Region.Name.제주국제공항);
 *     Tamra.startMonitoring(Region.Name.제주창조경제혁신센터);
 *     Tamra.startMonitoring(Region.Id.of(7));
 * </code></pre>
 *
 * <p>
 * App에서 관심 있는 지역은 {@link Region} 객체로 {@link #startMonitoring(Region)} 직접 호출하여 모니터링해야 한다.
 * <code>Tamra</code>가 초기화된 후에는 아래와 같은 방식으로 주변 spot 정보를 얻을 수 있다.
 * </p>
 *
 * <pre><code>
 *     // 방법1: event 방식으로 spot 정보 처리
 *     Tamra.addObserver(new TamraObserverAdapter() {
 *          &#064;Override
 *          public void ranged(NearbySpots spots) {
 *              for(Spot spot : spots) {
 *                  doWithSpot(spot);
 *              }
 *          }
 *     });
 *     // 방법2: 직접 현재 주변의 spot을 조회하여 처리
 *     for(Spot spot : Tamra.spots()) {
 *         doWithSpot(spot);
 *     }
 * </code></pre>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public final class Tamra {
    /**
     * 버전 정보.
     */
    public static final String VERSION = Version.asString();
    private static final RangedSpotsHolder spotsHolder = new RangedSpotsHolder();
    private static final TamraErrorHolder errorHolder = new TamraErrorHolder();
    private static final TamraObservers tamraObservers = new TamraObservers();
    private static AbstractBeaconService beaconService;
    private static Status status = Status.NOT_INITIALIZED;
    private static OxHttpClient httpClient;
    /**
     * SDK 사용 로그 서버 연동 객체
     *
     * @since 0.11.0
     */
    private static SDKLogSync sdkLogSync;
    /**
     * SDK 사용 로그 객체
     * @since 0.11.0
     */
    private static SDKLogger sdkLogger;
    /**
     * @since 0.11.0
     */
    private static HistoryService historyService;

    static {
        // observe internal exception.
        OxErrors.addObserver(errorHolder.errorObserver());
        // catch spots and hold into spotsHolder
        tamraObservers.addObserver(RunOn.BACKGROUND, spotsHolder);
    }

    private Tamra() { /* DO NOT CREATE INSTANCE */ }

    /**
     * region의 beacon들을 모니터링한다.
     *
     * @param region monitoring할 region
     */
    public static void startMonitoring(Region region) {
        if (beaconService == null) {
            OxErrors.notifyAll(new TamraNotInitializedException());
            return;
        }
        beaconService.startMonitoring(region);
    }

    /**
     * monitoring을 중단한다.
     *
     * @param region region
     */
    public static void stopMonitoring(Region region) {
        if (beaconService == null) {
            OxErrors.notifyAll(new TamraNotInitializedException());
            return;
        }
        beaconService.stopMonitoring(region);
    }

    /**
     * 현재 monitoring 중인 region 목록.
     *
     * @return region collection
     */
    public static Collection<Region> monitoringRegions() {
        if (beaconService == null) {
            OxErrors.notifyAll(new TamraNotInitializedException());
            return ImmutableSet.of();
        }
        return beaconService.monitoringRegions();
    }

    /**
     * Tamra observer 등록.
     *
     * @param tamraObserver observer
     */
    public static void addObserver(TamraObserver tamraObserver) {
        tamraObservers.addObserver(tamraObserver);
    }

    /**
     * 등록했던 observer를 제거.
     *
     * @param tamraObserver observer
     */
    public static void removeObserver(TamraObserver tamraObserver) {
        tamraObservers.removeObserver(tamraObserver);
    }

    /**
     * 현재 시점에 range되고 있는 beacon에 대한 spot 목록.
     *
     * @return spots
     */
    public static NearbySpots spots() {
        return spotsHolder.nearbySpots();
    }

    /**
     * 초기화.
     *
     * @param config 설정
     */
    public static void init(Config config) {
        if (status != Status.NOT_INITIALIZED) {
            OxErrors.notifyAll(new TamraException("already initialized"));
            return;
        }
        ExceptionFree.exec(() -> {
            // device checker
            OxIterable.from(DeviceStateChecker.Util.all(config.context()))
                    .doEach(DeviceStateChecker::assertOk);

            // setup beacon service.
            httpClient = HttpClientFactory.create(config);

            sdkLogger = SDKLoggerSyncFactory.getLogger(config);

            beaconService = OreumBeaconServiceFactory.create(config, httpClient, sdkLogger);
            beaconService.setObserver(tamraObservers);

            sdkLogSync = SDKLoggerSyncFactory.getSDKLogSync(config.context(),
                    httpClient,
                    config.sdkLogSyncIntervalInSeconds(),
                    config.macAddressCollectible());

            historyService = HistoryServiceFactory.create(config.context());

            status = Status.INITIALIZED;

        }, OxErrors.wrapAndNotify(e -> {
            status = Status.INIT_FAILED;
            return new TamraInitException(e);
        }));
    }

    /**
     * Tamra 내부에서 발생한 오류정보를 리턴.
     *
     * @return 오류 목록
     * @see #recentErrors()
     * @deprecated 0.8.0
     */
    @Deprecated // TODO 1.0.0 릴리즈시 삭제할 것
    public static List<TamraError> errors() {
        return errorHolder.recentErrors().toList();
    }

    /**
     * @return 최근 오류 정보를 담는 RecentTamraErrors
     * @since 0.8.0
     */
    public static RecentTamraErrors recentErrors() {
        return errorHolder.recentErrors();
    }

    /**
     * 특정 기간에 쌓은 히스토리 로그 조회
     *
     * @param period
     * @return
     * @since 0.11.0
     */
    public static MyHistories histories(Period period) {
        return historyService.fetch(period);
    }

    /**
     * 특성 지점에 쌓은 히스토리 로그 조회
     *
     * @param spot
     * @return
     * @since 0.11.0
     */
    public static MyHistories histories(Spot spot) {
        return historyService.hasVisited(spot);
    }

    /**
     * 최근 N개의 히스토리 로그 조회
     *
     * @param n
     * @return
     * @since 0.11.0
     */
    public static MyHistories histories(int n) {
        return historyService.last(n);
    }

    /**
     * 지점에 대한 히스토리 로그 쌓기
     *
     * @param spot
     * @since 0.11.0
     */
    public static void logHistory(Spot spot) {
        OxTasks.runOnBackground(ExceptionFree.runnable(() -> historyService.log(spot)));
    }

    /**
     * 히스토리 로그 모두 삭제하기
     *
     * @since 0.11.0
     */
    public static void clearHistory() {
        OxTasks.runOnBackground(ExceptionFree.runnable(() -> historyService.clear()));
    }

    /**
     * 데이터 분석 등에 사용할 수 있는 사용로그 기록을 남길 수 있다.
     *
     * @param action 로그에 남길 작업 키
     * @param sdkLogParams 로그 내용
     */
    public static void sdkLog(String action, SDKLogParams sdkLogParams) {
        OxTasks.runOnBackground(ExceptionFree.runnable(() -> sdkLogger.log(action, sdkLogParams)));
    }

    /**
     *  모니터링 기능을 백그라운드 모드로 전환
     *  activity (or fragment) 에서 pause 이벤트 발생시 호출 (call in onPause)
     */
    public static void onPause() {
        if (beaconService == null) {
            OxErrors.notifyAll(new TamraNotInitializedException());
            return;
        }

        beaconService.onPause();
    }

    /**
     *  백그라운드 모드로 전환된 모니터링 기능을 재개
     *  activity (or fragment) 에서 resume 이벤트 발생시 호출 (call in onResume)
     */
    public static void onResume() {
        if (beaconService == null) {
            OxErrors.notifyAll(new TamraNotInitializedException());
            return;
        }

        beaconService.onResume();
    }

    private enum Status {
        NOT_INITIALIZED,
        INITIALIZED,
        INIT_FAILED
    }

}
