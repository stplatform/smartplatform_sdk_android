package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.FunctionalInterface;
import com.kakao.oreum.common.function.Consumer;

/**
 * 비동기 데이터 Fetcher.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
@FunctionalInterface
@Accessibility.Public
public interface AsyncFetcher<T> {

    void fetch(Consumer<? super T> callback);

}
