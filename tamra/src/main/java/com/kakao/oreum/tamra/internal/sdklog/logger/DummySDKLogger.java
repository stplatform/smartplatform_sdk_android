package com.kakao.oreum.tamra.internal.sdklog.logger;

import com.kakao.oreum.tamra.base.SDKLogParams;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class DummySDKLogger implements SDKLogger {

    @Override
    public SDKLogger log(String action, SDKLogParams params) {
        return this;
    }

    @Override
    public SDKLogger addStaticParams(String key, String value) {
        return this;
    }
}
