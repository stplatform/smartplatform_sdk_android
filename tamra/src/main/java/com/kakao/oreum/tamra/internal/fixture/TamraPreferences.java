package com.kakao.oreum.tamra.internal.fixture;

import android.content.Context;
import android.content.SharedPreferences;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * Tarma SDK의 Preferences.
 *
 * 패키지 내부에서만 쓴다
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
final class TamraPreferences {

    public  TamraPreferences() {
        throw new UnsupportedOperationException();
    }

    private static final String PreferenceFile = "com.kakao.oreum.tamra.preferences";

    /**
     * context에서 SDK의 preference를 가져온다.
     *
     * @param context
     * @return
     */
    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(PreferenceFile, Context.MODE_PRIVATE);
    }
}
