package com.kakao.oreum.tamra.internal.history;

import android.content.Context;

import com.kakao.oreum.infra.persistence.sqlite.HistoryDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;

/**
 * History 기록/조회 서비스 생성
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class HistoryServiceFactory {
    private HistoryServiceFactory() {
        /* DO NOT CREATE INSTANCE */
    }

    /**
     * HistoryService 구현체를 생성한다.
     *
     * @param context
     * @return
     */
    public static HistoryService create(Context context) {
        SystemTimeService timeService = new SystemTimeService();
        return new HistoryServiceImpl(
                new HistoryDBRepository(new TamraDBHelper(context), new ISO8601DateFormat())
                , timeService);
    }
}
