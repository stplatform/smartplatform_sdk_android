package com.kakao.oreum.tamra.internal.history;

import com.kakao.oreum.tamra.base.MyHistories;
import com.kakao.oreum.tamra.base.Period;
import com.kakao.oreum.tamra.base.Spot;

/**
 * 방문 기록 조회 서비스
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface HistoryService {
    /**
     * 특정 기간의 방문 기록 조회
     *
     * @param period
     * @return 방문 기록
     */
    MyHistories fetch(Period period);

    /**
     * 최근 n개의 방문 기록 조회
     *
     * @param n
     * @return 방문 기록
     */
    MyHistories last(int n);

    /**
     * 특정 지점에 방문한 기록 보기
     *
     * @param spot
     * @return 예전 방문 목록
     */
    MyHistories hasVisited(Spot spot);

    /**
     * 방문기록 남기기
     *
     * @param spot
     */
    void log(Spot spot);

    /**
     * 방문기록 전체 삭제
     */
    void clear();
}
