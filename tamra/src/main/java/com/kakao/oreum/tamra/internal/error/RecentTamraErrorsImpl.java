package com.kakao.oreum.tamra.internal.error;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.tamra.error.RecentTamraErrors;
import com.kakao.oreum.tamra.error.TamraError;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * {@link RecentTamraErrors} 기본 구현 클래스.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
class RecentTamraErrorsImpl implements RecentTamraErrors {
    private final Iterable<TamraError> errors;
    private final int cumulativeCount;

    private RecentTamraErrorsImpl(Iterable<TamraError> errors, int cumulativeCount) {
        this.errors = errors;
        this.cumulativeCount = cumulativeCount;
    }

    public static RecentTamraErrors of(Iterable<TamraError> errors, int cumulativeCount) {
        return new RecentTamraErrorsImpl(errors, cumulativeCount);
    }

    private RecentTamraErrors of(Iterable<TamraError> errors) {
        return of(errors, this.cumulativeCount);
    }

    @Override
    public RecentTamraErrors orderBy(Comparator<? super TamraError> comparator) {
        return of(OxIterable.from(errors)
                .sort(comparator));
    }

    @Override
    public RecentTamraErrors limit(int maxCount) {
        return of(OxIterable.from(errors)
                .limit(maxCount));
    }

    @Override
    public RecentTamraErrors filter(Predicate<? super TamraError> predicate) {
        return of(OxIterable.from(errors)
                .filter(predicate));
    }

    @Override
    public Optional<TamraError> mostRecent() {
        return OxIterable.from(errors)
                .sort(TamraError.RECENTLY_ORDER)
                .first();
    }

    @Override
    public void foreach(Consumer<? super TamraError> consumer) {
        OxIterable.from(errors)
                .doEach(consumer);
    }

    @Override
    public boolean isEmpty() {
        return !OxIterable.from(errors)
                .first() // size를 쓰면 모두 iterate하니깐 첫번째꺼만 확인한다
                .isPresent();
    }

    @Override
    public List<TamraError> toList() {
        return OxIterable.from(errors)
                .toList();
    }

    @Override
    public int cumulativeCount() {
        return cumulativeCount;
    }

    @Override
    public int retainCount() {
        return OxIterable.from(errors).size();
    }

    @Override
    public Iterator<TamraError> iterator() {
        return errors.iterator();
    }
}
