package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * {@link TamraObserver}에서 받고자 하는 이벤트에 대한 감도
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@Accessibility.Public
public enum ObserverSensitivity {
    /**
     * 응답성 우선
     */
    Responsive,
    /**
     * 기본 모드
     */
    Balanced,
    /**
     * 정확도 우선 (Experimental)
     */
    Accurate,
}
