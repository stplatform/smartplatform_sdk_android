package com.kakao.oreum.tamra.base;


import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.XComparator;
import com.kakao.oreum.common.function.XComparators;
import com.kakao.oreum.ox.helper.Asserts;

/**
 * beacon에 의한 어떤 지점을 표현하는 객체.
 *
 * <p>
 * apple iBeacon의 beacon과 유사하지만, Oreum Platform에서는 BeaconRegion을 Platform에서 직접 관리하므로
 * <code>uuid</code>, <code>major</code>, <code>minor</code> 등의 beacon 식별자를 캡슐화하며
 * beacon의 부가적인 속성(실내여부, 위경도좌표 등)을 제공하기 위한 객체로 {@link Spot} 클래스를 정의한다.
 * </p>
 *
 * <p>
 * apple의 CoreLocation과는 달리
 * 안드로이드 환경의 특성상 accuracy 및 proximity의 정확도는 보장하지 못한다.
 * 즉, device 종류 및 beacon 에 따라 동일한 위치에서도 accuracy와 proximity는 다르게 측정될 수 있다.
 *
 * 따라서 proximity 및 accuracy는 <strong>상대적인 측정치</strong>로서 사용하는 것을 권장하며 절대적인 값으로써의 사용하지 않도록 한다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
@Immutable
public final class Spot {
    /**
     * id 속성 기준의 comparator.
     *
     * @since 0.7.0
     */
    public static final XComparator<Spot> ID_ORDER =
            XComparators.of((lhs, rhs) -> (int) (lhs.id() - rhs.id()));
    /**
     * region id 기준의 comparator.
     *
     * @since 0.7.0
     */
    public static final XComparator<Spot> REGION_ORDER =
            XComparators.of((lhs, rhs) -> Region.ID_ORDER.compare(lhs.region(), rhs.region()));
    /**
     * accuracy 기준의 comparator.
     *
     * @since 0.7.0
     */
    public static final XComparator<Spot> ACCURACY_ORDER =
            XComparators.of((lhs, rhs) -> Double.compare(lhs.accuracy, rhs.accuracy));
    /**
     * proximity 기준의 comparator.
     *
     * @since 0.7.0
     */
    public static final XComparator<Spot> PROXIMITY_ORDER =
            XComparators.of((lhs, rhs) -> lhs.proximity.compareTo(rhs.proximity()));

    private final long id;
    private final String description;
    private final Proximity proximity;
    private final double accuracy;
    private final Region region;
    private final boolean indoor;
    private final boolean movable;
    private final double latitude;
    private final double longitude;
    private final String geohash;
    private final AsyncFetcher<Optional<String>> dataFetcher;

    private Spot(long id,
                 String description,
                 Proximity proximity,
                 double accuracy,
                 Region region,
                 boolean indoor,
                 boolean movable,
                 double latitude,
                 double longitude,
                 String geohash,
                 AsyncFetcher dataFetcher) {
        this.id = id;
        this.description = description;
        this.proximity = proximity;
        this.accuracy = accuracy;
        this.region = region;
        this.indoor = indoor;
        this.movable = movable;
        this.latitude = latitude;
        this.longitude = longitude;
        this.geohash = geohash;
        this.dataFetcher = dataFetcher;
    }

    /**
     * builder factory method.
     *
     * @param id spot id
     * @return builder for spot.
     */
    @Accessibility.Private
    public static Builder of(long id) {
        return new Builder(id);
    }

    /**
     * identifier.
     *
     * @return id
     */
    public long id() {
        return id;
    }

    /**
     * description.
     *
     * @return description
     */
    public String description() {
        return description;
    }

    /**
     * proximity.
     *
     * @return proximity
     */
    public Proximity proximity() {
        return proximity;
    }

    /**
     * accuracy.
     *
     * <strong>주의</strong>
     *
     * 측정된 beacon 신호를 기반으로 계산된 거리 값이지만,
     * Apple의 <code>iBeacon</code> 문서에서 기술된 바와 같이 거리개념보다는 정확도 개념으로 보는 것이 좋다.
     * 특히, `Near` 이상에서는 신호간섭 때문에 정확도가 떨어지기 때문에 실내측위 용도로 사용하는 것은 권장하지 않는다.
     *
     * @return accuracy
     */
    public double accuracy() {
        return accuracy;
    }

    /**
     * region.
     *
     * @return region
     */
    public Region region() {
        return region;
    }

    /**
     * indoor.
     *
     * @return 실내의 beacon이면 true, 아니면 false.
     */
    public boolean indoor() {
        return indoor;
    }

    /**
     * movable.
     *
     * @return 이동형 beacon이면 true, 아니면 false.
     */
    public boolean movable() {
        return movable;
    }

    /**
     * 위도 좌표.
     *
     * @return 위도. 실내 비콘의 경우 해당 건물의 위도좌표값을 가진다.
     */
    public double latitude() {
        return latitude;
    }

    /**
     * 경도 좌표.
     *
     * @return 경도. 실내 비콘의 경우 해당 건물의 경도좌표값을 가진다.
     */
    public double longitude() {
        return longitude;
    }

    /**
     * geohash.
     *
     * @return 위경도 좌표에 대한 geohash.
     */
    public String geohash() {
        return geohash;
    }

    /**
     * 지점 데이터 조회.
     *
     * v0.7.0에서 deprecate되고 현재는 {@link Optional#empty()}를 반환한다.
     *
     * @return empty optional.
     * @deprecated at 0.7.0. replaced by {@link #data(Consumer)}.
     */
    @Deprecated // TODO 1.0.0 release시에 제거할 것
    public Optional<String> data() {
        return Optional.empty();
    }

    /**
     * spot에 대한 data 조회(비동기).
     *
     * OreumZigi를 통해 등록한 데이터를 조회한다.
     *
     * @param callback data를 받을 callback function.
     * @since 0.7.0
     */
    public void data(Consumer<Optional<String>> callback) {
        if (dataFetcher != null) {
            dataFetcher.fetch(callback);
        } else {
            callback.accept(Optional.empty());
        }
    }

    @Override
    public String toString() {
        return "Spot{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", proximity=" + proximity +
                ", accuracy=" + accuracy +
                ", region=" + region +
                ", indoor=" + indoor +
                ", movable=" + movable +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", geohash='" + geohash + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return id == ((Spot) o).id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }


    /**
     * Spot Builder 클래스.
     *
     * SDK 내부에서 사용하며, SDK 사용자는 Test용도 외에는 사용하지 않도록 한다.
     */
    @Accessibility.Private
    public static class Builder {
        private final long id;
        private String description = null;
        private Proximity proximity = null;
        private Double accuracy = null;
        private Region region = null;
        private boolean indoor = true;
        private boolean movable = false;
        private Double latitude = null;
        private Double longitude = null;
        private String geohash = null;
        private AsyncFetcher<Optional<String>> dataFetcher = null;

        private Builder(long id) {
            this.id = id;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder proximity(Proximity proximity) {
            this.proximity = proximity;
            return this;
        }

        public Builder accuracy(double accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public Builder region(Region region) {
            this.region = region;
            return this;
        }

        public Builder indoor(boolean indoor) {
            this.indoor = indoor;
            return this;
        }

        public Builder movable(boolean movable) {
            this.movable = movable;
            return this;
        }

        public Builder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder geohash(String geohash) {
            this.geohash = geohash;
            return this;
        }

        public Builder dataFetcher(AsyncFetcher<Optional<String>> dataFetcher) {
            this.dataFetcher = dataFetcher;
            return this;
        }

        public Spot build() {
            Asserts.notNull(description);
            Asserts.notNull(proximity);
            Asserts.notNull(accuracy);
            Asserts.notNull(region);
            Asserts.notNull(latitude);
            Asserts.notNull(longitude);
            Asserts.notNull(geohash);
            return new Spot(
                    id,
                    description,
                    proximity,
                    accuracy,
                    region,
                    indoor,
                    movable,
                    latitude,
                    longitude,
                    geohash,
                    dataFetcher);
        }
    }
}
