package com.kakao.oreum.tamra.error;

import com.kakao.oreum.tamra.base.Config;

/**
 * Tamra 초기화에 실패한 경우 발생하는 예외.
 *
 * <p>
 * {@link com.kakao.oreum.tamra.Tamra#init(Config)} 과정에서 예외 발생시 wrapping하기 위한 용도의 클래스다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class TamraInitException extends TamraException {

    public TamraInitException(Throwable throwable) {
        super(throwable);
    }
    
}
