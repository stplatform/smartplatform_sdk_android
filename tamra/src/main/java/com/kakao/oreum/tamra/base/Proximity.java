package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * 어떤 {@link Spot}과의 현재 Device가 인접한 정도를 표현하기 위한 타입.
 *
 * <p>
 * apple의 iBeacon 정의를 따른다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public enum Proximity implements Comparable<Proximity> {
    /**
     * 매우 가까움. (1m 이내)
     */
    Immediate,
    /**
     * 가까움. (1 ~ 3m)
     */
    Near,
    /**
     * 먼 거리 (수 meter)
     */
    Far,
    /**
     * 정의되지 않음.
     */
    Unknown;

}
