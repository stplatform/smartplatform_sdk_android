package com.kakao.oreum.tamra.internal.data.parser;

import com.kakao.oreum.tamra.internal.data.data.BeaconData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconDataParser extends JsonDataParser<BeaconData> {
    @Override
    protected BeaconData map(JSONObject jsonObject) throws JSONException {
        return BeaconData.builder()
                .id(jsonObject.getLong("id"))
                .regionId(jsonObject.getLong("regionId"))
                .uuid(jsonObject.getString("uuid"))
                .major(jsonObject.getInt("major"))
                .minor(jsonObject.getInt("minor"))
                .indoor(jsonObject.getBoolean("indoor"))
                .movable(jsonObject.getBoolean("movable"))
                .txPower(jsonObject.getInt("txPower"))
                .latitude(jsonObject.getDouble("latitude"))
                .longitude(jsonObject.getDouble("longitude"))
                .description(jsonObject.getString("description"))
                .beaconType(jsonObject.getString("beaconType"))
                .geohash(jsonObject.getString("geohash"))
                .build();
    }
}
