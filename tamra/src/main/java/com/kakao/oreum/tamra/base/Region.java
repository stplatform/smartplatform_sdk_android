package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.common.function.XComparator;
import com.kakao.oreum.common.function.XComparators;

/**
 * Oreum Platform의 지역을 지정하기 위한 클래스.
 *
 * <p>
 * Oreum에서의 Region은 <code>iBeacon</code>의 <code>BeaconRegion</code>과 유사하게
 * 지리적 개념의 Region이라기 보다는 Beacon Grouping을 위한 개념이다.
 * 따라서 동일한 지리적 지역에 대해 다수의 Region이 정의될 수 있으며, 넓은 지리적 범위에 대해서 단일 Region이 정의될 수 도 있다.
 * </p>
 *
 * <p>
 * SDK가 릴리즈되는 시점에 OreumZigi에 등록되어 있는 Region은 {@link Regions}를 통해 상수로 제공하며,
 * {@link Regions}에 없는 Region이라도 {@link #of(long)}를 사용하여 <code>id</code> 값으로 직접 Region 객체를 생성할 수 있다.
 * </p>
 * <p>
 * Region은 <code>id</code> 속성만으로 식별되며, <code>name</code>은 SDK 내부에서 사용되지 않는 속성이다.
 * {@link #name()} 속성을 로직에 사용하지 않도록 한다.
 * </p>
 *
 * <p>
 * 참고: {@link TamraObserver}를 통해 전달되는 {@link Region} 객체는 서버에서 가져온 <code>name</code> 값을 가지고 있다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
@Immutable
public class Region {
    /**
     * id 기준의 comparator.
     *
     * @since 0.7.0
     */
    public static final XComparator<Region> ID_ORDER = XComparators.of((lhs, rhs) -> (int) (lhs.id() - rhs.id()));
    public static final Region UNKNOWN = of(-1, "UNKNOWN");

    private final long id;
    private final String name;


    protected Region(long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * factory method.
     *
     * @param id region id
     * @return region
     */
    public static Region of(long id) {
        return new Region(id, "N/A");
    }

    /**
     * factory method.
     *
     * @param id   region id
     * @param name region name
     * @return region.
     */
    public static Region of(long id, String name) {
        return new Region(id, name);
    }

    /**
     * region의 식별자.
     *
     * 불변성과 유일성이 보장된다.
     *
     * @return id
     */
    public long id() {
        return id;
    }


    /**
     * region name.
     *
     * SDK 사용자가 확인하기 위한 값이며, 이름은 변경될 수 있으므로 이름을 기준으로 logic을 작성하지 않도록 한다.
     *
     * @return name
     */
    public String name() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof Region) {
            return id() == ((Region) o).id();
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return (int) (id() ^ (id() >>> 32));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Region{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}