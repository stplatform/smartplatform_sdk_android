package com.kakao.oreum.tamra.internal.data.parser;

import com.kakao.oreum.tamra.internal.data.data.RegionData;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class RegionDataParser extends JsonDataParser<RegionData> {
    @Override
    protected RegionData map(JSONObject jsonObject) throws JSONException {
        return RegionData.builder()
                .id(jsonObject.getLong("id"))
                .uuid(jsonObject.getString("uuid"))
                .regionType(jsonObject.getString("regionType"))
                .name(jsonObject.getString("name"))
                .latitude(jsonObject.getDouble("latitude"))
                .longitude(jsonObject.getDouble("longitude"))
                .radius(jsonObject.getInt("radius"))
                .geohash(jsonObject.getString("geohash"))
                .description(jsonObject.getString("description"))
                .build();
    }
}
