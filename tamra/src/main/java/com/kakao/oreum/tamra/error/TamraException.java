package com.kakao.oreum.tamra.error;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * Tamra 내부 예외를 위한 최상위 Exception.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public class TamraException extends RuntimeException {
    /**
     * {@inheritDoc}
     */
    public TamraException() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public TamraException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * {@inheritDoc}
     */
    public TamraException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * {@inheritDoc}
     */
    public TamraException(Throwable throwable) {
        super(throwable);
    }
}
