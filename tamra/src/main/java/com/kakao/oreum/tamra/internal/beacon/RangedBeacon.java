package com.kakao.oreum.tamra.internal.beacon;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class RangedBeacon {
    private final BeaconRegion region;
    private final Beacon beacon;
    private final Date rangedAt;

    public RangedBeacon(BeaconRegion region, Beacon beacon, Date rangedAt) {
        this.region = region;
        this.beacon = beacon;
        this.rangedAt = rangedAt;
    }

    public BeaconRegion region() {
        return region;
    }

    public Beacon beacon() {
        return beacon;
    }

    public Date rangedAt() {
        return rangedAt;
    }

    @Override
    public String toString() {
        return "RangedBeacon{" +
                "region=" + region +
                ", beacon=" + beacon +
                ", rangedAt=" + rangedAt +
                '}';
    }
}
