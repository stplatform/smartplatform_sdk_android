package com.kakao.oreum.tamra.internal.data;

import android.content.Context;

import com.kakao.oreum.infra.persistence.sqlite.RegionDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.http.OxHttpClient;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class RegionRepositoryFactory {
    private RegionRepositoryFactory() {
        /* DO NOT INSTANTIATE */
    }

    public static RegionRepository create(Context context, OxHttpClient httpClient, boolean persistent) {
        if (persistent) {
            SystemTimeService timeService = new SystemTimeService();
            return new RegionPersistentRepository(
                    new RegionDBRepository(new TamraDBHelper(context), timeService),
                    httpClient, timeService);
        } else {
            return new RegionTransientRepository(httpClient);
        }
    }
}
