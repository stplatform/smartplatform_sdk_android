package com.kakao.oreum.tamra.internal.sdklog;

import java.util.Date;

/**
 * SDK 사용 로그
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SDKLog {
    private String action;
    private String param;
    private Date issuedAt;

    public SDKLog(String action, String param, Date issuedAt) {
        this.action = action;
        this.param = param;
        this.issuedAt = issuedAt;
    }

    public String action() {
        return action;
    }

    public String param() {
        return param;
    }

    public Date issuedAt() {
        return issuedAt;
    }

    @Override
    public String toString() {
        return SDKLog.class.getSimpleName() + ":{" +
                "action:\"" + action +
                "\", param:\"" + param +
                "\", issuedAt:" + issuedAt +
                "}";
    }
}
