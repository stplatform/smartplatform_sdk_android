package com.kakao.oreum.tamra.internal.history;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.tamra.base.History;
import com.kakao.oreum.tamra.base.MyHistories;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * {@link MyHistories}에 대한 구현체.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class MyHistoriesImpl implements MyHistories {
    private final Iterable<History> histories;

    private MyHistoriesImpl(Iterable<History> histories) {
        Asserts.notNull(histories);
        this.histories = histories;
    }

    @Override
    public Iterator<History> iterator() {
        return histories.iterator();
    }

    public static MyHistories copyOf(Iterable<History> histories) {
        return new MyHistoriesImpl(histories);
    }

    public static MyHistories of(Iterable<History> histories) {
        return new MyHistoriesImpl(histories);
    }


    @Override
    public MyHistories limit(int max) {
        return copyOf(OxIterable.from(histories).limit(max));
    }

    @Override
    public MyHistories filter(Predicate<? super History> filter) {
        return copyOf(OxIterable.from(histories).filter(filter));
    }

    @Override
    public boolean all(Predicate<? super History> predicate) {
        return OxIterable.from(histories).all(predicate);
    }

    @Override
    public boolean any(Predicate<? super History> predicate) {
        return OxIterable.from(histories).any(predicate);
    }

    @Override
    public Optional<History> first() {
        return OxIterable.from(histories).first();
    }

    @Override
    public Optional<History> last() {
        return OxIterable.from(histories).last();
    }

    @Override
    public Optional<History> get(int position) {
        return OxIterable.from(histories).get(position);
    }

    @Override
    public MyHistories orderBy(Comparator<? super History> comparator) {
        return copyOf(OxIterable.from(histories).sort(comparator));
    }

    @Override
    public List<History> toList() {
        return OxIterable.from(histories).toList();
    }

    @Override
    public Set<History> toSet() {
        return OxIterable.from(histories).toSet();
    }

    @Override
    public <T> Map<T, Collection<History>> index(Function<? super History, ? extends T> indexFunction) {
        return OxIterable.from(histories).index(indexFunction);
    }

    @Override
    public <T> Map<History, T> toMap(Function<? super History, ? extends T> valueFunction) {
        return OxIterable.from(histories).toMap(valueFunction);
    }

    @Override
    public <T> Collection<T> transform(Function<? super History, ? extends T> mapFunction) {
        return OxIterable.from(histories).map(mapFunction).toList();
    }

    @Override
    public void foreach(Consumer<? super History> action) {
        OxIterable.from(histories).doEach(action);
    }

    @Override
    public int size() {
        return OxIterable.from(histories).size();
    }

    @Override
    public boolean isEmpty() {
        return OxIterable.from(histories).first().isPresent();
    }
}
