package com.kakao.oreum.tamra.base;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class Period {
    private DateTime start;
    private DateTime end;

    public Period(DateTime start, DateTime end) {
        this.start = start;
        this.end = end;
    }

    public static Period lastSeconds(int amount) {
        Date now = new Date();
        return new Period(new DateTime(now).agoInSeconds(amount), new DateTime(now));
    }

    public static Period lastMinutes(int amount) {
        Date now = new Date();
        return new Period(new DateTime(now).agoInMinutes(amount), new DateTime(now));
    }

    public static Period lastHours(int amount) {
        Date now = new Date();
        return new Period(new DateTime(now).agoInHours(amount), new DateTime(now));
    }

    public Date from() {
        return start.getTime();
    }

    public Date to() {
        return end.getTime();
    }
}
