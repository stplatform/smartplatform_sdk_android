package com.kakao.oreum.tamra.internal.sdklog.logger;

import com.kakao.oreum.infra.persistence.sqlite.SDKLogDBRepository;
import com.kakao.oreum.tamra.base.SDKLogParams;
import com.kakao.oreum.tamra.internal.sdklog.SDKLog;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.logger.Logger;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class PersistentSDKLogger implements SDKLogger {
    private static final Logger LOG = getLogger(PersistentSDKLogger.class);
    private final SDKLogDBRepository repository;
    private final TimeService timeService;
    private final SDKLogParams defaultParams;

    PersistentSDKLogger(SDKLogDBRepository repository, TimeService timeService) {
        this.repository = repository;
        this.timeService = timeService;
        this.defaultParams = SDKLogParams.emptyParams();
    }

    @Override
    public SDKLogger log(String action, SDKLogParams sdkLogParams) {
        SDKLogParams mergedParams = SDKLogParams.of(defaultParams.params());
        mergedParams.add(sdkLogParams.params());
        SDKLog log = new SDKLog(action, sdkLogParams.toString(), timeService.now());
        repository.insert(log);
        LOG.info(log.toString());
        return this;
    }

    @Override
    public SDKLogger addStaticParams(String key, String value) {
        defaultParams.add(key, value);
        return this;
    }
}
