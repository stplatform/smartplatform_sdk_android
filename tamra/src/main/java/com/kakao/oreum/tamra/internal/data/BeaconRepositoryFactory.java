package com.kakao.oreum.tamra.internal.data;

import android.content.Context;

import com.kakao.oreum.infra.persistence.sqlite.BeaconDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.http.OxHttpClient;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class BeaconRepositoryFactory {
    private BeaconRepositoryFactory() {
        /* DO NOT INSTANTIATE */
    }

    public static BeaconRepository create(Context context, OxHttpClient httpClient, boolean persistent) {
        if (persistent) {
            SystemTimeService timeService = new SystemTimeService();
            return new BeaconPersistentRepository(
                    new BeaconDBRepository(new TamraDBHelper(context), timeService),
                    httpClient, timeService);
        } else {
            return new BeaconTransientRepository(httpClient);
        }
    }
}
