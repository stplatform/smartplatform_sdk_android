package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.persistence.sqlite.BeaconDBRepository;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;

import java.util.Collection;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class BeaconPersistentRepository extends ApiConnectedBeaconRepository {
    private final Logger LOG = getLogger(BeaconPersistentRepository.class);
    private final BeaconDBRepository repository;
    private final TimeService timeService;

    public BeaconPersistentRepository(BeaconDBRepository repository, OxHttpClient httpClient, TimeService timeService) {
        super(httpClient);
        this.repository = repository;
        this.timeService = timeService;

        OxTasks.runOnSync(ExceptionFree.runnable(this::sync));
    }

    @Override
    public Optional<BeaconData> find(long id) {
        return Optional.ofNullable(repository.findById(id));
    }

    @Override
    public Collection<BeaconData> findBy(Region region) {
        return repository.findByRegion(region);
    }

    @Override
    public Optional<BeaconData> findBy(Beacon beacon) {
        return Optional.ofNullable(repository.findByBeacon(beacon));
    }

    protected void sync() {
        ExceptionFree.exec(() -> {
                // HttpClient를 통한 서버 인증시간이 걸릴수 있다. (이게 완전한 해결책은 아님)
                Thread.sleep(300);

                String currentForBefore = timeService.now().toString();

                LOG.info("[BEACONS] start to sync...");
                String json = getJsonForAllBeacons();
                OxIterable.from(parser.parseArray(json)).doEach(beaconData -> {
                    if (repository.findById(beaconData.id()) != null)
                        repository.update(beaconData);
                    else
                        repository.insert(beaconData);
                });

                repository.removeNotSynced(currentForBefore);
                LOG.info("[BEACONS] synchronized");
            },
            OxErrors.wrapAndNotify(e -> new TamraException("Fail to sync Beacons", e))
        );
    }
}
