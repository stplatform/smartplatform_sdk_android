package com.kakao.oreum.tamra.internal.fixture;

import android.os.Build;

import java.util.Locale;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class UserAgent {
    private final static String value = generate();

    private UserAgent(){}

    public static String get() {
        return value;
    }

    private static String generate() {
        return String.format(Locale.ENGLISH, "Tamra/%s (%s; %s) Android/%d (%s; %s; %s)",
                Version.asString(), Build.MODEL, Build.MANUFACTURER,
                Build.VERSION.SDK_INT, Build.VERSION.CODENAME, Build.VERSION.RELEASE, Locale.getDefault());
    }
}
