package com.kakao.oreum.tamra.internal.spots;

import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.TamraObserverAdapter;

/**
 * Range 되는 Spot 객체를 보관하는 객체.
 *
 * <p>
 * {@link com.kakao.oreum.tamra.Tamra}의 inner class로 구현되었다가 <code>0.8.0</code>에서 분리됨. (error 패키지와 구조를 맞추기 위해서)
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class RangedSpotsHolder extends TamraObserverAdapter {
    private NearbySpots spots = NearbySpotsImpl.of();

    public NearbySpots nearbySpots() {
        synchronized (this) {
            return spots == null ? NearbySpotsImpl.of() : spots;
        }
    }

    @Override
    public void ranged(NearbySpots spots) {
        synchronized (this) {
            this.spots = spots;
        }
    }
}
