package com.kakao.oreum.tamra.internal.http.auth;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.Date;

/**
 * 서버 인증 정보를 담는 객체 클래스.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class Authentication {

    private final String authToken;

    private final Date expireAt;

    private final TimeService timeService;

    Authentication(@Nullable String authToken, @Nullable Date expireAt, @Nonnull TimeService timeService) {
        Asserts.notNull(timeService);
        this.authToken = authToken;
        this.timeService = timeService;
        this.expireAt = (expireAt == null) ? timeService.now() : expireAt;
    }

    /**
     * 인증 토큰.
     *
     * 서버 통신시에 사용한다.
     *
     * @return
     */
    public String authToken() {
        return isValid() ? authToken : null;
    }

    public boolean isValid() {
        return authToken != null && expireAt.after(timeService.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Authentication that = (Authentication) o;
        if (authToken != null ? !authToken.equals(that.authToken) : that.authToken != null) {
            return false;
        }
        return expireAt != null ?
                expireAt.equals(that.expireAt) : that.expireAt == null;

    }

    @Override
    public int hashCode() {
        int result = authToken != null ? authToken.hashCode() : 0;
        result = 31 * result + (expireAt != null ? expireAt.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Authentication{" +
                "authToken='" + authToken + '\'' +
                ", expireAt=" + expireAt +
                '}';
    }
}
