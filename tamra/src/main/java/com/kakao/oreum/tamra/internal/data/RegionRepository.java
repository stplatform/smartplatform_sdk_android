package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.RegionData;

import java.util.Collection;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public interface RegionRepository {
    Optional<RegionData> find(long id);

    Optional<RegionData> find(Region region);

    Collection<RegionData> all();
}
