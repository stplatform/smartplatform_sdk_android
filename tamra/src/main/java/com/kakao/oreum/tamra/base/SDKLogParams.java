package com.kakao.oreum.tamra.base;

import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.collection.Sets;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.Set;

/**
 * SDKLog의 Parameter 필드 대응
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SDKLogParams {
    private static final String DELIMITER_PARAM = "&";
    private static final String DELIMITER_KEY_VALUE = "=";
    private final Set<Param> params;

    private SDKLogParams() {
        params = Sets.hashSet();
    }
    private SDKLogParams(Set<Param> paramSet) {
        this.params = Sets.hashSet(paramSet);
    }

    public static SDKLogParams emptyParams() {
        return new SDKLogParams();
    }

    public static SDKLogParams of(String key, String value) {
        SDKLogParams params = new SDKLogParams();
        params.add(key, value);
        return params;
    }

    public static SDKLogParams of(Set<Param> paramSet) {
        return new SDKLogParams(paramSet);
    }

    public SDKLogParams add(String key, String value) {
        params.add(new Param(key, value));
        return this;
    }

    public SDKLogParams add(Set<Param> paramSet) {
        this.params.addAll(paramSet);
        return this;
    }

    public Set<Param> params() {
        return ImmutableSet.of(params);
    }

    public String toString() {
        return OxIterable.from(params).join(DELIMITER_PARAM);
    }

    private static final class Param {
        private String key;
        private String value;

        private Param(String key, String value) {
            Asserts.notNull(key);
            Asserts.notNull(value);
            this.key = key;
            this.value = value;
        }

        public int hashCode() {
            return key.hashCode();
        }

        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Param)) return false;

            Param that = (Param) o;

            return key.equals(that.key);
        }

        public String toString() {
            return key() + DELIMITER_KEY_VALUE + value();
        }

        public String key() {
            return key;
        }

        public String value() {
            return value;
        }
    }
}
