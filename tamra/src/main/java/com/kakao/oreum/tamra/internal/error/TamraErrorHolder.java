package com.kakao.oreum.tamra.internal.error;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.error.OxErrorObserver;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.error.RecentTamraErrors;
import com.kakao.oreum.tamra.error.TamraError;

import java.util.LinkedList;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * Tamra 내부 에러 보관 클래스.
 * <p>
 * {@link com.kakao.oreum.ox.error.OxErrors}에 {@link #errorObserver}를 등록하여 오류를 notify 받는다.
 * {@link com.kakao.oreum.ox.error.OxErrors}를 통해 오류데이터 흐름을 만들고, 이 클래스에서 오류를 저장하고 공급한다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public final class TamraErrorHolder {
    private static final Logger LOG = getLogger(TamraErrorHolder.class);
    private final LinkedList<TamraError> retainedErrors = new LinkedList<>();
    private final TimeService timeService = new SystemTimeService();
    private int cumulativeErrorCount = 0;
    private final OxErrorObserver errorObserver = this::addError;

    /**
     * @return 보관중인 오류를 RecentTamraErrors로 리턴
     */
    public RecentTamraErrors recentErrors() {
        return RecentTamraErrorsImpl.of(retainedErrors, cumulativeErrorCount);
    }

    public OxErrorObserver errorObserver() {
        return this.errorObserver;
    }

    /**
     * 직접 사용하지 말것.
     * {@link #errorObserver}를 {@link com.kakao.oreum.ox.error.OxErrors}, {@link com.kakao.oreum.ox.error.ExceptionFree}와 연동하여 사용하도록 한다.
     *
     * @param exception 추가할 exception.
     */
    @Accessibility.InternalTesting
    public void addError(Exception exception) {
        synchronized (retainedErrors) {
            LOG.warn("Exception occurred", exception);
            TamraError tamraError = new TamraError(exception, timeService.now());
            retainedErrors.addFirst(tamraError);
            cumulativeErrorCount++;
            while (retainedErrors.size() > RecentTamraErrors.MAX_RETAIN_COUNT) {
                retainedErrors.removeLast();
            }
        }
    }

}
