package com.kakao.oreum.tamra.internal.http.auth;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.function.Supplier;
import com.kakao.oreum.ox.helper.Streams;
import com.kakao.oreum.ox.http.Header;
import com.kakao.oreum.ox.http.Method;
import com.kakao.oreum.ox.http.OxRequest;
import com.kakao.oreum.ox.http.Response;
import com.kakao.oreum.ox.logger.Logger;

import java.net.URL;
import java.util.Collection;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class AuthHeaderSupplier implements Supplier<Header> {
    private static final Logger LOG = getLogger(AuthHeaderSupplier.class);
    private static final char QUOT = '"';
    private final OxRequest authRequest;
    private final AuthenticationParser authParser = new AuthenticationParser();
    private Optional<Authentication> authentication = Optional.empty();

    public AuthHeaderSupplier(@Nonnull URL apiURL,
                              @Nonnull String appId,
                              @Nonnull String appKey,
                              @Nonnull Collection<Header> authReqHeaders) {
        authRequest = OxRequest.forURL(apiURL)
                .headers(authReqHeaders)
                .body(authRequestBody(appId, appKey));
    }


    @Override
    public Header get() {
        if (!isTokenValid()) {
            createToken();
        }
        return Header.of(
                "Oreum-Token",
                authentication
                        .map(Authentication::authToken)
                        .orElse("N/A"));
    }

    private void createToken() {
        try {
            Response response = authRequest.request(Method.POST);
            if (response.status().success()) {
                String bodyString = Streams.readAsString(response.body());
                LOG.debug("auth: {} ", bodyString);
                authentication = authParser.parse(bodyString);
            }
        } catch (Exception e) {
            LOG.error("fail to authenticate", e);
        }
    }


    private String authRequestBody(String appId, String appKey) {
        return new StringBuilder()
                .append("{")
                .append(QUOT).append("appId").append(QUOT)
                .append(":")
                .append(QUOT).append(appId).append(QUOT)
                .append(QUOT).append("appKey").append(QUOT)
                .append(":")
                .append(QUOT).append(appKey).append(QUOT)
                .append("}")
                .toString();
    }

    private boolean isTokenValid() {
        return authentication
                .map(Authentication::isValid)
                .orElse(false); // empty이면 invalid 처리
    }
}
