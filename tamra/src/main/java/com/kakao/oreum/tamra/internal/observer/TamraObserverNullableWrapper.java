package com.kakao.oreum.tamra.internal.observer;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.TamraObserver;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class TamraObserverNullableWrapper implements TamraObserver {
    private Optional<TamraObserver> delegate = Optional.empty();

    public void setDelegate(TamraObserver tamraObserver) {
        this.delegate = Optional.of(tamraObserver);
    }

    @Override
    public void didEnter(Region region) {
        delegate.ifPresent(d -> d.didEnter(region));
    }

    @Override
    public void didExit(Region region) {
        delegate.ifPresent(d -> d.didExit(region));
    }

    @Override
    public void ranged(NearbySpots spots) {
        delegate.ifPresent(d -> d.ranged(spots));
    }
}
