package com.kakao.oreum.tamra.internal.beacon;

import com.kakao.oreum.ox.collection.ImmutableList;
import com.kakao.oreum.ox.logger.Logger;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class ByPassRangedBeaconStabilizer implements RangedBeaconStabilizer {
    private static final Logger LOG = getLogger(ByPassRangedBeaconStabilizer.class);
    private final Set<RangedBeacon> beacons = new LinkedHashSet<>();

    @Override
    public void ranged(Collection<RangedBeacon> rangedBeacons) {
        synchronized (beacons) {
            beacons.clear();
            beacons.addAll(rangedBeacons);
        }
    }

    @Override
    public Collection<RangedBeacon> stabledBeacons() {
        LOG.info("{}", beacons.toArray());
        synchronized (beacons) {
            return ImmutableList.copyOf(beacons);
        }
    }
}
