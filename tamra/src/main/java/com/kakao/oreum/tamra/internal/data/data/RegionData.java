package com.kakao.oreum.tamra.internal.data.data;

import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.tamra.base.Region;

import java.util.UUID;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class RegionData {
    private final long id;
    private final String name;
    private final UUID uuid;
    private final double latitude;
    private final double longitude;
    private final int radius;
    private final String geohash;
    private final String description;
    private final String regionType;

    private RegionData(long id, String name, UUID uuid, double latitude, double longitude, int radius, String geohash, String description, String regionType) {
        this.id = id;
        this.name = name;
        this.uuid = uuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
        this.geohash = geohash;
        this.description = description;
        this.regionType = regionType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public long id() {
        return id;
    }

    public String name() {
        return name;
    }

    public UUID uuid() {
        return uuid;
    }

    public double latitude() {
        return latitude;
    }

    public double longitude() {
        return longitude;
    }

    public int radius() {
        return radius;
    }

    public String geohash() {
        return geohash;
    }

    public String description() {
        return description;
    }

    public String regionType() {
        return regionType;
    }

    public Region region() {
        return Region.of(id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof RegionData))
            return false;

        RegionData that = (RegionData) o;

        return id == that.id();
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "RegionData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", uuid=" + uuid +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                ", geohash='" + geohash + '\'' +
                ", description='" + description + '\'' +
                ", regionType='" + regionType + '\'' +
                '}';
    }

    public static class Builder {
        private Long id;
        private String name;
        private UUID uuid;
        private Double latitude;
        private Double longitude;
        private Integer radius;
        private String geohash;
        private String description;
        private String regionType;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder uuid(UUID uuid) {
            this.uuid = uuid;
            return this;
        }

        public Builder uuid(String uuid) {
            this.uuid = UUID.fromString(uuid);
            return this;
        }

        public Builder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder radius(int radius) {
            this.radius = radius;
            return this;
        }

        public Builder geohash(String geohash) {
            this.geohash = geohash;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder regionType(String regionType) {
            this.regionType = regionType;
            return this;
        }

        public RegionData build() {
            Asserts.notNull(id);
            Asserts.notNull(name);
            Asserts.notNull(uuid);
            Asserts.notNull(latitude);
            Asserts.notNull(longitude);
            Asserts.notNull(radius);
            Asserts.notNull(geohash);
            Asserts.notNull(description);
            Asserts.notNull(regionType);
            return new RegionData(id, name, uuid, latitude, longitude, radius, geohash, description, regionType);
        }
    }
}
