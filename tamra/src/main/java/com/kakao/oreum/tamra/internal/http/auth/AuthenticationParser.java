package com.kakao.oreum.tamra.internal.http.auth;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Supplier;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;
import com.kakao.oreum.ox.logger.Logger;

import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class AuthenticationParser {
    private static final Logger LOG = getLogger(AuthenticationParser.class);
    private static final TimeService DEFAULT_TIMESERVCE = new SystemTimeService();
    private final DateFormat dateFormat = new ISO8601DateFormat();
    private final Supplier<TimeService> timeServiceSupplier;

    public AuthenticationParser(Supplier<TimeService> timeServiceSupplier) {
        this.timeServiceSupplier = timeServiceSupplier;
    }

    public AuthenticationParser() {
        this(() -> DEFAULT_TIMESERVCE);
    }

    public Optional<Authentication> parse(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            String token = jsonObject.getString("token");
            String expireAtAsString = jsonObject.getString("expireAt");

            Date expireAt = dateFormat.parse(expireAtAsString);
            return Optional.of(new Authentication(token, expireAt, timeServiceSupplier.get()));
        } catch (Exception e) {
            LOG.error("parsing error", e);
        }
        return Optional.empty();
    }

}
