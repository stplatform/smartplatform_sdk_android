package com.kakao.oreum.tamra.internal.history;

import com.kakao.oreum.infra.persistence.sqlite.HistoryDBRepository;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.tamra.base.History;
import com.kakao.oreum.tamra.base.MyHistories;
import com.kakao.oreum.tamra.base.Period;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.internal.data.data.HistoryData;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class HistoryServiceImpl implements HistoryService {
    private HistoryDBRepository repository;
    private TimeService timeService;

    HistoryServiceImpl(HistoryDBRepository repository, TimeService timeService) {
        this.repository = repository;
        this.timeService = timeService;
    }

    @Override
    public MyHistories fetch(Period period) {
        return MyHistoriesImpl.of(OxIterable.from(repository.find(period))
            .map(historyData -> History.build()
                    .spodId(historyData.spotId())
                    .description(historyData.description())
                    .issuedAt(historyData.issuedAt())
                    .build()));
    }

    @Override
    public MyHistories last(int n) {
        return MyHistoriesImpl.of(OxIterable.from(repository.last(n))
                .map(historyData -> History.build()
                        .spodId(historyData.spotId())
                        .description(historyData.description())
                        .issuedAt(historyData.issuedAt())
                        .build()));
    }

    @Override
    public MyHistories hasVisited(Spot spot) {
        return MyHistoriesImpl.of(OxIterable.from(repository.find(spot.id()))
                .map(historyData -> History.build()
                        .spodId(historyData.spotId())
                        .description(historyData.description())
                        .issuedAt(historyData.issuedAt())
                        .build()));
    }

    @Override
    public void log(Spot spot) {
        repository.insert(HistoryData.builder()
                .spotId(spot.id())
                .description(spot.description())
                .proximity(spot.proximity())
                .accuracy(spot.accuracy())
                .latitude(spot.latitude())
                .longitude(spot.longitude())
                .issuedAt(timeService.now())
                .build());
    }

    @Override
    public void clear() {
        repository.removeAll();
    }
}
