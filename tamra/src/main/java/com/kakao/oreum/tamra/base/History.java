package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.Date;

/**
 * 방문했던 기록을 표현하는 객체.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@Accessibility.Public
@Immutable
public final class History {
    private final long spotId;
    private final String description;
    private final Date issuedAt;

    private History(long spotId,
                    String description,
                    Date issuedAt) {
        this.spotId = spotId;
        this.description = description;
        this.issuedAt = issuedAt;
    }

    public static Builder build() {
        return new Builder();
    }

    public long spotId() {
        return spotId;
    }

    public String description() {
        return description;
    }

    public Date issuedAt() {
        return issuedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof History))
            return false;

        History that = (History) o;

        return spotId == that.spotId && issuedAt.equals(that.issuedAt());
    }

    @Override
    public int hashCode() {
        return (int) ((long)super.hashCode() ^ ((long)super.hashCode() >>> 32));
    }

    @Override
    public String toString() {
        return "History{" +
                "spotId=" + spotId +
                ", description='" + description + "'" +
                ", issuedAt='" + issuedAt + "'" +
                "}";
    }

    /**
     * History Builder
     *
     * SDK 내부에서 사용하며 SDK 사용자는 Test 용도 외에는 사용하지 않는다.
     */
    @Accessibility.Private
    public static class Builder {
        private Long spotId;
        private String description;
        private Date issuedAt;

        private Builder() {

        }

        public Builder spodId(long spotId) {
            this.spotId = spotId;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder issuedAt(Date issuedAt) {
            this.issuedAt = issuedAt;
            return this;
        }

        public History build() {
            Asserts.notNull(spotId);
            Asserts.notNull(description);
            Asserts.notNull(issuedAt);
            return new History(
                    spotId,
                    description,
                    issuedAt
            );
        }
    }
}
