package com.kakao.oreum.tamra.error;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.function.XComparator;
import com.kakao.oreum.common.function.XComparators;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.Date;

/**
 * Tamra SDK 내부 오류 정보를 담기 위한 객체 클래스.
 *
 * {@link Exception} 정보와 발생시각 정보를 갖는다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public final class TamraError {
    /**
     * @since 0.8.0
     */
    public static final XComparator<TamraError> RECENTLY_ORDER =
            XComparators.reverseOrder((lhs, rhs) -> lhs.occurredAt.compareTo(rhs.occurredAt));

    private final Throwable causedBy;
    private final Date occurredAt;

    public TamraError(@Nonnull Throwable causedBy, @Nonnull Date occurredAt) {
        Asserts.notNull(causedBy);
        Asserts.notNull(occurredAt);
        this.causedBy = causedBy;
        this.occurredAt = occurredAt;
    }

    /**
     * 발생한 예외.
     *
     * @return 발생한 예외
     * @deprecated use {@link #causedBy()}
     */
    @Deprecated // TODO 1.0.0 릴리즈시에 삭제할 것
    public Throwable exception() {
        return causedBy;
    }

    /**
     * @return 발생한 예외
     * @since 0.8.0
     */
    @Nonnull
    public Throwable causedBy() {
        return causedBy;
    }

    /**
     * 발생시각.
     *
     * @return 발생시각
     */
    @Nonnull
    public Date occurredAt() {
        return occurredAt;
    }

    @Override
    public String toString() {
        return "TamraError{" +
                "causedBy=" + causedBy +
                ", occurredAt=" + occurredAt +
                '}';
    }
}
