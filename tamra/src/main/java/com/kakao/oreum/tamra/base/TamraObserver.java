package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * {@link com.kakao.oreum.tamra.Tamra}에 대한 Observer.
 *
 * <p>
 * {@link com.kakao.oreum.tamra.Tamra} 이벤트를 callback 받는 interface이다.
 * {@link com.kakao.oreum.tamra.Tamra}는 이 인터페이스를 통해
 * {@link Region}의 진입 및 이탈과 range되는 beacon에 대한 {@link Spot} 목록을 이벤트로 전달해준다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public interface TamraObserver {

    /**
     * region 진입 이벤트.
     *
     * @param region 진입 region
     */
    void didEnter(Region region);

    /**
     * region 이탈 이벤트.
     *
     * @param region 이탈 region.
     */
    void didExit(Region region);

    /**
     * 현 시점에서 range되는 beacon에 대한 spot 목록.
     *
     * @param spots range된 beacon에 대한 spots
     */
    void ranged(NearbySpots spots);

}
