package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * Profile.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public enum Profile {
    /**
     * 실서비스 환경.
     *
     * <p>
     * 이 환경에서는 http://oreum.kakao.com 의 오름지기 서버 데이터와 연동된다.
     * </p>
     */
    PRODUCTION,
    /**
     * 테스트서비스 환경.
     *
     * <p>
     * 이 환경에서는 http://test-oreum.kakao.com 의 오름지기 서버 데이터와 연동된다.
     * </p>
     */
    TEST,
    /**
     * Tamra 내부 개발용. (외부사용안됨)
     */
    @Accessibility.InternalTesting
    DEVELOP,
    /**
     * 개발자 local PC용. (외부사용불가)
     */
    @Accessibility.InternalTesting
    LOCAL;
}
