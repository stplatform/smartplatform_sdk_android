package com.kakao.oreum.tamra.internal.history;

import com.kakao.oreum.infra.persistence.sqlite.HistoryDBRepository;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.internal.data.data.HistoryData;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class HistoryLoggerImpl implements HistoryLogger {
    private static final Logger LOG = getLogger(HistoryLoggerImpl.class);
    private HistoryDBRepository repository;
    private TimeService timeService;

    HistoryLoggerImpl(HistoryDBRepository repository, TimeService timeService) {
        this.repository = repository;
        this.timeService = timeService;
    }

    @Override
    public void log(Spot spot) {
        LOG.info("{}", spot);
        repository.insert(HistoryData.builder()
                .spotId(spot.id())
                .description(spot.description())
                .proximity(spot.proximity())
                .accuracy(spot.accuracy())
                .latitude(spot.latitude())
                .longitude(spot.longitude())
                .issuedAt(timeService.now())
                .build());
    }
}
