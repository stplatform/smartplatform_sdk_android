package com.kakao.oreum.tamra.internal.observer;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.ox.task.RunOn;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.ox.error.ExceptionFree;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * {@link TamraObserver}를 aggregate하는 역할과 observer 실행 thread를 제어하는 클래스.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class TamraObservers implements TamraObserver {
    private final Map<TamraObserver, RunOn> observers = new LinkedHashMap<>();

    public void addObserver(TamraObserver observer) {
        addObserver(OxTasks.currentRunOn(), observer);
    }

    public void addObserver(@Nonnull RunOn runOn, @Nonnull TamraObserver observer) {
        this.observers.put(observer, runOn);
    }

    public void removeObserver(TamraObserver observer) {
        this.observers.remove(observer);
    }


    @Override
    public void didEnter(Region region) {
        for (Map.Entry<TamraObserver, RunOn> entry : observers.entrySet()) {
            TamraObserver observer = entry.getKey();
            RunOn runOn = entry.getValue();
            OxTasks.runOn(runOn, ExceptionFree.runnable(
                    () -> observer.didEnter(region)
            ));
        }
    }

    @Override
    public void didExit(Region region) {
        for (Map.Entry<TamraObserver, RunOn> entry : observers.entrySet()) {
            TamraObserver observer = entry.getKey();
            RunOn runOn = entry.getValue();
            OxTasks.runOn(runOn, ExceptionFree.runnable(
                    () -> observer.didExit(region)
            ));
        }
    }

    @Override
    public void ranged(NearbySpots spots) {
        for (Map.Entry<TamraObserver, RunOn> entry : observers.entrySet()) {
            TamraObserver observer = entry.getKey();
            RunOn runOn = entry.getValue();
            OxTasks.runOn(runOn, ExceptionFree.runnable(
                    () -> observer.ranged(spots)
            ));
        }
    }

}
