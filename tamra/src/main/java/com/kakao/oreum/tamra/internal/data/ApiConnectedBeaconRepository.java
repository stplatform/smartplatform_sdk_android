package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.HttpCallException;
import com.kakao.oreum.ox.http.Method;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;
import com.kakao.oreum.tamra.internal.data.parser.BeaconDataParser;
import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import java.io.IOException;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public abstract class ApiConnectedBeaconRepository implements BeaconRepository {
    private static final Logger LOG = getLogger(ApiConnectedBeaconRepository.class);
    protected final OxHttpClient httpClient;
    protected final JsonDataParser<BeaconData> parser = new BeaconDataParser();

    public ApiConnectedBeaconRepository(OxHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    protected String getJsonForAllBeacons() {
        LOG.info("TRACE> ApiConnectedBeaconRepository#getJsonForAllBeacons started");
        try {
            return httpClient.requestFor("/beacons")
                    .params("limit", 10000)
                    .emptyBody()
                    .request(Method.GET)
                    .bodyAsString();
        } catch (IOException | HttpCallException e) {
            throw new RemoteApiException(e);
        } finally {
            LOG.info("TRACE> ApiConnectedBeaconRepository#getJsonForAllBeacons ended");
        }
    }

    @Override
    public Optional<String> getSpotData(long id) {
        // 서버에서 SDK에서의 접근이력통계를 취합하기 위해서 당분간 spot data api 호출은 캐시하지 않는다.
        return ExceptionFree.get(() -> httpClient.requestFor("/spots/" + id)
                        .emptyBody()
                        .request(Method.GET)
                        .bodyAsString(),
                OxErrors.wrapAndNotify(e ->
                        new TamraException("Fail to fetch spot data: " + id, e)
                ));
    }
}
