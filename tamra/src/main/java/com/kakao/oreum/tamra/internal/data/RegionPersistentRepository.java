package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.persistence.sqlite.RegionDBRepository;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.data.data.RegionData;

import java.util.Collection;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class RegionPersistentRepository extends ApiConnnectedRegionRepository {
    private final Logger LOG = getLogger(RegionPersistentRepository.class);
    private final RegionDBRepository repository;
    private final TimeService timeService;


    public RegionPersistentRepository(RegionDBRepository repository, OxHttpClient httpClient, TimeService timeService) {
        super(httpClient);
        this.repository = repository;
        this.timeService = timeService;

        OxTasks.runOnSync(ExceptionFree.runnable(this::sync));
    }

    @Override
    public Optional<RegionData> find(long id) {
        return Optional.ofNullable(repository.findById(id));
    }

    @Override
    public Optional<RegionData> find(Region region) {
        return Optional.ofNullable(repository.findByRegion(region));
    }

    @Override
    public Collection<RegionData> all() {
        return ImmutableSet.of(repository.findAll());
    }

    /**
     * 서버 호출하여 DB 데이터 sync 맞춘다.
     */
    protected void sync() {
        ExceptionFree.exec(() -> {
                // HttpClient를 통한 서버 인증시간이 걸릴수 있다. (이게 완전한 해결책은 아님)
                Thread.sleep(1000);

                String currentForBefore = timeService.now().toString();

                LOG.info("[REGIONS] start to sync...");
                String json = getJsonForAllRegions();
                OxIterable.from(parser.parseArray(json)).doEach(regionData -> {
                    if (repository.findById(regionData.id()) != null)
                        repository.update(regionData);
                    else
                        repository.insert(regionData);
                });
                repository.removeNotSynced(currentForBefore);
                LOG.info("[REGIONS] synchronized ");
            },
            OxErrors.wrapAndNotify(e -> new TamraException("Fail to sync Regions", e))
        );
    }
}
