package com.kakao.oreum.tamra.internal.props;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.base.Profile;
import com.kakao.oreum.ox.helper.ClasspathFiles;

import java.util.Properties;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class TamraProperties {
    private static final Logger LOG = getLogger(TamraProperties.class);
    private final Properties properties;

    private TamraProperties(Properties properties) {
        this.properties = properties;
    }

    public static TamraProperties load(Profile profile) {
        Properties properties = new Properties();
        try {
            String file = "tamra/" + profile.name().toLowerCase() + ".properties";
            properties.load(ClasspathFiles.getStream(file));

        } catch (Exception e) {
            LOG.warn("cannot load properties for " + profile, e);
        }
        return new TamraProperties(properties);
    }

    @Nonnull
    public PropValue get(PropName name) {
        return PropValue.of(properties.getProperty(name.key));
    }
}
