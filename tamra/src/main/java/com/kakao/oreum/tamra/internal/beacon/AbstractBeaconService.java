package com.kakao.oreum.tamra.internal.beacon;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLogger;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.ox.task.RunOn;
import com.kakao.oreum.tamra.base.AsyncFetcher;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Proximity;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.tamra.internal.data.BeaconRepository;
import com.kakao.oreum.tamra.internal.data.RegionRepository;
import com.kakao.oreum.tamra.internal.data.data.RegionData;
import com.kakao.oreum.tamra.internal.data.data.RegionUtil;
import com.kakao.oreum.tamra.internal.observer.TamraObserverNullableWrapper;
import com.kakao.oreum.tamra.internal.spots.NearbySpotsImpl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public abstract class AbstractBeaconService {
    protected final BeaconService beaconService;
    protected final RegionRepository regionRepository;
    protected final BeaconRepository beaconRepository;
    protected final TamraObserverNullableWrapper observer = new TamraObserverNullableWrapper();
    protected final RangedBeaconStabilizer rangedBeaconStabilizer;
    protected final TimeService timeService = new SystemTimeService();
    protected final Set<Region> monitoringRegions = new HashSet<>();
    protected final SDKLogger sdkLogger;

    public AbstractBeaconService(BeaconService beaconService,
                                 RegionRepository regionRepository,
                                 BeaconRepository beaconRepository,
                                 RangedBeaconStabilizer rangedBeaconStabilizer,
                                 SDKLogger sdkLogger) {
        this.rangedBeaconStabilizer = rangedBeaconStabilizer;
        this.regionRepository = regionRepository;
        this.beaconRepository = beaconRepository;
        this.beaconService = beaconService;
        this.sdkLogger = sdkLogger;
    }

    public abstract void startMonitoring();
    public abstract void stopMonitoring();
    public abstract void startMonitoring(Region region);
    public abstract void stopMonitoring(Region region);
    public abstract Collection<Region> monitoringRegions();

    public void setObserver(TamraObserver tamraObserver) {
        this.observer.setDelegate(tamraObserver);
    }

    protected void notifyNearbySpots() {
        Iterable<Spot> curSpots = OxIterable.from(rangedBeaconStabilizer.stabledBeacons())
                .map(this::createSpot)
                .filter(Optional::isPresent)
                .map(Optional::get);
        if (curSpots.iterator().hasNext()) {
            NearbySpots nearbySpots = NearbySpotsImpl.copyOf(curSpots);
            if (nearbySpots.size() > 0)
                observer.ranged(nearbySpots);
        }
    }

    private Optional<Spot> createSpot(RangedBeacon rangedBeacon) {
        Optional<RegionData> regionData = regionRepository.find(RegionUtil.toRegion(rangedBeacon.region()));
        if (!regionData.isPresent()) {
            return Optional.empty();
        }
        return beaconRepository.findBy(rangedBeacon.beacon())
                .map(beacon -> Spot.of(beacon.id())
                        .region(regionData.get().region())
                        .proximity(convert(rangedBeacon.beacon().proximity()))
                        .accuracy(rangedBeacon.beacon().accuracy())
                        .description(beacon.description())
                        .movable(beacon.isMovable())
                        .indoor(beacon.isIndoor())
                        .latitude(beacon.latitude())
                        .longitude(beacon.longitude())
                        .geohash(beacon.geohash())
                        .dataFetcher(new SpotDataFetcher(beacon.id()))
                        .build()
                );
    }

    private Proximity convert(com.kakao.oreum.infra.beacon.Proximity proximity) {
        switch (proximity) {
            case Far:
                return Proximity.Far;
            case Near:
                return Proximity.Near;
            case Immediate:
                return Proximity.Immediate;
            case Unknown:
                return Proximity.Unknown;
            default:
                return Proximity.Unknown;
        }
    }

    private class SpotDataFetcher implements AsyncFetcher<Optional<String>> {
        private final long id;

        private SpotDataFetcher(long id) {
            this.id = id;
        }

        @Override
        public void fetch(Consumer<? super Optional<String>> callback) {
            RunOn runOn = OxTasks.currentRunOn();
            OxTasks.runOnBackground(() -> {
                Optional<String> data = beaconRepository.getSpotData(id);
                OxTasks.runOn(runOn, () -> callback.accept(data));
            });
        }
    }

    public void onPause() {
        beaconService.pause();
    }

    public void onResume() {
        beaconService.resume();
    }

}
