package com.kakao.oreum.tamra.internal.beacon;

import java.util.Collection;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public interface RangedBeaconStabilizer {


    void ranged(Collection<RangedBeacon> rangedBeacons);


    Collection<RangedBeacon> stabledBeacons();

}
