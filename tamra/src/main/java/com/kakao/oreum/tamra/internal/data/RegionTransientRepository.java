package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.data.data.RegionData;
import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;
import com.kakao.oreum.tamra.internal.data.parser.RegionDataParser;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class RegionTransientRepository extends ApiConnnectedRegionRepository {
    private static final Logger LOG = getLogger(RegionTransientRepository.class);
    private final Set<RegionData> fetched = new LinkedHashSet<>();

    public RegionTransientRepository(OxHttpClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<RegionData> find(long id) {
        return OxIterable.from(all())
                .find(r -> r.id() == id);
    }

    @Override
    public Optional<RegionData> find(Region region) {
        return find(region.id());
    }

    public Collection<RegionData> all() {
        if (fetched.isEmpty()) {
            ExceptionFree.exec(() -> {
                        String json = getJsonForAllRegions();
                        LOG.debug("region data:{}", json);
                        fetched.addAll(parser.parseArray(json));
                    },
                    OxErrors.wrapAndNotify(e ->
                            new TamraException("Fail to fetch region data", e)
                    ));
        }
        return fetched;
    }

}
