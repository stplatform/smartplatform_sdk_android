package com.kakao.oreum.tamra.error;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Predicate;

import java.util.Comparator;
import java.util.List;

/**
 * {@link TamraError}에 대한 collection helper.
 *
 * <p>
 * {@link TamraError}에 대한 {@link Iterable} 인터페이스를 구현하고 있으므로 <code>for-loop</code> 등에서 바로 쓸 수 있고,
 * 이 인터페이스에서 제공하는 메소드를 이용하여 functional programming 스타일로 Tamra 내부 오류들을 navigate, filtering, sorting 할 수 있다.
 * </p>
 * <p>
 * 내부적으로 {@link #MAX_RETAIN_COUNT} 수 만큼의 최근 {@link TamraError}만 유지한다.
 * 따라서 짧은 시간 많은 수의 내부 오류가 발생한 경우 모든 오류를 다 접근하지 못할 수 있다.
 * </p>
 * <p>
 * 이 인터페이스의 구현은 기본적으로 immutable로 구현된다.
 * 따라서 {@link #orderBy(Comparator)}, {@link #limit(int)}, {@link #filter(Predicate)}의 리턴 값은 새로 생성된 객체이다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
@Immutable
public interface RecentTamraErrors extends Iterable<TamraError> {
    /**
     * 보관 가능한 최대 오류 개수
     */
    int MAX_RETAIN_COUNT = 100;

    /**
     * 오류들을 정렬한다.
     *
     * @param comparator 정렬에 사용할 comparator
     * @return comparator 로 정렬된 RecentTamraErrors
     */
    RecentTamraErrors orderBy(Comparator<? super TamraError> comparator);

    /**
     * @param maxCount 최대 개수
     * @return maxCount 만큼의 오류를 가진 tamraErrors
     */
    RecentTamraErrors limit(int maxCount);

    /**
     * @param predicate filter 조건
     * @return predicate에 의해 필터링된 RecentTamraErrors
     */
    RecentTamraErrors filter(Predicate<? super TamraError> predicate);

    /**
     * @return 가장 최신 오류 1개. 현재 오류가 없을 경우 Optional.empty() 리턴.
     */
    Optional<TamraError> mostRecent();

    /**
     * functional loop.
     *
     * @param consumer consumer.
     */
    void foreach(Consumer<? super TamraError> consumer);

    /**
     * @return 오류가 없으면 true, 아니면 false
     */
    boolean isEmpty();

    /**
     * @return 오류 리스트.
     */
    List<TamraError> toList();

    /**
     * 누적 오류 개수.
     * orderBy, limit, filter 등의 메소드로 새로운 RecentTamraErrors 객체가 생성되더라도 이 갯수는 계속 유지된다.
     * {@link #retainCount()} 보다 크거나 같다.
     *
     * @return 오류 누적 개수.
     * @see #retainCount()
     */
    int cumulativeCount();

    /**
     * @return 객체 내에 보관하고 있는 오류 갯수.
     * @see #cumulativeCount()
     */
    int retainCount();

}
