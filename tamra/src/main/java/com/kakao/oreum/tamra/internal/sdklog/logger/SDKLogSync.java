package com.kakao.oreum.tamra.internal.sdklog.logger;

import com.kakao.oreum.infra.device.NetworkChecker;
import com.kakao.oreum.infra.persistence.sqlite.SDKLogDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.SyncDBRepository;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.Method;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.http.Status;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.ox.task.Period;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.sdklog.SDKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * Client에 쌓인 SDK 로그를 서버로 주기적으로 전송
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SDKLogSync {
    private static final Logger LOG = getLogger(SDKLogSync.class);
    private final SyncDBRepository syncDBRepository;
    private final SDKLogDBRepository sdkLogDBRepository;
    private final OxHttpClient httpClient;
    private final TimeService timeService;
    private final DateFormat dateFormat;
    private final NetworkChecker networkChecker;
    // MAC address 전송 여부, 앱 실행시마다 첫 sync 때 전송한다.
    private boolean macAddressSent;
    private boolean macAddressCollectible;

    public SDKLogSync(SyncDBRepository syncDBRepository,
                      SDKLogDBRepository sdkLogDBRepository,
                      OxHttpClient httpClient,
                      TimeService timeService,
                      DateFormat dateFormat,
                      NetworkChecker networkChecker,
                      final int syncIntervalInSeconds,
                      final boolean macAddressCollectible) {
        this.syncDBRepository = syncDBRepository;
        this.sdkLogDBRepository = sdkLogDBRepository;
        this.httpClient = httpClient;
        this.timeService = timeService;
        this.dateFormat = dateFormat;
        this.networkChecker = networkChecker;
        this.macAddressCollectible = macAddressCollectible;

        int interval = (syncIntervalInSeconds < 1) ? 300 : syncIntervalInSeconds;
        OxTasks.execRepeatly(Period.seconds(interval), ExceptionFree.executable(this::sync));
    }

    void sync() {
        LOG.info("SDKLogSync run scheduled.");
        Date current = timeService.now();

        List<SDKLog> notSynced = sdkLogDBRepository.find(current);

        if (!notSynced.isEmpty()) {
            requestSync(notSynced, current);
        } else {
            syncDBRepository.updateLastSyncDateTimeForSDKLog(current);
        }

        // MAC 주소는 전송가능한 경우, 앱 실행시 한번만 전송한다.
        if (macAddressCollectible && !macAddressSent) {
            requestMacAddress();
        }
    }

    /**
     * 일반적인 SDK Log 전송
     * @param sdkLogs
     * @param current
     */
    void requestSync(List<SDKLog> sdkLogs, Date current) {
        ExceptionFree.exec(() -> {
            if (networkChecker.wifiConnected()) {
                String body = convertSDKLogListToJson(sdkLogs);
                if ("[]".equals(body.trim())) return;
                Status status = httpClient.requestFor("/sdklog")
                        .body(body)
                        .request(Method.POST)
                        .status();

                if (status.success()) {
                    LOG.info("SDKLog synced successfully.");
                    sdkLogDBRepository.remove(current);
                    syncDBRepository.updateLastSyncDateTimeForSDKLog(current);
                } else {
                    LOG.error("fail to submit SDKLog - {}", status.code());
                }
            } else {
                LOG.info("WiFi is not activated");
            }
        }, OxErrors.wrapAndNotify(e -> new TamraException("Fail to submit SDKLog", e)));
    }

    String convertSDKLogListToJson(List<SDKLog> sdkLogs) {
        JSONArray jsonArray = OxIterable.from(sdkLogs).map(log -> {
            JSONObject obj = new JSONObject();
            ExceptionFree.exec(() -> {
                obj.put("action", log.action());
                obj.put("param", log.param());
                obj.put("issuedAt", dateFormat.format(log.issuedAt()));
            }, OxErrors.wrapAndNotify(e -> new TamraException(e.getMessage())));
            return obj;
        }).reduce(JSONArray::put, new JSONArray());
        return jsonArray.toString();
    }

    /**
     * MacAddress 주소 전송
     */
    void requestMacAddress() {
        ExceptionFree.exec(() -> {
            if (networkChecker.wifiConnected()) {
                String body = macAddressToJson();
                if ("[]".equals(body.trim())) return;
                Status status = httpClient.requestFor("/sdklog")
                        .body(body)
                        .request(Method.POST)
                        .status();

                if (status.success()) {
                    macAddressSent = true;

                    LOG.info("MA sent successfully.");
                } else {
                    LOG.error("fail to submit MA - {}", status.code());
                }
            } else {
                LOG.info("WiFi is not activated");
            }
        }, OxErrors.wrapAndNotify(e -> new TamraException("Fail to submit MA", e)));
    }

    String macAddressToJson() {
        JSONArray jsonArray = new JSONArray();
        JSONObject obj = new JSONObject();
        try {
            obj.put("action", "macAddresses");
            obj.put("param", mapToQueryString(networkChecker.getHardwareAddresses()));
            obj.put("issuedAt", dateFormat.format(timeService.now()));
            jsonArray.put(obj);
            return jsonArray.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    String mapToQueryString(Map<String, String> origin) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String,String> entry : origin.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s", entry.getKey(), entry.getValue()));
        }
        return sb.toString();
    }
}
