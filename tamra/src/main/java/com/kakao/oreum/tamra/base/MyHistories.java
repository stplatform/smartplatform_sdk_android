package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 조회된 Hitory 목록.
 *
 * <p>
 *     {@link History}에 대한 {@link Iterable} 구현이며 functional style coding을 위해 설계됨.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface MyHistories extends Iterable<History> {
    /**
     * 현재 정렬 기준으로 max 개수만큼 history를 포함하는 myHistories.
     *
     * @param max 최대 개수.
     * @return myHistories.
     */
    MyHistories limit(int max);

    /**
     * 특정 조건으로 history를 filter.
     *
     * @param filter 필터 조건.
     * @return filter에 포함된 history들만 포함하는 myHistories.
     */
    MyHistories filter(Predicate<? super History> filter);

    /**
     * 모든 history가 predicate에 만족하는지 검사.
     *
     * @param predicate 조건.
     * @return 모든 history이 predicate을 만족하면 true, 아니면 false.
     */
    boolean all(Predicate<? super History> predicate);

    /**
     * 어떤 history라도 predicate에 만족하는지 검사.
     *
     * @param predicate 조건
     * @return history 중 하나라도 predicate을 만족하면 true, 아니면 false.
     */
    boolean any(Predicate<? super History> predicate);

    /**
     * 현재 정렬 기준으로 첫 history.
     *
     * @return
     */
    Optional<History> first();

    /**
     * 현재 정렬 기준으로 마지막 history.
     *
     * @return
     */
    Optional<History> last();

    Optional<History> get(int position);

    /**
     * iterable을 정렬한다.
     *
     * @param comparator 정렬시 사용할 comparator.
     * @return comparator로 정렬된 myHistories.
     */
    MyHistories orderBy(Comparator<? super History> comparator);

    /**
     * history list.
     *
     * @return history list.
     */
    List<History> toList();

    /**
     * set으로 변환.
     *
     * @return history 집합.
     */
    Set<History> toSet();

    /**
     * history를 index 기준으로 분류하는 map 생성.
     *
     * @param indexFunction index 함수
     * @param <T>           index 객체 클래스.
     * @return index를 key로, history collection 을 value로 하는 map.
     */
    <T> Map<T, Collection<History>> index(Function<? super History, ? extends T> indexFunction);

    /**
     * {@link History} 객체를 key로 하는 map으로 변환한다.
     *
     * @param valueFunction value 함수.
     * @param <T>           value 객체 클래스.
     * @return history를 key로하는 map 객체.
     */
    @Nonnull
    <T> Map<History, T> toMap(Function<? super History, ? extends T> valueFunction);

    /**
     * {@link History}를 다른 객체의 {@link Collection}으로 변환한다.
     *
     * @param mapFunction 변환 함수
     * @param <T>         변환 결과 객체 클래스
     * @return 변환 객체의 collection.
     */
    @Nonnull
    <T> Collection<T> transform(Function<? super History, ? extends T> mapFunction);

    /**
     * 각 {@link History}에 대해 작업을 수행한다.
     *
     * @param action 수행할 작업.
     */
    void foreach(Consumer<? super History> action);

    /**
     * history 갯수.
     *
     * @return size
     */
    int size();

    /**
     * @return empty 여부
     */
    boolean isEmpty();
}
