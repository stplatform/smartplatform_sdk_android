package com.kakao.oreum.tamra.error;

import com.kakao.oreum.common.function.XPredicate;
import com.kakao.oreum.common.function.XPredicates;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public final class TamraErrorFilters {

    private TamraErrorFilters() { /* DO NOT CREATE INSTANCE */ }


    /**
     * @param date 기준 시각
     * @return date 이후에 발생된 error들만 포함하는 필터.
     */
    public static XPredicate<TamraError> occurredSince(Date date) {
        return XPredicates.of(te -> te.occurredAt().after(date));
    }

    /**
     * @param date 기준 시각
     * @return date 이전에 발생된 error들만 포함하는 필터
     */
    public static XPredicate<TamraError> occurredBefore(Date date) {
        return XPredicates.of(te -> te.occurredAt().before(date));
    }

    /**
     * @param exceptionClass exception class
     * @return 예외가 exceptionClass인 error만 포함하는 필터.
     */
    public static XPredicate<TamraError> causedBy(Class<? extends Exception> exceptionClass) {
        return XPredicates.of(te -> exceptionClass.isInstance(te.causedBy()));
    }

}
