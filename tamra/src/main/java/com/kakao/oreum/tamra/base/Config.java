package com.kakao.oreum.tamra.base;

import android.content.Context;
import android.os.Build;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.tamra.Tamra;

/**
 * SDK 설정.
 *
 * <p>
 * Tamra SDK 초기화에서 사용하는 객체로 Tamra의 설정 정보를 담는다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Public
public final class Config {
    private final Context context;
    private final String appkey;
    private final Profile profile;
    private boolean simulation;
    private boolean automaticMonitoring;
    private boolean macAddressCollectible = true;
    private int sdkLogSyncIntervalInSeconds = 60;
    private String deviceNameForSimulation;
    private ObserverSensitivity observerSensitivity = ObserverSensitivity.Balanced;

    private Config(Context context, String appkey, Profile profile) {
        this.context = context.getApplicationContext();
        this.appkey = appkey;
        this.profile = profile;
    }

    /**
     * 릴리즈용 config 생성.
     *
     * <code>Production</code> 환경에서 동작하는 SDK을 위한 config를 생성한다.
     *
     * @param context applicationContext
     * @param appkey  발급받은 appKey
     * @return config
     */
    public static Config forRelease(Context context, String appkey) {
        return new Config(context, appkey, Profile.PRODUCTION);
    }

    /**
     * 테스트용 config 생성.
     *
     * <code>Testing</code> 환경에서 동작하는 SDK를 위한 config를 생성한다.
     *
     * @param context applicationContext
     * @param appkey  발급받은 appkey.
     * @return config
     */
    public static Config forTesting(Context context, String appkey) {
        return new Config(context, appkey, Profile.TEST);
    }

    /**
     * SDK 내부 테스트용 factory method.
     *
     * @param context applicationContext
     * @param appkey  발급된 appkey
     * @param profile 환경 profile
     * @return
     */
    @Accessibility.InternalTesting
    public static Config createFor(Context context, String appkey, Profile profile) {
        return new Config(context, appkey, profile);
    }

    /**
     * Location Simulation 모드 활성화.
     *
     * @return config
     * @since 0.9.0
     */
    public Config onSimulation() {
        return onSimulation(Build.MODEL);
    }

    /**
     * Location Simulation 모드 활성화.
     *
     * @param deviceName Location Simulation 도구에 표시하고 싶은 기기명
     * @return config
     * @since 0.9.0
     */
    public Config onSimulation(String deviceName) {
        this.simulation = true;
        this.deviceNameForSimulation = deviceName;
        return this;
    }

    /**
     * 이벤트 감지에 대한 민감도 설정
     *
     * @return config
     * @since 0.10.0
     */
    public Config observerSensitivity(ObserverSensitivity observerSensitivity) {
        this.observerSensitivity = observerSensitivity;
        return this;
    }

    /**
     * 자동 모니터링 기능 활성화
     *
     * @return
     * @since 0.11.0
     */
    public Config onAutomaticMonitoring() {
        this.automaticMonitoring = true;
        return this;
    }

    /**
     * MAC 주소 자동 수집 끄도록 설정함. 기본 모드는 전송
     *
     * @return
     * @since 0.11.0
     */
    public Config dontCollectMacAddress() {
        this.macAddressCollectible = false;
        return this;
    }

    /**
     * SDK Log 전송 주기 설정
     *
     * <p>주의: 10 미만의 값은 무시되고 내부 설정값으로 진행된다. 설정하지 않을 경우 기본값은 60초 이다.</p>
     *
     * @return
     * @since 0.11.0
     */
    public Config sdkLogSyncIntervalInSeconds(int seconds) {
        this.sdkLogSyncIntervalInSeconds = seconds;
        return this;
    }

    /**
     * @return applicationContext
     */
    public Context context() {
        return context;
    }

    /**
     * @return applicationId.
     * @since 0.8.0
     */
    public String appId() {
        return context.getPackageName();
    }

    /**
     * @return appkey
     */
    public String appkey() {
        return appkey;
    }

    /**
     * @return profile
     */
    public Profile profile() {
        return profile;
    }

    /**
     * @return Location Simulation 모드 활성화 여부
     * @since 0.9.0
     */
    public boolean simulation() { return simulation; }

    /**
     * @return Location Simulation 도구에서 표시할 기기 이름.
     * @since 0.9.0
     */
    public String deviceName() { return deviceNameForSimulation; }

    /**
     * @return Observer에서 받을 이벤트 감지 민감도.
     * @since 0.10.0
     */
    public ObserverSensitivity observerSensitivity() { return observerSensitivity; }

    /**
     * @return 자동 모니터링 활성화 여부
     * @since 0.11.0
     */
    public boolean automaticMonitoring() { return automaticMonitoring; }

    /**
     * @return MAC 주소 자동 수집 여부 지정
     * @since 0.11.0
     */
    public boolean macAddressCollectible() { return macAddressCollectible; }

    /**
     * @return SDK Log 전송 주기
     * @since 0.11.0
     */
    public int sdkLogSyncIntervalInSeconds() { return sdkLogSyncIntervalInSeconds; }

}
