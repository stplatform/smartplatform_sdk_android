package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.ox.http.HttpCallException;
import com.kakao.oreum.ox.http.Method;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.http.Response;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.internal.data.data.RegionData;
import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;
import com.kakao.oreum.tamra.internal.data.parser.RegionDataParser;

import java.io.IOException;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public abstract class ApiConnnectedRegionRepository implements RegionRepository {
    private static final Logger LOG = getLogger(ApiConnnectedRegionRepository.class);
    protected final JsonDataParser<RegionData> parser = new RegionDataParser();
    private final OxHttpClient httpClient;

    public ApiConnnectedRegionRepository(OxHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    protected String getJsonForAllRegions() {
        LOG.info("TRACE> ApiConnectedRegionRepository#getJsonForAllRegions started");
        Response response;
        try {
            response = httpClient.requestFor("/regions")
                    .params("limit", 1000)
                    .request(Method.GET);
            return response.bodyAsString();
        } catch (HttpCallException | IOException e) {
            throw new RemoteApiException(e);
        } finally {
            LOG.info("TRACE> ApiConnectedRegionRepository#getJsonForAllRegions ended");
        }
    }
}
