package com.kakao.oreum.tamra.internal.http.common;

import android.content.Context;

import com.kakao.oreum.ox.collection.Lists;
import com.kakao.oreum.ox.http.Header;
import com.kakao.oreum.tamra.internal.fixture.ClientId;
import com.kakao.oreum.tamra.internal.fixture.UserAgent;

import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public final class CommonHeaders {
    private CommonHeaders() { /* DO NOT CREATE INSTANCE */ }

    public static List<Header> get(Context context) {
        return Lists.arrayList(
                Header.of("User-Agent", UserAgent.get()),
                Header.of("Content-Type", "application/json; charset=UTF-8"),
                Header.of("Oreum-CID", ClientId.get(context))
        );
    }
}
