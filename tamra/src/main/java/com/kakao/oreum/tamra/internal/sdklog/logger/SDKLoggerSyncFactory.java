package com.kakao.oreum.tamra.internal.sdklog.logger;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakao.oreum.infra.device.NetworkChecker;
import com.kakao.oreum.infra.persistence.sqlite.SDKLogDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.SyncDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.tamra.base.Config;

import java.text.DateFormat;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SDKLoggerSyncFactory {
    private static volatile SDKLogger sdkLogger;
    private static final Lock lock = new ReentrantLock(true);
    private static SystemTimeService timeService = new SystemTimeService();
    private static DateFormat dateFormat = new ISO8601DateFormat();

    private static final DummySDKLogger dummyLogger = new DummySDKLogger();

    private SDKLoggerSyncFactory() {
        /* DO NOT CREATE AN INSTANCE */
    }

    /**
     * SDKLogger 객체 생성
     *
     * @param config sdk configuration
     * @return
     */
    public static SDKLogger getLogger(Config config) {
        if (config.simulation()) {
            return dummyLogger;
        } else {
            return getSDKLogger(config.context());
        }
    }

    /**
     * SDKLogger 객체 생성
     *
     * @param context Android Application 레벨의 Context
     * @return
     */
    private static SDKLogger getSDKLogger(Context context) {
        if (sdkLogger == null) {
            lock.lock();
            try {
                sdkLogger = new PersistentSDKLogger(
                        new SDKLogDBRepository(new TamraDBHelper(context), dateFormat)
                        , timeService);
            } finally {
                lock.unlock();
            }
        }
        return sdkLogger;
    }

    /**
     * Simulation 모드 등의 경우 SDK Log를 쌓지 않을 경우 사용
     *
     * @return
     */
    public static SDKLogger getDummySDKLogger() {
        return dummyLogger;
    }

    /**
     * SDKLogSync 객체 생성
     *
     * @param context
     * @param httpClient
     * @param syncIntervalInSeconds
     * @return
     */
    public static SDKLogSync getSDKLogSync(Context context,
                                           OxHttpClient httpClient,
                                           int syncIntervalInSeconds,
                                           boolean macAddressCollectible) {
        SQLiteOpenHelper dbHelper = new TamraDBHelper(context);
        return new SDKLogSync(
                new SyncDBRepository(dbHelper, dateFormat),
                new SDKLogDBRepository(dbHelper, dateFormat),
                httpClient,
                timeService,
                dateFormat,
                new NetworkChecker(context),
                syncIntervalInSeconds,
                macAddressCollectible
        );
    }
}
