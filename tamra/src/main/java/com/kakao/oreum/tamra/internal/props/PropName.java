package com.kakao.oreum.tamra.internal.props;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public enum PropName {
    API_BASEURL("api.server.baseurl"),
    LOG_LEVEL("log.level");

    final String key;

    PropName(String key) {
        this.key = key;
    }
}
