package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * Predefined Regions.
 *
 * <p>
 * SDK release 시점에 등록된 모든 Region들을 상수로 제공하는 class.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
@Accessibility.Public
public final class Regions {
    private static final Region[] PRE_DEFINED = new Region[] {
            Region.of(2, "제주국제공항"),
            Region.of(3, "제주창조경제혁신센터"),
            Region.of(4, "중문관광단지"),
            Region.of(5, "KAKAO Space.2"),
            Region.of(6, "플레이케이팝 박물관"),
            Region.of(7, "제주 4.3길"),
            Region.of(10, "동문재래시장")
    };

    public static final Region 제주국제공항 = id(2);
    public static final Region 제주창조경제혁신센터 = id(3);
    public static final Region 중문관광단지 = id(4);
    public static final Region 카카오스페이스닷투 = id(5);
    public static final Region 플레이케이팝_박물관 = id(6);
    public static final Region 제주43길 = id(7);
    public static final Region 동문재래시장 = id(10);

    private Regions() { /* DO NOT CREATE INSTANCE */ }

    /**
     * 모든 사전 정의된 Region을 반환.
     *
     * <p>
     * <strong>주의</strong> 호출되는 시점에 서버 등록된 모든 region을 리턴하는 것이 아니라, 이 클래스에 정의된 region 전체를 리턴한다.
     * </p>
     *
     * @return all regions.
     */
    public static Region[] all() {
        Region[] values = new Region[PRE_DEFINED.length];
        System.arraycopy(PRE_DEFINED, 0, values, 0, PRE_DEFINED.length);
        return values;
    }

    private static Region id(long id) {
        for (Region region : PRE_DEFINED) {
            if (region.id() == id) {
                return region;
            }
        }
        return Region.UNKNOWN;
    }


}
