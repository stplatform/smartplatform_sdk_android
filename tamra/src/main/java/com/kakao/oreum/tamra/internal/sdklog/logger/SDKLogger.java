package com.kakao.oreum.tamra.internal.sdklog.logger;

import com.kakao.oreum.tamra.base.SDKLogParams;

/**
 * SDK 사용로그 인터페이스
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface SDKLogger {
    SDKLogger log(String action, SDKLogParams params);
    SDKLogger addStaticParams(String key, String value);
}
