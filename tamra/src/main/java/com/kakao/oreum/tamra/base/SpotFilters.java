package com.kakao.oreum.tamra.base;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.common.function.XPredicate;
import com.kakao.oreum.common.function.XPredicates;
import com.kakao.oreum.ox.collection.OxIterable;

/**
 * {@link Spot} 필터링을 위한 {@link com.kakao.oreum.common.function.Predicate}의 factory method 제공 클래스.
 *
 * <p>
 * {@link Spot} 객체의 속성 별 filter를 제공하는데, 리턴 객체가 {@link XPredicate}이므로
 * {@link XPredicate#and(Predicate)}, {@link XPredicate#or(Predicate)}, {@link XPredicate#negate()}를 이용하여
 * 필터들의 조합이 가능하다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
@Accessibility.Public
public final class SpotFilters {
    private SpotFilters() { /* DO NOT CREATE INSTANCE */ }

    /**
     * @param region 필터 조건의 region
     * @return 특정 region에 속하는 spot을 필터링하는 predicate
     */
    public static XPredicate<Spot> belongsTo(Region region) {
        return XPredicates.of(spot -> spot.region().equals(region));
    }

    /**
     * 특정 spots에 포함되는 spot만 포함할 predicate를 리턴한다.
     *
     * argument로 {@link Iterable} 타입을 받는 이유는 {@link NearbySpots}을 argument로 사용하기 위해서임.
     *
     * @param spots 필터에 포함시킬 spot 목록.
     * @return spots에 포함된 spot에 대해서 true를 리턴하는 predicate.
     */
    public static XPredicate<Spot> oneOf(Iterable<Spot> spots) {
        return XPredicates.of(spot ->
                OxIterable.from(spots)
                        .any(spot::equals)
        );
    }

    /**
     * @param ids 필터링 후 포함시킬 id 목록
     * @return id가 ids에 포함되는 spot만 true를 리턴하는 predicate.
     */
    public static XPredicate<Spot> oneOf(long... ids) {
        return XPredicates.of(spot ->
                OxIterable.from(ids)
                        .any(id -> id.equals(spot.id())));
    }

    /**
     * @param isIndoor indoor 여부.
     * @return isIndoor의 값과 spot.indoor()이 같은 spot만 포함하는 필터.
     */
    public static XPredicate<Spot> indoor(boolean isIndoor) {
        return XPredicates.of(spot -> spot.indoor() == isIndoor);
    }

    /**
     * @param isMovable movable 여부.
     * @return isMovable의 값과 spot.movable()이 같은 spot만 포함하는 필터.
     */
    public static XPredicate<Spot> movable(boolean isMovable) {
        return XPredicates.of(spot -> spot.movable() == isMovable);
    }

    /**
     * @param proximity 체크에 사용할 proimxity
     * @return spot.proximity()가 proximity가 같은 spot만 포함하는 필터.
     */
    public static XPredicate<Spot> proximity(Proximity proximity) {
        return XPredicates.of(spot -> spot.proximity() == proximity);
    }

    /**
     * @param minProximity 체크에 사용할 proximity
     * @return spot.proximity()가 minProximity보다 같거나 큰(더 먼) spot만 포함하는 필터.
     */
    public static XPredicate<Spot> minProximity(Proximity minProximity) {
        return XPredicates.of(spot -> spot.proximity().compareTo(minProximity) >= 0);
    }

    /**
     * @param maxProximity 체크에 사용할 proximity
     * @return spot.proximity()가 maxProximity보다 같거나 작은(더 가까운) spot만 포함하는 필터.
     */
    public static XPredicate<Spot> maxProximity(Proximity maxProximity) {
        return XPredicates.of(spot -> spot.proximity().compareTo(maxProximity) <= 0);
    }

    /**
     * @param accuracy 체크에 사용할 accuracy값.
     * @return spot.accurarcy()가 accuracy보다 같거나 큰 spot만 포함하는 필터.
     */
    public static XPredicate<Spot> minAccuracy(double accuracy) {
        return XPredicates.of(spot -> spot.accuracy() >= accuracy);
    }

    /**
     * @param accuracy 체크에 사용할 accuracy값.
     * @return spot.accurarcy()가 accuracy보다 같거나 작은 spot만 포함하는 필터.
     */
    public static XPredicate<Spot> maxAccuracy(double accuracy) {
        return XPredicates.of(spot -> spot.accuracy() <= accuracy);
    }

    /**
     * @return null인 값은 exclude 하는 필터.
     */
    public static XPredicate<Spot> notNull() {
        return XPredicates.of(s -> s != null);
    }

}
