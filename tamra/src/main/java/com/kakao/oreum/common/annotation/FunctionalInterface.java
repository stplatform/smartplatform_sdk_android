package com.kakao.oreum.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Functional interface임을 나타내기 위한 annotation.
 *
 * <p>
 * JDK8의 <code>java.lang.FuntionalInterface</code>와 동일한 역할.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FunctionalInterface {
}
