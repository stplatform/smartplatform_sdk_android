/**
 * oreum common 패키지.
 *
 * <p>
 * SDK 기능과 별개로 일반적인 목적의 interface, annotation, class 들을 정의하기 위한 패키지다.
 * </p>
 */
package com.kakao.oreum.common;