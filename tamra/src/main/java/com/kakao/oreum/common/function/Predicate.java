package com.kakao.oreum.common.function;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * Represents a predicate (boolean-valued function) of one argument.
 * <p>
 * JDK8의 <code>java.util.function.Predicate</code>에 해당하는 interface.
 * (<a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html</a>)
 *
 * static method와 default method들은 {@link XPredicates}와 {@link XPredicate}에 나누어 구현되어 있다.
 * </p>
 *
 * @param <T> tye type of the input to the predicate
 * @author cocoon.tf@kakaocorp.com
 * @see XPredicate
 * @see XPredicates
 * @since 0.5.0
 */
@FunctionalInterface
public interface Predicate<T> {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param t the input argument
     * @return true if the input argument matches the predicate, otherwise false
     */
    boolean test(T t);

}
