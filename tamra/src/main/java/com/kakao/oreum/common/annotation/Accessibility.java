package com.kakao.oreum.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * SDK의 class, interface, method 등에 대한
 * SDK 외부에서의 접근가능성을 표기하위 해서 사용하는 annotations.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
@Accessibility.Public
@Stability.Evolving
public class Accessibility {
    private Accessibility() { /* DO NOT USE */ }

    /**
     * SDK 외부에서 직접 접근 및 사용할 수 있는 클래스, 메소드에 대한 annotation.
     *
     * 이 어노테이션이 붙은 클래스, 인터페이스는 사용자가 직접적으로 접근하거나 사용할 수 있도록 설계되어야 함.
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Public {
    }

    /**
     * SDK 내부에서만 사용하는 클래스 및 메소드에 사용하는 annotation.
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Private {
    }

    /**
     * SDK 내부에서 테스트를 위해 사용하는 비공개 메소드에 사용한 annotation.
     *
     * package 접근자 사용을 권하지만 안될 경우에 반드시 이 annotation을 붙일 것.
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface InternalTesting {
    }
}