package com.kakao.oreum.common.function;

/**
 * JDK8의 <code>java.util.function.Predicate</code>의 default 메소드 구현 클래스.
 *
 * <p>
 * 참고 : <a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html</a>
 * </p>
 * <p>
 * static method는 {@link XPredicates}에 구현되어 있다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see Predicate
 * @see XPredicates
 * @since 0.7.0
 */
public abstract class XPredicate<T> implements Predicate<T> {

    /**
     * Returns a composed predicate that represents a short-circuiting logical AND of this predicate and another.
     * When evaluating the composed predicate, if this predicate is false, then the other predicate is not evaluated.
     *
     * <p>
     * Any exceptions thrown during evaluation of either predicate are relayed to the caller;
     * if evaluation of this predicate throws an exception, the other predicate will not be evaluated.
     * </p>
     *
     * @param other a predicate that will be logically-ANDed with this predicate
     * @return a composed predicate that represents the short-circuiting logical AND of this predicate and the other predicate
     * @throws NullPointerException if other is null
     */
    public final XPredicate<T> and(Predicate<? super T> other) {
        return XPredicates.of(t -> test(t) && other.test(t));
    }

    /**
     * Returns a predicate that represents the logical negation of this predicate.
     *
     * @return a predicate that represents the logical negation of this predicate
     */
    public final XPredicate<T> negate() {
        return XPredicates.of(t -> !test(t));
    }

    /**
     * Returns a composed predicate that represents a short-circuiting logical OR of this predicate and another.
     * When evaluating the composed predicate, if this predicate is true, then the other predicate is not evaluated.
     *
     * <p>
     * Any exceptions thrown during evaluation of either predicate are relayed to the caller; if evaluation of this predicate throws an exception, the other predicate will not be evaluated.
     * </p>
     *
     * @param other a predicate that will be logically-ORed with this predicate
     * @return a composed predicate that represents the short-circuiting logical OR of this predicate and the other predicate
     * @throws NullPointerException if other is null
     */
    public final XPredicate<T> or(Predicate<? super T> other) {
        return XPredicates.of(t -> test(t) || other.test(t));
    }
}
