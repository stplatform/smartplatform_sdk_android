package com.kakao.oreum.common.function;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@FunctionalInterface
public interface Reducer<S, T> {

    S reduce(S s, T t);

}
