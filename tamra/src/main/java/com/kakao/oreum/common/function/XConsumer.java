package com.kakao.oreum.common.function;

/**
 * JDK8의 <code>java.util.function.Conumser</code>의 default 메소드를 구현하는 클래스.
 * <p>
 * 참고 : <a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Consumer.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Consumer.html</a>
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see XComparators
 * @see Consumer
 * @since 0.7.0
 */
public abstract class XConsumer<T> implements Consumer<T> {

    /**
     * Returns a composed Consumer that performs, in sequence, this operation followed by the after operation.
     * If performing either operation throws an exception, it is relayed to the caller of the composed operation.
     * If performing this operation throws an exception, the after operation will not be performed.
     *
     * @param after the operation to perform after this operation
     * @return a composed Consumer that performs in sequence this operation followed by the after operation
     * @throws NullPointerException if after is null
     */
    public final XConsumer<T> andThen(Consumer<? super T> after) {
        return XConsumers.of(t -> {
            accept(t);
            after.accept(t);
        });
    }
}
