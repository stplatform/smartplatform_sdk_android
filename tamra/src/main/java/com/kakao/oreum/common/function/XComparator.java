package com.kakao.oreum.common.function;


import java.util.Comparator;

/**
 * JDK8의 <code>java.util.Comparator</code>에 대한 default 메소드 구현 클래스.
 *
 * <p>
 * 참고 : <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html">https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html</a>
 * </p>
 * <p>
 * static method들은 {@link XComparators}에 정의되어 있다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see XComparators
 * @see Comparator
 * @since 0.7.0
 */
public abstract class XComparator<T> implements Comparator<T> {
    /**
     * Returns a comparator that imposes the reverse ordering of this comparator.
     *
     * @return a comparator that imposes the reverse ordering of this comparator.
     */
    public final XComparator<T> reversed() {
        return XComparators.of((lhs, rhs) -> compare(rhs, lhs));
    }

    /**
     * Returns a lexicographic-order comparator with another comparator.
     * If this Comparator considers two elements equal, i.e. compare(a, b) == 0,
     * other is used to determine the order. The returned comparator is serializable if the specified comparator is also serializable.
     *
     * @param other the other comparator to be used when this comparator compares two objects that are equal.
     * @return a lexicographic-order comparator composed of this and then the other comparator
     * @throws NullPointerException if the argument is null.
     */
    public final XComparator<T> thenComparing(Comparator<? super T> other) {
        if (other == null) {
            throw new NullPointerException();
        }
        return XComparators.of((lhs, rhs) -> {
            int c = this.compare(lhs, rhs);
            return c == 0 ? other.compare(lhs, rhs) : c;
        });
    }

    /**
     * Returns a lexicographic-order comparator with a function that extracts a key to be compared with the given Comparator.
     *
     * @param keyExtractor 다음 정렬 조건의 comparable 추출기
     * @param <U>          key 타입
     * @return 조합된 comparator.
     * @throws NullPointerException if the argument is null.
     * @see XComparators#comparing(Function)
     * @see #thenComparing(Comparator)
     */
    public final <U extends Comparable<? super U>> XComparator<T> thenComparing(Function<? super T, ? extends U> keyExtractor) {
        if (keyExtractor == null) {
            throw new NullPointerException();
        }
        return thenComparing(XComparators.comparing(keyExtractor));
    }

    /**
     * Returns a lexicographic-order comparator with a function that extracts a key to be compared with the given Comparator.
     *
     * @param keyExtractor  the function used to extract the sort key
     * @param keyComparator the Comparator used to compare the sort key
     * @param <U>           the type of the sort key
     * @return a lexicographic-order comparator composed of this comparator and then comparing on the key extracted by the keyExtractor function
     * @throws NullPointerException if either argument is null.
     * @see XComparators#comparing(Function, Comparator)
     * @see #thenComparing(Comparator)
     */
    public final <U> XComparator<T> thenComparing(Function<? super T, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        if (keyExtractor == null || keyComparator == null) {
            throw new NullPointerException();
        }
        return thenComparing(XComparators.comparing(keyExtractor, keyComparator));
    }

}
