package com.kakao.oreum.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 변경불가능한(immutable) 객체임을 표시하기 위한 annotation.
 *
 * <p>
 * method에 붙은 경우 return 객체가 immutable 객체임을 의미하고,
 * class에 붙인 경우 해당 객체 자체가 immutable 객체임을 나타낸다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Immutable {
}
