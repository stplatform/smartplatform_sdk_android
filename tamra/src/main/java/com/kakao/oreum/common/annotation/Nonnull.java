package com.kakao.oreum.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * method의 파라메터나 리턴값이 NotNull 임을 표기하기 위한 annotation.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.METHOD})
public @interface Nonnull {
}
