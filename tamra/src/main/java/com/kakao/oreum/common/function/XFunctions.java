package com.kakao.oreum.common.function;

/**
 * JDK8의 <code>java.util.function.Function</code>의 static 메소드 구현 클래스.
 *
 * <p>
 * 참고: <a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html</a>
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public final class XFunctions {
    private XFunctions() { /* DO NOT CREATE INSTANCE */ }

    /**
     * {@link XFunction}에 대한 factory method.
     *
     * @param function funtion to wrap
     * @param <T>      the type of the input object to the function
     * @param <R>      the type of the output object to the function
     * @return a XFunction to wrap function
     */
    public static <T, R> XFunction<T, R> of(Function<? super T, ? extends R> function) {
        return new XFunction<T, R>() {
            @Override
            public R apply(T t) {
                return function.apply(t);
            }
        };
    }

    /**
     * Returns a function that always returns its input argument.
     *
     * @param <T> the type of the input and output objects to the function
     * @return a XFunction that always returns its input argument
     */
    public static <T> XFunction<T, T> identity() {
        return of(t -> t);
    }
}
