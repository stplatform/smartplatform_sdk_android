package com.kakao.oreum.common.function;

import java.util.Comparator;

/**
 * JDK8의 <code>java.util.Comparator</code>에 대한 default 메소드 구현 클래스.
 *
 * <p>
 * 참고 : <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html">https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html</a>
 * </p>
 *
 * <p>
 * default method들은 {@link XComparator}에 정의되어 있다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see XComparator
 * @see Comparator
 * @since 0.7.0
 */
public final class XComparators {
    private XComparators() {/* DO NOT CREATE INSTANCE */ }

    /**
     * Create {@link XComparator} wrapping Comparator.
     *
     * @param comparator comparator
     * @param <T>        the type of objects that may be compared by this comparator.
     * @return XComparator wrapping comparator
     * @throws NullPointerException if the arguement is null.
     */
    public static <T> XComparator<T> of(Comparator<? super T> comparator) {
        if (comparator == null) {
            throw new NullPointerException();
        }
        return new XComparator<T>() {
            @Override
            public int compare(T lhs, T rhs) {
                return comparator.compare(lhs, rhs);
            }
        };
    }

    /**
     * Returns a comparator that compares Comparable objects in natural order.
     *
     * <p>
     * The returned comparator is serializable and throws NullPointerException when comparing null.
     * </p>
     *
     * @param <T> the {@link Comparable} type of element to be compared
     * @return a comparator that imposes the natural ordering on Comparable objects.
     * @see Comparable
     */
    public static <T extends Comparable<? super T>> XComparator<T> naturalOrder() {
        return of((lhs, rhs) -> lhs.compareTo(rhs));
    }

    /**
     * Accepts a function that extracts a Comparable sort key from a type T, and returns a Comparator&lt;T&gt; that compares by that sort key.
     *
     * <p>
     * The returned comparator is serializable if the specified function is also serializable.
     * </p>
     *
     * @param keyExtractor the function used to extract the Comparable sort key
     * @param <T>          the type of element to be compared
     * @param <U>          the type of the Comparable sort key
     * @return a comparator that compares by an extracted key
     * @throws NullPointerException if the argument is null
     */
    public static <T, U extends Comparable<? super U>> XComparator<T> comparing(Function<? super T, ? extends U> keyExtractor) {
        if (keyExtractor == null) {
            throw new NullPointerException();
        }
        return of((lhs, rhs) -> keyExtractor.apply(lhs).compareTo(keyExtractor.apply(rhs)));
    }

    /**
     * Accepts a function that extracts a sort key from a type T, and returns a Comparator&lt;T&gt;
     * that compares by that sort key using the specified Comparator.
     *
     * <p>
     * The returned comparator is serializable if the specified function and comparator are both serializable.
     * </p>
     *
     * @param keyExtractor  the function used to extract the sort key
     * @param keyComparator the Comparator used to compare the sort key
     * @param <T>           the type of element to be compared
     * @param <U>           the type of the sort key
     * @return a comparator that compares by an extracted key using the specified Comparator
     * @throws NullPointerException if either argument is null
     */
    public static <T, U> XComparator<T> comparing(Function<? super T, ? extends U> keyExtractor, Comparator<? super U> keyComparator) {
        if (keyExtractor == null || keyComparator == null) {
            throw new NullPointerException();
        }
        return of((lhs, rhs) -> keyComparator.compare(keyExtractor.apply(lhs), keyExtractor.apply(rhs)));
    }

    /**
     * Returns a null-friendly comparator that considers null to be less than non-null. When both are null, they are considered equal.
     * If both are non-null, the specified Comparator is used to determine the order. If the specified comparator is null, then the returned comparator considers all non-null values to be equal.
     *
     * <p>
     * The returned comparator is serializable if the specified comparator is serializable.
     * </p>
     *
     * @param comparator a Comparator for comparing non-null values
     * @param <T>        the type of the elements to be compared
     * @return a comparator that considers null to be less than non-null, and compares non-null objects with the supplied Comparator.
     */
    public static <T> XComparator<T> nullsFirst(Comparator<? super T> comparator) {
        return of((lhs, rhs) -> {
            if (lhs != null && rhs != null) {
                return comparator.compare(lhs, rhs);
            }
            return (lhs == null ? -1 : 1) + (rhs == null ? 1 : -1);
        });
    }

    /**
     * Returns a null-friendly comparator that considers null to be greater than non-null.
     * When both are null, they are considered equal. If both are non-null, the specified Comparator is used to determine the order.
     * If the specified comparator is null, then the returned comparator considers all non-null values to be equal.
     *
     * <p>
     * The returned comparator is serializable if the specified comparator is serializable.
     * </p>
     *
     * @param comparator a Comparator for comparing non-null values
     * @param <T>        the type of the elements to be compared
     * @return a comparator that considers null to be greater than non-null, and compares non-null objects with the supplied Comparator.
     */
    public static <T> XComparator<T> nullsLast(Comparator<? super T> comparator) {
        return of((lhs, rhs) -> {
            if (lhs != null && rhs != null) {
                return comparator.compare(lhs, rhs);
            }
            return (lhs == null ? 1 : -1) + (rhs == null ? -1 : 1);
        });
    }

    /**
     * Returns a comparator that imposes the reverse of the natural ordering.
     *
     * <p>
     * The returned comparator is serializable and throws {@link NullPointerException} when comparing null.
     * </p>
     *
     * @param comparator 원래 comparator
     * @param <T>        the {@link Comparable} type of element to be compared
     * @return a comparator that imposes the reverse of the natural ordering on Comparable objects.
     * @see Comparable
     */
    public static <T> XComparator<T> reverseOrder(Comparator<? super T> comparator) {
        return of((lhs, rhs) -> comparator.compare(rhs, lhs));
    }
}
