/**
 * annotation package.
 *
 * <p>
 * SDK 내부 사용을 전제로 하는 annotation들을 위한 패키지.
 * 외부 library에 대한 의존성을 줄이기 위해 표준 annotation이 있더라도 별도 정의해서 사용한다.
 * </p>
 *
 * @since 0.2.0
 */
package com.kakao.oreum.common.annotation;