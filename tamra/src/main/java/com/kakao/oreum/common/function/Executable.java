package com.kakao.oreum.common.function;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * No-Argument, No-Return function.
 *
 * <p>
 * {@link Runnable}은 java의 {@link Thread}에서의 사용을 목적으로 정의된 interface이므로
 * No argument, no return functional interface는 이 클래스를 사용한다.
 * </p>
 * <p>
 * {@link Runnable} 객체가 필요한 경우 interface를 wrapping해서 사용하고, SDK 내부에서는 이 인터페이스로 funtional style을 구성한다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
@FunctionalInterface
public interface Executable {

    void exec();

}
