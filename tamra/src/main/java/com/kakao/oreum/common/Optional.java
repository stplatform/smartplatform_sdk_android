package com.kakao.oreum.common;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.common.function.Supplier;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.NoSuchElementException;


/**
 * null 회피를 위한 wrapper 클래스.
 *
 * <p>
 * JDK8의 <code>java.util.Optional</code>에 해당하는 클래스.
 * (<a href="https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html">https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html</a>)
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Optional<T> implements Serializable {

    private final T reference;

    private Optional(T reference) {
        this.reference = reference;
    }

    /**
     * null을 reference하는 빈 객체를 생성.
     *
     * @param <T> generic type.
     * @return 빈 객체.
     */
    @Nonnull
    public static <T> Optional<T> empty() {
        return new Optional<>(null);
    }

    /**
     * value 값을 reference하는 객체를 생성.
     *
     * value는 <code>null</code>이 아니어야 한다.
     *
     * @param value wrap할 객체. null이어서는 안된다.
     * @param <T>   value type.
     * @return value를 wrap하는 객체.
     * @throws IllegalArgumentException value가 null일 경우 발생.
     */
    @Nonnull
    public static <T> Optional<T> of(@Nonnull T value) {
        if (value != null) {
            return new Optional<>(value);
        }
        throw new IllegalArgumentException("value MUST NOT be null.");
    }

    /**
     * null일 수 있는 value에 대한 reference 객체 생성.
     *
     * {@link #of(Object)}와의 차이는 null을 value로 받을 수 있다는 점.
     *
     * @param value wrap할 객체. (null일 수 있다)
     * @param <T>   value type.
     * @return value를 reference하는 객체.
     */
    public static <T> Optional<T> ofNullable(@Nullable T value) {
        return new Optional<>(value);
    }

    /**
     * reference 객체에 대한 getter.
     *
     * 만약 <code>null</code>을 reference하고 있다면 예외가 발생한다.
     *
     * @return reference하는 객체를 리턴.
     * @throws NoSuchElementException null을 reference하는 경우 발생.
     */
    public T get() {
        if (isPresent()) {
            return reference;
        }
        throw new NoSuchElementException();
    }

    /**
     * filter.
     *
     * @param predicate filter condition.
     * @return reference가 null이거나 predicate 결과가 false이면 {@link Optional#empty()}을 리턴하고, 아니면 자신을 리턴한다.
     */
    public Optional<T> filter(Predicate<? super T> predicate) {
        return isPresent() && predicate.test(reference) ? this : Optional.<T>empty();
    }


    /**
     * 다른 type의 {@link Optional} 객체로 매핑한다.
     *
     * @param mapper 변환 함수.
     * @param <U>    변환 결과 타입.
     * @return 변환 결과에 대한 Optional 객체. empty객체인 경우 empty 객체를 반환한다.
     */
    public <U> Optional<U> flatMap(Function<? super T, Optional<U>> mapper) {
        Optional<U> mapped = isPresent() ? mapper.apply(reference) : Optional.<U>empty();
        return mapped != null ? mapped : Optional.<U>empty();
    }

    @Override
    public int hashCode() {
        return isPresent() ? reference.hashCode() : 0;
    }

    /**
     * reference 객체가 존재하는지 테스트.
     *
     * @return reference가 null이면 false, 아니면 true.
     */
    public boolean isPresent() {
        return reference != null;
    }

    /**
     * reference 객체가 null이 아니면 해당 객체에 대한 처리.
     *
     * {@link #isPresent()}일 때만 <code>consumer</code>가 수행된다.
     *
     * @param consumer 값 객체 처리 함수.
     */
    public void ifPresent(Consumer<? super T> consumer) {
        if (isPresent()) {
            consumer.accept(reference);
        }
    }

    /**
     * 값을 다른 타입의 값으로 매핑.
     *
     * @param mapper 변환 함수
     * @param <U>    변환 결과 타입
     * @return empty이면 {@link Optional#empty()}가 리턴되고, 아니면 mapper 적용 결과에 대한 Optional 객체가 리턴된다.
     */
    public <U> Optional<U> map(Function<? super T, ? extends U> mapper) {
        U mapped = isPresent() ? mapper.apply(reference) : null;
        return Optional.ofNullable(mapped);
    }

    /**
     * get or else.
     *
     * {@link #isPresent()}가 true이면 {@link #get()}과 동일하고, 아니면 other 리턴.
     *
     * @param other 값이 null일 경우에 대한 기본 값.
     * @return 값이 없을 경우 other, 아니면 reference 객체 리턴
     */
    public T orElse(T other) {
        return isPresent() ? reference : other;
    }

    /**
     * get or else.
     *
     * @param supplier
     * @return 빈 객체이면 supplier로부터 받은 값을 리턴.
     */
    public T orElseGet(Supplier<? extends T> supplier) {
        return isPresent() ? reference : supplier.get();
    }

    /**
     * get or throw.
     *
     * @param exceptionSupplier 값이 없을 경우 예외를 제공할 supplier.
     * @param <X>               예외 타입.
     * @return 값이 있으면 값을 리턴하고, 아니면 예외를 던진다.
     * @throws X
     */
    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (isPresent()) {
            return reference;
        }
        throw exceptionSupplier.get();
    }

    @Override
    public String toString() {
        return "Optional.of(" + reference + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        Object oValue = o;
        if (o instanceof Optional) {
            oValue = ((Optional) o).reference;
        }

        if (this.reference == null && oValue == null) {
            return true;
        }
        return this.reference != null && this.reference.equals(oValue)
                && oValue != null && oValue.equals(reference);

    }

    //-------
    // https://docs.oracle.com/javase/7/docs/api/java/io/ObjectOutputStream.html
    //-------
    private void writeObject(java.io.ObjectOutputStream out) throws IOException { /* NOT Implement */ }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException { /* NOT Implement */ }

    private void readObjectNoData() throws ObjectStreamException { /* NOT Implement */ }
}
