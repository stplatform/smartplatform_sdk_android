package com.kakao.oreum.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 어떤 객체 및 메소드가 threadSafe를 보장함을 표시하기 위한 annotation.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface ThreadSafe {
}
