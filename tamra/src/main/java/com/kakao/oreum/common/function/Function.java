package com.kakao.oreum.common.function;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * Represents a function that accepts one argument and produces a result.
 * <p>
 * JDK8의 <code>java.util.function.Function</code>에 해당하는 interface.
 * (<a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html</a>)
 * </p>
 *
 * <p>
 * static method는 {@link XFunctions}, default method는 {@link XFunction}이 구현한다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see XFunction
 * @see XFunctions
 * @since 0.5.0
 */
@FunctionalInterface
public interface Function<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t);

}
