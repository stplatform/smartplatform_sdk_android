package com.kakao.oreum.common.function;

/**
 * JDK8의 <code>java.util.function.Predicate</code>의 static 메소드 구현 클래스.
 *
 * <p>
 * 참고 : <a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html</a>
 * </p>
 * <p>
 * default method는 {@link XPredicate}에 구현되어 있다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see XPredicate
 * @see Predicate
 * @since 0.7.0
 */
public final class XPredicates {
    private XPredicates() { /* DO NOT CREATE INSTANCE */ }

    /**
     * Create XPredicate wrapping given predicate.
     *
     * @param predicate a predicate
     * @param <T>       the type of arguments to the predicate
     * @return a predicate that wraps argument
     */
    public static <T> XPredicate<T> of(Predicate<? super T> predicate) {
        return new XPredicate<T>() {
            @Override
            public boolean test(T t) {
                return predicate.test(t);
            }
        };
    }

    /**
     * Returns a predicate that tests if two arguments are equal according to Objects.equals(Object, Object).
     *
     * @param targetRef the object reference with which to compare for equality, which may be null
     * @param <T>       the type of arguments to the predicate
     * @return a predicate that tests if two arguments are equal according to Objects.equals(Object, Object)
     */
    public static <T> XPredicate<T> isEqual(Object targetRef) {
        // NOTICE: android api 18에서는 Objects.equals(a,b)를 사용할 수 없다.
        return of(t -> (t == null) ? (targetRef == null) : t.equals(targetRef));
    }
}
