package com.kakao.oreum.common.function;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * JDK8의 <code>java.util.function.Supplier</code>에 해당하는 interface.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@FunctionalInterface
public interface Supplier<T> {

    T get();

}
