package com.kakao.oreum.common.function;

/**
 * {@link Consumer}의 static 메소드 구현을 위한 클래스.
 *
 * 현재 factory method 외엔 없다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public final class XConsumers {
    private XConsumers() { /* DO NOT CREATE INSTANCE */ }

    /**
     *
     * @param consumer consumer
     * @param <T> the type of the input to the operation
     * @return xconsumer wrapping consumer
     */
    public static <T> XConsumer<T> of(Consumer<? super T> consumer) {
        return new XConsumer<T>() {
            @Override
            public void accept(T t) {
                consumer.accept(t);
            }
        };
    }


}
