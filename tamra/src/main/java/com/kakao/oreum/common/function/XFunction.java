package com.kakao.oreum.common.function;

/**
 * JDK8의 <code>java.util.function.Function</code>의 default 메소드 구현 클래스.
 *
 * <p>
 * 참고: <a href="https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html">https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html</a>
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public abstract class XFunction<T, R> implements Function<T, R> {
    /**
     * Returns a composed function that first applies this function to its input,
     * and then applies the after function to the result.
     *
     * If evaluation of either function throws an exception,
     * it is relayed to the caller of the composed function.
     *
     * @param after the function to apply after this function is applied
     * @param <V>   the type of output of the after function, and of the composed function
     * @return a composed function that first applies this function and then applies the after function
     * @throws NullPointerException if after is null
     * @see #compose(Function)
     */
    public final <V> XFunction<T, V> andThen(Function<? super R, ? extends V> after) {
        if (after == null) {
            throw new NullPointerException();
        }
        return XFunctions.of(t -> after.apply(apply(t)));
    }

    /**
     * Returns a composed function that first applies the before function to its input,
     * and then applies this function to the result.
     *
     * If evaluation of either function throws an exception,
     * it is relayed to the caller of the composed function.
     *
     * @param before the function to apply before this function is applied
     * @param <V>    the type of input to the before function, and to the composed function
     * @return a composed function that first applies the before function and then applies this function
     * @throws NullPointerException if before is null
     * @see #andThen(Function)
     */
    public final <V> XFunction<V, R> compose(Function<? super V, ? extends T> before) {
        if (before == null) {
            throw new NullPointerException();
        }
        return XFunctions.of(v -> apply(before.apply(v)));
    }
}
