package com.kakao.oreum.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * SDK에서 제공하는 클래스, 메소드 등의 구현 안전성을 나타내기 위한 annotation.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
@Accessibility.Public
public class Stability {
    private Stability() { /* DO NOT USE */ }

    /**
     * 안정.
     * 메이저 버전이 변경되기 까지는 변경되지 않음.
     * 즉, 마이너 버전 변경에 대해서는 호환 유지.
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Stable {
    }

    /**
     * 개발진행중.
     * 마이너 버전이 바뀔 때 코드레벨 호환이 안될 수 있음.
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Evolving {
    }

    /**
     * 불안정.
     * 아무것도 믿지 말것. 언제든지 바뀔 수 있음.
     */
    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Unstable {
    }

}
