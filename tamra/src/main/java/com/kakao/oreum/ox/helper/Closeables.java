package com.kakao.oreum.ox.helper;

import com.kakao.oreum.ox.logger.Logger;

import java.io.Closeable;
import java.io.IOException;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Closeables {
    private static final Logger LOG = getLogger(Closeables.class);

    private Closeables() { /* DO NOT CREATE INSTANCE */ }

    public static void closeQuietly(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException e) {
            LOG.warn("IOException was thrown while closing Closeable.", e);
        } catch (Exception e) {
            LOG.warn("Exception was thrown while closing Closeable.", e);
        }
    }
}
