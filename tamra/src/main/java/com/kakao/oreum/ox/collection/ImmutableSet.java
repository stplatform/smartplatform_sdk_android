package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.Collection;
import java.util.Set;


/**
 * 명시적으로 immutable 임을 드러내기 위해 public class로 정의한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E> {
    private final Set<E> set;

    private ImmutableSet(@Nonnull Set<? extends E> set) {
        Asserts.notNull(set);
        this.set = (Set<E>) set;
    }

    public static <E> ImmutableSet<E> of(E... elements) {
        return new ImmutableSet<>(Sets.linkedHashSet(elements));
    }

    public static <E> ImmutableSet<E> of(Set<? extends E> set) {
        return new ImmutableSet<>(set);
    }

    public static <E> ImmutableSet<E> copyOf(Iterable<? extends E> iterable) {
        return new ImmutableSet<>(Sets.linkedHashSet(iterable));
    }

    @Override
    public boolean contains(Object object) {
        return set.contains(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return set.containsAll(collection);
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Nonnull
    @Override
    public UnmodifiableIterator<E> iterator() {
        return UnmodifiableIterator.from(set.iterator());
    }

    @Override
    public int size() {
        return set.size();
    }

    @Nonnull
    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Nonnull
    @Override
    public <T> T[] toArray(T[] array) {
        return set.toArray(array);
    }

}
