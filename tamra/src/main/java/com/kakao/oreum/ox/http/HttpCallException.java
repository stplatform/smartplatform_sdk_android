package com.kakao.oreum.ox.http;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class HttpCallException extends Exception {
    public HttpCallException() {
        super();
    }

    public HttpCallException(String detailMessage) {
        super(detailMessage);
    }

    public HttpCallException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public HttpCallException(Throwable throwable) {
        super(throwable);
    }
}
