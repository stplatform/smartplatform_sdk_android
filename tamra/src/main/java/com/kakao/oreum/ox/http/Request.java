package com.kakao.oreum.ox.http;

import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;
import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.collection.ImmutableList;
import com.kakao.oreum.ox.helper.Asserts;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * HTTP Request.
 *
 * https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
 *
 * 위 링크에 따라 header는 동일 이름의 필드가 존재할 수 있으며, 중복시 순서가 중요하다. 따라서 Set, Collection으로 다루지 않고 List로 다룬다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Immutable
public final class Request {
    private final static String CRLF = "\n"; // CR이 있으면 android log에 정상적으로 안찍힘
    private final Method method;
    private final URL url;
    private final List<Header> headers;
    private final InputStream body;

    private Request(@Nonnull Method method,
                    @Nonnull URL url,
                    @Nonnull List<Header> headers,
                    @Nullable InputStream body) {
        Asserts.notNull(method);
        Asserts.notNull(url);
        Asserts.notNull(headers);
        this.method = method;
        this.url = url;
        List<Header> headerList = new ArrayList<>();
        // default header for HTTP/1.1
        headerList.add(Header.of("Host", url.getHost() +
                ((url.getPort() == -1) ? "" : ":" + url.getPort())));
        headerList.addAll(headers);
        this.headers = ImmutableList.of(headerList);
        this.body = body;
    }

    public static Request create(Method method, URL url, List<Header> headers, InputStream body) {
        return new Request(
                method,
                url,
                Optional.ofNullable(headers).orElse(new ArrayList<>()),
                body
        );
    }

    public static Request create(Method method, URL url, List<Header> headers) {
        return create(method, url, headers, null);
    }

    public static Request create(Method method, URL url) {
        return create(method, url, null, null);
    }

    public static Request create(Method method, URL url, InputStream body) {
        return create(method, url, null, body);
    }

    public Method method() {
        return method;
    }

    public URL url() {
        return url;
    }

    @Immutable
    public List<Header> headers() {
        return headers;
    }

    @Nullable
    public InputStream body() {
        return body;
    }

    public boolean hasBody() {
        return body != null;
    }

    public String rawMessage() {
        return new StringBuilder()
                .append(method).append(" ").append(url.getFile()).append(" HTTP/1.1").append(CRLF)
                .append(OxIterable.from(headers)
                        .map(h -> h.rawMessage() + CRLF)
                        .join(""))
                .append(CRLF)
                .append("<< Message body cannot be displayed >>")
                .toString();
    }

    @Override
    public String toString() {
        return "HTTP Request:\n" + rawMessage();
    }
}
