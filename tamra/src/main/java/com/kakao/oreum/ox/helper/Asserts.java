package com.kakao.oreum.ox.helper;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public final class Asserts {

    private Asserts() {
        // DO NOT create instance
    }

    public static <T> void notNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
    }

    public static <T> void notNull(T reference, @Nullable String errorMessage) {
        if (reference == null) {
            throw new NullPointerException(errorMessage);
        }
    }

    public static void shouldBe(boolean state) {
        if (!state) {
            throw new IllegalStateException();
        }
    }

    public static void shouldBe(boolean state, @Nonnull String errorMessage) {
        if (!state) {
            throw new IllegalStateException(errorMessage);
        }
    }

    public static void shouldNotBe(boolean state) {
        if (state) {
            throw new IllegalStateException();
        }
    }

    public static void shouldNotBe(boolean state, @Nonnull String errorMessage) {
        if (state) {
            throw new IllegalStateException(errorMessage);
        }
    }
}
