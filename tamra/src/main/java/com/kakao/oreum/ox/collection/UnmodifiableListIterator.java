package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.ListIterator;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class UnmodifiableListIterator<E> implements ListIterator<E> {
    private final ListIterator<E> iterator;

    private UnmodifiableListIterator(@Nonnull ListIterator<? extends E> iterator) {
        this.iterator = (ListIterator<E>) iterator;
    }

    public static <E> UnmodifiableListIterator<E> from(ListIterator<? extends E> iterator) {
        Asserts.notNull(iterator);
        return new UnmodifiableListIterator<>(iterator);
    }

    @Override
    public void add(E object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public boolean hasPrevious() {
        return iterator.hasPrevious();
    }

    @Override
    public E next() {
        return iterator.next();
    }

    @Override
    public int nextIndex() {
        return iterator.nextIndex();
    }

    @Override
    public E previous() {
        return iterator.previous();
    }

    @Override
    public int previousIndex() {
        return iterator.previousIndex();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void set(E object) {
        throw new UnsupportedOperationException();
    }
}
