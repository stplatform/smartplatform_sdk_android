package com.kakao.oreum.ox.task;

/**
 * 실행 타입.
 *
 * main thread에서 수행될 것인지, background에서 수행될 것인지를 나타낸다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public enum RunOn {
    /**
     * UI Thread
     */
    UI,
    /**
     * NON-UI Thread
     */
    BACKGROUND,
    /**
     * Sync Thread
     */
    SYNC,
    /**
     * Beacon Thread
     */
    BEACON,
    /**
     * Geofence Thread
     */
    GEOFENCE
}
