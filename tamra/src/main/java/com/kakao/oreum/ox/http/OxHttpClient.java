package com.kakao.oreum.ox.http;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Supplier;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.helper.Asserts;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * baseUrl의 server에 대해서 {@link OxRequest}의 Factory 역할을 하는 클래스.
 *
 * {@link #forBaseUrl(URL)}로 생성하여 method chaining으로 기본 설정을 할 수 있다.
 * 그렇게 해서 {@link OxHttpClient} 타입의 변수에 assign 된 후에는
 * {@link #requestFor(String)} 메소드만 사용가능하다.
 *
 * <pre><code>
 *    OxHttpClient httpClient = OxHttpClient.forBaseUrl(serverUrl)
 *      .defaultHeader("User-Agent", "Tamra/0.0.0")
 *      .defaultHeader("X-Custom-Header", "some-value")
 *      .defaultOpts(HttpOpts.create()
 *          .connectTimeout(5000)
 *          .followRedirects(true));
 *
 *    Response response = httpClient.requestFor("/info")
 *          .param("lang", "ko")
 *          .header("Content-Type", "application/json")
 *          .request(Method.GET);
 * </code></pre>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public abstract class OxHttpClient {

    public static Impl forBaseUrl(URL baseURL) {
        return new Impl(baseURL);
    }

    public abstract OxRequest requestFor(String relPath);

    /**
     * 특정 서버에 대한 요청을 처리하기 위한 객체.
     *
     * @author cocoon.tf@kakaocorp.com
     * @since 0.6.0
     */
    public static class Impl extends OxHttpClient {
        private final URL baseURL;
        private final List<Header> staticHeaders = new ArrayList<>();
        private final List<Supplier<Header>> dynamicHeaders = new ArrayList<>();
        private HttpOpts defaultOpts = HttpOpts.create();

        private Impl(URL baseURL) {
            this.baseURL = baseURL;
        }

        public Impl defaultHeader(Header header) {
            Asserts.notNull(header);
            this.staticHeaders.add(header);
            return this;
        }

        public Impl defaultHeaders(Collection<Header> headers) {
            Asserts.notNull(headers);
            this.staticHeaders.addAll(headers);
            return this;
        }

        public Impl defaultHeader(String name, String value) {
            return defaultHeader(Header.of(name, value));
        }

        public Impl defaultHeader(Supplier<Header> headerSupplier) {
            Asserts.notNull(headerSupplier);
            this.dynamicHeaders.add(headerSupplier);
            return this;
        }

        public Impl defaultOpts(HttpOpts opts) {
            defaultOpts = Optional.ofNullable(opts).orElse(defaultOpts);
            return this;
        }

        @Override
        public OxRequest requestFor(String relPath) {
            return OxRequest.forURL(concatPath(relPath))
                    .headers(staticHeaders)
                    .headers(dynamicHeaders())
                    .withOpts(HttpOpts.copyOf(defaultOpts));
        }

        private Collection<Header> dynamicHeaders() {
            return OxIterable.from(dynamicHeaders)
                    .map(Supplier::get)
                    .toList();
        }

        private URL concatPath(String relPath) {
            String basePath = baseURL.getPath();
            String newPath = basePath.replaceAll("/$", "") + "/" + relPath.replaceAll("^/", ""); // path 구분자는 항상 하나로만.
            try {
                return new URL(
                        baseURL.getProtocol(),
                        baseURL.getHost(),
                        baseURL.getPort(),
                        newPath + (baseURL.getQuery() == null ? "" : "?" + baseURL.getQuery())
                );
            } catch (MalformedURLException e) {
                throw new AssertionError("Fail to append path to url");
            }
        }
    }
}
