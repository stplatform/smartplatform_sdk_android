package com.kakao.oreum.ox.helper;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class CryptoException extends Exception {
    public CryptoException(Throwable throwable) {
        super(throwable);
    }
}
