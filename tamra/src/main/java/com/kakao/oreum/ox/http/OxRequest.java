package com.kakao.oreum.ox.http;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.helper.Asserts;

import java.io.ByteArrayInputStream;
import java.io.IOError;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * {@link Request}의 생성과 요청수행까지 함께 제공하는 helper.
 *
 * {@link Request}와 {@link HttpCall}을 조합한다.
 *
 * 생성할 때 받은 url에 parameter(query part)를 추가할 수 있는 기능을 제공한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class OxRequest {
    private static final Logger LOG = getLogger(OxRequest.class);
    private final URL url;
    private final List<Param> params = new ArrayList<>();
    private final List<Header> headers = new ArrayList<>();
    private InputStream bodyStream = null;
    private HttpOpts opts = HttpOpts.create();

    private OxRequest(URL url) {
        this.url = url;
    }

    /**
     * 주어진 url에 대한 request.
     *
     * @param url
     * @return
     */
    public static OxRequest forURL(URL url) {
        return new OxRequest(url);
    }

    public OxRequest params(String name, Object value) {
        this.params.add(Param.of(name, value));
        return this;
    }

    public OxRequest encodedParam(String name, String urlEncoded) {
        this.params.add(Param.encodedOf(name, urlEncoded));
        return this;
    }

    public OxRequest header(Header header) {
        this.headers.add(header);
        return this;
    }

    public OxRequest headers(Collection<Header> headers) {
        this.headers.addAll(headers);
        return this;
    }

    public OxRequest header(String name, String value) {
        this.headers.add(Header.of(name, value));
        return this;
    }

    public OxRequest body(InputStream body) {
        if (body == null) return emptyBody();
        this.bodyStream = body;
        return this;
    }

    public OxRequest body(byte[] body) {
        return body(
                Optional.ofNullable(body)
                        .map(ByteArrayInputStream::new)
                        .orElse(null)
        );
    }

    public OxRequest body(String body) {
        return body(
                Optional.ofNullable(body)
                        .map(s -> s.getBytes(Charset.forName("UTF-8")))
                        .orElse(null));
    }

    public OxRequest emptyBody() {
        this.bodyStream = null;
        return this;
    }

    public OxRequest withOpts(HttpOpts opts) {
        this.opts = Optional.ofNullable(opts)
                .orElse(HttpOpts.create());
        return this;
    }


    public Response request(Method method) throws HttpCallException {
        return HttpCall.of(buildRequest(method))
                .withOpts(opts)
                .call();
    }

    private Request buildRequest(Method method) {
        Request request = Request.create(method,
                queryAppendedURL(),
                headers,
                bodyStream);
        LOG.debug("request to send: " + request);
        return request;
    }

    private URL queryAppendedURL() {
        String query = generateQuery(url.getQuery());
        try {
            return new URL(
                    url.getProtocol(),
                    url.getHost(),
                    url.getPort(),
                    url.getPath() + (query == null ? "" : "?" + query)
            );
        } catch (MalformedURLException e) {
            throw new AssertionError("Invalid URL? WHY?");
        }
    }

    private String generateQuery(String initQuery) {
        if (params.isEmpty()) {
            return initQuery;
        }
        return Optional.ofNullable(initQuery)
                .map(q -> q + "&")
                .orElse("") + OxIterable.from(params)
                .map(Param::toString)
                .join("&");
    }

    private static class Param {
        private static final Logger LOG = getLogger(Param.class);
        private final String name;
        private final String value;

        private Param(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public static Param of(String name, Object value) {
            return encodedOf(name, urlEncode(value == null ? "" : String.valueOf(value)));
        }

        private static String urlEncode(String str) {
            try {
                return URLEncoder.encode(str, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // AssertionError를 쓰려 했으나 throwable을 받는 생성자를 API-19부터 지원.
                LOG.warn("'UTF-8' is NOT supported.", e);
                throw new IOError(e);
            }
        }

        public static Param encodedOf(String name, String value) {
            Asserts.notNull(name);
            Asserts.notNull(value);
            return new Param(name, value);
        }

        @Override
        public String toString() {
            return name + "=" + value;
        }
    }

}
