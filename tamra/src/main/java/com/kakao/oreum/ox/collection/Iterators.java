package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Iterators {

    private Iterators() {
    }

    public static <T> Iterator<T> concat(final Iterator<? extends T>... iterators) {
        return new ConcatedIterator<>(iterators);
    }

    public static <T> Iterator<T> cycle(final Iterable<? extends T> iterable) {
        return new CyclicIterator<>(iterable);
    }

    public static <T> Iterator<T> cycle(T... element) {
        return cycle(Lists.arrayList(element));
    }

    public static <T> UnmodifiableIterator<T> forArray(T... elements) {
        return UnmodifiableIterator.from(Lists.arrayList(elements).iterator());
    }

    public static <T> Iterator<T> filter(final Iterator<? extends T> iterator, final Predicate<? super T> predicate) {
        return new FilteredIterator<>(iterator, predicate);
    }

    public static <T> Iterator<T> limit(final Iterator<? extends T> iterator, final int limit) {
        return new LimitedIterator<>(iterator, limit);
    }

    public static <T, U> Iterator<U> map(final Iterator<? extends T> iterator, final Function<? super T, ? extends U> mapper) {
        return new MappedIterator<>(iterator, mapper);
    }

    //
    // java7 이상에서는 Collections.emptyIterator()를 쓰면된다
    // 하지만 안드로이드에서는 KITKAT 이상에서만 가능. 그래서 그냥 만들어씀
    public static <T> Iterator<T> emptyIterator() {
        return new EmptyIterator<>();
    }

    private static class ConcatedIterator<T> implements Iterator<T> {
        private final Queue<Iterator<? extends T>> itrQueue = new LinkedList<>();
        private Iterator<? extends T> currentItr; // 최초값.

        private ConcatedIterator(Iterator<? extends T>... iterators) {
            for (Iterator<? extends T> iterator : iterators) {
                itrQueue.add(iterator);
            }
            this.currentItr = itrQueue.poll();
        }

        @Override
        public boolean hasNext() {
            if (currentItr == null) { // 최초 iterators가 비어 있거나, 모든 iterator들을 소모한 경우.
                return !itrQueue.isEmpty();
            }
            if (!currentItr.hasNext()) {
                currentItr = itrQueue.poll();
                return hasNext();
            }
            return true;
        }

        @Override
        public T next() {
            if (hasNext()) {
                return currentItr.next();
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            if (currentItr != null) {
                currentItr.remove();
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private static class CyclicIterator<T> implements Iterator<T> {
        private final Iterable<? extends T> iterable;
        private Iterator<? extends T> iterator = emptyIterator();

        private CyclicIterator(Iterable<? extends T> iterable) {
            this.iterable = iterable;
        }

        @Override
        public boolean hasNext() {
            if (!iterator.hasNext()) {
                iterator = iterable.iterator();
            }
            return iterator.hasNext();
        }

        @Override
        public T next() {
            if (iterator.hasNext()) {
                return iterator.next();
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            iterator.remove();
        }
    }

    private static class FilteredIterator<T> implements Iterator<T> {
        private final Predicate<? super T> predicate;
        private final Iterator<? extends T> iterator;
        private T prefetch = null;

        private FilteredIterator(Iterator<? extends T> iterator, Predicate<? super T> predicate) {
            this.predicate = predicate;
            this.iterator = iterator;
        }

        @Override
        public boolean hasNext() {
            if (prefetch != null) {
                return true;
            }
            while (iterator.hasNext()) {
                prefetch = iterator.next();
                if (predicate.test(prefetch)) {
                    return true;
                } else {
                    prefetch = null; // skip and next
                }
            }
            return false;
        }

        @Override
        public T next() {
            if (hasNext()) {
                T next = prefetch;
                prefetch = null;
                return next;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            if (prefetch != null) {
                iterator.remove();
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private static class LimitedIterator<T> implements Iterator<T> {
        private final Iterator<? extends T> iterator;
        private final int limit;
        private int count = 0;

        private LimitedIterator(Iterator<? extends T> iterator, int limit) {
            this.iterator = iterator;
            this.limit = limit;
        }

        @Override
        public boolean hasNext() {
            if (count >= limit) {
                return false;
            }
            return iterator.hasNext();
        }

        @Override
        public T next() {
            if (hasNext()) {
                count++;
                return iterator.next();
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            iterator.remove();
        }
    }

    private static class EmptyIterator<T> implements Iterator<T> {
        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public T next() {
            throw new NoSuchElementException();
        }

        @Override
        public void remove() {
            throw new IllegalStateException();
        }
    }

    private static class MappedIterator<T, U> implements Iterator<U> {
        private final Iterator<? extends T> iterator;
        private final Function<? super T, ? extends U> mapper;

        private MappedIterator(Iterator<? extends T> iterator, Function<? super T, ? extends U> mapper) {
            this.iterator = iterator;
            this.mapper = mapper;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public U next() {
            return mapper.apply(iterator.next());
        }

        @Override
        public void remove() {
            iterator.remove();
        }
    }

}
