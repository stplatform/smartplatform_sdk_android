package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.Iterator;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class UnmodifiableIterator<E> implements Iterator<E>{
    private final Iterator<E> iterator;

    private UnmodifiableIterator(@Nonnull Iterator<? extends E> iterator) {
        this.iterator = (Iterator<E>) iterator;
    }

    public static <E> UnmodifiableIterator<E> from(Iterator<? extends E> iterator) {
        Asserts.notNull(iterator);
        return new UnmodifiableIterator<>(iterator);
    }

    public static <E> UnmodifiableIterator<E> from(Iterable<? extends E> iterable) {
        Asserts.notNull(iterable);
        return from(iterable.iterator());
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public E next() {
        return iterator.next();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
