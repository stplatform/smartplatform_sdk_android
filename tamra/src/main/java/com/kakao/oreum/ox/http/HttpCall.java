package com.kakao.oreum.ox.http;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.helper.Closeables;
import com.kakao.oreum.ox.helper.Streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * HTTP 요청 및 응답 처리 객체.
 *
 * {@link #call()} 호출 시에 connection 처리가 이루어지므로,
 * 동일한 HTTPRequest 처리를 여러 번 수행할 경우 이 객체를 재사용할 수 있다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public final class HttpCall {
    private static final Logger LOG = getLogger(HttpCall.class);
    private final Request request;
    private HttpOpts opts = HttpOpts.create();

    private HttpCall(Request request) {
        this.request = request;
    }

    public static HttpCall of(Request request) {
        return new HttpCall(request);
    }

    public static Response call(Request request) throws HttpCallException {
        return of(request).call();
    }

    public HttpCall withOpts(HttpOpts opts) {
        this.opts = Optional.ofNullable(opts).orElse(this.opts);
        return this;
    }

    public Response call() throws HttpCallException {
        try {
            ignoreInvalidSSL();
            HttpURLConnection connection = (HttpURLConnection) request.url().openConnection();

            if (connection instanceof HttpsURLConnection) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) connection;
                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });
            }
            setConnectOptions(connection);
            sendRequest(request, connection);
            Response response = readResponse(connection);
            return response;
        } catch (Exception e) {
            throw new HttpCallException(e);
        }
    }

    private void ignoreInvalidSSL() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs,
                                           String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs,
                                           String authType) {
            }
        } };

        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection
                .setDefaultSSLSocketFactory(sc.getSocketFactory());
    }

    private Response readResponse(HttpURLConnection connection) throws IOException {
        int code = connection.getResponseCode();
        List<Header> responseHeader = readResponseHeaders(connection);
        InputStream bodyStream = getResponseBodyStream(connection);

        return new Response(
                Status.fromCode(code),
                responseHeader,
                bodyStream
        );
    }

    private InputStream getResponseBodyStream(HttpURLConnection connection) {
        try {
            if (connection.getResponseCode() < 400) {
                return connection.getInputStream();
            } else {
                return connection.getErrorStream();
            }
        } catch (IOException e) {
            LOG.warn("Cannot get inputStream of response body.", e);
            return connection.getErrorStream();
        }
    }

    private void sendRequest(Request request, HttpURLConnection connection) throws IOException {
        connection.setRequestMethod(request.method().rawName());
        for (Header header : request.headers()) {
            connection.setRequestProperty(header.name(), header.value());
        }

        if (request.hasBody()) {
            InputStream in = request.body();
            OutputStream out = connection.getOutputStream();
            try {
                Streams.readAndWrite(in, out);
            } finally {
                Closeables.closeQuietly(in);
                Closeables.closeQuietly(out);
            }
        }
    }

    private void setConnectOptions(HttpURLConnection connection) {
        connection.setConnectTimeout(opts.connectTimeout());
        connection.setReadTimeout(opts.readTimeout());
        connection.setInstanceFollowRedirects(opts.followRedirects());
        connection.setDoInput(true);
        if (request.hasBody()) {
            // android의 HttpURLConnection 구현에서 body없을 때 아래 옵션들 건드리면, "Request entity expected but not supplied" 이런 응답이 오게됨(by spray)
            connection.setDoOutput(true);
            connection.setChunkedStreamingMode(0);
        }
    }

    private List<Header> readResponseHeaders(HttpURLConnection connection) {
        List<Header> headers = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : connection.getHeaderFields().entrySet()) {
            String fieldName = entry.getKey();
            if (fieldName == null) { // if null, it's status line.
                continue;
            }
            headers.addAll(
                    OxIterable.from(entry.getValue())
                            .map(v -> Header.of(fieldName, v))
                            .toList());
        }
        return headers;
    }
}
