package com.kakao.oreum.ox.task;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import com.kakao.oreum.common.function.Executable;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.TimerTask;

/**
 * android를 위한 Thread helper.
 *
 * Main Handler와 Background Handler를 이용하여 원하는 thread에서 주어진 {@link Runnable}을 수행한다.
 *
 * <code>v0.8.0</code>부터 반복수행 기능이 추가됨.({@link #execRepeatly(Period, Executable)})
 * 앞으로도 thread 수행과 관련된 기능들이 이쪽으로 추가될 듯 한데, 더 비대해지면 split 할 것.
 *
 * <code>v0.8.0</code>에서 이름이 변경됨 (<code>OxRunnable</code>에서 <code>OxTasks</code> 으로)
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public final class OxTasks {
    // thread 이름이 될 녀석들. 여기로 모아둔다.
    private static final String BACKGROUND_LOOPER_NAME = "OX-BACKGROUND-LOOPER";
    private static final String BEACON_LOOPER_NAME = "OX-BEACON-LOOPER";
    private static final String GEOFENCE_LOOPER_NAME = "OX-GEOFENCE-LOOPER";
    private static final String SYNC_LOOPER_NAME = "OX-SYNC-LOOPER";
    private static final String DEFAULT_SCHEDULER_NAME = "OX-SCHEDULER";


    private OxTasks() { /* DO NOT CREATE INSTANCE */ }

    public static void runOnBackground(Runnable runnable) {
        Handlers.BACKGROUND_HANDLER.post(runnable);
    }

    /**
     *
     * @param runnable
     * @since 0.11.0
     */
    public static void runOnSync(Runnable runnable) {
        Handlers.SYNC_HANDLER.post(runnable);
    }

    /**
     *
     * @param runnable
     * @since 0.11.0
     */
    public static void runOnBeacon(Runnable runnable) {
        Handlers.BEACON_HANDLER.post(runnable);
    }

    /**
     *
     * @param runnable
     * @since 0.11.0
     */
    public static void runOnGeofence(Runnable runnable) {
        Handlers.GEOFENCE_HANDLER.post(runnable);
    }

    public static void runOnUi(Runnable runnable) {
        Handlers.UI_HANDLER.post(runnable);
    }

    public static void runOn(RunOn runOn, Runnable runnable) {
        getHandler(runOn).post(runnable);
    }

    /**
     * 반복 작업은 schduler로 수행.
     *
     * @since 0.8.0
     */
    public static OxTask execRepeatly(Period interval, Executable executable) {
        return new OxTask(Schedulers.DEFAULT.runRepeatly(interval, executable));
    }

    public static RunOn currentRunOn() {
        return onUiThread() ? RunOn.UI : RunOn.BACKGROUND;
    }

    public static boolean onUiThread() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return Loopers.MAIN_LOOPER.isCurrentThread();
        } else {
            return Loopers.MAIN_LOOPER.getThread().equals(Thread.currentThread());
        }
    }

    private static Handler getHandler(RunOn runOn) {
        switch (runOn) {
            case UI:
                return Handlers.UI_HANDLER;
            case BACKGROUND:
                return Handlers.BACKGROUND_HANDLER;
            case SYNC:
                return Handlers.SYNC_HANDLER;
            case BEACON:
                return Handlers.BEACON_HANDLER;
            case GEOFENCE:
                return Handlers.GEOFENCE_HANDLER;
            default:
                return Handlers.BACKGROUND_HANDLER;
        }
    }

    /**
     * lazy init을 위한 inner class.
     *
     * test 시 접근을 위해서 public 과 protected로 접근자를 세팅한다.
     */
    public static final class Loopers {
        protected static final Looper BACKGROUND_LOOPER = createLooper(BACKGROUND_LOOPER_NAME);
        protected static final Looper SYNC_LOOPER = createLooper(SYNC_LOOPER_NAME);
        protected static final Looper BEACON_LOOPER = createLooper(BEACON_LOOPER_NAME);
        protected static final Looper GEOFENCE_LOOPER = createLooper(GEOFENCE_LOOPER_NAME);
        protected static final Looper MAIN_LOOPER = Looper.getMainLooper();

        private Loopers() { /* DO NOT CREATE INSTANCE */ }

        private static Looper createLooper(String threadName) {
            HandlerThread handlerThread = new HandlerThread(threadName);
            handlerThread.start();
            return handlerThread.getLooper();
        }

        public static Looper getMainLooper() {
            return MAIN_LOOPER;
        }

        public static Looper getBackgroundLooper() {
            return BACKGROUND_LOOPER;
        }
        public static Looper getSyncLooper() {
            return SYNC_LOOPER;
        }
    }

    private static final class Handlers {
        /**
         * main looper에 대한 handler.
         */
        private static final Handler UI_HANDLER = new Handler(Loopers.MAIN_LOOPER);
        /**
         * background task를 위한 handler.
         */
        private static final Handler BACKGROUND_HANDLER = new Handler(Loopers.BACKGROUND_LOOPER);
        /**
         * sync task를 위한 handler.
         */
        private static final Handler SYNC_HANDLER = new Handler(Loopers.SYNC_LOOPER);
        /**
         * beacon task를 위한 handler.
         */
        private static final Handler BEACON_HANDLER = new Handler(Loopers.BEACON_LOOPER);
        /**
         * geofence task를 위한 handler.
         */
        private static final Handler GEOFENCE_HANDLER = new Handler(Loopers.GEOFENCE_LOOPER);

        private Handlers() { /* DO NOT CREATE INSTANCE */ }


    }

    private static final class Schedulers {
        /**
         * 기본 스케줄러.
         */
        private static final Scheduler DEFAULT = new Scheduler(DEFAULT_SCHEDULER_NAME);

        private Schedulers() { /* DO NOT CREATE INSTANCE */ }
    }

    public static final class OxTask {
        private final TimerTask task;

        private OxTask(TimerTask timerTask) {
            Asserts.notNull(timerTask);
            this.task = timerTask;
        }
        public void cancel() {
            task.cancel();
        }
    }
}
