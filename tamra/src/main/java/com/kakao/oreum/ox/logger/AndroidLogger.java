package com.kakao.oreum.ox.logger;

import android.util.Log;

import com.kakao.oreum.ox.format.MessageFormatter;

/**
 * Android Logger에 대한 wrapper.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */

final class AndroidLogger implements Logger {
    private final String tag;
    private final String messagePrefix;
    private final Level level;

    AndroidLogger(String tag, Class<?> klass, Level level) {
        this.tag = tag;
        this.level = level;
        this.messagePrefix = klass.getSimpleName() + " - ";
    }

    private String formatLog(String format, Object... args) {
        return messagePrefix + MessageFormatter.format(format, args);
    }

    @Override
    public void trace(String format, Object arg) {
        if (level.accept(Level.TRACE))
            Log.v(tag, formatLog(format, arg));

    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {
        if (level.accept(Level.TRACE))
            Log.v(tag, formatLog(format, arg1, arg2));

    }

    @Override
    public void trace(String format, Object... arguments) {
        if (level.accept(Level.TRACE))
            Log.v(tag, formatLog(format, arguments));

    }

    @Override
    public void trace(String message, Throwable throwable) {
        if (level.accept(Level.TRACE))
            Log.v(tag, formatLog(message), throwable);
    }

    @Override
    public void debug(String format, Object arg) {
        if (level.accept(Level.DEBUG))
            Log.d(tag, formatLog(format, arg));
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        if (level.accept(Level.DEBUG))
            Log.d(tag, formatLog(format, arg1, arg2));
    }

    @Override
    public void debug(String format, Object... arguments) {
        if (level.accept(Level.DEBUG))
            Log.d(tag, formatLog(format, arguments));
    }

    @Override
    public void debug(String message, Throwable throwable) {
        if (level.accept(Level.DEBUG))
            Log.d(tag, formatLog(message), throwable);
    }

    @Override
    public void info(String format, Object arg) {
        if (level.accept(Level.INFO))
            Log.i(tag, formatLog(format, arg));
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        if (level.accept(Level.INFO))
            Log.i(tag, formatLog(format, arg1, arg2));
    }

    @Override
    public void info(String format, Object... arguments) {
        if (level.accept(Level.INFO))
            Log.i(tag, formatLog(format, arguments));
    }

    @Override
    public void info(String message, Throwable throwable) {
        if (level.accept(Level.INFO))
            Log.i(tag, formatLog(message), throwable);
    }

    @Override
    public void warn(String format, Object arg) {
        if (level.accept(Level.WARN))
            Log.w(tag, formatLog(format, arg));
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        if (level.accept(Level.WARN))
            Log.w(tag, formatLog(format, arg1, arg2));
    }

    @Override
    public void warn(String format, Object... arguments) {
        if (level.accept(Level.WARN))
            Log.w(tag, formatLog(format, arguments));
    }

    @Override
    public void warn(String message, Throwable throwable) {
        if (level.accept(Level.WARN))
            Log.w(tag, formatLog(message), throwable);
    }

    @Override
    public void error(String format, Object arg) {
        if (level.accept(Level.ERROR))
            Log.e(tag, formatLog(format, arg));
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        if (level.accept(Level.ERROR))
            Log.e(tag, formatLog(format, arg1, arg2));
    }

    @Override
    public void error(String format, Object... arguments) {
        if (level.accept(Level.ERROR))
            Log.e(tag, formatLog(format, arguments));
    }

    @Override
    public void error(String message, Throwable throwable) {
        if (level.accept(Level.ERROR))
            Log.e(tag, formatLog(message), throwable);
    }

    @Override
    public boolean isLoggable(Level level) {
        return this.level.accept(level);
    }
}
