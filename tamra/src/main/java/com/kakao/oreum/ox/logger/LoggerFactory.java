package com.kakao.oreum.ox.logger;

import com.kakao.oreum.tamra.BuildConfig;

/**
 * Logger 생성자.
 *
 * 빌드환경이나 설정에 따라 Logger를 선택한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */

public final class LoggerFactory {
    private static final String TAG = "OREUM";
    private static Level logLevel = defaultLogLevel();
    private static boolean forTesting = false;

    private LoggerFactory() {
    }

    public static Logger getLogger(Class<?> klass) {
        return forTesting ?
                createConsoleLogger(klass) : createAndroidLogger(klass);
    }

    public static void setLogLevel(Level logLevel) {
        LoggerFactory.logLevel = logLevel;
    }

    public static void setDefaultLevel() {
        LoggerFactory.logLevel = defaultLogLevel();
    }

    /**
     * Android Logger는 unitTest 수행시 내용을 볼 수 없으모로 UnitTest에서는 consoleOut 세팅을 해서 사용한다.
     *
     * @param bool
     */
    public static void onUnitTest(boolean bool) {
        forTesting = bool;
    }

    private static Logger createConsoleLogger(Class<?> klass) {
        // testing 용이므로 모든걸 다 찍는다.
        return new UnitTestLogger(klass, Level.TRACE);
    }

    private static Logger createAndroidLogger(Class<?> klass) {
        // 여긴 oreum package다. tamra가 아니다!
        return new AndroidLogger(TAG, klass, logLevel);
    }

    /**
     * default log level.
     *
     * DEBUG 빌드일 때는 INFO 레벨 오류를 SDK 사용자에게 제공한다.
     * RELEASE 빌드시에는 ERROR레벨 로깅만 SDK 사용자에게 제공한다.
     *
     * @return
     */
    private static Level defaultLogLevel() {
        return BuildConfig.DEBUG ? Level.INFO : Level.ERROR;
    }

}
