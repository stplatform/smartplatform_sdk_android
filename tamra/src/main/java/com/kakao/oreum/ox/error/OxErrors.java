package com.kakao.oreum.ox.error;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Executable;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.ox.logger.Logger;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * {@link OxErrorObserver}를 aggregate하는 유틸리티 클래스.
 *
 * <p>
 * {@link #addObserver(OxErrorObserver)}, {@link #removeObserver(OxErrorObserver)}로 다수의 errorHandler를 관리하고,
 * {@link #notifyAll(Exception)}로 등록된 모든 errorHandler에게 예외를 전달하는 역할을 한다.
 * {@link #NOTIFIER}는 등록된 모든 {@link OxErrorObserver}에게 예외를 전달하는 {@link OxErrorObserver}이다.
 * </p>
 * <p>
 * 이 클래스는 예외 처리를 위한 단일 점접으로 사용되기 위해 만들어졌으며,
 * 추후 발생하는 예외를 서버로 전송한다거나, 파일로 별도 로깅하려는 needs가 있을 때 이 클래스가 그 시작지점이 될 것이다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public final class OxErrors {
    private static final Logger LOG = getLogger(OxErrors.class);
    private static final Set<OxErrorObserver> OBSERVERS = new LinkedHashSet<>();

    /**
     * 등록된 모든 {@link OxErrorObserver}에게 예외를 전달하는 {@link OxErrorObserver}.
     */
    public static final OxErrorObserver NOTIFIER = OxErrors::notifyAll;

    private OxErrors() { /* DO NOT CREATE INSTANCE */ }

    /**
     * @param errorObserver 등록할 errorObserver.
     */
    public static void addObserver(OxErrorObserver errorObserver) {
        if (!(errorObserver instanceof NotRegisterableObserver)) {
            OBSERVERS.add(errorObserver);
        }
    }

    /**
     * @param errorObserver 등록해제할 errorObserver
     */
    public static void removeObserver(OxErrorObserver errorObserver) {
        OBSERVERS.remove(errorObserver);
    }

    /**
     * 등록된 모든 {@link OxErrorObserver}에게 예외 발생을 알린다.
     *
     * <p>
     * 예외 핸들링 중 발생한 예외는 로깅하고 무시한다.
     * </p>
     *
     * @param exception 발생한 예외
     */
    public static void notifyAll(Exception exception) {
        for (OxErrorObserver handler : OBSERVERS) {
            try {
                handler.notify(exception);
            } catch (Exception e) {
                LOG.error("ERROR on handling exception.", e);
            }
        }
    }

    /**
     * 등록된 {@link OxErrorObserver}에게 오류를 전달하고 추가 로직을 수행하는 {@link OxErrorObserver}를 생성하는 factory 메소드.
     *
     * <p>
     * 이 메소드로 생성된 {@link OxErrorObserver}는 {@link #addObserver(OxErrorObserver)}로 등록될 수 없다(무시된다).
     * </p>
     *
     * @param executable 예외 처리 후 추가처리 로직.
     * @return notifyAll(e)와 executable를 수행하는 OxErrorObserver.
     */
    public static OxErrorObserver notifyAndExec(Executable executable) {
        return new NotifyAndExecObserver(executable);
    }

    /**
     * 예외를 다른 예외 클래스로 변환(or wrap)해서 notify하는 {@link OxErrorObserver}를 생성하는 factory 메소드.
     * <p>
     * 이 메소드로 생성된 {@link OxErrorObserver}는 {@link #addObserver(OxErrorObserver)}로 등록될 수 없다(무시된다).
     * </p>
     *
     * @param exceptionMap exception을 다른 exception으로 mapping할 함수
     * @return 예외 변환 후 notifyAll(e)을 수행하는 OxErrorObserver
     */
    public static OxErrorObserver wrapAndNotify(Function<? super Exception, ? extends Exception> exceptionMap) {
        return new MapAndNotifier(exceptionMap);
    }

    /**
     * 예외를 등록된 다른 observer들에게 전달하고 추가적인 예외 처리를 작업을 수행하는 {@link OxErrorObserver}를 생성하는 factory 메소드.
     *
     * <p>
     * 이 메소드로 생성된 {@link OxErrorObserver}는 {@link #addObserver(OxErrorObserver)}로 등록될 수 없다(무시된다).
     * </p>
     *
     * @param errorHandler 예외 핸들러.
     * @return notifyAll(e) 후에 추가적인 예외 처리를 하는 OxErrorObserver.
     */
    public static OxErrorObserver notifyAndHandle(Consumer<? super Exception> errorHandler) {
        return new NotifyAndHandler(errorHandler);
    }

    /**
     * 등록된 모든 error handler를 등록해제한다.
     */
    @Accessibility.InternalTesting
    public static void removeAllObservers() {
        OBSERVERS.clear();
    }


    private interface NotRegisterableObserver extends OxErrorObserver {
        /* OxErrors에 등록할 수 없는 OxErrorObserver */
    }

    /**
     * 예외를 받으면 다른 notify한 후 추가 코드(executable)을 수행하는 OxErrorObserver.
     */
    private static class NotifyAndExecObserver implements NotRegisterableObserver {
        private final Executable executable;

        private NotifyAndExecObserver(Executable executable) {
            this.executable = executable;
        }

        @Override
        public void notify(Exception exception) {
            OxErrors.notifyAll(exception);
            try {
                executable.exec();
            } catch (Exception e) {
                // exec 중 발생한 예외 역시 notify한다.
                LOG.warn("ERR: while executing custom exception logic", e);
                OxErrors.notifyAll(e);
            }
        }
    }

    private static class MapAndNotifier implements NotRegisterableObserver {
        private final Function<? super Exception, ? extends Exception> exceptionMapper;

        private MapAndNotifier(Function<? super Exception, ? extends Exception> exceptionMapper) {
            this.exceptionMapper = exceptionMapper;
        }

        @Override
        public void notify(Exception exception) {
            try {
                Exception mapped = exceptionMapper.apply(exception);
                OxErrors.notifyAll(mapped);
            } catch (Exception e) {
                LOG.warn("ERR: while wrapping exception", e);
                // 주의: 아래는 순서에 의미가 있음 (먼저 발생한 것을 먼저 알린다)
                OxErrors.notifyAll(exception); // 원본을 변환하지 않은 상태로 먼저 알린다.
                OxErrors.notifyAll(e); // 그 다음 변환 예외를 알린다.
            }
        }
    }

    private static class NotifyAndHandler implements NotRegisterableObserver {
        private final Consumer<? super Exception> handler;

        private NotifyAndHandler(Consumer<? super Exception> handler) {
            this.handler = handler;
        }

        @Override
        public void notify(Exception exception) {
            OxErrors.notifyAll(exception);
            try {
                handler.accept(exception);
            } catch (Exception e) {
                // 핸들링 중 발생한 예외도 noti한다
                LOG.warn("ERR: while handling exception", e);
                OxErrors.notifyAll(exception);
            }
        }
    }
}
