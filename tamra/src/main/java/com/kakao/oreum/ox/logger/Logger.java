package com.kakao.oreum.ox.logger;

/**
 * Logger interface.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */

public interface Logger {

    void trace(String format, Object arg);

    void trace(String format, Object arg1, Object arg2);

    void trace(String format, Object... arguments);

    void trace(String message, Throwable throwable);

    void debug(String format, Object arg);

    void debug(String format, Object arg1, Object arg2);

    void debug(String format, Object... arguments);

    void debug(String message, Throwable throwable);

    void info(String format, Object arg);

    void info(String format, Object arg1, Object arg2);

    void info(String format, Object... arguments);

    void info(String message, Throwable throwable);

    void warn(String format, Object arg);

    void warn(String format, Object arg1, Object arg2);

    void warn(String format, Object... arguments);

    void warn(String message, Throwable throwable);

    void error(String format, Object arg);

    void error(String format, Object arg1, Object arg2);

    void error(String format, Object... arguments);

    void error(String message, Throwable throwable);

    boolean isLoggable(Level level);

}
