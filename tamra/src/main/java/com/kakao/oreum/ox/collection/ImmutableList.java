package com.kakao.oreum.ox.collection;

import com.kakao.oreum.ox.helper.Asserts;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class ImmutableList<E> extends ImmutableCollection<E> implements List<E> {
    private final List<E> list;

    private ImmutableList(List<? extends E> list) {
        Asserts.notNull(list);
        this.list = (List<E>) list;
    }

    public static <E> ImmutableList<E> of(E... elements) {
        return new ImmutableList<>(Lists.arrayList(elements));
    }

    public static <E> ImmutableList<E> of(List<? extends E> list) {
        return new ImmutableList<>(list);
    }

    public static <E> ImmutableList<E> copyOf(Iterable<? extends E> iterable) {
        return new ImmutableList<>(Lists.arrayList(iterable));
    }

    @Override
    public void add(int location, E object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int location, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E get(int location) {
        return list.get(location);
    }

    @Override
    public int indexOf(Object object) {
        return list.indexOf(object);
    }

    @Override
    public int lastIndexOf(Object object) {
        return list.lastIndexOf(object);
    }

    @Override
    public ListIterator<E> listIterator() {
        return UnmodifiableListIterator.from(list.listIterator());
    }

    @Override
    public ListIterator<E> listIterator(int location) {
        return UnmodifiableListIterator.from(list.listIterator(location));
    }

    @Override
    public E remove(int location) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E set(int location, E object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<E> subList(int start, int end) {
        return list.subList(start, end);
    }

    @Override
    public boolean contains(Object object) {
        return list.contains(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return list.containsAll(collection);
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public UnmodifiableIterator<E> iterator() {
        return UnmodifiableIterator.from(list.iterator());
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return list.toArray(array);
    }

    @Override
    public String toString() {
        return "ImmutableList{" +
                "list=" + list +
                '}';
    }
}
