package com.kakao.oreum.ox.http;

/**
 * HTTP Methods
 *
 * https://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
 *
 * (참고) ProGuard 적용시 enum 이름이 변경될 수 있으므로,
 * 실제 HTTP Protocol 레벨에서 사용할 때는 {@link #name()}를 사용하지 말고, {@link #rawName()}을 사용하도록 한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public enum Method {
    OPTIONS("OPTIONS"),
    GET("GET"),
    HEAD("HEAD"),
    POST("POST"),
    PUT("PUT"),
    DELETE("DELETE"),
    TRACE("TRACE"),
    CONNECT("CONNECT")
    ;

    private final String rawName;

    public String rawName() {
        return rawName;
    }

    Method(String rawName) {
        this.rawName = rawName;
    }
}
