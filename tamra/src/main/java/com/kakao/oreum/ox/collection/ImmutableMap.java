package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Nonnull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class ImmutableMap<K, V> implements Map<K, V> {

    private final Map<K, V> map;

    private ImmutableMap(Map<? extends K, ? extends V> map) {
        this.map = (Map<K, V>) map;
    }

    public static <K, V> ImmutableMap<K, V> of(Map<? extends K, ? extends V> map) {
        return new ImmutableMap<>(map);
    }

    public static <K, V> ImmutableMap<K, V> copyOf(Map<? extends K, ? extends V> map) {
        return new ImmutableMap<>(new HashMap<>(map));
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Nonnull
    @Override
    public Set<Entry<K, V>> entrySet() {
        return ImmutableSet.copyOf(map.entrySet());
    }

    @Override
    public V get(Object key) {
        return map.get(key);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Nonnull
    @Override
    public Set<K> keySet() {
        return ImmutableSet.copyOf(map.keySet());
    }

    @Override
    public V put(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @Override
    public V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return map.size();
    }

    @Nonnull
    @Override
    public Collection<V> values() {
        return ImmutableList.copyOf(map.values());
    }
}
