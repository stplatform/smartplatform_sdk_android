package com.kakao.oreum.ox.format;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * SLF4J 스타일의 포맷터.
 *
 * 참고 : http://www.slf4j.org/api/org/slf4j/helpers/MessageFormatter.html
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class MessageFormatter {
    private static final char CH_ESCAPE = '\\';
    private static final char CH_MSTART = '{';
    private static final char CH_MSTOP = '}';

    private MessageFormatter() { /* DO NOT CREATE INSTANCE */ }

    public static String format(String pattern, Object... args) {
        if (pattern == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        State state = State.NORMAL;
        Values values = new Values(args);
        for (int i = 0; i < pattern.length(); i++) {
            char cur = pattern.charAt(i);
            state = state.process(sb, cur, values);
        }
        if (state == State.WAIT_MSTOP) {
            sb.append(CH_MSTART);
        }
        return sb.toString();
    }

    private enum State implements CharProccessor {
        NORMAL {
            @Override
            public State process(StringBuilder sb, char ch, Values values) {
                switch (ch) {
                    case CH_ESCAPE:
                        return ESCAPE;
                    case CH_MSTART:
                        return WAIT_MSTOP;
                    default:
                        sb.append(ch);
                        return NORMAL;
                }
            }
        },
        ESCAPE {
            @Override
            public State process(StringBuilder sb, char ch, Values values) {
                sb.append(ch);
                return NORMAL;
            }
        },
        WAIT_MSTOP {
            @Override
            public State process(StringBuilder sb, char ch, Values values) {
                if (ch == CH_MSTOP) {
                    sb.append(values.next());
                    return NORMAL;
                } else {
                    sb.append(CH_MSTART);
                    return NORMAL.process(sb, ch, values);
                }
            }
        }
    }

    @FunctionalInterface
    private interface CharProccessor {
        State process(StringBuilder sb, char ch, Values values);
    }


    private static final class Values {
        private final Object[] valueArray;
        private int index = 0;

        private Values(Object[] valueArray) {
            this.valueArray = (valueArray == null) ? new Object[0] : valueArray;
        }

        private String next() {
            if (index >= 0 && index < valueArray.length) {
                return String.valueOf(valueArray[index++]);
            }
            return "{}";
        }
    }
}
