package com.kakao.oreum.ox.collection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Maps {

    private Maps() {
    }

    public static <K, V> Builder<K, V> entry(K k, V v) {
        Map<K,V> map = new HashMap<>();
        return new Builder<>(map)
                .entry(k, v);
    }

    public static <K, V> Builder<K, V> linkedHashMap() {
        return new Builder<K,V>(new LinkedHashMap<K,V>());
    }

    public final static class Builder<K, V> {
        private final Map<K, V> map;

        public Builder(Map<K, V> map) {
            this.map = map;
        }

        public Builder<K, V> entry(K k, V v) {
            this.map.put(k, v);
            return this;
        }

        public Map<K, V> map() {
            return map;
        }

        public Map<K, V> immutableMap() {
            return ImmutableMap.of(map);
        }
    }
}
