package com.kakao.oreum.ox.error;

import com.kakao.oreum.common.annotation.FunctionalInterface;

/**
 * Exception 던지는 Functional Interface를 정의하기 위한 클래스.
 *
 * <p>
 * Functional Interface를 적극 활용하는 Tamra SDK 내에서 functional interface 실행시 예외를 처리하기 위해서 사용한다.
 * Tamra 내부 구현에서는 여기에 정의된 인터페이스를 사용하여 로직을 기술하고 {@link ExceptionFree} 클래스를 이용하여 기본 functional interface로 변환한다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see ExceptionFree
 * @since 0.8.0
 */
public final class Throwing {
    private Throwing() { /* DO NOT CREATE INSTANCE */ }

    /**
     * {@link com.kakao.oreum.common.function.Executable}의 throwing exception 버전.
     */
    @FunctionalInterface
    public interface Executable {
        void exec() throws Exception;
    }

    /**
     * {@link com.kakao.oreum.common.function.Function}의 throwing exception 버전
     *
     * @param <T> function의 input type
     * @param <R> function의 output type
     */
    @FunctionalInterface
    public interface Function<T, R> {
        R apply(T t) throws Exception;
    }

    /**
     * {@link com.kakao.oreum.common.function.Consumer}의 throwing exception 버전
     *
     * @param <T> consumer의 input type
     */
    @FunctionalInterface
    public interface Consumer<T> {
        void accept(T t) throws Exception;
    }

    /**
     * {@link com.kakao.oreum.common.function.Supplier}의 throwing exception 버전.
     *
     * @param <T> supplier의 output type.
     */
    @FunctionalInterface
    public interface Supplier<T> {
        T get() throws Exception;
    }

}
