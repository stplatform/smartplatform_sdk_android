package com.kakao.oreum.ox.error;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Executable;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Supplier;

/**
 * Funtional Interface에 대한 예외 처리를 위한 helper 클래스.
 *
 * <p>
 * lambda expression을 사용하는 것을 전제로 만들어졌다.
 * {@link Function}, {@link Executable}, {@link Consumer}에 대한 Unchecked Exception 뿐아니라,
 * 이와 대응되는 {@link com.kakao.oreum.ox.error.Throwing.Function}, {@link com.kakao.oreum.ox.error.Throwing.Consumer}, {@link com.kakao.oreum.ox.error.Throwing.Executable}에서
 * 발생할 수 있는 Checked Exception에 대한 핸들링도 지원한다.
 * </p>
 * <p>
 * 직접 {@link OxErrorObserver}를 지정하지 않는 메소드의 경우, {@link OxErrors#NOTIFIER}가 {@link OxErrorObserver}로 사용된다.
 * 예외에 대한 notify와 추가 처리를 하고 싶은 경우에는 {@link OxErrors#notifyAndExec(Executable)}로 생성된 {@link OxErrorObserver}를 사용하라.
 * </p>
 *
 * <pre><code>
 *     // func 내에서 checked exception을 던지고 싶은 경우
 *     OxTasks.runOnBackground(
 *          ExceptionFree.runnable(() -> { // Throwing.Executable : 이건 예외를 던질 수 있다.
 *              // do something
 *              throw new IOException(); // it's possible
 *          })
 *     );
 *
 *     // unchecked exception도 안전하게 핸들링 하고 싶은 경우
 *     Function original = ...;
 *     Function safeFunc = ExceptionFree.of(original);
 *
 *     // runnable의 경우 직접 실행도 가능
 *     ExceptionFree.exec(() -> {
 *          if(ok()) {
 *              // do something
 *          } else {
 *              // oxErrors에 등록된 errorObserver들에게 전달된다
 *              throw new IOException();
 *          }
 *     });
 *
 *     // 직접 exception handler를 지정하면 실패시 분기를 처리할 수 있다.
 *     ExceptionFree.exec(() -> {
 *          doSomething(); // may raise exception
 *          status = OK;
 *     }, OxErrors.notifyAndExec(() -> {
 *          status = FAIL;
 *     });
 * </code></pre>
 *
 * @author cocoon.tf@kakaocorp.com
 * @see Throwing
 * @see OxErrors
 * @since 0.8.0
 */
public final class ExceptionFree {

    private ExceptionFree() { /* DO NOT CREATE INSTANCE */ }

    /**
     * @param executable    Throwing.Executable
     * @param errorObserver errorObserver
     * @return error observing executable
     */
    public static Executable executable(Throwing.Executable executable, OxErrorObserver errorObserver) {
        return () -> {
            try {
                executable.exec();
            } catch (Exception e) {
                errorObserver.notify(e);
            }
        };
    }

    /**
     * @param executable Throwing.Executable
     * @return error notifying executable.
     */
    public static Executable executable(Throwing.Executable executable) {
        return executable(executable, OxErrors.NOTIFIER);
    }

    /**
     * @param executable    executable
     * @param errorObserver errorObserver
     * @return error observing executable
     */
    public static Executable of(Executable executable, OxErrorObserver errorObserver) {
        return () -> {
            try {
                executable.exec();
            } catch (Exception e) {
                errorObserver.notify(e);
            }
        };
    }

    /**
     * @param executable executable
     * @return error notifying executable
     */
    public static Executable of(Executable executable) {
        return of(executable, OxErrors.NOTIFIER);
    }

    /**
     * @param executable    Throwing.Executable
     * @param errorObserver errorObserver
     * @return error observing runnable
     */
    public static Runnable runnable(Throwing.Executable executable, OxErrorObserver errorObserver) {
        return () -> {
            try {
                executable.exec();
            } catch (Exception e) {
                errorObserver.notify(e);
            }
        };
    }

    /**
     * @param executable Throwing.Executable
     * @return error notifying runnable
     */
    public static Runnable runnable(Throwing.Executable executable) {
        return runnable(executable, OxErrors.NOTIFIER);
    }

    /**
     * @param function      Throwing.Function
     * @param errorObserver errorObserver
     * @param returnOnError 예외시 리턴 값
     * @param <T>           function의 input type
     * @param <R>           function의 return type
     * @return error observing function
     */
    public static <T, R> Function<T, R> function(Throwing.Function<? super T, ? extends R> function, OxErrorObserver errorObserver, R returnOnError) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Exception e) {
                errorObserver.notify(e);
                return returnOnError;
            }
        };
    }

    /**
     * @param function      Throwing.Function
     * @param returnOnError 예외 발생시 리턴값
     * @param <T>           function의 input type
     * @param <R>           function의 return type
     * @return error notifying function
     */
    public static <T, R> Function<T, R> function(Throwing.Function<? super T, ? extends R> function, R returnOnError) {
        return function(function, OxErrors.NOTIFIER, returnOnError);
    }

    /**
     * @param function Throwing.Function
     * @param <T>      function의 input type
     * @param <R>      function의 return type
     * @return error notifying function(예외 발생시 null을 리턴한다)
     */
    public static <T, R> Function<T, R> function(Throwing.Function<? super T, ? extends R> function) {
        return function(function, OxErrors.NOTIFIER, null);
    }

    /**
     * @param function      function
     * @param errorObserver errorObserver
     * @param returnOnError 예외 발생시 function의 리턴 값
     * @param <T>           function의 input type
     * @param <R>           function의 return type
     * @return error observing function.
     */
    public static <T, R> Function<T, R> of(Function<? super T, ? extends R> function, OxErrorObserver errorObserver, R returnOnError) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Exception e) {
                errorObserver.notify(e);
                return returnOnError;
            }
        };
    }

    /**
     * @param function      function
     * @param returnOnError 예외 발생시 function의 return 값
     * @param <T>           function의 input type
     * @param <R>           function의 return type
     * @return error notifying function
     */
    public static <T, R> Function<T, R> of(Function<? super T, ? extends R> function, R returnOnError) {
        return of(function, OxErrors.NOTIFIER, returnOnError);
    }

    /**
     * @param function function
     * @param <T>      function의 input type
     * @param <R>      function의 return type
     * @return error notifying function
     */
    public static <T, R> Function<T, R> of(Function<? super T, ? extends R> function) {
        return of(function, OxErrors.NOTIFIER, null);
    }

    /**
     * @param consumer      Throwing.Consumer
     * @param errorObserver errorObserver
     * @param <T>           consumer의 input type
     * @return error observing consumer
     */
    public static <T> Consumer<T> consumer(Throwing.Consumer<? super T> consumer, OxErrorObserver errorObserver) {
        return t -> {
            try {
                consumer.accept(t);
            } catch (Exception e) {
                errorObserver.notify(e);
            }
        };
    }

    /**
     * @param consumer Throwing.Consumer
     * @param <T>      consumer의 input type
     * @return error notifying consumer
     */
    public static <T> Consumer<T> consumer(Throwing.Consumer<? super T> consumer) {
        return consumer(consumer, OxErrors.NOTIFIER);
    }

    /**
     * @param consumer      consumer
     * @param errorObserver errorObserver
     * @param <T>           consumer의 input type
     * @return error observing consumer
     */
    public static <T> Consumer<T> of(Consumer<? super T> consumer, OxErrorObserver errorObserver) {
        return t -> {
            try {
                consumer.accept(t);
            } catch (Exception e) {
                errorObserver.notify(e);
            }
        };
    }

    /**
     * @param consumer consumer
     * @param <T>      consumer의 input type
     * @return error notifying consumer
     */
    public static <T> Consumer<T> of(Consumer<? super T> consumer) {
        return of(consumer, OxErrors.NOTIFIER);
    }


    public static <T> Supplier<Optional<T>> supplier(Throwing.Supplier<? extends T> supplier, OxErrorObserver errorObserver) {
        return () -> {
            try {
                return Optional.ofNullable(supplier.get());
            } catch (Exception e) {
                errorObserver.notify(e);
                return Optional.empty();
            }
        };
    }

    public static <T> Supplier<Optional<T>> supplier(Throwing.Supplier<? extends T> supplier) {
        return supplier(supplier, OxErrors.NOTIFIER);
    }

    public static <T> Supplier<Optional<T>> of(Supplier<? extends T> supplier, OxErrorObserver errorObserver) {
        return () -> {
            try {
                return Optional.ofNullable(supplier.get());
            } catch (Exception e) {
                errorObserver.notify(e);
                return Optional.empty();
            }
        };
    }

    public static <T> Supplier<Optional<T>> of(Supplier<? extends T> supplier) {
        return of(supplier, OxErrors.NOTIFIER);
    }

    /**
     * executable을 실행하고 예외 발생시 errorObserver에 알린다.
     *
     * <p>
     * 기본 예외 핸들러처럼 예외를 일괄 처리한 후 추가 처리를 하기 위해서는 {@link OxErrors#notifyAndExec(Executable)}를 사용할 것.
     * </p>
     *
     * <pre><code>
     *     ExceptionFree.exec(() -> {
     *         doSomething();
     *         status = OK;
     *     }, OxErrors.notifyAndExec(() -> { // 예외를 모든 observer에 알리고 아래 블럭을 실행.
     *         status = FAILED;
     *     });
     * </code></pre>
     *
     * @param executable    Throwing.Executable
     * @param errorObserver 예외 핸들러.
     */
    public static void exec(Throwing.Executable executable, OxErrorObserver errorObserver) {
        executable(executable, errorObserver).exec();
    }

    /**
     * executable을 실행하고 예외 발생시 {@link OxErrors}를 통해 error를 notify한다.
     *
     * @param executable 실행할 코드 블럭
     */
    public static void exec(Throwing.Executable executable) {
        exec(executable, OxErrors.NOTIFIER);
    }

    /**
     * supplier를 실행하고 예외 발생시 errorObserver에게 전달한다.
     *
     * @param supplier supplier
     * @param errorObserver error observer
     * @param <T> supplier의 output type
     * @return supplier.get()을 optional로 wrap해서 리턴한다. 예외 발생시 Optional.empty()가 리턴된다.
     */
    public static <T> Optional<T> get(Throwing.Supplier<? extends T> supplier, OxErrorObserver errorObserver) {
        return supplier(supplier, errorObserver).get();
    }

    /**
     * supplier를 실행하고 예외 발생시 등록된 모든 errorObserver들에게 알린다.
     *
     * @param supplier supplier
     * @param <T> supplier의 output type.
     * @return supplier.get()을 optional로 wrap해서 리턴한다. 예외 발생시 Optional.empty()가 리턴된다.
     */
    public static <T> Optional<T> get(Throwing.Supplier<? extends T> supplier) {
        return get(supplier, OxErrors.NOTIFIER);
    }

}
