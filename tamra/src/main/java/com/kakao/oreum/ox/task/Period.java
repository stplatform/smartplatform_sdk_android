package com.kakao.oreum.ox.task;

import java.util.concurrent.TimeUnit;

/**
 * 시간의 양을 다루기 위한 데이터 클래스.
 *
 * <p>
 * 시간의 양과 TimeUnit의 조합을 표현하기 위해서 만듬.
 * 어차피 초단위 이하만 쓸거라서 모든 TimeUnit에 대한 method를 만들지는 않는다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public final class Period {
    private final long amount;
    private final TimeUnit unit;


    private Period(long amount, TimeUnit unit) {
        this.amount = amount;
        this.unit = unit;
    }

    public static Period milliseconds(long amount) {
        return new Period(amount, TimeUnit.MILLISECONDS);
    }

    public static Period seconds(long amount) {
        return new Period(amount, TimeUnit.SECONDS);
    }

    public static Period minutes(long amount) {
        return new Period(amount, TimeUnit.MINUTES);
    }

    public static Period hours(long amount) {
        return new Period(amount, TimeUnit.HOURS);
    }

    public long toMillis() {
        return unit.toMillis(amount);
    }

    public long toSeconds() {
        return unit.toSeconds(amount);
    }


}
