package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;

import java.util.Iterator;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Iterables {
    private Iterables() {
    }

    public static <T> Iterable<T> concat(final Iterable<? extends T>... iterables) {
        return () -> {
            Iterator<? extends T>[] iterators = new Iterator[iterables.length];
            for (int i = 0; i < iterables.length; i++) {
                iterators[i] = iterables[i].iterator();
            }
            return Iterators.concat(iterators);
        };
    }

    public static <T> Iterable<T> empty() {
        return () -> Iterators.emptyIterator();
    }

    public static <T> Iterable<T> cycle(final Iterable<? extends T> iterable) {
        return () -> Iterators.cycle(iterable);
    }

    public static <T> Iterable<T> cycle(final T... elements) {
        return () -> Iterators.cycle(elements);
    }

    public static <T> Iterable<T> filter(final Iterable<? extends T> iterable, final Predicate<? super T> predicate) {
        return () -> Iterators.filter(iterable.iterator(), predicate);
    }

    public static <T> Iterable<T> limit(final Iterable<? extends T> iterable, final int limit) {
        return () -> Iterators.limit(iterable.iterator(), limit);
    }

    public static <T, U> Iterable<U> map(final Iterable<? extends T> iterable, final Function<? super T, ? extends U> mapper) {
        return () -> Iterators.map(iterable.iterator(), mapper);
    }

    public static <T> int size(Iterable<T> iterable) {
        int size = 0;
        for (T ignored : iterable) {
            size++;
        }
        return size;
    }

    public static <T> Optional<T> find(Iterable<? extends T> iterable, Predicate<? super T> predicate) {
        for (T t : iterable) {
            if (predicate.test(t)) {
                return Optional.of(t);
            }
        }
        return Optional.empty();
    }

    public static <T> Optional<T> get(Iterable<? extends T> iterable, int position) {
        int i = 0;
        for (T t : iterable) {
            if (i > position) {
                break;
            }
            if (i++ == position) {
                return Optional.of(t);
            }
        }
        return Optional.empty();
    }
}
