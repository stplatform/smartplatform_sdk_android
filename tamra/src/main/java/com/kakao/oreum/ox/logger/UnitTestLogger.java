package com.kakao.oreum.ox.logger;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.kakao.oreum.ox.format.MessageFormatter.format;

/**
 * UnitTest시에 사용할 console 출력용 logger.
 *
 * UnitTest시에만 사용되므로 I/O 성능등은 고려하지 않는다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */

final class UnitTestLogger implements Logger {
    private static final String THIS_CLASS_NAME = UnitTestLogger.class.getName();
    private final Class<?> klass;
    private final Level level;
    private final PrintStream msgOut;
    private final PrintStream errOut;

    /**
     * 기본 생성자.
     *
     * @param klass  어떤 클래스의 로거인지.
     * @param level  로그 레벨
     * @param msgOut 메시지출력
     * @param errOut 오류출력. throwable 객체 출력용.
     */
    UnitTestLogger(Class<?> klass, Level level, PrintStream msgOut, PrintStream errOut) {
        this.klass = klass;
        this.level = level;
        this.msgOut = msgOut;
        this.errOut = errOut;
    }

    /**
     * System.out, System.err를 사용하는 생성자.
     *
     * @param klass logging class.
     * @param level logging level.
     */
    UnitTestLogger(Class<?> klass, Level level) {
        // sonar 님이 머라고 하시니깐 System.out, System.err를 직접 사용하지 않는다.
        this(klass, level,
                new PrintStream(new FileOutputStream(FileDescriptor.out)), // same as System.out
                new PrintStream(new FileOutputStream(FileDescriptor.err)) // same as System.err
        );
    }

    private void printLog(Level level, String message, Throwable throwable) {
        printLog(level, message);
        printException(throwable);
    }

    private void printLog(Level level, String message) {
        msgOut.println(formatMessage(level, message));
        msgOut.flush(); // 섞이는 걸 막기 위해 flush한다.
    }

    private void printException(Throwable throwable) {
        errOut.println(formatThrowable(throwable));
        errOut.flush(); // 섞이는 걸 막기 위해 flush한다.
    }

    private String formatThrowable(Throwable throwable) {
        StringBuilder sb = new StringBuilder();
        sb.append(throwable.getClass().getName()).append(": ")
                .append(throwable.getMessage())
                .append("\n");
        for (StackTraceElement ste : throwable.getStackTrace()) {
            sb.append("   at ").append(ste).append("\n");
        }
        if (throwable.getCause() != null) {
            sb.append("Caused by: ").append(formatThrowable(throwable.getCause()));
        }
        return sb.toString();
    }

    private String formatMessage(Level level, String message) {
        return new StringBuilder()
                .append(timestamp())
                .append(" [").append(Thread.currentThread().getName()).append("] ")
                .append(level).append(" ")
                .append(loggedAt(klass)).append(" - ")
                .append(message.replaceAll("\n", "\n\t\t\t\t\t"))
                .toString();
    }

    private String timestamp() {
        return new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.getDefault())
                .format(new Date());
    }

    /**
     * 친절하게 소스코드 위치도 찍어준다.
     *
     * @param klass
     * @return
     */
    private String loggedAt(Class<?> klass) {
        StackTraceElement ste = findCaller();
        if (ste == null) {
            return shortClassName(klass.getName());
        } else {
            return new StringBuilder()
                    .append(shortClassName(ste.getClassName()))
                    .append(".")
                    .append(ste.getMethodName())
                    .append("(")
                    .append(ste.getFileName()).append(":").append(ste.getLineNumber())
                    .append(")")
                    .toString();

        }

    }

    /**
     * 이름을 줄인다.
     *
     * e.g. com.kakao.oreum.tamra.Tamra -> c.k.o.t.Tamra
     *
     * @param className
     * @return
     */
    private String shortClassName(String className) {
        // 이름을 줄인다 : (e.g. com.kakao.oreum.tamra.Tamra -> c.k.o.t.Tamra)
        String[] token = className.split("\\.");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < token.length; i++) {
            if (i == (token.length - 1)) {
                sb.append(token[i]);
            } else {
                sb.append(token[i].charAt(0)).append(".");
            }
        }
        return sb.toString();
    }

    private StackTraceElement findCaller() {
        int state = 0; // 0: init, 1: in this class, 2: caller class.
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            // 참고 : stack trace에서 첫번째는 Thread 클래스 쪽이고, 그 이후 이 class의 호출 스택이다.
            //     그리고 더 스택을 따라가서 이 class를 탈출하면 거기가 logger를 호출한 쪽이다.
            // this.klass와 비교하지 않는 이유는 klass의 innerClass등에서 호출하는 경우를 처리하지 못하기 때문.
            if (THIS_CLASS_NAME.equals(ste.getClassName())) {
                state = 1;
            } else if (state == 1) { // 호출 스택에서 이 class를 탈출했음.
                state = 2;
            }
            if (state == 2) { // found caller.
                return ste;
            }
        }
        return null;
    }

    @Override
    public void trace(String format, Object arg) {
        if (level.accept(Level.TRACE))
            printLog(Level.TRACE, format(format, arg));
    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {
        if (level.accept(Level.TRACE))
            printLog(Level.TRACE, format(format, arg1, arg2));
    }

    @Override
    public void trace(String format, Object... arguments) {
        if (level.accept(Level.TRACE))
            printLog(Level.TRACE, format(format, arguments));
    }

    @Override
    public void trace(String message, Throwable throwable) {
        if (level.accept(Level.TRACE))
            printLog(Level.TRACE, message, throwable);
    }

    @Override
    public void debug(String format, Object arg) {
        if (level.accept(Level.DEBUG))
            printLog(Level.DEBUG, format(format, arg));
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        if (level.accept(Level.DEBUG))
            printLog(Level.DEBUG, format(format, arg1, arg2));
    }

    @Override
    public void debug(String format, Object... arguments) {
        if (level.accept(Level.DEBUG))
            printLog(Level.DEBUG, format(format, arguments));
    }

    @Override
    public void debug(String message, Throwable throwable) {
        if (level.accept(Level.DEBUG))
            printLog(Level.DEBUG, message, throwable);
    }

    @Override
    public void info(String format, Object arg) {
        if (level.accept(Level.INFO))
            printLog(Level.INFO, format(format, arg));
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        if (level.accept(Level.INFO))
            printLog(Level.INFO, format(format, arg1, arg2));
    }

    @Override
    public void info(String format, Object... arguments) {
        if (level.accept(Level.INFO))
            printLog(Level.INFO, format(format, arguments));
    }

    @Override
    public void info(String message, Throwable throwable) {
        if (level.accept(Level.INFO))
            printLog(Level.INFO, message, throwable);
    }

    @Override
    public void warn(String format, Object arg) {
        if (level.accept(Level.WARN))
            printLog(Level.WARN, format(format, arg));
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        if (level.accept(Level.WARN))
            printLog(Level.WARN, format(format, arg1, arg2));
    }

    @Override
    public void warn(String format, Object... arguments) {
        if (level.accept(Level.WARN))
            printLog(Level.WARN, format(format, arguments));
    }

    @Override
    public void warn(String message, Throwable throwable) {
        if (level.accept(Level.WARN))
            printLog(Level.WARN, message, throwable);
    }

    @Override
    public void error(String format, Object arg) {
        if (level.accept(Level.ERROR))
            printLog(Level.ERROR, format(format, arg));
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        if (level.accept(Level.ERROR))
            printLog(Level.ERROR, format(format, arg1, arg2));
    }

    @Override
    public void error(String format, Object... arguments) {
        if (level.accept(Level.ERROR))
            printLog(Level.ERROR, format(format, arguments));
    }

    @Override
    public void error(String message, Throwable throwable) {
        if (level.accept(Level.ERROR))
            printLog(Level.ERROR, message, throwable);
    }

    @Override
    public boolean isLoggable(Level level) {
        return this.level.accept(level);
    }
}
