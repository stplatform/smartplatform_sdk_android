/**
 * Oreum Platform utility classes.
 *
 * Android 환경에서 library 의존성 없이 개발하기 위한 잡다한 utility들을 모두 포함한다.
 *
 * {@link com.kakao.oreum.common} 패키지에만 의존한다.
 *
 */
package com.kakao.oreum.ox;
