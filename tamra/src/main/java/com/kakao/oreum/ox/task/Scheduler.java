package com.kakao.oreum.ox.task;

import com.kakao.oreum.common.function.Executable;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Timer 기반의 스케줄러.
 *
 * <p>
 * functional interface를 직접 쓸 수 있도록 {@link Timer}를 wrapping한다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public final class Scheduler {
    private final Timer timer;

    public Scheduler(String name) {
        this.timer = new Timer(name);
    }

    /**
     *
     * @param interval
     * @param executable
     * @return 생성된 TimerTask (작업 중에 중단을 위해서 필요)
     */
    public TimerTask runRepeatly(Period interval, Executable executable) {
        TimerTask task = createTask(executable);
        timer.schedule(
                task, // task
                0, // delay
                interval.toMillis()// period
        );
        return task;
    }

    public void runWhen(Date when, Executable executable) {
        timer.schedule(
                createTask(executable),
                when
        );
    }

    private TimerTask createTask(Executable executable) {
        return new TimerTask() {
            @Override
            public void run() {
                executable.exec();
            }
        };
    }
}
