package com.kakao.oreum.ox.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Classpath 경로의 파일을 읽기 위한 유틸클래스.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
public class ClasspathFiles {

    private ClasspathFiles() {
        // DON'T CREATE INSTANCE.
    }

    /**
     * classpath의 파일에 대한 {@link InputStream}을 반환.
     *
     * @param filePath 파일 경로.
     * @return input stream
     */
    public static InputStream getStream(String filePath) {
        return ClasspathFiles.class.getClassLoader().getResourceAsStream(filePath);
    }

    /**
     * classpath에 있는 파일에 대해 {@link Reader}를 반환.
     *
     * @param filePath 파일 경로
     * @return reader.
     */
    public static Reader getReader(String filePath) {
        return new InputStreamReader(getStream(filePath));
    }

    /**
     * classpath 경로의 파일을 읽어 string으로 반환.
     *
     * @param filePath 파일 경로.
     * @return 파일 내용.
     * @throws IOException IO오류 발생시
     */
    public static String read(String filePath) throws IOException {
        BufferedReader reader = new BufferedReader(getReader(filePath));
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

}
