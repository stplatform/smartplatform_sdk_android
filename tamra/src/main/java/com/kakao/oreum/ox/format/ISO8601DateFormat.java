package com.kakao.oreum.ox.format;

import com.kakao.oreum.common.annotation.ThreadSafe;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Android JVM에서 timezone에 대한 'XXX' 포맷 지시자를 못쓴다.
 * 즉, ISO8601의 '+09:00' 형태의 Timezone을 못쓴다는 얘기.
 *
 * 그래서 별도 tweak.
 *
 * http://stackoverflow.com/questions/28373610/android-parse-string-to-date-unknown-pattern-character-x
 *
 * SimpleDateFormat의 thread-safe 이슈는 매번 생성하는 걸로 해서 회피.

 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@ThreadSafe
public class ISO8601DateFormat extends DateFormat {
    private static final String FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    @Override
    public StringBuffer format(Date date, StringBuffer buffer, FieldPosition field) {
        String format = new SimpleDateFormat(FORMAT, Locale.getDefault()).format(date);
        if (format.matches(".+[+-][0-9]{4}$")) { // '+0900'를 '+09:00' 형태로 바꾼다
            String adjusted = format.substring(0, format.length() - 2) + ":" + format.substring(format.length() - 2);
            buffer.append(adjusted);
        } else {
            buffer.append(format);
        }
        return buffer;
    }

    @Override
    public Date parse(String string, ParsePosition position) {
        String input = string;
        if (string.endsWith("Z")) { // 얘는 'Z'로 처리 못한다. zero timezone은 +0000으로 수정.
            input = input.replaceAll("Z$", "+0000");
        }
        if (string.matches(".+[+-][0-9]{2}:[0-9]{2}$")) { // '+09:00' 형식을 얘가 처리할 수 있게 '+0900'으로 바꿔준다.
            input = input.substring(0, input.length() - 3) + input.substring(input.length() - 2);
        }
        return new SimpleDateFormat(FORMAT, Locale.getDefault()).parse(input, position);
    }
}
