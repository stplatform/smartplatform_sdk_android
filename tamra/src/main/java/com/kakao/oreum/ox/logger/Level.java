package com.kakao.oreum.ox.logger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */

public enum Level {
    NONE(0),
    ERROR(1),
    WARN(2),
    INFO(3),
    DEBUG(4),
    TRACE(5),
    ;

    private final int order;
    Level(int order) {
        this.order = order;
    }

    /**
     * 이 로깅 레벨에서 level의 로그를 출력할 수 있는지 여부.
     *
     * @param level
     * @return
     */
    boolean accept(Level level) {
        return order >= level.order;
    }
}
