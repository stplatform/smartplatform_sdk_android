package com.kakao.oreum.ox.http;

import com.kakao.oreum.ox.helper.Asserts;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public abstract class HttpOpts {
    public static Impl create() {
        return new Impl();
    }

    public static Impl copyOf(HttpOpts opts) {
        Asserts.notNull(opts);
        return new Impl()
                .connectTimeout(opts.connectTimeout())
                .readTimeout(opts.readTimeout())
                .followRedirects(opts.followRedirects());
    }

    public abstract int connectTimeout();

    public abstract int readTimeout();

    public abstract boolean followRedirects();

    @Override
    public String toString() {
        return "HttpOpts{" +
                "connectTimeout=" + connectTimeout() +
                ", readTimeout=" + readTimeout() +
                ", followRedirects=" + followRedirects() +
                '}';
    }


    public static final class Impl extends HttpOpts {
        private int connectTimeout = 3_000;
        private int readTimeout = 3_000;
        private boolean followRedirects = false;

        private Impl() {
        }

        @Override
        public int connectTimeout() {
            return connectTimeout;
        }

        @Override
        public int readTimeout() {
            return readTimeout;
        }

        @Override
        public boolean followRedirects() {
            return followRedirects;
        }

        public Impl connectTimeout(int connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
        }


        public Impl readTimeout(int readTimeout) {
            this.readTimeout = readTimeout;
            return this;
        }

        public Impl followRedirects(boolean followRedirects) {
            this.followRedirects = followRedirects;
            return this;
        }
    }
}
