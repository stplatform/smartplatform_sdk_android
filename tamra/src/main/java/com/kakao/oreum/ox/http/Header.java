package com.kakao.oreum.ox.http;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;
import com.kakao.oreum.ox.helper.Asserts;

/**
 * HTTP header.
 *
 * {@code (name, value)} 쌍을 속성으로 갖는 immutable 객체.
 *
 * 참고: https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Immutable
public final class Header {
    private final String name;
    private final String value;

    private Header(@Nonnull String name, @Nonnull String value) {
        Asserts.notNull(name);
        Asserts.notNull(value);
        this.name = name;
        this.value = value;
    }

    public static Header of(String name, Object value) {
        return new Header(name,
                Optional.ofNullable(value)
                        .map(String::valueOf)
                        .orElse(""));
    }

    @Nonnull
    public String name() {
        return name;
    }

    @Nonnull
    public String value() {
        return value;
    }

    /**
     * 동일 필드인지 체크.
     *
     * @param header 다른 필드.
     * @return name이 같으면 true, 아니면 false.
     */
    public boolean sameField(@Nullable Header header) {
        return Optional.ofNullable(header)
                .map(h -> h.name.equals(this.name))
                .orElse(false);
    }

    public String rawMessage() {
        return name + ": " + value;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Header)) return false;

        Header header = (Header) o;

        if (!name.equals(header.name)) return false;
        return value.equals(header.value);

    }

    @Override
    public String toString() {
        return rawMessage();
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
