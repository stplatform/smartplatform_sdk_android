package com.kakao.oreum.ox.error;

/**
 * Error Observer Interface.
 *
 * <p>
 * 예외가 발생했을 때 callback 받기 위한 interface다. 
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public interface OxErrorObserver {

    void notify(Exception exception);

}
