package com.kakao.oreum.ox.http;

import com.kakao.oreum.common.annotation.Immutable;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;
import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.collection.ImmutableList;
import com.kakao.oreum.ox.helper.Streams;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
@Immutable
public final class Response {
    private final static String CRLF = "\n"; // CR이 있으면 android log에 정상적으로 안찍힘
    private final Status status;
    private final List<Header> headers;
    private final InputStream body;

    Response(@Nonnull Status status, @Nonnull List<Header> headers, @Nullable InputStream body) {
        this.status = status;
        this.headers = ImmutableList.copyOf(headers);
        this.body = body;
    }

    private String statusLine() {
        return "HTTP/1.1 " + status.code() + " " + status.message();
    }

    @Nonnull
    public Status status() {
        return status;
    }

    @Immutable
    public List<Header> headers() {
        return headers;
    }

    public Optional<String> header(String name) {
        return OxIterable.from(headers)
                .find(h -> h.name().equals(name))
                .map(Header::value);
    }

    public InputStream body() {
        return body;
    }

    public byte[] bodyAsBytes() throws IOException {
        return Streams.readAsBytes(body);
    }

    public String bodyAsString() throws IOException {
        return bodyAsString(Charset.forName("UTF-8"));
    }

    public String bodyAsString(Charset charset) throws IOException {
        return new String(bodyAsBytes(), charset);
    }

    @Override
    public String toString() {
        return "HTTP Response: \n" + rawMessage();
    }

    public String rawMessage() {
        return new StringBuilder()
                .append(statusLine()).append(CRLF)
                .append(OxIterable.from(headers)
                        .map(h -> h.rawMessage() + CRLF)
                        .join(""))
                .append(CRLF)
                .append("<< Message body cannot be displayed >>")
                .toString();
    }
}
