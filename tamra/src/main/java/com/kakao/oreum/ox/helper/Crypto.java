package com.kakao.oreum.ox.helper;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class Crypto {
    private Crypto() { /* PREVENT from instantiation */ }
    public static byte[] decrypt(byte[] encrypted, String key) throws CryptoException {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher encipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            encipher.init(Cipher.DECRYPT_MODE, secretKey);
            return encipher.doFinal(encrypted);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException e) {
            throw new CryptoException(e);
        }
    }
}
