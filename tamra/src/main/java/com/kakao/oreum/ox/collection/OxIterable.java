package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Reducer;
import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Iterable을 functional language 스타일로 다루기 위한 클래스.
 *
 * guava의 fluentIterable과 유사
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public final class OxIterable<T> implements Iterable<T> {
    private final Iterable<? extends T> iterable;

    private OxIterable(@Nonnull Iterable<? extends T> iterable) {
        Asserts.notNull(iterable);
        this.iterable = iterable;
    }

    public static <T> OxIterable<T> from(Iterable<? extends T> iterable) {
        if (iterable instanceof OxIterable) {
            return (OxIterable<T>) iterable;
        }
        return new OxIterable<>(iterable);
    }

    public static <T> OxIterable<T> from(T... items) {
        return new OxIterable<>(Lists.arrayList(items));
    }

    public static OxIterable<Long> from(long[] longs) {
        return new OxIterable<>(Lists.arrayList(longs));
    }

    public static <T> OxIterable<T> empty() {
        return new OxIterable<>(Iterables.<T>empty());
    }


    public OxIterable<T> concat(Iterable<? extends T> other) {
        return new OxIterable<>(Iterables.concat(iterable, other));
    }


    public OxIterable<T> filter(@Nonnull Predicate<? super T> predicate) {
        Asserts.notNull(predicate);
        return new OxIterable<>(Iterables.filter(iterable, predicate));
    }

    public <U> OxIterable<U> map(@Nonnull Function<? super T, ? extends U> mapper) {
        Asserts.notNull(mapper);
        return new OxIterable<>(Iterables.map(iterable, mapper));
    }

    public <U> OxIterable<U> flatMap(@Nonnull Function<? super T, Collection<? extends U>> mapper) {
        Asserts.notNull(mapper);
        OxIterable<U> rv = new OxIterable<>(Iterables.empty());
        for (T t : iterable) {
            rv.concat(mapper.apply(t));
        }
        return rv;
    }


    public void doEach(@Nonnull Consumer<? super T> consumer) {
        for (T t : iterable) {
            consumer.accept(t);
        }
    }

    public <S> S reduce(Reducer<S, T> reducer, S initial) {
        S value = initial;
        for (T t : iterable) {
            value = reducer.reduce(value, t);
        }
        return value;
    }

    public <K> ImmutableMap<K, Collection<T>> index(@Nonnull Function<? super T, ? extends K> indexFunc) {
        Map<K, Collection<T>> map = new HashMap<>();
        for (T t : iterable) {
            K key = indexFunc.apply(t);
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<T>());
            }
            map.get(key).add(t);
        }
        return ImmutableMap.of(map);
    }

    public <K> ImmutableMap<T, K> toMap(@Nonnull Function<? super T, ? extends K> valueFunc) {
        Map<T, K> map = new HashMap<>();
        for (T t : iterable) {
            K value = valueFunc.apply(t);
            map.put(t, value);
        }
        return ImmutableMap.of(map);
    }

    public OxIterable<T> sort(Comparator<? super T> comparator) {
        List<T> list = Lists.arrayList(iterable);
        Collections.sort(list, comparator);
        return new OxIterable<>(list);
    }

    public String join(String separator) {
        StringBuilder sb = new StringBuilder();
        for (T t : iterable) {
            sb.append(t.toString()).append(separator);
        }
        if (sb.length() > separator.length()) {
            return sb.substring(0, sb.length() - separator.length());
        } else {
            return sb.toString();
        }
    }

    public Optional<T> find(Predicate<? super T> predicate) {
        return Iterables.find(iterable, predicate);
    }

    public boolean all(Predicate<? super T> predicate) {
        for (T t : iterable) {
            if (!predicate.test(t))
                return false;
        }
        return true;
    }

    public boolean any(Predicate<? super T> predicate) {
        for (T t : iterable) {
            if (predicate.test(t))
                return true;
        }
        return false;
    }

    @Nonnull
    public Optional<T> first() {
        for (T t : iterable) {
            return Optional.of(t);
        }
        return Optional.empty();
    }

    @Nonnull
    public Optional<T> last() {
        T last = null;
        for (T t : iterable) {
            last = t;
        }
        return Optional.ofNullable(last);
    }

    public Optional<T> get(int position) {
        int idx = 0;
        for (T t : iterable) {
            if (idx++ == position) {
                return Optional.of(t);
            }
        }
        return Optional.empty();
    }

    public OxIterable<T> limit(int maxSize) {
        return new OxIterable<>(Iterables.limit(iterable, maxSize));
    }


    public int size() {
        return Iterables.size(iterable);
    }

    @Nonnull
    public ImmutableSet<T> toSet() {
        return ImmutableSet.copyOf(iterable);
    }

    @Nonnull
    public ImmutableList<T> toList() {
        return ImmutableList.copyOf(iterable);
    }

    @Override
    public UnmodifiableIterator<T> iterator() {
        return UnmodifiableIterator.from(iterable.iterator());
    }
}
