package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Lists {

    private Lists() {
    }

    public static <E> List<E> arrayList(@Nonnull Iterable<? extends E> iterable) {
        Asserts.notNull(iterable);
        ArrayList<E> list = new ArrayList<>();
        for (E e : iterable) list.add(e);
        return list;
    }

    public static <E> List<E> arrayList(E... elements) {
        Asserts.notNull(elements);
        ArrayList<E> list = new ArrayList<>();
        for (E e : elements) list.add(e);
        return list;
    }

    public static List<Long> arrayList(long[] elements) {
        Asserts.notNull(elements);
        ArrayList<Long> list = new ArrayList<>();
        for (Long e : elements) list.add(e);
        return list;
    }
    public static List<Integer> arrayList(int[] elements) {
        Asserts.notNull(elements);
        ArrayList<Integer> list = new ArrayList<>();
        for (Integer e : elements) list.add(e);
        return list;
    }

    public static <E> List<E> linkedList(@Nonnull Iterable<? extends E> iterable) {
        Asserts.notNull(iterable);
        LinkedList<E> list = new LinkedList<>();
        for (E e : iterable) list.add(e);
        return list;
    }

    public static <E> List<E> linkedList(E... elements) {
        Asserts.notNull(elements);
        LinkedList<E> list = new LinkedList<>();
        for (E e : elements) list.add(e);
        return list;
    }

}
