package com.kakao.oreum.ox.collection;

import com.kakao.oreum.ox.helper.Asserts;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public final class Sets {

    private Sets() {
    }

    public static <E> Set<E> hashSet() {
        return new HashSet<>();
    }

    public static <E> Set<E> hashSet(Iterable<? extends E> iterable) {
        Asserts.notNull(iterable);
        HashSet<E> set = new HashSet<>();
        for (E e : iterable) set.add(e);
        return set;
    }

    public static <E> Set<E> hashSet(E... elements) {
        Asserts.notNull(elements);
        HashSet<E> set = new HashSet<>();
        for (E e : elements) set.add(e);
        return set;
    }

    public static Set<Long> hashSet(long... elements) {
        Asserts.notNull(elements);
        HashSet<Long> set = new HashSet<>(elements.length);
        for (long e : elements) set.add(e);
        return set;
    }

    public static <E> Set<E> linkedHashSet(Iterable<? extends E> iterable) {
        Asserts.notNull(iterable);
        LinkedHashSet<E> set = new LinkedHashSet<>();
        for (E e : iterable) set.add(e);
        return set;
    }

    public static <E> Set<E> linkedHashSet(E... elements) {
        Asserts.notNull(elements);
        LinkedHashSet<E> set = new LinkedHashSet<>();
        for (E e : elements) set.add(e);
        return set;
    }

    public static <E> Set<E> difference(Set<? extends E> set1, Set<? extends E> set2) {
        Set<E> diff = hashSet();
        for (E e : set1) {
            if (!set2.contains(e)) {
                diff.add(e);
            }
        }
        return diff;
    }

    public static <E> Set<E> intersection(Set<? extends E> set1, Set<? extends E> set2) {
        Set<E> intersection = hashSet();
        for (E e : set1) {
            if (set2.contains(e)) {
                intersection.add(e);
            }
        }

        return intersection;
    }

    public static <E> Set<E> union(Set<? extends E> set1, Set<? extends E> set2) {
        Set<E> union = hashSet(set1);
        union.addAll(set2);
        return union;
    }
}
