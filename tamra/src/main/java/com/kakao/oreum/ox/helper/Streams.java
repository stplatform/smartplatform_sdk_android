package com.kakao.oreum.ox.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public final class Streams {
    private Streams() {
    }

    public static long readAndWrite(InputStream inputStream, OutputStream outputStream) throws IOException {
        long totalBytes = 0;
        int read;
        byte[] buffer = new byte[1024 * 8];

        while ((read = inputStream.read(buffer)) >= 0) {
            outputStream.write(buffer, 0, read);
            totalBytes += read;
        }
        return totalBytes;
    }

    public static byte[] readAsBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        readAndWrite(inputStream, outputStream);
        return outputStream.toByteArray();
    }

    public static String readAsString(InputStream inputStream) throws IOException {
        return readAsString(inputStream, Charset.forName("UTF-8"));
    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        return new String(readAsBytes(inputStream), charset);
    }
}
