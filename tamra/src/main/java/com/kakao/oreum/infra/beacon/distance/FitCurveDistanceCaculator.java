package com.kakao.oreum.infra.beacon.distance;

/**
 * 또다른 거리 계산식.
 *
 * 아래 링크 참조. altbeacon 개발자인 davidgyoung 가 2014년에 쓴 글.
 * http://stackoverflow.com/questions/20416218/understanding-ibeacon-distancing
 *
 * altbeacon 최신코드보다 옛날 글이라 얼마나 신뢰도 있는지 모르겠음.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class FitCurveDistanceCaculator implements DistanceCalculator {
    @Override
    public double distance(int txPower, int rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
        }
    }
}
