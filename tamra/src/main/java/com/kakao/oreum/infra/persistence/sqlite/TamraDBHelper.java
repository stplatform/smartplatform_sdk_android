package com.kakao.oreum.infra.persistence.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class TamraDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "Tamra.db";
    private static final String SQL_DROP = "DROP TABLE IF EXISTS '";

    public TamraDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TamraContract.SyncEntry.SQL_CREATE);
        db.execSQL(TamraContract.BeaconEntry.SQL_CREATE);
        db.execSQL(TamraContract.RegionEntry.SQL_CREATE);
        db.execSQL(TamraContract.SDKLogEntry.SQL_CREATE);
        db.execSQL(TamraContract.HistoryEntry.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP + TamraContract.SyncEntry.TABLE_NAME + "'");
        db.execSQL(SQL_DROP + TamraContract.BeaconEntry.TABLE_NAME + "'");
        db.execSQL(SQL_DROP + TamraContract.RegionEntry.TABLE_NAME + "'");
        db.execSQL(SQL_DROP + TamraContract.SDKLogEntry.TABLE_NAME + "'");
        db.execSQL(SQL_DROP + TamraContract.HistoryEntry.TABLE_NAME + "'");
        onCreate(db);
    }
}
