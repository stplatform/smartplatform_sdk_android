package com.kakao.oreum.infra.geofence;

import com.kakao.oreum.ox.helper.Asserts;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class CircularRegion {
    private long id;
    private double latitude;
    private double longitude;
    private int radius;

    private CircularRegion(long id,
                           double latitude,
                           double longitude,
                           int radius) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.radius = radius;
    }

    public long id() {
        return id;
    }

    public double latitude() {
        return latitude;
    }

    public double longitude() {
        return longitude;
    }

    public int radius() {
        return radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CircularRegion))
            return false;

        CircularRegion that = (CircularRegion) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "CircularRegion{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", radius=" + radius +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long id;
        private Double latitude;
        private Double longitude;
        private Integer radius;

        private Builder() {}

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder radius(int radius) {
            this.radius = radius;
            return this;
        }

        public CircularRegion build() {
            Asserts.notNull(id);
            Asserts.notNull(latitude);
            Asserts.notNull(longitude);
            Asserts.notNull(radius);
            return new CircularRegion(id, latitude, longitude, radius);
        }
    }
}
