package com.kakao.oreum.infra.beacon.altbeacon;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.ox.logger.Logger;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;


/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconServiceFactory {
    private static final Logger LOG = getLogger(BeaconServiceFactory.class);
    private static final LazyInitBeaconServiceProxy beaconServiceProxy = new LazyInitBeaconServiceProxy();

    private BeaconServiceFactory() {/* DO NOT CREATE INSTANCE */}

    public static BeaconService create(Context context) {
        if (!beaconServiceProxy.initialized()) {
            requestAltBeaconConnector(context);
        }
        return beaconServiceProxy;
    }

    private static void requestAltBeaconConnector(Context context) {
        Intent intent = new Intent(context.getApplicationContext(), AltBeaconBridgeService.class);
        context.bindService(intent, new AltBeaconBridgeServiceConnection(), Context.BIND_AUTO_CREATE);
    }

    private static class AltBeaconBridgeServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof AltBeaconConnector) {
                LOG.info("TRACE> AltBeaconBridgeServiceConnection#onServiceConnected called");
                ((AltBeaconConnector) service).setConnectListener(new AltBeaconServiceConnectListener());
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) { /* DO NOTHING */ }
    }

    private static class AltBeaconServiceConnectListener implements AltBeaconConnectListener {
        @Override
        public void connected(AltBeaconBridgeService altBeaconBridgeService) {
            beaconServiceProxy.initWith(new BeaconServiceBasedAltBeacon(altBeaconBridgeService));
        }

        @Override
        public void disconnected() {
            beaconServiceProxy.deinit();
        }
    }
}
