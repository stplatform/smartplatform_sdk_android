package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class SimulatorBeaconParser extends JsonDataParser<SimulatorBeaconData.BeaconSpec> {
    @Override
    protected SimulatorBeaconData.BeaconSpec map(JSONObject jsonObject) throws JSONException {
        return SimulatorBeaconData.BeaconSpec.builder()
                .uuid(jsonObject.getString("uuid"))
                .major(jsonObject.getInt("major"))
                .minor(jsonObject.getInt("minor"))
                .txPower(jsonObject.getInt("txPower"))
                .build();
    }
}
