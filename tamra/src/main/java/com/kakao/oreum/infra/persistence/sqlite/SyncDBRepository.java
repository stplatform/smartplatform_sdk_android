package com.kakao.oreum.infra.persistence.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakao.oreum.ox.logger.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static com.kakao.oreum.infra.persistence.sqlite.TamraContract.SyncEntry;
import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SyncDBRepository {
    private static final Logger LOG = getLogger(SyncDBRepository.class);
    private final SQLiteOpenHelper sqLiteOpenHelper;
    private final DateFormat dateFormat;

    public SyncDBRepository(SQLiteOpenHelper sqLiteOpenHelper, DateFormat dateFormat) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
        this.dateFormat = dateFormat;

        checkInit();
    }

    private void checkInit() {
        if (getLastSyncDateTimeForBeacon() == null) {
            init();
        }
    }

    private void init() {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = new ContentValues();
        Calendar initCal = Calendar.getInstance();
        initCal.add(Calendar.YEAR, -1);
        String initValue = dateFormat.format(initCal.getTime());
        item.put(SyncEntry.COLUMN_NAME_BEACON_LAST_TIME, initValue);
        item.put(SyncEntry.COLUMN_NAME_REGION_LAST_TIME, initValue);
        item.put(SyncEntry.COLUMN_NAME_SDKLOG_LAST_TIME, initValue);

        db.insert(
                SyncEntry.TABLE_NAME,
                null,
                item
        );
    }

    private Cursor getSelectionCursor(String[] columns, String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query (
                SyncEntry.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );
    }

    private Date getLastSyncDateTimeFor(String columnName) {
        Cursor c = getSelectionCursor(
                new String[]{ columnName },
                null,
                null
                );
        Date result = null;
        if (c.moveToNext()) {
            try {
                result = dateFormat.parse(c.getString(c.getColumnIndex(columnName)));
            } catch (ParseException e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }

    private int updateLastSyncDateTimeFor(String columnName, Date last) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = new ContentValues();
        item.put(columnName, dateFormat.format(last));

        return db.update(
                SyncEntry.TABLE_NAME,
                item,
                null,
                null
        );
    }

    public Date getLastSyncDateTimeForRegion() {
        return getLastSyncDateTimeFor(SyncEntry.COLUMN_NAME_REGION_LAST_TIME);
    }

    public int updateLastSyncDateTimeForRegion(Date last) {
        return updateLastSyncDateTimeFor(SyncEntry.COLUMN_NAME_REGION_LAST_TIME, last);
    }

    public Date getLastSyncDateTimeForBeacon() {
        return getLastSyncDateTimeFor(SyncEntry.COLUMN_NAME_BEACON_LAST_TIME);
    }

    public int updateLastSyncDateTimeForBeacon(Date last) {
        return updateLastSyncDateTimeFor(SyncEntry.COLUMN_NAME_BEACON_LAST_TIME, last);
    }

    public Date getLastSyncDateTimeForSDKLog() {
        return getLastSyncDateTimeFor(SyncEntry.COLUMN_NAME_SDKLOG_LAST_TIME);
    }

    public int updateLastSyncDateTimeForSDKLog(Date last) {
        return updateLastSyncDateTimeFor(SyncEntry.COLUMN_NAME_SDKLOG_LAST_TIME, last);
    }
}
