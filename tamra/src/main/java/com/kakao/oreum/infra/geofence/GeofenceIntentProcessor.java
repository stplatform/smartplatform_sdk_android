package com.kakao.oreum.infra.geofence;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.logger.Logger;

import java.util.List;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class GeofenceIntentProcessor extends IntentService {
    private static final Logger LOG = getLogger(GeofenceIntentProcessor.class);

    public GeofenceIntentProcessor() {
        super(GeofenceIntentProcessor.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            LOG.error("#onHandleIntent {}", geofencingEvent.getErrorCode());
            return;
        }

        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

        switch (geofenceTransition) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                OxIterable.from(triggeringGeofences)
                        .doEach(GeofenceEventHandlers::onEnter);
                break;
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                OxIterable.from(triggeringGeofences)
                        .doEach(GeofenceEventHandlers::onExit);
                break;
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                OxIterable.from(triggeringGeofences)
                        .doEach(GeofenceEventHandlers::onDwell);
                break;
            default:
                LOG.info("[GEOFENCE] Other transition is not supported - {}", geofenceTransition);
                break;
        }
    }
}
