package com.kakao.oreum.infra.time;

import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public interface TimeService {

    Date now();

    long currentTimeMillis();

    Date add(long intervalInMillis);

    Date addMilliseconds(Date origin, long intervalInMillis);

    Date addMonth(Date origin, int intervalInMonth);

    Date addYear(Date origin, int intervalInYear);

}
