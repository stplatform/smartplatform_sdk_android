package com.kakao.oreum.infra.beacon.altbeacon;

import android.os.RemoteException;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconFactory;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.logger.Logger;

import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * AltBeacon과 객체 및 이벤트를 translate 하는 역할을 하는 클래스.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
class BeaconServiceBasedAltBeacon implements BeaconService {
    private static final Logger LOG = getLogger(BeaconServiceBasedAltBeacon.class);
    private final AltBeaconBridgeService altBeaconBridgeService;

    private final Set<BeaconRegion> monitoringRegions = new HashSet<>();
    private final Set<BeaconRegion> rangingRegions = new HashSet<>();

    private final BeaconServiceObservers observers = new BeaconServiceObservers();

    BeaconServiceBasedAltBeacon(AltBeaconBridgeService altBeaconBridgeService) {
        this.altBeaconBridgeService = altBeaconBridgeService;
        this.altBeaconBridgeService.addMonitorNotifier(new $MonitorNotifier());
        this.altBeaconBridgeService.addRangeNotifier(new $RangeNotifier());
    }

    @Override
    public void startMonitoringFor(BeaconRegion beaconRegion) {
        try {
            if (monitoringRegions.contains(beaconRegion)) {
                return;
            }
            Region altRegion = convert(beaconRegion);
            altBeaconBridgeService.startMonitoringBeaconsInRegion(altRegion);
            monitoringRegions.add(beaconRegion);
        } catch (RemoteException e) {
            LOG.error("Fail to start monitoring : " + beaconRegion, e);
        }
    }

    @Override
    public void stopMonitoringFor(BeaconRegion beaconRegion) {
        try {
            if (!monitoringRegions.contains(beaconRegion)) {
                return;
            }
            Region altRegion = convert(beaconRegion);
            altBeaconBridgeService.stopMonitoringBeaconsInRegion(altRegion);
            monitoringRegions.remove(beaconRegion);
        } catch (RemoteException e) {
            LOG.error("Fail to stop monitoring : " + beaconRegion, e);
        }
    }

    @Override
    public void startRangingBeaconsIn(BeaconRegion beaconRegion) {
        try {
            if (rangingRegions.contains(beaconRegion)) {
                return;
            }
            Region altRegion = convert(beaconRegion);
            altBeaconBridgeService.startRangingBeaconsInRegion(altRegion);
            rangingRegions.add(beaconRegion);
        } catch (RemoteException e) {
            LOG.error("Fail to start ranging : " + beaconRegion, e);
        }
    }

    @Override
    public void stopRangingBeaconsIn(BeaconRegion beaconRegion) {
        try {
            if (!rangingRegions.contains(beaconRegion)) {
                return;
            }
            Region altRegion = convert(beaconRegion);
            altBeaconBridgeService.stopRangingBeaconsInRegion(altRegion);
            rangingRegions.remove(beaconRegion);
        } catch (RemoteException e) {
            LOG.error("Fail to stop ranging : " + beaconRegion, e);
        }
    }

    @Override
    public Set<BeaconRegion> monitoringRegions() {
        return ImmutableSet.copyOf(monitoringRegions);
    }

    @Override
    public Set<BeaconRegion> rangingRegions() {
        return ImmutableSet.copyOf(rangingRegions);
    }

    @Override
    public void addObserver(BeaconServiceObserver beaconServiceObserver) {
        this.observers.addObserver(beaconServiceObserver);
    }

    @Override
    public void removeObserver(BeaconServiceObserver beaconServiceObserver) {
        this.observers.removeObserver(beaconServiceObserver);
    }

    @Override
    public void pause() {
        altBeaconBridgeService.onPause();
    }

    @Override
    public void resume() {
        altBeaconBridgeService.onResume();
    }

    public BeaconRegion convert(Region region) {
        return new BeaconRegion(
                region.getUniqueId(), // identifier
                region.getId1().toUuid(), // uuid
                region.getId2() == null ? null : region.getId2().toInt(), // major
                region.getId3() == null ? null : region.getId3().toInt() // minor
        );
    }

    public Region convert(BeaconRegion beaconRegion) {
        LOG.info("{}", beaconRegion);
        return new Region(
                beaconRegion.identifier(),
                Identifier.fromUuid(beaconRegion.proximityUUID()),
                beaconRegion.major() == null ? null : Identifier.fromInt(beaconRegion.major()),
                beaconRegion.minor() == null ? null : Identifier.fromInt(beaconRegion.minor())
        );
    }

    //=============================================================================
    // AltBeacon의 monitorNotifier
    //=============================================================================
    private final class $MonitorNotifier implements MonitorNotifier {
        @Override
        public void didEnterRegion(Region region) {
            LOG.info("TRACE> $MonitorNotifier#didEnterRegion");
            BeaconRegion beaconRegion = convert(region);
            if (monitoringRegions.contains(beaconRegion)) {
                observers.didEnterRegion(beaconRegion);
            }
        }

        @Override
        public void didExitRegion(Region region) {
            BeaconRegion beaconRegion = convert(region);
            if (monitoringRegions.contains(beaconRegion)) {
                observers.didExitRegion(beaconRegion);
            }
        }

        @Override
        public void didDetermineStateForRegion(int i, Region region) {
            // not interest
        }
    }

    //=============================================================================
    // AltBeacon의 RangeNotifier
    //=============================================================================
    private final class $RangeNotifier implements RangeNotifier {

        @Override
        public void didRangeBeaconsInRegion(Collection<org.altbeacon.beacon.Beacon> collection,
                                            Region region) {
            BeaconRegion beaconRegion = convert(region);
            // 거리 계산을 다르게 하려면 distanceCaculator를 지정해서 생성할 것
            final BeaconFactory beaconFactory = BeaconFactory.getDefault();
            Set<Beacon> beacons = OxIterable.from(collection)
                    .map(beacon -> beaconFactory.create(
                            beacon.getId1().toUuid(),
                            beacon.getId2().toInt(),
                            beacon.getId3().toInt(),
                            beacon.getTxPower(),
                            beacon.getRssi()
                    )).toSet();
            observers.didRangeBeacons(beacons, beaconRegion);
        }
    }
}
