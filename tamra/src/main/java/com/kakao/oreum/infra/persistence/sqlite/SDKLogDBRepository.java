package com.kakao.oreum.infra.persistence.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakao.oreum.tamra.internal.sdklog.SDKLog;
import com.kakao.oreum.ox.logger.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static com.kakao.oreum.infra.persistence.sqlite.TamraContract.SDKLogEntry;
import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SDKLogDBRepository {
    private static final Logger LOG = getLogger(SDKLogDBRepository.class);
    private final SQLiteOpenHelper sqLiteOpenHelper;
    private final DateFormat dateFormat;

    public SDKLogDBRepository(SQLiteOpenHelper sqLiteOpenHelper, DateFormat dateFormat) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
        this.dateFormat = dateFormat;
    }

    private Cursor getSelectionCursor(String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query(
                SDKLogEntry.TABLE_NAME,
                SDKLogEntry.getAllColumns(),
                selection,
                selectionArgs,
                null,
                null,
                null
        );
    }

    private SDKLog getSDKLogOnCursor(Cursor c) {
        try {
            return new SDKLog(c.getString(c.getColumnIndex(SDKLogEntry.COLUMN_NAME_ACTION)),
                    c.getString(c.getColumnIndex(SDKLogEntry.COLUMN_NAME_PARAM)),
                    dateFormat.parse(c.getString(c.getColumnIndex(SDKLogEntry.COLUMN_NAME_ISSUED_AT)))
                    );
        } catch (ParseException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    public long insert(SDKLog log) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = new ContentValues();
        item.put(SDKLogEntry.COLUMN_NAME_ACTION, log.action());
        item.put(SDKLogEntry.COLUMN_NAME_PARAM, log.param());
        item.put(SDKLogEntry.COLUMN_NAME_ISSUED_AT, dateFormat.format(log.issuedAt()));

        return db.insert(
                SDKLogEntry.TABLE_NAME,
                null,
                item
        );
    }

    public List<SDKLog> find(Date before) {
        Cursor c = getSelectionCursor(
                SDKLogEntry.COLUMN_NAME_ISSUED_AT + " <= ?",
                new String[] { dateFormat.format(before) }
        );

        List<SDKLog> resultSet = new LinkedList<>();
        while (c.moveToNext()) {
            SDKLog sdkLog = getSDKLogOnCursor(c);
            if (sdkLog != null)
                resultSet.add(sdkLog);
        }
        c.close();
        return resultSet;
    }

    public int remove(Date before) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();

        return db.delete(
                SDKLogEntry.TABLE_NAME,
                SDKLogEntry.COLUMN_NAME_ISSUED_AT + " <= ?",
                new String[] { dateFormat.format(before) }
        );
    }
}
