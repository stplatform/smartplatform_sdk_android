package com.kakao.oreum.infra.beacon;

import java.util.Set;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public interface BeaconService {

    void startMonitoringFor(BeaconRegion beaconRegion);

    void stopMonitoringFor(BeaconRegion beaconRegion);

    void startRangingBeaconsIn(BeaconRegion beaconRegion);

    void stopRangingBeaconsIn(BeaconRegion beaconRegion);

    Set<BeaconRegion> monitoringRegions();

    Set<BeaconRegion> rangingRegions();

    void addObserver(BeaconServiceObserver beaconServiceObserver);

    void removeObserver(BeaconServiceObserver beaconServiceObserver);

    void pause();

    void resume();

}
