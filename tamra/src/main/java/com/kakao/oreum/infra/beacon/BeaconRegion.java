package com.kakao.oreum.infra.beacon;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.common.annotation.Nullable;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.UUID;


/**
 * 디바이스 주변의 Bluetooth beacon에 기반한 지역(Region)의 종류를 정의하기 위한 객체.
 *
 * iOS의 CLBeaconRegion에 대응하는 클래스이다.
 *
 * Geographic location에 기반한 Region과는 달리 Beacon을 기반으로 Region을 식별하기 위한 용도로 사용된다.
 * (GeoFence와 유사하게) 지정한 Region에 들어가거나(entered) 빠져나갈때(exited) notification이 발생한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public final class BeaconRegion {

    private final String identifier;

    private final UUID proximityUUID;

    private final Integer major;

    private final Integer minor;

    public BeaconRegion(
            @Nonnull String identifier,
            @Nonnull UUID proximityUUID,
            @Nullable Integer major,
            @Nullable Integer minor) {
        Asserts.notNull(identifier);
        Asserts.notNull(proximityUUID);
        this.identifier = identifier;
        this.proximityUUID = proximityUUID;
        this.major = major;
        this.minor = minor;
    }

    public static BeaconRegion initWith(String identifier, UUID proximityUUID) {
        return new BeaconRegion(identifier, proximityUUID, null, null);
    }

    public static BeaconRegion initWith(String identifier, UUID proximityUUID, int major) {
        return new BeaconRegion(identifier, proximityUUID, major, null);
    }

    public static BeaconRegion initWith(String identifier, UUID proximityUUID, int major, int minor) {
        return new BeaconRegion(identifier, proximityUUID, major, minor);
    }

    @Nonnull
    public String identifier() {
        return identifier;
    }

    @Nonnull
    public UUID proximityUUID() {
        return proximityUUID;
    }

    @Nullable
    public Integer major() {
        return major;
    }

    @Nullable
    public Integer minor() {
        return minor;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BeaconRegion that = (BeaconRegion) o;

        return identifier.equals(that.identifier);
    }

    @Override
    public int hashCode() {
        return identifier.hashCode();
    }

    @Override
    public String toString() {
        return "BeaconRegion{" +
                "identifier='" + identifier + '\'' +
                ", proximityUUID=" + proximityUUID +
                ", major=" + major +
                ", minor=" + minor +
                '}';
    }
}
