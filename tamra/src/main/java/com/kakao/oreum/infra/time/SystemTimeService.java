package com.kakao.oreum.infra.time;

import java.util.Calendar;
import java.util.Date;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class SystemTimeService implements TimeService {

    @Override
    public Date now() {
        return new Date();
    }

    @Override
    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    @Override
    public Date add(long intervalInMillis) {
        return addMilliseconds(now(), intervalInMillis);
    }

    /**
     * 기존 날짜에 interval 만큼의 시간을 더한 날짜를 계산한다
     *
     * @param origin 원래 날짜
     * @param intervalInMillis 추가하고 싶은 시간 (음수 가능)
     * @return 새로운 시간
     * @since 0.11.0
     */
    @Override
    public Date addMilliseconds(Date origin, long intervalInMillis) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(origin.getTime() + intervalInMillis);
        return instance.getTime();
    }

    @Override
    public Date addMonth(Date origin, int intervalInMonth) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(origin.getTime());
        instance.add(Calendar.MONTH, intervalInMonth);
        return instance.getTime();
    }

    @Override
    public Date addYear(Date origin, int intervalInYear) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(origin.getTime());
        instance.add(Calendar.YEAR, intervalInYear);
        return instance.getTime();
    }
}
