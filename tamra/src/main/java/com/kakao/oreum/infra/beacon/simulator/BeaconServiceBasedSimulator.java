package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconFactory;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.infra.beacon.altbeacon.BeaconServiceObservers;
import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.http.Method;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.ox.task.Period;
import com.kakao.oreum.tamra.error.TamraException;
import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * <p>
 *  Simulator 모드에서 내부적인 비콘신호를 서버를 통해서 받아온다.
 * </p>
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class BeaconServiceBasedSimulator implements BeaconService {
    private static final Logger LOG = getLogger(BeaconServiceBasedSimulator.class);

    private final Set<BeaconRegion> monitoringRegions = new HashSet<>();
    private final Set<BeaconRegion> rangingRegions = new HashSet<>();

    private final Set<BeaconRegion> enteredRegions = new HashSet<>();

    private final BeaconServiceObservers observers = new BeaconServiceObservers();

    private final JsonDataParser<SimulatorBeaconData> parser = new SimulatorSignalParser();

    private final String deviceName;
    private final OxHttpClient httpClient;

    private volatile OxTasks.OxTask task;

    /**
     *
     */
    BeaconServiceBasedSimulator(String deviceName, OxHttpClient httpClient) {
        this.deviceName = deviceName;
        this.httpClient = httpClient;
    }

    @Override
    public void startMonitoringFor(BeaconRegion beaconRegion) {
        if (task == null) {
            synchronized (this) {
                task = OxTasks.execRepeatly(Period.milliseconds(450),
                        ExceptionFree.executable(this::getSignal));
            }
        }
        try {
            if (monitoringRegions.contains(beaconRegion)) {
                return;
            }
            monitoringRegions.add(beaconRegion);
        } catch (Exception e) {
            LOG.error("Fail to start monitoring : " + beaconRegion, e);
        }
    }

    @Override
    public void stopMonitoringFor(BeaconRegion beaconRegion) {
        monitoringRegions.remove(beaconRegion);
        checkAndStopTask();
    }

    /**
     * Monitoring/Ranging 하는 Region이 없을 경우 구동된 task를 종료한다
     */
    private void checkAndStopTask() {
        if (monitoringRegions.isEmpty() && rangingRegions.isEmpty()) {
            task.cancel();
            task = null;
        }
    }

    @Override
    public void startRangingBeaconsIn(BeaconRegion beaconRegion) {
        if (task == null) {
            synchronized (this) {
                task = OxTasks.execRepeatly(Period.milliseconds(450),
                        ExceptionFree.executable(this::getSignal));
            }
        }
        try {
            if (rangingRegions.contains(beaconRegion)) {
                return;
            }
            rangingRegions.add(beaconRegion);
        } catch (Exception e) {
            LOG.error("Fail to start ranging : " + beaconRegion, e);
        }
    }

    @Override
    public void stopRangingBeaconsIn(BeaconRegion beaconRegion) {
        rangingRegions.remove(beaconRegion);
        checkAndStopTask();
    }

    @Override
    public Set<BeaconRegion> monitoringRegions() {
        return ImmutableSet.copyOf(monitoringRegions);
    }

    @Override
    public Set<BeaconRegion> rangingRegions() {
        return ImmutableSet.copyOf(rangingRegions);
    }

    @Override
    public void addObserver(BeaconServiceObserver beaconServiceObserver) {
        observers.addObserver(beaconServiceObserver);
    }

    @Override
    public void removeObserver(BeaconServiceObserver beaconServiceObserver) {
        observers.removeObserver(beaconServiceObserver);
    }

    @Override
    public void pause() {
        // do nothing
    }

    @Override
    public void resume() {
        // do nothing
    }

    private void getSignal() {
        ExceptionFree.exec(() -> {
            String json = httpClient.requestFor("/locationsimulation/signals")
                    .params("deviceName", deviceName)
                    .emptyBody()
                    .request(Method.GET)
                    .bodyAsString();
            List<? extends SimulatorBeaconData> fetched = parser.parseArray(json);
            LOG.debug("fetch size : {}", fetched.size());
            monitorRegion(fetched);
            rangedBeacons(fetched);
        }, OxErrors.wrapAndNotify(e ->
                new TamraException("Fail to fetch beacon signal", e)
        ));
    }

    void monitorRegion(List<? extends SimulatorBeaconData> simulatorBeaconDatas) {
        Set<BeaconRegion> sessionAdded = new HashSet<>();
        for (SimulatorBeaconData simulatorBeaconData : simulatorBeaconDatas) {
            BeaconRegion region = getMatched(monitoringRegions, simulatorBeaconData.uuid()).orElse(null);

            if (region != null) {
                if (!enteredRegions.contains(region)) {
                    observers.didEnterRegion(region);
                    enteredRegions.add(region);
                }
                sessionAdded.add(region);
            }
        }

        Set<BeaconRegion> removed = new HashSet<>();
        for (BeaconRegion region : enteredRegions) {
            if (!sessionAdded.contains(region)) {
                observers.didExitRegion(region);
                removed.add(region);
            }
        }

        sessionAdded.clear();

        for (BeaconRegion region: removed) {
            enteredRegions.remove(region);
        }

        removed.clear();
    }

    void rangedBeacons(List<? extends SimulatorBeaconData> simulatorBeaconDatas) {
        Map<BeaconRegion, Set<Beacon>> regionBeaconMap = new HashMap<>();

        for (SimulatorBeaconData simulatorBeaconData: simulatorBeaconDatas) {
            BeaconRegion region = getMatched(rangingRegions, simulatorBeaconData.uuid()).orElse(null);
            if (region != null) {
                Beacon beacon = BeaconFactory.getDefault()
                        .create(UUID.fromString(simulatorBeaconData.uuid()),
                                simulatorBeaconData.major(),
                                simulatorBeaconData.minor(),
                                simulatorBeaconData.txPower(),
                                generateRssi(simulatorBeaconData.txPower(), simulatorBeaconData.accuracy()));
                Set<Beacon> beaconInRegion;
                if (regionBeaconMap.containsKey(region)) {
                    beaconInRegion = regionBeaconMap.get(region);
                } else {
                    beaconInRegion = new HashSet<>();
                    regionBeaconMap.put(region, beaconInRegion);
                }
                beaconInRegion.add(beacon);
            }
        }
        for (BeaconRegion region : regionBeaconMap.keySet()) {
            Set<Beacon> beaconSet = regionBeaconMap.get(region);
            observers.didRangeBeacons(beaconSet, region);
            beaconSet.clear();
        }
        regionBeaconMap.clear();
    }

    private Optional<BeaconRegion> getMatched(Set<BeaconRegion> regions, String uuid) {
        return OxIterable.from(regions).find(region ->
                region.proximityUUID().toString().equalsIgnoreCase(uuid));
    }

    protected int generateRssi(int txPower, double accuracy) {
        double distance = accuracy - Math.random() % accuracy;
        return (int)Math.round(txPower - 2.0 * Math.log10(distance));
    }
}
