package com.kakao.oreum.infra.geofence;

import android.app.PendingIntent;
import android.support.annotation.NonNull;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.error.TamraException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * 구글 API 기반의 geofence 구현
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class GeofenceServiceBasedGoogleAPI implements GeofenceService {
    private static final Logger LOG = getLogger(GeofenceServiceBasedGoogleAPI.class);
    private static final int GEOFENCE_EXPIRATION_IN_HOURS = 1;
    private final Set<GeofenceObserver> observers = new HashSet<>();
    private final GoogleApiClientDelegate googleApiClientDelegate;

    private final Map<String, CircularRegion> mapRegions = new LinkedHashMap<>();
    private final Map<CircularRegion, Geofence> circularRegionGeofenceMap = new LinkedHashMap<>();

    private final List<Geofence> geofenceList = new LinkedList<>();
    private final PendingIntent geofencePendingIntent;

    GeofenceServiceBasedGoogleAPI(GoogleApiClientDelegate googleApiClientDelegate, PendingIntent geofencePendingIntent) {
        Asserts.notNull(googleApiClientDelegate);
        Asserts.notNull(geofencePendingIntent);
        this.googleApiClientDelegate = googleApiClientDelegate;
        this.geofencePendingIntent = geofencePendingIntent;

        GeofenceEventHandlers.register(geofenceEventHandler);
    }

    @Override
    public void startMonitoring(CircularRegion region) {
        String requestId = getRegionRequestID(region);
        Geofence geofence = buildGeofence(requestId, region.latitude(), region.longitude(), region.radius());
        geofenceList.add(geofence);

        if (!mapRegions.containsKey(requestId)) {
            mapRegions.put(requestId, region);
            circularRegionGeofenceMap.put(region, geofence);
        }

        requestGeofenceApi();
    }

    @Override
    public void stopMonitoring(CircularRegion region) {
        removeGeofence(circularRegionGeofenceMap.get(region));
    }

    @Override
    public void addObserver(GeofenceObserver observer) {
        // if (!observers.contains(observer))
        observers.add(observer);
    }

    @Override
    public void removeObserver(GeofenceObserver observer) {
        observers.remove(observer);
    }
    
    private String getRegionRequestID(CircularRegion region) {
        return String.format(Locale.KOREA, "%f-%f", region.latitude(), region.longitude());
    }
    
    /**
     * Android 내부의 Geofence객체를 생성
     *
     * @param key            Geofence 별 구분자
     * @param latitude       위도
     * @param longitude      경도
     * @param radiusInMeters 반경
     * @return 내부 Geofence 구현체
     */
    @NonNull
    private Geofence buildGeofence(String key, double latitude, double longitude, float radiusInMeters) {
        return new Geofence.Builder()
                .setRequestId(key)
                .setCircularRegion(latitude, longitude, radiusInMeters)
                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    @NonNull
    private GeofencingRequest getGeofencingRequest() {
        return new GeofencingRequest.Builder()
                // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
                // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
                // is already inside that geofence.
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)

                // Add the geofences to be monitored by geofencing service.
                .addGeofences(geofenceList)

                // Return a GeofencingRequest.
                .build();
    }

    private void requestGeofenceApi() {
        try {
            LocationServices.GeofencingApi.addGeofences(
                    googleApiClientDelegate.get(),
                    getGeofencingRequest(),
                    geofencePendingIntent
            ).setResultCallback(status -> {
                LOG.debug("#requestGeofenceApi [GOOGLE API STATUS] {}", status);
                if (!status.isSuccess()) {
                    throw new TamraException(status.getStatusMessage());
                }
            });
        } catch (SecurityException ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            OxErrors.wrapAndNotify(TamraException::new);
        }
    }

    /**
     * Removes geofences, which stops further notifications when the device enters or exits
     * previously registered geofences.
     */
    private void removeGeofence(Geofence geo) {
        geofenceList.remove(geo);

        List<String> requestIds = new ArrayList<>();
        requestIds.add(geo.getRequestId());
        try {
            LocationServices.GeofencingApi.removeGeofences(
                    googleApiClientDelegate.get(),
                    requestIds
            ).setResultCallback(status -> {
                LOG.debug("#removeGeofence [GOOGLE API STATUS] {}", status);
                if (!status.isSuccess()) {
                    throw new TamraException(status.getStatusMessage());
                }
            });
        } catch (SecurityException ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            OxErrors.wrapAndNotify(TamraException::new);
        }
    }

    /**
     * 구글의 {@link Geofence}에 대한 Event를 처리할 핸들러.
     */
    private final GeofenceEventHandler geofenceEventHandler = new GeofenceEventHandler() {
        /**
         * 내부에서 올라오는 Enter 이벤트 handler
         *
         * @param geofence 이벤트가 발생한 geofence
         */
        @Override
        public void onEnter(Geofence geofence) {
            CircularRegion region = mapRegions.get(geofence.getRequestId());
            if (region != null) {
                OxIterable.from(observers).doEach(observer -> observer.didEnterRegion(region));
            }
        }

        /**
         * 내부에서 올라오는 Exit 이벤트 Handler
         *
         * @param geofence 이벤트가 발생한 geofence
         */
        @Override
        public void onExit(Geofence geofence) {
            CircularRegion region = mapRegions.get(geofence.getRequestId());
            if (region != null) {
                OxIterable.from(observers).doEach(observer -> observer.didExitRegion(region));
            }
        }

        /**
         * 내부에서 올라오는 Dwell 이벤트 Handler
         *
         * @param geofence 이벤트가 발생한 geofence
         */
        @Override
        public void onDwell(Geofence geofence) {
            // Ignore
        }
    };
}
