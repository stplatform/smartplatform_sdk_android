package com.kakao.oreum.infra.beacon.altbeacon;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;

import org.altbeacon.beacon.service.RssiFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.3.0
 */
public class AverageRssiFilter implements RssiFilter {
    public static final long DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS = 20000; /* 20 seconds */
    private static final Logger LOG = getLogger(AverageRssiFilter.class);
    private static long sampleExpirationMilliseconds = DEFAULT_SAMPLE_EXPIRATION_MILLISECONDS;
    private final TimeService timeService;
    private List<Measurement> mMeasurements = new ArrayList<>();

    public AverageRssiFilter() {
        this(new SystemTimeService());
    }

    public AverageRssiFilter(TimeService timeService) {
        this.timeService = timeService;
    }

    public static void setSampleExpirationMilliseconds(long newSampleExpirationMilliseconds) {
        sampleExpirationMilliseconds = newSampleExpirationMilliseconds;
    }

    @Override
    public void addMeasurement(Integer rssi) {
        Measurement measurement = new Measurement();
        measurement.rssi = rssi;
        measurement.timestamp = timeService.now().getTime();
        mMeasurements.add(measurement);
    }

    @Override
    public boolean noMeasurementsAvailable() {
        return mMeasurements.isEmpty();
    }

    @Override
    public double calculateRssi() {
        refreshMeasurements();
        int size = mMeasurements.size();
        int startIndex = 0;
        int endIndex = size - 1;
        if (size > 2) {
            startIndex = size / 10 + 1;
            endIndex = size - size / 10 - 2;
        }

        double sum = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            sum += mMeasurements.get(i).rssi;
        }
        double runningAverage = sum / (endIndex - startIndex + 1);

        LOG.debug("Running average mRssi based on %s measurements: %s", size, runningAverage);
        return runningAverage;
    }

    private synchronized void refreshMeasurements() {
        Date now = timeService.now();
        List<Measurement> newMeasurements = new ArrayList<>();
        for (Measurement measurement : mMeasurements) {
            if (now.getTime() - measurement.timestamp < sampleExpirationMilliseconds) {
                newMeasurements.add(measurement);
            }
        }
        mMeasurements = newMeasurements;
        Collections.sort(mMeasurements);
    }

    private class Measurement implements Comparable<Measurement> {
        Integer rssi;
        long timestamp;

        @Override
        public int compareTo(Measurement arg0) {
            return rssi.compareTo(arg0.rssi);
        }

        @Override
        public boolean equals(Object other) {
            if (other instanceof Measurement) {
                Measurement that = (Measurement) other;
                return rssi != null && rssi.equals(that.rssi) && timestamp == that.timestamp;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return 31 * Long.valueOf(timestamp).hashCode() + (rssi != null ? rssi.hashCode() : 0);
        }
    }
}
