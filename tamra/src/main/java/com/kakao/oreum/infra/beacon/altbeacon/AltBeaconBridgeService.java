package com.kakao.oreum.infra.beacon.altbeacon;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import com.kakao.oreum.common.annotation.Nonnull;
import com.kakao.oreum.ox.logger.Logger;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.logging.LogManager;
import org.altbeacon.beacon.logging.Loggers;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;


/**
 * altbeacon과의 bridging을 위한 service.
 *
 * altbeacon service의 초기화를 담당하며 altbeacon과의 연결을 위해 {@link AltBeaconConnector}를 {@link IBinder}로 제공한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class AltBeaconBridgeService extends Service implements BeaconConsumer {
    private static final String IBEACON_LAYOUT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";
    private static final Logger LOG = getLogger(AltBeaconBridgeService.class);
    private final AltBeaconConnector altBeaconConnector = new AltBeaconConnector();
    private BeaconManager beaconManager;

    @Override
    public void onCreate() {
        LOG.debug("onCreate");
        this.beaconManager = BeaconManager.getInstanceForApplication(this);
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(IBEACON_LAYOUT));
        beaconManager.bind(this);

        // 비콘 안정화 할 때 평균되니깐 여기선 안해도 되지 않을까?
//        BeaconManager.setRssiFilterImplClass(AverageRssiFilter.class);

        LogManager.setLogger(Loggers.warningLogger());
        LogManager.setVerboseLoggingEnabled(false);
    }

    @Override
    public void onDestroy() {
        LOG.debug("onDestroy");
        beaconManager.removeAllRangeNotifiers();
        beaconManager.removeAllMonitorNotifiers();

        beaconManager.unbind(this);
        altBeaconConnector.disconnect();
    }

    @Nonnull
    @Override
    public IBinder onBind(Intent intent) {
        LOG.debug("onBind");
        return altBeaconConnector;
    }

    @Override
    public void onBeaconServiceConnect() {
        LOG.debug("onBeaconServiceConnect");
        altBeaconConnector.connect(AltBeaconBridgeService.this);
    }

    public void startMonitoringBeaconsInRegion(Region region) throws RemoteException {
        beaconManager.startMonitoringBeaconsInRegion(region);
    }

    public void stopMonitoringBeaconsInRegion(Region region) throws RemoteException {
        beaconManager.stopMonitoringBeaconsInRegion(region);
    }

    public void startRangingBeaconsInRegion(Region region) throws RemoteException {
        beaconManager.startRangingBeaconsInRegion(region);
    }

    public void stopRangingBeaconsInRegion(Region region) throws RemoteException {
        beaconManager.stopRangingBeaconsInRegion(region);
    }

    public void addMonitorNotifier(MonitorNotifier monitorNotifier) {
        beaconManager.addMonitorNotifier(monitorNotifier);

    }

    public void addRangeNotifier(RangeNotifier rangeNotifier) {
        beaconManager.addRangeNotifier(rangeNotifier);
    }

    public void onPause() {
        if (beaconManager.isBound(this)) {
            beaconManager.setBackgroundMode(true);
        }
    }

    public void onResume() {
        if (beaconManager.isBound(this)) {
            beaconManager.setBackgroundMode(false);
        }
    }
}
