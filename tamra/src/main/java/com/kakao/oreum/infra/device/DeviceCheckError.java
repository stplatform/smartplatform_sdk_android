package com.kakao.oreum.infra.device;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class DeviceCheckError extends RuntimeException {
    public DeviceCheckError() {
        super();
    }

    public DeviceCheckError(String detailMessage) {
        super(detailMessage);
    }

    public DeviceCheckError(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public DeviceCheckError(Throwable throwable) {
        super(throwable);
    }
}
