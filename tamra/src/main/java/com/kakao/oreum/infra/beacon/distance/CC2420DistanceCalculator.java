package com.kakao.oreum.infra.beacon.distance;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class CC2420DistanceCalculator implements DistanceCalculator {
    private static final double distanceFactor = 2.0;

    @Override
    public double distance(int txPower, int rssi) {
        /*
         * RSSI = TxPower - 10 * n * lg(d)
         * n = 2 (in free space)
         *
         * d = 10 ^ ((TxPower - RSSI) / (10 * n))
         */
        return Math.pow(10d, ((double) txPower - rssi) / (10 * distanceFactor));
    }
}
