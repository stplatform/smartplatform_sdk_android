package com.kakao.oreum.infra.beacon;

import com.kakao.oreum.common.annotation.Accessibility;

import java.util.UUID;

/**
 * Region 모니터링 중에 만나게 되는 beacon을 표현하기 위한 클래스.
 * <p>
 * iOS의 CLBeacon 클래스에 대응하는 클래스이다.
 * <p>
 * 직접 이 클래스의 인스턴스를 생성할 수 없다. 오직 주변의 beacon들은 {@link BeaconServiceObserver}를 통해서만 전달된다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public final class Beacon {

    private final UUID proximityUUID;

    private final int major;

    private final int minor;

    private final Proximity proximity;

    private final double accuracy;

    private final int rssi;

    private final int txPower;

    @Accessibility.Private
    public Beacon(
            UUID proximityUUID,
            int major,
            int minor,
            Proximity proximity,
            double accuracy,
            int rssi, int txPower) {
        this.proximityUUID = proximityUUID;
        this.major = major;
        this.minor = minor;
        this.proximity = proximity;
        this.accuracy = accuracy;
        this.rssi = rssi;
        this.txPower = txPower;
    }

    public UUID proximityUUID() {
        return proximityUUID;
    }

    public int major() {
        return major;
    }

    public int minor() {
        return minor;
    }

    public Proximity proximity() {
        return proximity;
    }

    public double accuracy() {
        return accuracy;
    }

    public int rssi() {
        return rssi;
    }

    public int txPower() {
        return txPower;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Beacon beacon = (Beacon) o;
        if (major != beacon.major || minor != beacon.minor) {
            return false;
        }
        return proximityUUID.equals(beacon.proximityUUID);

    }

    @Override
    public int hashCode() {
        int result = proximityUUID.hashCode();
        result = 31 * result + major;
        result = 31 * result + minor;
        return result;
    }

    @Override
    public String toString() {
        return "Beacon{" +
                "proximityUUID='" + proximityUUID + '\'' +
                ", major=" + major +
                ", minor=" + minor +
                ", proximity=" + proximity +
                ", accuracy=" + accuracy +
                ", rssi=" + rssi +
                ", txPower=" + txPower +
                '}';
    }

}
