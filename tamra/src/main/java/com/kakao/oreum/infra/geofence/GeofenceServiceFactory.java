package com.kakao.oreum.infra.geofence;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class GeofenceServiceFactory {
    private GeofenceServiceFactory() { /* DO NOT CREATE INSTANCE */ }

    public static GeofenceService create(Context context) {
        PendingIntent pendingIntent =
                PendingIntent.getService(context, 0, new Intent(context, GeofenceIntentProcessor.class),
                        PendingIntent.FLAG_UPDATE_CURRENT);

        return new GeofenceServiceBasedGoogleAPI(
                new GoogleApiClientDelegate(context, pendingIntent), pendingIntent);
    }
}
