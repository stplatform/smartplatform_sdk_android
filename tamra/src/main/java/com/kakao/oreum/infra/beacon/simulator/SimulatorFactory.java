package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.ox.http.OxHttpClient;

/**
 *
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class SimulatorFactory {
    private static BeaconServiceBasedSimulator simulator;
    private SimulatorFactory() { /* FORBIDDEN FROM INSTANTIATION */ }

    public static BeaconService create(OxHttpClient httpClient, String deviceName) {
        if (null == simulator) {
            simulator = new BeaconServiceBasedSimulator(deviceName, httpClient);
        }
        return simulator;
    }
}