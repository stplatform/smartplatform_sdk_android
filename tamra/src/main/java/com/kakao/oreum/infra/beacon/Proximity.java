package com.kakao.oreum.infra.beacon;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public enum Proximity {
    Immediate,
    Near,
    Far,
    Unknown;
}
