package com.kakao.oreum.infra.persistence.sqlite;

import android.provider.BaseColumns;

import java.util.Arrays;

/**
 * SQLite용 스키마 정보
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public final class TamraContract {
    private TamraContract() {}

    public abstract static class TamraEntry implements BaseColumns {
        public static final String COLUMN_NAME_CREATED_AT = "created_at";
        public static final String COLUMN_NAME_UPDATED_AT = "updated_at";
    }

    public abstract static class SyncEntry extends TamraEntry {
        public static final String TABLE_NAME = "sync";
        public static final String COLUMN_NAME_BEACON_LAST_TIME = "beacon_last_time";
        public static final String COLUMN_NAME_REGION_LAST_TIME = "region_last_time";
        public static final String COLUMN_NAME_SDKLOG_LAST_TIME = "sdklog_last_time";

        public static final String SQL_CREATE =
                String.format("CREATE TABLE %s (\n" +
                              _ID + " INTEGER PRIMARY KEY,\n" +
                              COLUMN_NAME_BEACON_LAST_TIME + " TEXT,\n" +
                              COLUMN_NAME_REGION_LAST_TIME + " TEXT,\n" +
                              COLUMN_NAME_SDKLOG_LAST_TIME + " TEXT,\n" +
                              COLUMN_NAME_CREATED_AT + " TEXT,\n" +
                              COLUMN_NAME_UPDATED_AT + " TEXT\n" +
                              ")", TABLE_NAME);
    }

    public abstract static class BeaconEntry extends TamraEntry {
        public static final String TABLE_NAME = "beacon";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_REGION_ID = "region_id";
        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_MAJOR = "major";
        public static final String COLUMN_NAME_MINOR = "minor";
        public static final String COLUMN_NAME_INDOOR = "indoor";
        public static final String COLUMN_NAME_MOVABLE = "movable";
        public static final String COLUMN_NAME_TXPOWER = "txpower";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_BEACONTYPE = "beacon_type";
        public static final String COLUMN_NAME_GEOHASH = "geohash";

        private static final String[] ALL_FIELDS = {
                COLUMN_NAME_ID,
                COLUMN_NAME_REGION_ID,
                COLUMN_NAME_UUID,
                COLUMN_NAME_MAJOR,
                COLUMN_NAME_MINOR,
                COLUMN_NAME_INDOOR,
                COLUMN_NAME_MOVABLE,
                COLUMN_NAME_TXPOWER,
                COLUMN_NAME_LATITUDE,
                COLUMN_NAME_LONGITUDE,
                COLUMN_NAME_DESCRIPTION,
                COLUMN_NAME_BEACONTYPE,
                COLUMN_NAME_GEOHASH
        };

        public static String[] getAllColumns() {
            return Arrays.copyOf(ALL_FIELDS, ALL_FIELDS.length);
        }

        public static final String SQL_CREATE =
                String.format("CREATE TABLE %s (\n" +
                              _ID + " INTEGER PRIMARY KEY,\n" +
                              COLUMN_NAME_ID + " INTEGER unique,\n" +
                              COLUMN_NAME_REGION_ID + " INTEGER,\n" +
                              COLUMN_NAME_UUID + " TEXT,\n" +
                              COLUMN_NAME_MAJOR + " INTEGER,\n" +
                              COLUMN_NAME_MINOR + " INTEGER,\n" +
                              COLUMN_NAME_INDOOR + " NUMERIC,\n" +
                              COLUMN_NAME_MOVABLE + " NUMERIC,\n" +
                              COLUMN_NAME_TXPOWER + " INTEGER,\n" +
                              COLUMN_NAME_LATITUDE + " REAL,\n" +
                              COLUMN_NAME_LONGITUDE + " REAL,\n" +
                              COLUMN_NAME_DESCRIPTION + " TEXT,\n" +
                              COLUMN_NAME_BEACONTYPE + " TEXT,\n" +
                              COLUMN_NAME_GEOHASH + " TEXT,\n" +
                              COLUMN_NAME_CREATED_AT + " TEXT,\n" +
                              COLUMN_NAME_UPDATED_AT + " TEXT\n" +
                              ")", TABLE_NAME);
    }

    public abstract static class RegionEntry extends TamraEntry {
        public static final String TABLE_NAME = "region";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_RADIUS = "radius";
        public static final String COLUMN_NAME_GEOHASH = "geohash";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_REGIONTYPE = "region_type";

        private static final String[] ALL_FIELDS = {
                COLUMN_NAME_ID,
                COLUMN_NAME_NAME,
                COLUMN_NAME_UUID,
                COLUMN_NAME_LATITUDE,
                COLUMN_NAME_LONGITUDE,
                COLUMN_NAME_RADIUS,
                COLUMN_NAME_GEOHASH,
                COLUMN_NAME_DESCRIPTION,
                COLUMN_NAME_REGIONTYPE
        };

        public static String[] getAllColumns() {
            return Arrays.copyOf(ALL_FIELDS, ALL_FIELDS.length);
        }

        public static final String SQL_CREATE =
                String.format("CREATE TABLE %s (\n" +
                        _ID + " INTEGER PRIMARY KEY,\n" +
                        COLUMN_NAME_ID + " INTEGER unique,\n" +
                        COLUMN_NAME_NAME + " TEXT,\n" +
                        COLUMN_NAME_UUID + " TEXT,\n" +
                        COLUMN_NAME_RADIUS + " INTEGER,\n" +
                        COLUMN_NAME_LATITUDE + " REAL,\n" +
                        COLUMN_NAME_LONGITUDE + " REAL,\n" +
                        COLUMN_NAME_DESCRIPTION + " TEXT,\n" +
                        COLUMN_NAME_REGIONTYPE + " TEXT,\n" +
                        COLUMN_NAME_GEOHASH + " TEXT,\n" +
                        COLUMN_NAME_CREATED_AT + " TEXT,\n" +
                        COLUMN_NAME_UPDATED_AT + " TEXT\n" +
                        ")", TABLE_NAME);
    }

    /**
     * @since 0.11.0
     */
    public abstract static class SDKLogEntry implements BaseColumns {
        public static final String TABLE_NAME = "sdklog";
        public static final String COLUMN_NAME_ACTION = "action";
        public static final String COLUMN_NAME_PARAM = "param";
        public static final String COLUMN_NAME_ISSUED_AT = "issued_at";

        private static final String[] ALL_FIELDS = {
                COLUMN_NAME_ACTION,
                COLUMN_NAME_PARAM,
                COLUMN_NAME_ISSUED_AT
        };

        public static String[] getAllColumns() {
            return Arrays.copyOf(ALL_FIELDS, ALL_FIELDS.length);
        }

        public static final String SQL_CREATE =
                String.format("CREATE TABLE %s (\n" +
                        _ID + " INTEGER PRIMARY KEY,\n" +
                        COLUMN_NAME_ACTION + " TEXT,\n" +
                        COLUMN_NAME_PARAM + " TEXT,\n" +
                        COLUMN_NAME_ISSUED_AT + " TEXT\n" +
                        ")", TABLE_NAME);
    }

    /**
     * @since 0.11.0
     */
    public abstract static class HistoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "history";
        public static final String COLUMN_NAME_SPOT_ID = "spot_id";
        public static final String COLUMN_NAME_PROXIMITY = "proximity";
        public static final String COLUMN_NAME_ACCURACY = "accuracy";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_ISSUED_AT = "issued_at";

        private static final String[] ALL_FIELDS = {
                COLUMN_NAME_SPOT_ID,
                COLUMN_NAME_PROXIMITY,
                COLUMN_NAME_ACCURACY,
                COLUMN_NAME_DESCRIPTION,
                COLUMN_NAME_DATA,
                COLUMN_NAME_ISSUED_AT
        };

        public static String[] getAllColumns() {
            return Arrays.copyOf(ALL_FIELDS, ALL_FIELDS.length);
        }

        public static final String SQL_CREATE =
                String.format("CREATE TABLE %s (\n" +
                        _ID + " INTEGER PRIMARY KEY,\n" +
                        COLUMN_NAME_SPOT_ID + " INTEGER,\n" +
                        COLUMN_NAME_PROXIMITY + " TEXT,\n" +
                        COLUMN_NAME_ACCURACY + " REAL,\n" +
                        COLUMN_NAME_DESCRIPTION + " TEXT,\n" +
                        COLUMN_NAME_DATA + " TEXT,\n" +
                        COLUMN_NAME_ISSUED_AT + " TEXT\n" +
                        ")", TABLE_NAME);
    }
}
