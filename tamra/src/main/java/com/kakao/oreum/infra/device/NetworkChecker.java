package com.kakao.oreum.infra.device;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 네트워크 접속 상태 확인
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class NetworkChecker implements DeviceStateChecker {
    private final Context context;

    public NetworkChecker(Context context) {
        this.context = context;
    }

    /**
     * 네트워크 접속 상태
     *
     * @return 네트워크에 접속된 경우 true, 아니면 false
     */
    @Override
    public boolean check() {
        NetworkInfo networkInfo = getNetworkInfo();

        return networkInfo != null &&
                (networkInfo.getType() == ConnectivityManager.TYPE_WIFI ||
                        networkInfo.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * 네트워크 정보 객체 추출
     *
     * @return
     */
    private NetworkInfo getNetworkInfo() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return connectivityManager.getActiveNetworkInfo();
    }

    /**
     * WiFi 접속인지 확인 (서버 통신시 사용)
     *
     * @return WiFi 접속이 된 상태이면 true, 아니면 false
     * @since 0.11.0
     */
    public boolean wifiConnected() {
        NetworkInfo networkInfo = getNetworkInfo();

        return networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**
     * Assert 방식으로 체크
     *
     * @throws DeviceCheckError
     */
    @Override
    public void assertOk() throws DeviceCheckError {
        if (!check()) {
            throw new DeviceCheckError("Networks is NOT enable");
        }
    }

    /**
     * 기기의 MAC 주소들을 가져온다.
     *
     * @return
     */
    public Map<String, String> getHardwareAddresses() {
        Map<String, String> addresses = new LinkedHashMap<>();
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                byte[] raw = intf.getHardwareAddress();
                if (raw == null) continue;
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < raw.length; idx++)
                    buf.append(String.format("%02X:", raw[idx]));
                if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
                addresses.put(intf.getName(), buf.toString());
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return addresses;
    }
}
