package com.kakao.oreum.infra.beacon.altbeacon;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconServiceObservers implements BeaconServiceObserver {
    private static final Logger LOG = getLogger(BeaconServiceObservers.class);
    private final Set<BeaconServiceObserver> observers = new LinkedHashSet<>();

    public void addObserver(BeaconServiceObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(BeaconServiceObserver observer) {
        this.observers.remove(observer);
    }

    @Override
    public void didEnterRegion(BeaconRegion beaconRegion) {
        for (BeaconServiceObserver observer : observers) {
            try {
                observer.didEnterRegion(beaconRegion);
            } catch (Exception e) {
                LOG.error("Error occurred while handling 'didEnterRegion' event : " + observer, e);
            }
        }
    }

    @Override
    public void didExitRegion(BeaconRegion beaconRegion) {
        for (BeaconServiceObserver observer : observers) {
            try {
                observer.didExitRegion(beaconRegion);
            } catch (Exception e) {
                LOG.error("Error notifyError while handling 'didExitRegion' event: " + observer, e);
            }
        }
    }

    @Override
    public void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion) {
        for (BeaconServiceObserver observer : observers) {
            try {
                observer.didRangeBeacons(beacons, beaconRegion);
            } catch (Exception e) {
                LOG.error("Error notifyAll while handling 'didRangeBeacons' event: " + observer, e);
            }
        }
    }
}
