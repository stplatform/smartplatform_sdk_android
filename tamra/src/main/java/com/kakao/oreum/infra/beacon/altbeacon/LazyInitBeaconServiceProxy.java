package com.kakao.oreum.infra.beacon.altbeacon;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * lazy initialize를 위한 {@link BeaconService} proxy.
 *
 * {@link #initWith(BeaconService)}가 호출되기 전까지 모든 작업이 지연된다.
 * 지연된 작업은 {@link #initWith(BeaconService)} 때 수행된다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
class LazyInitBeaconServiceProxy implements BeaconService {
    private static final Logger LOG = getLogger(LazyInitBeaconServiceProxy.class);
    private final List<Task> deferredTasks = new ArrayList<>();
    private BeaconService delegate;

    /**
     * 실제 작업을 수행할 {@link BeaconService}로 초기화한다.
     *
     * 이때 지연되었던 작업을 모두 순서대로 수행한다.
     *
     * @param beaconService 실제 beaconService 구현 객체.
     */
    public void initWith(BeaconService beaconService) {
        Asserts.notNull(beaconService);
        this.delegate = beaconService;
        executeDeferred();
    }

    /**
     * 역-초기화.
     *
     * {@link #initWith(BeaconService)}에 의해 세팅된 beaconService 객체를 clear한다.
     * 이 메소드가 호출된 후에는 다시 모든 작업요청이 지연된다.
     */
    public void deinit() {
        this.delegate = null;
    }

    public boolean initialized() {
        return this.delegate != null;
    }

    @Override
    public void startMonitoringFor(final BeaconRegion beaconRegion) {
        defer(() -> delegate.startMonitoringFor(beaconRegion));
    }

    @Override
    public void stopMonitoringFor(final BeaconRegion beaconRegion) {
        defer(() -> delegate.stopMonitoringFor(beaconRegion));
    }

    @Override
    public void startRangingBeaconsIn(final BeaconRegion beaconRegion) {
        defer(() -> delegate.startRangingBeaconsIn(beaconRegion));
    }

    @Override
    public void stopRangingBeaconsIn(final BeaconRegion beaconRegion) {
        defer(() -> delegate.stopRangingBeaconsIn(beaconRegion));
    }

    @Override
    public Set<BeaconRegion> monitoringRegions() {
        if (delegate != null) {
            return delegate.monitoringRegions();
        } else {
            return new HashSet<>();
        }
    }

    @Override
    public Set<BeaconRegion> rangingRegions() {
        if (delegate != null) {
            return delegate.rangingRegions();
        } else {
            return new HashSet<>();
        }
    }

    @Override
    public void addObserver(final BeaconServiceObserver beaconServiceObserver) {
        defer(() -> delegate.addObserver(beaconServiceObserver));
    }

    @Override
    public void removeObserver(final BeaconServiceObserver beaconServiceObserver) {
        defer(() -> delegate.removeObserver(beaconServiceObserver));
    }

    @Override
    public void pause() {
        if (delegate != null) {
            delegate.pause();
        }
    }

    @Override
    public void resume() {
        if (delegate != null) {
            delegate.resume();
        }
    }

    private void defer(Task task) {
        if (delegate == null) {
            deferredTasks.add(task);
        } else {
            task.execute();
        }
    }

    private void executeDeferred() {
        for (Task task : deferredTasks) {
            try {
                task.execute();
            } catch (Exception e) {
                LOG.error("Fail to execute deffered tasks", e);
            }
        }
        deferredTasks.clear();
    }

    private interface Task {
        void execute();
    }
}

