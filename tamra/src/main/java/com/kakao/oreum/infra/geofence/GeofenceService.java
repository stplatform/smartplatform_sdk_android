package com.kakao.oreum.infra.geofence;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface GeofenceService {
    void startMonitoring(CircularRegion region);
    void stopMonitoring(CircularRegion region);
    void addObserver(GeofenceObserver observer);
    void removeObserver(GeofenceObserver observer);
}
