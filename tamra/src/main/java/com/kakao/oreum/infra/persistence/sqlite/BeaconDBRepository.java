package com.kakao.oreum.infra.persistence.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.kakao.oreum.infra.persistence.sqlite.TamraContract.BeaconEntry;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class BeaconDBRepository {
    private final SQLiteOpenHelper sqLiteOpenHelper;
    private final TimeService timeService;

    public BeaconDBRepository(SQLiteOpenHelper sqLiteOpenHelper, TimeService timeService) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
        this.timeService = timeService;
    }

    private Cursor getSelectionCursor(String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query(
                BeaconEntry.TABLE_NAME,
                BeaconEntry.getAllColumns(),
                selection,
                selectionArgs,
                null,
                null,
                null
        );
    }

    private BeaconData getBeaconDataOnCursor(Cursor c) {
        return BeaconData.builder()
                .id(c.getLong(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_ID)))
                .regionId(c.getLong(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_REGION_ID)))
                .uuid(c.getString(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_UUID)))
                .major(c.getInt(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_MAJOR)))
                .minor(c.getInt(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_MINOR)))
                .indoor(c.getInt(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_INDOOR)) != 0)
                .movable(c.getInt(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_MOVABLE)) != 0)
                .txPower(c.getInt(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_TXPOWER)))
                .latitude(c.getDouble(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_LATITUDE)))
                .longitude(c.getDouble(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_LONGITUDE)))
                .description(c.getString(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_DESCRIPTION)))
                .geohash(c.getString(c.getColumnIndexOrThrow(BeaconEntry.COLUMN_NAME_GEOHASH)))
                .build();
    }

    /**
     * <p>
     *     ID로 조회
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param id
     * @return
     */
    public BeaconData findById(long id) {
        Cursor c = getSelectionCursor(BeaconEntry.COLUMN_NAME_ID + " = ?", new String[] { String.valueOf(id) });

        BeaconData data = null;
        if (c.moveToFirst()) {
            data = getBeaconDataOnCursor(c);
        }
        c.close();
        return data;
    }

    /**
     * <p>
     *     Beacon 정보로 조회
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param beacon
     * @return
     */
    public BeaconData findByBeacon(Beacon beacon) {
        Cursor c = getSelectionCursor(
                BeaconEntry.COLUMN_NAME_UUID + " = ? AND " +
                BeaconEntry.COLUMN_NAME_MAJOR + " = ? AND " +
                BeaconEntry.COLUMN_NAME_MINOR + " = ?",
            new String[] { beacon.proximityUUID().toString(),
                String.valueOf(beacon.major()),
                String.valueOf(beacon.minor())
        });

        BeaconData data = null;
        if (c.moveToFirst()) {
            data = getBeaconDataOnCursor(c);
        }
        c.close();
        return data;
    }

    /**
     * <p>
     *     Region에 속한 BeaconData 조회
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param region
     * @return
     */
    public Set<BeaconData> findByRegion(Region region) {
        Cursor c = getSelectionCursor(BeaconEntry.COLUMN_NAME_REGION_ID + " = ?", new String[] { String.valueOf(region.id()) });

        Set<BeaconData> resultSet = new LinkedHashSet<>();
        while (c.moveToNext()) {
            resultSet.add(getBeaconDataOnCursor(c));
        }
        c.close();
        return resultSet;
    }

    /**
     * <p>
     *     신규 Beacon정보 추가
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param beaconData
     * @return 추가된 row id
     */
    public long insert(BeaconData beaconData) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = buildContentValues(beaconData);
        item.put(BeaconEntry.COLUMN_NAME_ID, beaconData.id());
        String now = timeService.now().toString();
        item.put(BeaconEntry.COLUMN_NAME_CREATED_AT, now);
        item.put(BeaconEntry.COLUMN_NAME_UPDATED_AT, now);

        return db.insert(
            BeaconEntry.TABLE_NAME,
            null,
            item
        );
    }

    private ContentValues buildContentValues(BeaconData beaconData) {
        ContentValues item = new ContentValues();
        item.put(BeaconEntry.COLUMN_NAME_REGION_ID, beaconData.regionId());
        item.put(BeaconEntry.COLUMN_NAME_UUID, beaconData.uuid().toString());
        item.put(BeaconEntry.COLUMN_NAME_MAJOR, beaconData.major());
        item.put(BeaconEntry.COLUMN_NAME_MINOR, beaconData.minor());
        item.put(BeaconEntry.COLUMN_NAME_INDOOR, beaconData.isIndoor());
        item.put(BeaconEntry.COLUMN_NAME_MOVABLE, beaconData.isMovable());
        item.put(BeaconEntry.COLUMN_NAME_TXPOWER, beaconData.txPower());
        item.put(BeaconEntry.COLUMN_NAME_LATITUDE, beaconData.latitude());
        item.put(BeaconEntry.COLUMN_NAME_LONGITUDE, beaconData.longitude());
        item.put(BeaconEntry.COLUMN_NAME_DESCRIPTION, beaconData.description());
        item.put(BeaconEntry.COLUMN_NAME_BEACONTYPE, beaconData.beaconType());
        item.put(BeaconEntry.COLUMN_NAME_GEOHASH, beaconData.geohash());
        return item;
    }

    /**
     * <p>
     *     기존 정보 업데이트
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param beaconData
     * @return 업데이트된 row 개수
     */
    public int update(BeaconData beaconData) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = buildContentValues(beaconData);
        String now = timeService.now().toString();
        item.put(BeaconEntry.COLUMN_NAME_UPDATED_AT, now);

        return db.update(
            BeaconEntry.TABLE_NAME,
            item,
            BeaconEntry.COLUMN_NAME_ID + " = ?",
            new String[] { String.valueOf(beaconData.id()) }
        );
    }

    /**
     * 외부 sync를 통해 업데이트되지 않은 정보는 삭제한다.
     *
     * @param before 해당 시점 이전에 업데이트된 데이터를 삭제
     * @return 삭제된 row 개수
     */
    public int removeNotSynced(String before) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        return db.delete(
                BeaconEntry.TABLE_NAME,
                BeaconEntry.COLUMN_NAME_UPDATED_AT + " < ?",
                new String[] { before }
        );
    }
}
