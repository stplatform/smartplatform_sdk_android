package com.kakao.oreum.infra.device;

import android.bluetooth.BluetoothAdapter;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BluetoothChecker implements DeviceStateChecker {

    @Override
    public boolean check() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return bluetoothAdapter.isEnabled();
    }

    @Override
    public void assertOk() {
        if (!check()) {
            throw new DeviceCheckError("bluetooth is NOT enable");
        }
    }
}
