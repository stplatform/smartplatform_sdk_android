package com.kakao.oreum.infra.geofence;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface GeofenceObserver {
    void didEnterRegion(CircularRegion region);
    void didExitRegion(CircularRegion region);
}
