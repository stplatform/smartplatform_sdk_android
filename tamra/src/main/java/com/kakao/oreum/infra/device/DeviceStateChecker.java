package com.kakao.oreum.infra.device;

import android.content.Context;

import com.kakao.oreum.ox.collection.Lists;

import java.util.Collection;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public interface DeviceStateChecker {

    boolean check();

    void assertOk() throws DeviceCheckError;

    class Util {
        public static Collection<DeviceStateChecker> all(Context context) {
            return Lists.arrayList(
                    new BluetoothChecker(),
                    new NetworkChecker(context)
            );
        }
    }

}
