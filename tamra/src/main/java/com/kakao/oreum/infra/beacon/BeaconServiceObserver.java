package com.kakao.oreum.infra.beacon;

import java.util.Set;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public interface BeaconServiceObserver {

    void didEnterRegion(BeaconRegion beaconRegion);

    void didExitRegion(BeaconRegion beaconRegion);

    void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion);
}
