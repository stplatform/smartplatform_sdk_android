package com.kakao.oreum.infra.beacon.altbeacon;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public interface AltBeaconConnectListener {

    void connected(AltBeaconBridgeService altBeaconBridgeService);

    void disconnected();

}
