package com.kakao.oreum.infra.beacon.distance;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public interface DistanceCalculator {
    double distance(int txPower, int rssi);
}
