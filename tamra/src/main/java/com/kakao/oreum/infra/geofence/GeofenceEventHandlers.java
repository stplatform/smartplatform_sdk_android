package com.kakao.oreum.infra.geofence;

import com.google.android.gms.location.Geofence;
import com.kakao.oreum.ox.collection.ImmutableSet;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.helper.Asserts;

import java.util.HashSet;
import java.util.Set;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class GeofenceEventHandlers {
    private final static Set<GeofenceEventHandler> observers = new HashSet<>();

    public static void register(GeofenceEventHandler observer) {
        Asserts.notNull(observer);
        observers.add(observer);
    }

    public static Set<GeofenceEventHandler> all() {
        return ImmutableSet.copyOf(observers);
    }

    public static void onEnter(Geofence geofence) {
        OxIterable.from(observers)
                .doEach(observer -> ExceptionFree.exec(() -> observer.onEnter(geofence)));
    }

    public static void onExit(Geofence geofence) {
        OxIterable.from(observers)
                .doEach(observer -> ExceptionFree.exec(() -> observer.onExit(geofence)));
    }

    public static void onDwell(Geofence geofence) {
        OxIterable.from(observers)
                .doEach(observer -> ExceptionFree.exec(() -> observer.onDwell(geofence)));
    }
}
