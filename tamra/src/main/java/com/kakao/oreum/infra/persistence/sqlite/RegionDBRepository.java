package com.kakao.oreum.infra.persistence.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.collection.Sets;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.RegionData;

import java.util.Set;

import static com.kakao.oreum.infra.persistence.sqlite.TamraContract.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class RegionDBRepository {
    private final SQLiteOpenHelper sqLiteOpenHelper;
    private final TimeService timeService;

    public RegionDBRepository(SQLiteOpenHelper sqLiteOpenHelper, TimeService timeService) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
        this.timeService = timeService;
    }

    private Cursor getSelectionCursor(String selection, String[] selectionArgs) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query(
                RegionEntry.TABLE_NAME,
                RegionEntry.getAllColumns(),
                selection,
                selectionArgs,
                null,
                null,
                null
        );
    }

    private RegionData getRegionDataOnCursor(Cursor c) {
        RegionData data;
        data = RegionData.builder()
            .id(c.getLong(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_ID)))
            .name(c.getString(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_NAME)))
            .uuid(c.getString(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_UUID)))
            .latitude(c.getDouble(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_LATITUDE)))
            .longitude(c.getDouble(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_LONGITUDE)))
            .radius(c.getInt(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_RADIUS)))
            .geohash(c.getString(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_GEOHASH)))
            .description(c.getString(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_DESCRIPTION)))
            .regionType(c.getString(c.getColumnIndexOrThrow(RegionEntry.COLUMN_NAME_REGIONTYPE)))
            .build();
        return data;
    }

    /**
     * <p>
     *     ID로 조회
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param id
     * @return
     */
    public RegionData findById(long id) {
        Cursor c = getSelectionCursor(
                RegionEntry.COLUMN_NAME_ID + " = ?",
                new String[] { String.valueOf(id) }
        );

        RegionData data = null;
        if (c.moveToFirst()) {
            data = getRegionDataOnCursor(c);
        }
        c.close();
        return data;
    }

    /**
     * <p>
     *     Region으로 조회
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param region
     * @return
     */
    public RegionData findByRegion(Region region) {
        Cursor c = getSelectionCursor(
                RegionEntry.COLUMN_NAME_ID + " = ?",
                new String[] { String.valueOf(region.id()) }
        );

        RegionData data = null;
        if (c.moveToFirst()) {
            data = getRegionDataOnCursor(c);
        }
        c.close();
        return data;
    }

    /**
     * <p>
     *      모든 RegionData 조회
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @return
     */
    public Set<RegionData> findAll() {
        Cursor c = getSelectionCursor("", new String[] {});

        Set<RegionData> regions = Sets.hashSet();
        while (c.moveToNext()) {
            regions.add(getRegionDataOnCursor(c));
        }
        c.close();
        return regions;
    }

    /**
     * <p>
     *     신규 Region 추가
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param regionData
     * @return
     */
    public long insert(RegionData regionData) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = buildContentValues(regionData);
        item.put(RegionEntry.COLUMN_NAME_ID, regionData.id());
        String now = timeService.now().toString();
        item.put(BeaconEntry.COLUMN_NAME_CREATED_AT, now);
        item.put(BeaconEntry.COLUMN_NAME_UPDATED_AT, now);

        return db.insert(
            RegionEntry.TABLE_NAME,
            null,
            item
        );
    }

    private ContentValues buildContentValues(RegionData regionData) {
        ContentValues item = new ContentValues();
        item.put(RegionEntry.COLUMN_NAME_NAME, regionData.name());
        item.put(RegionEntry.COLUMN_NAME_UUID, regionData.uuid().toString());
        item.put(RegionEntry.COLUMN_NAME_LATITUDE, regionData.latitude());
        item.put(RegionEntry.COLUMN_NAME_LONGITUDE, regionData.longitude());
        item.put(RegionEntry.COLUMN_NAME_RADIUS, regionData.radius());
        item.put(RegionEntry.COLUMN_NAME_GEOHASH, regionData.geohash());
        item.put(RegionEntry.COLUMN_NAME_DESCRIPTION, regionData.description());
        item.put(RegionEntry.COLUMN_NAME_REGIONTYPE, regionData.regionType());
        return item;
    }

    /**
     * <p>
     *     기존 Region 업데이트
     * </p>
     *
     * <p>(DB작업은 IO 작업 시간이 소요될 수 있으므로 별도의 Thread에서 호출해야 한다.)</p>
     *
     * @param regionData
     * @return
     */
    public int update(RegionData regionData) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = buildContentValues(regionData);
        String now = timeService.now().toString();
        item.put(BeaconEntry.COLUMN_NAME_UPDATED_AT, now);

        return db.update(
            RegionEntry.TABLE_NAME,
            item,
            RegionEntry.COLUMN_NAME_ID + " = ?",
            new String[] { String.valueOf(regionData.id()) }
        );
    }

    /**
     * 외부 sync를 통해 업데이트되지 않은 정보는 삭제한다.
     *
     * @param before 해당 시점 이전에 업데이트된 데이터를 삭제
     * @return 삭제된 row 개수
     */
    public int removeNotSynced(String before) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        return db.delete(
                RegionEntry.TABLE_NAME,
                RegionEntry.COLUMN_NAME_UPDATED_AT + " < ?",
                new String[] { before }
        );
    }
}
