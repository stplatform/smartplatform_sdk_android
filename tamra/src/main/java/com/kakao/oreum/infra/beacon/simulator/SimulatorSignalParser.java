package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class SimulatorSignalParser extends JsonDataParser<SimulatorBeaconData> {
    @Override
    protected SimulatorBeaconData map(JSONObject jsonObject) throws JSONException {
        return SimulatorBeaconData.builder()
                .id(jsonObject.getLong("beaconId"))
                .accuracy(jsonObject.getDouble("accuracy"))
                .support(jsonObject.getString("support"))
                .build();
    }
}
