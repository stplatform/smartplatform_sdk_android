package com.kakao.oreum.infra.geofence;

import com.google.android.gms.location.Geofence;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public interface GeofenceEventHandler {
    void onEnter(Geofence geofence);
    void onExit(Geofence geofence);
    void onDwell(Geofence geofence);
}
