package com.kakao.oreum.infra.persistence.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kakao.oreum.infra.persistence.sqlite.TamraContract.HistoryEntry;
import com.kakao.oreum.ox.collection.Sets;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.base.Period;
import com.kakao.oreum.tamra.base.Proximity;
import com.kakao.oreum.tamra.internal.data.data.HistoryData;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class HistoryDBRepository {
    private static final Logger LOG = getLogger(HistoryDBRepository.class);
    private final SQLiteOpenHelper sqLiteOpenHelper;
    private final DateFormat dateFormat;

    public HistoryDBRepository(SQLiteOpenHelper sqLiteOpenHelper, DateFormat dateFormat) {
        this.sqLiteOpenHelper = sqLiteOpenHelper;
        this.dateFormat = dateFormat;
    }

    private Cursor getSelectionCursor(String selection, String[] selectionArgs, String orderBy) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query(
                HistoryEntry.TABLE_NAME,
                HistoryEntry.getAllColumns(),
                selection,
                selectionArgs,
                null,
                null,
                orderBy
        );
    }

    private Cursor getSelectionCursor(String selection, String[] selectionArgs, String orderBy, String limit) {
        SQLiteDatabase db = sqLiteOpenHelper.getReadableDatabase();
        return db.query(
                HistoryEntry.TABLE_NAME,
                HistoryEntry.getAllColumns(),
                selection,
                selectionArgs,
                null,
                null,
                orderBy,
                limit
        );
    }

    private HistoryData getHistoryDataOnCursor(Cursor c) {
        try {
            return HistoryData.builder()
                    .spotId(c.getLong(c.getColumnIndex(HistoryEntry.COLUMN_NAME_SPOT_ID)))
                    .proximity(Proximity.valueOf(c.getString(c.getColumnIndex(HistoryEntry.COLUMN_NAME_PROXIMITY))))
                    .description(c.getString(c.getColumnIndex(HistoryEntry.COLUMN_NAME_DESCRIPTION)))
                    .issuedAt(dateFormat.parse(c.getString(c.getColumnIndex(HistoryEntry.COLUMN_NAME_ISSUED_AT))))
                    .build();
        } catch (ParseException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return null;
    }

    public long insert(HistoryData data) {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();

        ContentValues item = new ContentValues();
        item.put(HistoryEntry.COLUMN_NAME_SPOT_ID, data.spotId());
        item.put(HistoryEntry.COLUMN_NAME_PROXIMITY, data.proximity().name());
        item.put(HistoryEntry.COLUMN_NAME_DESCRIPTION, data.description());
        item.put(HistoryEntry.COLUMN_NAME_ISSUED_AT, dateFormat.format(data.issuedAt()));

        return db.insert(
                HistoryEntry.TABLE_NAME,
                null,
                item
        );
    }

    public List<HistoryData> find(Period period) {
        Cursor c = getSelectionCursor(
                HistoryEntry.COLUMN_NAME_ISSUED_AT + " >= ? AND " + HistoryEntry.COLUMN_NAME_ISSUED_AT + " < ?",
                new String[] {
                        dateFormat.format(period.from()),
                        dateFormat.format(period.to())
                },
                HistoryEntry.COLUMN_NAME_ISSUED_AT + " DESC"
        );

        List<HistoryData> resultList = new LinkedList<>();
        try {
            while (c.moveToNext()) {
                HistoryData data = getHistoryDataOnCursor(c);
                if (data != null)
                    resultList.add(data);
            }
        } finally {
            c.close();
        }
        return resultList;
    }

    public Set<HistoryData> find(long spotId) {
        Cursor c = getSelectionCursor(
                HistoryEntry.COLUMN_NAME_SPOT_ID + " = ?",
                new String[] { String.valueOf(spotId) },
                null
        );

        Set<HistoryData> resultSet = Sets.hashSet();
        try {
            while (c.moveToNext()) {
                HistoryData data = getHistoryDataOnCursor(c);
                if (data != null)
                    resultSet.add(data);
            }
        } finally {
            c.close();
        }
        return resultSet;
    }

    public Set<HistoryData> last(int n) {
        Cursor c = getSelectionCursor(
                null,
                null,
                HistoryEntry.COLUMN_NAME_ISSUED_AT + " DESC",
                String.valueOf(n)
        );

        Set<HistoryData> resultSet = Sets.hashSet();
        try {
            while (c.moveToNext()) {
                HistoryData data = getHistoryDataOnCursor(c);
                if (data != null)
                    resultSet.add(data);
            }
        } finally {
            c.close();
        }
        return resultSet;
    }

    public boolean removeAll() {
        SQLiteDatabase db = sqLiteOpenHelper.getWritableDatabase();
        int count = db.delete(
                HistoryEntry.TABLE_NAME,
                null,
                null
        );

        return count > 0;
    }
}
