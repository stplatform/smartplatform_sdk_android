package com.kakao.oreum.infra.beacon.simulator;

import android.util.Base64;

import com.kakao.oreum.ox.helper.Asserts;
import com.kakao.oreum.ox.helper.Crypto;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * Location Simulator API로부터 받아오는 데이터 객체
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class SimulatorBeaconData {
    private static final Logger LOG = getLogger(SimulatorBeaconData.class);
    private long id;
    private double accuracy;
    private BeaconSpec support;
    private static Key key = initKey();

    private SimulatorBeaconData(long id,
                                double accuracy,
                                BeaconSpec support) {
        this.id = id;
        this.accuracy = accuracy;
        this.support = support;
    }

    public static Builder builder() {
        return new Builder();
    }

    public long id() { return id; }
    public double accuracy() { return accuracy; }
    public String uuid() { return support.uuid; }
    public int major() { return support.major; }
    public int minor() { return support.minor; }
    public int txPower() { return support.txPower; }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof SimulatorBeaconData))
            return false;

        SimulatorBeaconData that = (SimulatorBeaconData) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "SimulatorBeaconData{" +
                "id=" + id +
                ", accuracy=" + accuracy +
                '}';
    }

    public static class Builder {
        private Long id;
        private Double accuracy;
        private BeaconSpec support;
        private String signKey = key.toString();

        private final JsonDataParser<BeaconSpec> parser = new SimulatorBeaconParser();

        private Builder() {
            /* DO NOT CREATE INSTANCE */
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder accuracy(double accuracy) {
            this.accuracy = accuracy;
            return this;
        }

        public Builder support(String encrypted) {
            support = decodeSupport(encrypted, signKey);
            return this;
        }

        public SimulatorBeaconData build() {
            Asserts.notNull(id);
            Asserts.notNull(accuracy);
            Asserts.notNull(support);
            return new SimulatorBeaconData(id, accuracy, support);
        }

        private BeaconSpec decodeSupport(String encrypted, String secret) {
            try {
                String json = new String(Crypto.decrypt(Base64.decode(encrypted, Base64.DEFAULT), secret), "UTF-8");
                return parser.parseObject(json);
            } catch (Exception e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
            return null;
        }
    }

    private static final class Key {
        private StringBuilder summary;

        private Key() {
            summary = new StringBuilder();
        }

        private void add(Literal literal) {
            summary.append(literal);
        }

        private void add(Numeric numeric) {
            summary.append(numeric);
        }

        @Override
        public String toString() {
            return summary.toString();
        }
    }

    private static final class Numeric {
        private static final int BASE = 48;
        private String value;

        private Numeric(int position) {
            value = Character.toString((char)(BASE + position));
        }

        @Override
        public String toString() {
            return value;
        }
    }

    private static final class Literal {
        private int base;
        private int position;

        private Literal(int base, int position) {
            this.base = 16 + (base * base);
            this.position = position;
        }

        @Override
        public String toString() {
            return Character.toString((char)(base + position));
        }
    }

    private static Key initKey() {
        Key key = new Key();

        int factor = 3;
        int diff = 2;

        int current = 23;
        int squareFactor = factor * factor;
        int upperLiteralBase = squareFactor - diff;
        key.add(new Literal(upperLiteralBase, current));
        current = current - 10;
        key.add(new Literal(upperLiteralBase, current));
        current = current - 9;
        key.add(new Numeric(current));
        current = current * 3 - 1;
        key.add(new Literal(squareFactor, current));
        current = current + current + 2;
        key.add(new Literal(squareFactor, current));
        current = current - 14;
        key.add(new Literal(squareFactor, current));
        current = current * 2;
        key.add(new Literal(squareFactor, current));
        current = current - 12;
        key.add(new Numeric(current));
        current = current / 4;
        key.add(new Numeric(current));
        current = current * 3;
        key.add(new Literal(upperLiteralBase, current));
        current = current - 5;
        key.add(new Literal(squareFactor, current));
        current = current * 18;
        key.add(new Literal(upperLiteralBase, current));
        current = current - 5;
        key.add(new Literal(squareFactor, current));
        current = current - 8;
        key.add(new Numeric(current));
        current = current * 4;
        key.add(new Literal(upperLiteralBase, current));
        key.add(new Literal(upperLiteralBase, current- 11));

        return key;
    }

    /**
     * 암호화된 Beacon 정보를 담는 객체
     *
     * @author cocoon.tf@kakaocorp.com
     * @since 0.9.0
     */
    static class BeaconSpec {
        private String uuid;
        private int major;
        private int minor;
        private int txPower;

        private BeaconSpec(String uuid,
                           int major,
                           int minor,
                           int txPower) {
            this.uuid = uuid;
            this.major = major;
            this.minor = minor;
            this.txPower = txPower;
        }

        public static Builder builder() {
            return new Builder();
        }

        public static class Builder {
            private String uuid;
            private Integer major;
            private Integer minor;
            private Integer txPower;

            public Builder uuid(String uuid) {
                this.uuid = uuid;
                return this;
            }
            public Builder major(int major) {
                this.major = major;
                return this;
            }
            public Builder minor(int minor) {
                this.minor = minor;
                return this;
            }
            public Builder txPower(int txPower) {
                this.txPower = txPower;
                return this;
            }

            public BeaconSpec build() {
                Asserts.notNull(uuid);
                Asserts.notNull(major);
                Asserts.notNull(minor);
                Asserts.notNull(txPower);

                return new BeaconSpec(uuid, major, minor, txPower);
            }
        }

        public String uuid() { return uuid; }
        public int major() { return major; }
        public int minor() { return minor; }
        public int txPower() { return txPower; }

    }
}
