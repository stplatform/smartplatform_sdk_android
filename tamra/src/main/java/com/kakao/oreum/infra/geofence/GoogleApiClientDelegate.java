package com.kakao.oreum.infra.geofence;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.task.OxTasks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class GoogleApiClientDelegate {
    private static final Logger LOG = getLogger(GoogleApiClientDelegate.class);
    private final Context context;
    private final PendingIntent geofencePendingIntent;
    private volatile GoogleApiClient googleApiClient;
    private Lock lock = new ReentrantLock();
    private volatile boolean googleConnected = false;

    public GoogleApiClientDelegate(Context context, PendingIntent pendingIntent) {
        this.context = context;
        this.geofencePendingIntent = pendingIntent;

        // preload
        OxTasks.runOnGeofence(ExceptionFree.runnable(this::requestGoogleApi));
    }

    public GoogleApiClient get() {
        if (!googleConnected) {
            OxTasks.runOnGeofence(ExceptionFree.runnable(this::requestGoogleApi));
            return null;
        }

        return googleApiClient;
    }

    private void requestGoogleApi() {
        ExceptionFree.exec(() -> {
            lock.lock();
            if (googleApiClient == null || !googleApiClient.isConnected()) {
                googleApiClient = new GoogleApiClient.Builder(context)
                        .addConnectionCallbacks(connectionCallbacks)
                        .addOnConnectionFailedListener(connectionFailedListener)
                        .addApi(LocationServices.API)
                        .build();
                googleApiClient.connect();
            }
            lock.unlock();
        });
    }

    private final GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            googleConnected = true;
            LOG.info("[GoogleApiClient.ConnectionCallbacks#onConnected]");
        }

        @Override
        public void onConnectionSuspended(int i) {
            googleConnected = false;

            LocationServices.GeofencingApi.removeGeofences(googleApiClient, geofencePendingIntent).setResultCallback(resultCallback);
        }
    };

    private final GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            // Google API 문제로 아래와 같은 로그 찍힐때가 있음. 이런 상황을 위한 뭔가가 필요할 듯.
            // ConnectionResult{statusCode=SERVICE_VERSION_UPDATE_REQUIRED, resolution=null, message=null}
            // 단서? http://stackoverflow.com/questions/25866837/android-wear-app-cant-connect-to-google-api-services
            LOG.error("[GOOGLE API ERROR] {}", connectionResult);
            googleConnected = false;
            googleApiClient = null;
        }
    };

    private final ResultCallback<Status> resultCallback = status -> LOG.info("[GOOGLE API STATUS] {}", status);
}
