package com.kakao.oreum.infra.beacon;

import com.kakao.oreum.common.annotation.Accessibility;
import com.kakao.oreum.infra.beacon.distance.DistanceCalculator;
import com.kakao.oreum.infra.beacon.distance.FitCurveDistanceCaculator;

import java.util.UUID;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public class BeaconFactory {
    private final DistanceCalculator distanceCalculator;

    public BeaconFactory(DistanceCalculator distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }

    public static BeaconFactory getDefault() {
        return new BeaconFactory(new FitCurveDistanceCaculator());
    }

    public Beacon create(
            UUID proximityUUID,
            int major,
            int minor,
            int txPower,
            int rssi) {
        double distance = distanceCalculator.distance(txPower, rssi);
        Proximity proximity = calculateProximity(distance);

        return new Beacon(proximityUUID, major, minor, proximity, distance, rssi, txPower);
    }

    private Proximity calculateProximity(double distance) {
        if (distance >0.0 && distance< 1.0) { // 센티 미터급
            return Proximity.Immediate;
        }
        if (distance >= 1.0 && distance <= 3.0) { // 10미터 이내
            return Proximity.Near;
        }
        if (distance > 3.0) { // 10미터 이상
            return Proximity.Far;
        }
        return Proximity.Unknown;
    }

}
