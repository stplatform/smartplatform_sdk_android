package com.kakao.oreum.infra.beacon.altbeacon;

import android.os.Binder;

import com.kakao.oreum.common.annotation.Accessibility;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@Accessibility.Private
public class AltBeaconConnector extends Binder {
    private AltBeaconConnectListener connectListener;
    private AltBeaconBridgeService altBeaconBridgeService;

    AltBeaconConnector() {
    }

    void connect(AltBeaconBridgeService altBeaconBridgeService) {
        this.altBeaconBridgeService = altBeaconBridgeService;
        if (connectListener != null) {
            this.connectListener.connected(altBeaconBridgeService);
        }
    }

    public boolean connected() {
        return altBeaconBridgeService != null;
    }

    void disconnect() {
        this.altBeaconBridgeService = null;
        this.connectListener.disconnected();
    }

    public void setConnectListener(AltBeaconConnectListener connectListener) {
        this.connectListener = connectListener;
        if (connected()) {
            this.connectListener.connected(altBeaconBridgeService);
        } else {
            this.connectListener.disconnected();
        }
    }
}
