package com.kakao.oreum.testhelper.base;

import com.github.tomakehurst.wiremock.junit.WireMockRule;

import org.junit.Rule;

import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

/**
 * REST API 테스트용 베이스 클래스.
 *
 * 이 클래스가 {@link AndroidContextTest}를 상속하면 문제가 생긴다.
 * 아마도 net 쪽 class 구현의 문제로 판단됨. context가 필요할 경우를 위해 여기서 직접 mock 객체를 생성하도록 한다.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class RestApiTest extends TestBase {
    protected final static String AppID = "com.kakao.oreum.tamra";
    protected final static String AppKey = "c6451b6c975c4a28a00bcdbcd4bc5f31";

    protected final static int port = 28888;

    @Rule
    public WireMockRule rule = new WireMockRule(wireMockConfig().port(port));
    protected String mockServerUrl = "http://localhost:" + port;

    protected URL urlFor(String file) throws MalformedURLException {
        return new URL(mockServerUrl +
                (file.startsWith("/") ? file : "/" + file)
        );
    }

}
