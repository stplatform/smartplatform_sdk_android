package com.kakao.oreum.testhelper.json;

import java.io.IOException;

/**
 * JsonWrapper.
 *
 * @author mitchell.geek
 */
public class RawJsonWrapper extends JsonBuilder {
    private final String rawJson;

    RawJsonWrapper(String rawJson) {
        this.rawJson = rawJson;
    }

    @Override
    public String toJson() {
        return rawJson;
    }

    @Override
    Object toObject() {
        try {
            return objectMapper.readValue(rawJson, Object.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
