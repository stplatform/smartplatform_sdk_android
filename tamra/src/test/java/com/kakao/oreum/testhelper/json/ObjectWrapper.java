package com.kakao.oreum.testhelper.json;

/**
 * ObjectWrapper.
 *
 * @author mitchell.geek
 */
public class ObjectWrapper extends JsonBuilder {
    private final Object obj;

    ObjectWrapper(Object obj) {
        this.obj = obj;
    }

    @Override
    Object toObject() {
        return obj;
    }
}
