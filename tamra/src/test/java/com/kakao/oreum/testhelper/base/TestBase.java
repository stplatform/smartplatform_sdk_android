package com.kakao.oreum.testhelper.base;

import com.kakao.oreum.ox.logger.Level;
import com.kakao.oreum.ox.logger.LoggerFactory;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public abstract class TestBase {
    static {
        // unit test 때는 console logger를 쓰자.
        LoggerFactory.onUnitTest(true);
        LoggerFactory.setLogLevel(Level.TRACE);
    }


}
