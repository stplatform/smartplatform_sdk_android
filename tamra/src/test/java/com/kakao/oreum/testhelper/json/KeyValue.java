package com.kakao.oreum.testhelper.json;

/**
 * MapEntry.
 *
 * @author mitchell.geek
 */
public class KeyValue {
    final String key;
    final Object value;

    private KeyValue(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public static class Builder {
        private final String key;

        Builder(String key) {
            this.key = key;
        }

        public KeyValue value(Object value) {
            return new KeyValue(key, value);
        }

        public KeyValue val(Object value) {
            return value(value);
        }
    }
}
