package com.kakao.oreum.testhelper;

import java.util.Random;
import java.util.UUID;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
public class RandomGenerator {
    private final Random random;

    public RandomGenerator() {
        this.random = new Random(System.currentTimeMillis());
    }

    public int getInt() {
        return Math.abs(random.nextInt());
    }

    public int getInt(int n) {
        return random.nextInt(n);
    }

    public int getInt(int min, int max) {
        return min + random.nextInt(max - min);
    }

    public long getLong() {
        return Math.abs(random.nextLong());
    }

    public boolean getBoolean() {
        return random.nextBoolean();
    }

    public double getDouble() {
        return random.nextDouble();
    }

    public double getDouble(double min, double max) {
        double variable = (max - min);
        double delta = random.nextDouble() / variable;
        return min + delta;
    }

    public float getFloat() {
        return random.nextFloat();
    }

    public <T extends Enum> T choose(Class<T> enumType) {
        T[] values = enumType.getEnumConstants();
        return values[getInt(values.length)];
    }

    public <T> T choose(T... values) {
        return values[getInt(values.length)];
    }

    public UUID getUUID() {
        return UUID.randomUUID();
    }

    public String getUUIDAsString() {
        return UUID.randomUUID().toString();
    }

    public String getString(int length) {
        return getString(length, Charset.ALPHA_NUMERIC);
    }

    public String getString(int length, Charset charset) {
        return getString(length, charset.chars);
    }

    public String getString(int length, String charset) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(charset.charAt(getInt(charset.length())));
        }
        return sb.toString();
    }

    public enum Charset {
        NUMERIC("0123456789"),
        LOWERCASE("abcdefghijklmnopqrstuvwxyz"),
        UPPERCASE(LOWERCASE.chars.toUpperCase()),
        ALPHA_NUMERIC(NUMERIC.chars + LOWERCASE.chars + UPPERCASE.chars);

        private final String chars;

        Charset(String chars) {
            this.chars = chars;
        }

    }


}
