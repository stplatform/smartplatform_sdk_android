package com.kakao.oreum.testhelper.base;

import android.app.Activity;
import android.content.Context;

import com.kakao.oreum.ox.logger.LoggerFactory;
import com.kakao.oreum.ox.task.OxTasks;
import com.kakao.oreum.tamra.base.Profile;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.internal.ShadowExtractor;
import org.robolectric.shadows.ShadowLooper;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
@Ignore
public abstract class AndroidContextTest extends TestBase {
    protected final static String AppID = "com.kakao.oreum.tamra";
    protected final static String AppKey = "c6451b6c975c4a28a00bcdbcd4bc5f31";

    @Before
    public void setUp() throws Exception {
        LoggerFactory.onUnitTest(true);
    }

    protected Context getContext() {
        Context context = Robolectric.buildActivity(DummyActivity.class).get().getApplicationContext();
        context = spy(context);
        when(context.getApplicationContext()).thenReturn(context);
        when(context.getPackageName()).thenReturn(AppID);
        return context;
    }

    protected com.kakao.oreum.tamra.base.Config getTamraConfig(Profile profile) {
        return com.kakao.oreum.tamra.base.Config.createFor(getContext(), AppKey, profile);
    }

    /**
     * OxTasks를 수행하는 코드를 테스트할 때 looper 제어를 위해 사용한다.
     *
     * @return
     */
    protected ShadowLooper backgroundLooper() {
        return (ShadowLooper) ShadowExtractor.extract(OxTasks.Loopers.getBackgroundLooper());
    }

    /**
     * OxTasks를 수행하는 코드를 테스트할 때 looper 제어를 위해 사용한다.
     *
     * @return
     */
    protected ShadowLooper mainLooper() {
        return (ShadowLooper) ShadowExtractor.extract(OxTasks.Loopers.getMainLooper());
    }

    /**
     * context를 얻기 위한 dummy activity.
     *
     * @author cocoon.tf@kakaocorp.com
     * @since 0.5.0
     */
    public static class DummyActivity extends Activity {
    }

}
