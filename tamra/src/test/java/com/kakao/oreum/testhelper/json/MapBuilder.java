package com.kakao.oreum.testhelper.json;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * MapBuilder.
 *
 * @author mitchell.geek
 */
public class MapBuilder extends JsonBuilder {
    private final Map<String, Object> map = new LinkedHashMap<>();

    MapBuilder(Map<String, Object> map) {
        this.map.putAll(map);
    }

    MapBuilder(KeyValue...entries) {
        for (KeyValue entry : entries) {
            entry(entry.key, entry.value);
        }
    }

    public MapBuilder entries(KeyValue... entries) {
        for (KeyValue entry:entries) {
            entry(entry);
        }
        return this;
    }

    public MapBuilder entry(KeyValue entry){
        return entry(entry.key, entry.value);
    }

    public MapBuilder entry(String key, Object value) {
        if (value instanceof JsonBuilder) {
            map.put(key, ((JsonBuilder) value).toObject());
        } else {
            map.put(key, value);
        }
        return this;
    }

    @Override
    Object toObject() {
        return map;
    }

}
