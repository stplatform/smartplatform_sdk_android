package com.kakao.oreum.testhelper;

import com.github.davidmoten.geo.GeoHash;
import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.Proximity;
import com.kakao.oreum.tamra.base.AsyncFetcher;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.internal.data.data.RegionData;

import java.util.UUID;

import lombok.Builder;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
public class Mocks {
    private final static RandomGenerator RANDOM = new RandomGenerator();

    @Builder(builderMethodName = "beaconRegion")
    private static BeaconRegion buildBeaconRegion(
            String id,
            UUID uuid,
            String uuidString,
            Integer major,
            Integer minor) {

        UUID proxUUID = uuid;
        if (proxUUID == null && uuidString != null) {
            proxUUID = UUID.fromString(uuidString);
        }

        return new BeaconRegion(
                optional(id).orElse(RANDOM.getString(12)),
                optional(proxUUID).orElse(UUID.randomUUID()),
                major,
                major == null ? null : minor
        );
    }

    @Builder(builderMethodName = "regionData")
    private static RegionData buildRegionData(
            Long id,
            String name,
            String description,
            UUID uuid,
            String regionType,
            Double latitude,
            Double longitude,
            Integer radius
    ) {

        double lat = optional(latitude)
                .orElse(RANDOM.getDouble(-90.0, 90.0));
        double lng = optional(longitude)
                .orElse(RANDOM.getDouble(-180.0, 180.0));
        String geohash = GeoHash.encodeHash(lat, lng);

        return RegionData.builder()
                .id(optional(id).orElse(RANDOM.getLong()))
                .name(optional(name).orElse(RANDOM.getString(12)))
                .uuid(optional(uuid).orElse(UUID.randomUUID()))
                .description(optional(description).orElse(RANDOM.getString(20)))
                .regionType(optional(regionType).orElse("NORMAL"))
                .latitude(lat)
                .longitude(lng)
                .geohash(geohash)
                .radius(optional(radius).orElse(RANDOM.getInt(10, 500)))
                .build();
    }

    @Builder(builderMethodName = "beacon")
    private static Beacon buildBeacon(
            UUID uuid,
            String uuidString,
            Integer major,
            Integer minor,
            Proximity proximity,
            Double accuracy,
            Integer rssi) {

        UUID proxUUID = uuid;
        if (proxUUID == null && uuidString != null) {
            proxUUID = UUID.fromString(uuidString);
        }

        return new Beacon(
                optional(proxUUID).orElse(UUID.randomUUID()),
                optional(major).orElse(RANDOM.getInt(65535)),
                optional(minor).orElse(RANDOM.getInt(65535)),
                optional(proximity).orElse(RANDOM.choose(Proximity.class)),
                optional(accuracy).orElse(RANDOM.getDouble()),
                optional(rssi).orElse(-1 * RANDOM.getInt(36)),
                RANDOM.getInt(-100, -8));
    }

    private static <T> Optional<T> optional(T value) {
        return Optional.ofNullable(value);
    }

    @Builder(builderMethodName = "spot")
    private static Spot buildSpot(
            Long id,
            String description,
            com.kakao.oreum.tamra.base.Proximity proximity,
            Double accuracy,
            Region region,
            Boolean indoor,
            Boolean movable,
            Double latitude,
            Double longitude,
            AsyncFetcher<Optional<String>> dataFetcher
    ) {

        double lat = optional(latitude)
                .orElse(RANDOM.getDouble(-90.0, 90.0));
        double lng = optional(longitude)
                .orElse(RANDOM.getDouble(-180.0, 180.0));
        String geohash = GeoHash.encodeHash(lat, lng);
        return Spot.of(optional(id).orElse(RANDOM.getLong()))
                .description(optional(description)
                        .orElse(RANDOM.getString(12)))
                .proximity(optional(proximity)
                        .orElse(RANDOM.choose(com.kakao.oreum.tamra.base.Proximity.class)))
                .accuracy(optional(accuracy)
                        .orElse(RANDOM.getDouble(0, 30)))
                .region(optional(region)
                        .orElse(RANDOM.choose(Regions.all())))
                .movable(optional(movable)
                        .orElse(false))
                .indoor(optional(indoor)
                        .orElse(true))
                .latitude(lat)
                .longitude(lng)
                .geohash(geohash)
                .dataFetcher(dataFetcher) // null to null
                .build();
    }


}
