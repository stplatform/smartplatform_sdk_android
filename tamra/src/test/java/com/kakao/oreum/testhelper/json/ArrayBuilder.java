package com.kakao.oreum.testhelper.json;

import java.util.ArrayList;
import java.util.List;

/**
 * ArrayBuilder.
 *
 * @author mitchell.geek
 */
public class ArrayBuilder extends JsonBuilder {
    private final List<Object> list = new ArrayList<>();

    ArrayBuilder(){}

    public ArrayBuilder elm(Object element) {
        if (element instanceof JsonBuilder) {
            list.add(((JsonBuilder) element).toObject());
        } else {
            list.add(element);
        }
        return this;
    }

    public ArrayBuilder elm(Object... elements) {
        for (Object elm : elements) {
            elm(elm);
        }
        return this;
    }

    @Override
    Object toObject() {
        return list;
    }

}
