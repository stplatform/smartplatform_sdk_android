package com.kakao.oreum.testhelper.json;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
public class TemplateBasedArrayBuilder extends ArrayBuilder {
    private final Map<String, Object> templateObj = new LinkedHashMap<>();

    public TemplateBasedArrayBuilder(JsonBuilder objBuilder) {
        Object temp = objBuilder.toObject();
        if (temp instanceof Map) {
            this.templateObj.putAll((Map<? extends String, ?>) temp);
        } else {
            throw new IllegalArgumentException("It support object type builder");
        }
    }

    public TemplateBasedArrayBuilder(String rawJson) {
        this(new RawJsonWrapper(rawJson));
    }

    public TemplateBasedArrayBuilder addCopy(KeyValue... entries) {
        MapBuilder templateBuilder = new MapBuilder(templateObj);
        templateBuilder.entries(entries);
        elm(templateBuilder);
        return this;
    }
}
