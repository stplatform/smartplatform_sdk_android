package com.kakao.oreum.testhelper.json;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.kakao.oreum.ox.format.ISO8601DateFormat;

import java.util.Date;

/**
 * json 데이터를 가독성 있는 java 코드로 작성하기 위한 Helper.
 * <p/>
 * <p/>
 * example:
 * <pre>
 *     import static com.daumkakao.searchdata.JsonBuilder.*
 *
 *     ...
 *     String sample1 = obj()
 *          .entry("key", "john")
 *          .entry("age", 32)
 *          .toJson();
 *
 *     String sample2 = list()
 *          .elm("john", 32);
 *          .toJson();
 *
 *     String sample3 = list("john", 32).toJson();
 *
 *     String sample4 = obj()
 *          .entry("list", list(1, 2, 3))
 *          .toJson();
 *
 *     String sample5 = list(
 *          obj()
 *              .entry("key", "john")
 *              .entry("age", 32),
 *          obj()
 *              .entry("key", "jane")
 *              .entry("age", 28),
 *     ).toJson();
 *
 *     String sample6 = obj()
 *          .entry("sample4", raw(sample4))
 *          .entry("sample5", raw(sample5))
 *          .toJson();
 *
 * </pre>
 *
 * @author mitchell.geek
 */
public abstract class JsonBuilder {
    protected final static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        // Field 기준으로 매핑. (getter/setter 안씀)
        objectMapper.configure(MapperFeature.AUTO_DETECT_FIELDS, true);
        objectMapper.configure(MapperFeature.AUTO_DETECT_GETTERS, false);
        objectMapper.configure(MapperFeature.AUTO_DETECT_SETTERS, false);
        VisibilityChecker visibilityChecker = objectMapper
                .getVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY);
        objectMapper.setVisibility(visibilityChecker);
        // ISO8601 millisec on / local timezone on
        objectMapper.setDateFormat(new ISO8601DateFormat());
    }

    public static TemplateBasedArrayBuilder template(JsonBuilder other) {
        return new TemplateBasedArrayBuilder(other);
    }

    public static TemplateBasedArrayBuilder template(String rawJson) {
        return new TemplateBasedArrayBuilder(rawJson);
    }

    public static MapBuilder obj(KeyValue... entries) {
        return new MapBuilder(entries);
    }

    public static KeyValue.Builder key(String key) {
        return new KeyValue.Builder(key);
    }

    public static ArrayBuilder array(Object... objects) {
        return new ArrayBuilder().elm(objects);
    }

    public static ObjectWrapper java(Object object) {
        return new ObjectWrapper(object);
    }

    public static RawJsonWrapper raw(String rawJson) {
        return new RawJsonWrapper(rawJson);
    }

    public static String iso8601Time(Date time) {
        return new ISO8601DateFormat().format(time);
    }

    public String toJson() {
        try {
            return objectMapper.writeValueAsString(toObject());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    abstract Object toObject();
}


