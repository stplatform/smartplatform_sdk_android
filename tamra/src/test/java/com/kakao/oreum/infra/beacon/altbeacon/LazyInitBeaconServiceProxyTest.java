package com.kakao.oreum.infra.beacon.altbeacon;

import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.testhelper.Mocks;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
public class LazyInitBeaconServiceProxyTest extends TestBase {

    LazyInitBeaconServiceProxy sut;

    BeaconService mockBeaconService;

    @Before
    public void setUp() throws Exception {
        sut = new LazyInitBeaconServiceProxy();
        mockBeaconService = mock(BeaconService.class);
    }

    @Test
    public void 초기화할때_사용된_beaconService에_작업을_위임한다() throws Exception {
        // Given
        sut.initWith(mockBeaconService);
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        sut.startMonitoringFor(region);
        sut.startRangingBeaconsIn(region);
        sut.stopRangingBeaconsIn(region);
        sut.stopMonitoringFor(region);

        // Then
        verify(mockBeaconService, times(1)).startMonitoringFor(region);
        verify(mockBeaconService, times(1)).startRangingBeaconsIn(region);
        verify(mockBeaconService, times(1)).stopRangingBeaconsIn(region);
        verify(mockBeaconService, times(1)).stopMonitoringFor(region);

    }

    @Test
    public void 초기화_전에_수행했던_작업은_초기화후에_순서대로_실행된다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();
        sut.startMonitoringFor(region);
        sut.startRangingBeaconsIn(region);
        sut.stopRangingBeaconsIn(region);
        sut.stopMonitoringFor(region);

        // When
        sut.initWith(mockBeaconService);

        // Then
        InOrder inOrder = inOrder(mockBeaconService);
        inOrder.verify(mockBeaconService, times(1)).startMonitoringFor(region);
        inOrder.verify(mockBeaconService, times(1)).startRangingBeaconsIn(region);
        inOrder.verify(mockBeaconService, times(1)).stopRangingBeaconsIn(region);
        inOrder.verify(mockBeaconService, times(1)).stopMonitoringFor(region);
    }

    @Test
    public void 역초기화하면_다시_작업수행이_지연된다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();
        sut.startMonitoringFor(region);
        sut.initWith(mockBeaconService);

        // When
        sut.deinit();
        sut.startRangingBeaconsIn(region);

        // Then
        verify(mockBeaconService, never()).startRangingBeaconsIn(eq(region));
    }

    @Test
    public void 초기화전에_질의메소드는_항상_빈값을_반환한다() throws Exception {
        // Given

        // When

        // Then
        assertThat(sut.rangingRegions(), empty());
        assertThat(sut.monitoringRegions(), empty());
    }

    @Test
    public void 초기화전에는_모니터링_요청을_했더라도_질의메소드는_빈값을_반환한다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        sut.startMonitoringFor(region);

        // Then
        assertThat(sut.monitoringRegions(), empty());
    }

    @Test
    public void 초기화전에는_ranging요청을_했더라도_질의메소드는_빈값을_리턴한다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        sut.startRangingBeaconsIn(region);

        // Then
        assertThat(sut.rangingRegions(), empty());
    }

    @Test
    public void 초기화후에는_질의메소드도_위임한다() throws Exception {
        // Given

        sut.initWith(mockBeaconService);

        // When
        sut.monitoringRegions();
        sut.rangingRegions();

        // Then
        verify(mockBeaconService, times(1)).monitoringRegions();
        verify(mockBeaconService, times(1)).rangingRegions();

    }

    @Test
    public void 초기화여부를_확인할_수_있다() throws Exception {
        assertThat(sut.initialized(), is(false));

        sut.initWith(mockBeaconService);
        assertThat(sut.initialized(), is(true));
    }

    @Test
    public void observer지정도_초기화까지_지연된다() throws Exception {
        // Given
        BeaconServiceObserver mockObserver = mock(BeaconServiceObserver.class);
        sut.addObserver(mockObserver);
        verify(mockBeaconService, never()).addObserver(mockObserver);

        // When
        sut.initWith(mockBeaconService);

        // Then
        verify(mockBeaconService, times(1)).addObserver(mockObserver);
    }

    @Test
    public void 지연된_작업실행중에_예외가_발생하더라도_지연된_작업은_모두_실행한다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();
        sut.startMonitoringFor(region);
        sut.stopMonitoringFor(region);
        doThrow(RuntimeException.class)
                .when(mockBeaconService).startMonitoringFor(region);

        // When
        sut.initWith(mockBeaconService);

        // Then
        verify(mockBeaconService, times(1)).stopMonitoringFor(region);

    }


    @Test
    public void 초기화후_pause요청_처리() throws Exception {
        // Given
        // When
        sut.pause();

        // Then
        verify(mockBeaconService, never()).pause();


        // Given
        sut.initWith(mockBeaconService);

        // When
        sut.pause();

        // Then
        verify(mockBeaconService, times(1)).pause();
    }

    @Test
    public void 초기화후_resume요청_처리() throws Exception {
        // Given
        // When
        sut.resume();

        // Then
        verify(mockBeaconService, never()).resume();


        // Given
        sut.initWith(mockBeaconService);

        // When
        sut.resume();

        // Then
        verify(mockBeaconService, times(1)).resume();
    }
}