package com.kakao.oreum.infra.persistence.sqlite;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconFactory;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;
import com.kakao.oreum.tamra.internal.data.data.RegionUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Set;
import java.util.UUID;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class BeaconDBRepositoryTest {
    BeaconDBRepository sut;
    TimeService timeService = new SystemTimeService();

    @Before
    public void setUp() throws Exception {
        sut = new BeaconDBRepository(new TamraDBHelper(RuntimeEnvironment.application), new SystemTimeService());
    }

    @Test
    public void testInsert() throws Exception {
        // given
        long id = 1;
        BeaconData beaconData = getBeaconData(id, 1, UUID.randomUUID().toString(), 1, 1, "blah");

        // when
        long rowId = sut.insert(beaconData);
        BeaconData dbData = sut.findById(id);

        // then
        assertThat(rowId, greaterThan(0L));
        assertThat(dbData, is(beaconData));

        // when
        rowId = sut.insert(beaconData);

        // then
        assertThat(rowId, is(-1L));
    }

    @Test
    public void testUpdate() throws Exception {
        // given
        long id = 1;
        BeaconData origin = getBeaconData(id, 1, UUID.randomUUID().toString(), 1, 1, "origin");
        BeaconData toUpdate = getBeaconData(id, 1, UUID.randomUUID().toString(), 1, 1, "origin -> update");

        // when
        sut.insert(origin);
        int updatedCount = sut.update(toUpdate);
        BeaconData dbData = sut.findById(id);

        // then
        assertThat(updatedCount, is(1));
        assertThat(dbData.description(), is(toUpdate.description()));
    }

    @Test
    public void testFindByBeacon() throws Exception {
        // given
        long id = 1;
        String uuid = UUID.randomUUID().toString();
        int major = 1;
        int minor = 1;
        BeaconData beaconData = getBeaconData(id, 1, uuid, major, minor, "조회용");
        Beacon beacon = BeaconFactory.getDefault().create(UUID.fromString(uuid), major, minor, -10, -40);

        // when
        sut.insert(beaconData);
        BeaconData dbData = sut.findByBeacon(beacon);

        // then
        assertThat(dbData, is(beaconData));
    }

    @Test
    public void testFindByRegion() throws Exception {
        // given
        long id = 1;
        long regionId = 1;
        String uuid = UUID.randomUUID().toString();
        BeaconData beaconData = getBeaconData(id, regionId, uuid, 1, 1, "테스트");
        Region region = RegionUtil.toRegion(BeaconRegion.initWith("Oreum#1/테스트", UUID.fromString(uuid)));

        // when
        sut.insert(beaconData);
        Set<BeaconData> dbDataSet = sut.findByRegion(region);

        // then
        assertThat(dbDataSet.size(), is(1));
        assertTrue(dbDataSet.contains(beaconData));
    }

    @Test
    public void testRemoveNotSynced() throws Exception {
        // given
        long id = 1;
        BeaconData origin = getBeaconData(id, 1, UUID.randomUUID().toString(), 1, 1, "origin");
        BeaconData toUpdate = getBeaconData(id, 1, UUID.randomUUID().toString(), 1, 1, "origin -> update");
        long id2 = 2;
        BeaconData origin2 = getBeaconData(id2, 1, UUID.randomUUID().toString(), 1, 1, "origin2");

        sut.insert(origin);
        sut.insert(origin2);

        // when
        Thread.sleep(1000);
        String now = timeService.now().toString();

        sut.update(toUpdate);

        sut.removeNotSynced(now);
        BeaconData dbData = sut.findById(id2);

        // then
        assertNull(dbData);
    }

    private BeaconData getBeaconData(long id, long regionId, String uuid, int major, int minor, String description) {
        return BeaconData.builder()
                .id(id)
                .regionId(regionId)
                .uuid(uuid)
                .major(major)
                .minor(minor)
                .txPower(-10)
                .indoor(true)
                .movable(false)
                .latitude(0.0)
                .longitude(0.0)
                .beaconType("Normal")
                .description(description)
                .geohash("avzzz")
                .build();
    }
}