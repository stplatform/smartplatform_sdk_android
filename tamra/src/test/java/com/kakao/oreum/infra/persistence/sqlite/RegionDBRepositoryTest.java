package com.kakao.oreum.infra.persistence.sqlite;

import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.tamra.internal.data.data.RegionData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.UUID;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class RegionDBRepositoryTest {
    RegionDBRepository sut;
    TimeService timeService = new SystemTimeService();

    @Before
    public void setUp() throws Exception {
        sut = new RegionDBRepository(new TamraDBHelper(RuntimeEnvironment.application), new SystemTimeService());
    }

    @Test
    public void testInsert() throws Exception {
        // given
        long id = 1;
        RegionData regionData = getRegionData(id, "abc");

        // when
        long rowId = sut.insert(regionData);
        RegionData dbData = sut.findById(id);

        // then
        assertThat(rowId, greaterThan(0L));
        assertThat(dbData, is(regionData));

        // when
        // 똑같은 데이터를 다시 넣었을때
        rowId = sut.insert(regionData);

        // then
        // 입력실패해야 한다.
        assertThat(rowId, is(-1L));
    }

    @Test
    public void testUpdate() throws Exception {
        // given
        long id = 1;
        RegionData origin = getRegionData(id, "origin");
        RegionData toUpdate = getRegionData(id, "origin -> update");

        // when
        sut.insert(origin);
        int updatedCount = sut.update(toUpdate);
        RegionData dbData = sut.findById(id);

        // then
        assertThat(updatedCount, is(1));
        assertThat(dbData.description(), is(toUpdate.description()));
    }

    @Test
    public void testRemoveNotSynced() throws Exception {
        // given
        long id = 1;
        RegionData origin = getRegionData(id, "origin");
        RegionData toUpdate = getRegionData(id, "origin -> update");
        long id2 = 2;
        RegionData origin2 = getRegionData(id2, "origin2");

        sut.insert(origin);
        sut.insert(origin2);

        // when
        Thread.sleep(1000);
        String now = timeService.now().toString();
        sut.update(toUpdate);
        sut.removeNotSynced(now);
        RegionData dbData = sut.findById(id2);

        // then
        assertNull(dbData);
    }

    private RegionData getRegionData(long id, String description) {
        return RegionData.builder()
                .id(id)
                .name("Test Region")
                .uuid(UUID.randomUUID())
                .latitude(0.0)
                .longitude(0.0)
                .radius(100)
                .description(description)
                .regionType("Normal")
                .geohash("avzzz")
                .build();
    }
}