package com.kakao.oreum.infra.time;

import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SystemTimeServiceTest {
    @Test
    public void testCurrentTimeMillis() throws Exception {
        // given
        TimeService timeService = new SystemTimeService();

        // when
        long time1 = timeService.currentTimeMillis();
        Thread.sleep(20);
        long time2 = timeService.currentTimeMillis();

        // then
        assertThat(time2, greaterThan(time1));

    }

}