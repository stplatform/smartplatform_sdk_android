package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.testhelper.base.RestApiTest;

import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class BeaconServiceBasedSimulatorTest extends RestApiTest {
    BeaconServiceBasedSimulator sut;
    private String uuid = "74704ede-18fe-4c49-bd24-95616c1e0fe8";

    @Before
    public void setUp() throws Exception {
        sut = spy(new BeaconServiceBasedSimulator("THE NAME", OxHttpClient.forBaseUrl(new URL(mockServerUrl))));
    }

    @Test
    public void generateRssi() throws Exception {
        // given
        double accuracy1 = 1;
        double accuracy2 = 3;
        double accuracy3 = 10;

        int txPower = -10;

        // when
        int rssi1 = sut.generateRssi(txPower, accuracy1);
        int rssi2 = sut.generateRssi(txPower, accuracy2);
        int rssi3 = sut.generateRssi(txPower, accuracy3);

        // then
        assertThat(rssi1, greaterThanOrEqualTo(-10));
        assertThat(rssi2, lessThanOrEqualTo(-10));
        assertThat(rssi3, lessThan(-10));
    }

    @Test
    public void startMonitoringFor() throws Exception {
        // given
        BeaconRegion region = getRegion();

        // when
        sut.startMonitoringFor(region);

        // then
        Set<BeaconRegion> regions = sut.monitoringRegions();
        assertThat(regions.size(), is(1));

        // when
        sut.stopMonitoringFor(region);

        // then
        regions = sut.monitoringRegions();
        assertThat(regions.size(), is(0));
    }

    @Test
    public void 서버_다운() throws Exception {
        // given
        BeaconRegion region = getRegion();

        // when
        sut.startMonitoringFor(region);

        // then
        Set<BeaconRegion> regions = sut.monitoringRegions();
        assertThat(regions.size(), is(1));
    }

    @Test
    public void startRangingBeaconsIn() throws Exception {
        // given
        BeaconRegion region = getRegion();

        // when
        sut.startRangingBeaconsIn(region);

        // then
        Set<BeaconRegion> regions = sut.rangingRegions();
        assertThat(regions.size(), is(1));

        // when
        sut.stopRangingBeaconsIn(region);

        // then
        regions = sut.rangingRegions();
        assertThat(regions.size(), is(0));
    }

    @Test
    public void testMonitorRegion() throws Exception {
        // given
        BeaconRegion region = getRegion();
        List<? extends SimulatorBeaconData> simulatorBeaconDatas = getSimulatorBeaconData();
        DummyObserver beaconServiceObserver = spy(new DummyObserver());

        // when
        sut.addObserver(beaconServiceObserver);
        sut.startMonitoringFor(region);
        sut.monitorRegion(simulatorBeaconDatas);

        // then
        verify(beaconServiceObserver, times(1)).didEnterRegion(region);

        // when
        sut.monitorRegion(new ArrayList<>());

        // then
        verify(beaconServiceObserver, times(1)).didExitRegion(region);

        // when
        sut.removeObserver(beaconServiceObserver);
        sut.monitorRegion(simulatorBeaconDatas);

        // then
        verify(beaconServiceObserver, times(1)).didEnterRegion(region);
    }

    @Test
    public void testRangedRegion() throws Exception {
        // given
        BeaconRegion region = getRegion();
        List<? extends SimulatorBeaconData> simulatorBeaconDatas = getSimulatorBeaconData();
        DummyObserver beaconServiceObserver = spy(new DummyObserver());

        // when
        sut.addObserver(beaconServiceObserver);
        sut.startRangingBeaconsIn(region);
        sut.rangedBeacons(simulatorBeaconDatas);

        // then
        verify(beaconServiceObserver, times(1)).didRangeBeacons(anyObject(), eq(region));

        // when
        sut.removeObserver(beaconServiceObserver);
        sut.rangedBeacons(simulatorBeaconDatas);

        // then
        verify(beaconServiceObserver, times(1)).didRangeBeacons(anyObject(), eq(region));
    }

    class DummyObserver implements BeaconServiceObserver {

        @Override
        public void didEnterRegion(BeaconRegion beaconRegion) {

        }

        @Override
        public void didExitRegion(BeaconRegion beaconRegion) {

        }

        @Override
        public void didRangeBeacons(Set<Beacon> beacons, BeaconRegion beaconRegion) {

        }
    }

    private BeaconRegion getRegion() {
        return BeaconRegion.initWith("Test Region", UUID.fromString(uuid));
    }

    private List<? extends SimulatorBeaconData> getSimulatorBeaconData() {
        List<SimulatorBeaconData> simulatorBeaconDatas = new ArrayList<>();
        SimulatorBeaconData item = mock(SimulatorBeaconData.class);

        when(item.id()).thenReturn(1L);
        when(item.accuracy()).thenReturn(3.0);
        when(item.uuid()).thenReturn(uuid);
        when(item.major()).thenReturn(1);
        when(item.minor()).thenReturn(1);
        when(item.txPower()).thenReturn(-10);

        simulatorBeaconDatas.add(item);

        item = mock(SimulatorBeaconData.class);

        when(item.id()).thenReturn(1L);
        when(item.accuracy()).thenReturn(5.0);
        when(item.uuid()).thenReturn(uuid);
        when(item.major()).thenReturn(1);
        when(item.minor()).thenReturn(2);
        when(item.txPower()).thenReturn(-10);

        simulatorBeaconDatas.add(item);

        return simulatorBeaconDatas;
    }
}