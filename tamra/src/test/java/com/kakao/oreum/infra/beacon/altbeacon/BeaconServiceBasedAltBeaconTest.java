package com.kakao.oreum.infra.beacon.altbeacon;

import android.os.RemoteException;

import com.google.common.collect.Lists;
import com.kakao.oreum.ox.collection.Sets;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.beacon.BeaconServiceObserver;
import com.kakao.oreum.testhelper.Mocks;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.UUID;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class BeaconServiceBasedAltBeaconTest {

    BeaconServiceBasedAltBeacon sut;

    @Mock
    AltBeaconBridgeService altBeaconBridgeService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        sut = new BeaconServiceBasedAltBeacon(altBeaconBridgeService);
    }

    @Test
    public void 생성하면_beaconManager에_monitorNotifier를_등록한다() throws Exception {
        // Given
        // When
        // Then
        verify(altBeaconBridgeService, times(1)).addMonitorNotifier(any(MonitorNotifier.class));
    }

    @Test
    public void 생성하면_beaconManager에_rangeNotifier를_등록한다() throws Exception {
        // Given
        // When
        // Then
        verify(altBeaconBridgeService, times(1)).addRangeNotifier(any(RangeNotifier.class));
    }


    @Test
    public void start_monitor_하면_alt_beacon객체로_변경되어_beaconManager에_전달된다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        sut.startMonitoringFor(region);

        // Then
        ArgumentCaptor<Region> captor = ArgumentCaptor.forClass(Region.class);
        verify(altBeaconBridgeService, times(1)).startMonitoringBeaconsInRegion(captor.capture());

        assertThat(captor.getValue().getId1().toUuid(), is(region.proximityUUID()));
    }

    @Test
    public void 동일_region에_대해_start_monitoring을_여러번_수행해도_beaconManager에는_한번만_전달된다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        sut.startMonitoringFor(region);
        sut.startMonitoringFor(region);
        sut.startMonitoringFor(region);

        // Then
        ArgumentCaptor<Region> captor = ArgumentCaptor.forClass(Region.class);
        verify(altBeaconBridgeService, times(1)).startMonitoringBeaconsInRegion(captor.capture());
    }

    @Test
    public void stop_monitor_하면_alt_beacon객체로_변경되어_beaconManager에_전달된다() throws Exception {
        // Given
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        sut.startMonitoringFor(region);
        sut.stopMonitoringFor(region);

        // Then
        ArgumentCaptor<Region> captor = ArgumentCaptor.forClass(Region.class);
        verify(altBeaconBridgeService, times(1)).stopMonitoringBeaconsInRegion(captor.capture());

        assertThat(captor.getValue().getId1().toUuid(), is(region.proximityUUID()));
    }

    @Test
    public void monitoring_중이지_않은_region을_stop_monitor하면_beacon_manager에는_아무런_작업도수행되지_않는다() throws Exception {
        BeaconRegion region = Mocks.beaconRegion().build();

        // When
        assertThat(sut.monitoringRegions(), not(contains(region)));
        sut.stopMonitoringFor(region);

        // Then
        verify(altBeaconBridgeService, never()).stopMonitoringBeaconsInRegion(any(Region.class));
    }

    @Test
    public void altbeacon이_새로운_region진입을_감지하면_이벤트를_변환하여_noti해준다() throws Exception {
        // Given
        BeaconServiceObserver mockObserver = mock(BeaconServiceObserver.class);
        BeaconRegion beaconRegion = Mocks.beaconRegion().build();
        sut.addObserver(mockObserver);

        // When
        sut.startMonitoringFor(beaconRegion);
        Region region = captureStartMonitoringRegion();
        captureMonitorNotifier().didEnterRegion(region);

        // Then
        verify(mockObserver, times(1)).didEnterRegion(beaconRegion);
    }

    @Test
    public void monitoring중이_아닌_region이라면_altbeacon의_이벤트는_처리되지_않는다() throws Exception {
        // Given
        BeaconServiceObserver mockObserver = mock(BeaconServiceObserver.class);
        BeaconRegion beaconRegion = Mocks.beaconRegion().build();
        sut.addObserver(mockObserver);

        // When
        assertThat(sut.monitoringRegions(), not(contains(beaconRegion)));
        Region fakeRegion = fakeRegion();
        captureMonitorNotifier().didEnterRegion(fakeRegion);

        // Then
        verify(mockObserver, never()).didEnterRegion(any(BeaconRegion.class));
    }

    @Test
    public void altbeacon이_region에서_이탈하면_이벤트를_변환하여_noti해준다() throws Exception {
        // Given
        BeaconServiceObserver mockObserver = mock(BeaconServiceObserver.class);
        BeaconRegion beaconRegion = Mocks.beaconRegion().build();
        sut.addObserver(mockObserver);

        // When
        sut.startMonitoringFor(beaconRegion);
        Region region = captureStartMonitoringRegion();
        captureMonitorNotifier().didEnterRegion(region);
        captureMonitorNotifier().didExitRegion(region);

        // Then
        verify(mockObserver, times(1)).didExitRegion(beaconRegion);
    }

    @Test
    public void monitoring중이_아닌_region에대한_지역이탈_이벤트는_처리되지_않는다() throws Exception {
        // Given
        BeaconServiceObserver mockObserver = mock(BeaconServiceObserver.class);
        BeaconRegion beaconRegion = Mocks.beaconRegion().build();
        sut.addObserver(mockObserver);

        // When
        assertThat(sut.monitoringRegions(), not(contains(beaconRegion)));
        Region fakeRegion = fakeRegion();
        captureMonitorNotifier().didExitRegion(fakeRegion);

        // Then
        verify(mockObserver, never()).didExitRegion(any(BeaconRegion.class));
    }

    private Region fakeRegion() {
        Region fakeRegion = mock(Region.class);
        when(fakeRegion.getId1()).thenReturn(Identifier.fromUuid(UUID.randomUUID()));
        when(fakeRegion.getUniqueId()).thenReturn("fake");
        return fakeRegion;
    }

    @Test
    public void range된_beacon이벤트가_변환되어_observer에_전달된다() throws Exception {
        // Given
        BeaconServiceObserver mockObserver = mock(BeaconServiceObserver.class);
        sut.addObserver(mockObserver);

        // When
        Region fakeRegion = fakeRegion();
        captureRangeNotifier().didRangeBeaconsInRegion(
                Lists.newArrayList(), // empty
                fakeRegion
        );

        // Then
        verify(mockObserver, times(1))
                .didRangeBeacons(
                        eq(Sets.hashSet()), // empty
                        any(BeaconRegion.class));
    }

    private Region captureStartMonitoringRegion() throws RemoteException {
        ArgumentCaptor<Region> captor = ArgumentCaptor.forClass(Region.class);
        verify(altBeaconBridgeService, times(1)).startMonitoringBeaconsInRegion(captor.capture());
        return captor.getValue();
    }

    private RangeNotifier captureRangeNotifier() {
        ArgumentCaptor<RangeNotifier> captor = ArgumentCaptor.forClass(RangeNotifier.class);
        verify(altBeaconBridgeService, times(1)).addRangeNotifier(captor.capture());
        return captor.getValue();
    }

    private MonitorNotifier captureMonitorNotifier() {
        ArgumentCaptor<MonitorNotifier> captor = ArgumentCaptor.forClass(MonitorNotifier.class);
        verify(altBeaconBridgeService, times(1)).addMonitorNotifier(captor.capture());
        return captor.getValue();
    }
}