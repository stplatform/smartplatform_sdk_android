package com.kakao.oreum.infra.device;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class NetworkCheckerTest {
    NetworkChecker sut;

    @Before
    public void setUp() throws Exception {
        sut = new NetworkChecker(RuntimeEnvironment.application);
    }

    @Test
    public void testCheck() throws Exception {
        // then
        assertTrue(sut.check());
    }

    @Test (expected = DeviceCheckError.class)
    public void testAssertOk() throws Exception {
        // given
        NetworkChecker mock = spy(sut);
        when(mock.check()).thenReturn(false);

        // when
        // then
        mock.assertOk();
    }
}