package com.kakao.oreum.infra.persistence.sqlite;

import com.kakao.oreum.tamra.internal.sdklog.SDKLog;
import com.kakao.oreum.ox.format.ISO8601DateFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class SDKLogDBRepositoryTest {
    SDKLogDBRepository sut;

    @Before
    public void setUp() throws Exception {
        sut = new SDKLogDBRepository(new TamraDBHelper(RuntimeEnvironment.application), new ISO8601DateFormat());
    }

    @Test
    public void testInsert() throws Exception {
        // given
        String action = "beacon";
        SDKLog sdkLog = new SDKLog(action, "1", new Date());

        // when
        long rowId = sut.insert(sdkLog);

        // then
        assertThat(rowId, greaterThan(0L));
    }

    @Test
    public void testFind() throws Exception {
        // given
        SDKLog sdkLog = new SDKLog("region", "1", new Date());

        // when
        sut.insert(sdkLog);
        Date before = new Date();
        List<SDKLog> sdkLogs = sut.find(before);

        // then
        assertThat(sdkLogs, hasSize(1));
    }

    @Test
    public void testRemove() throws Exception {
        // given
        SDKLog sdkLog = new SDKLog("region", "2", new Date());

        // when
        sut.insert(sdkLog);
        Date before = new Date();
        List<SDKLog> sdkLogs = sut.find(before);

        // then
        assertThat(sdkLogs, hasSize(1));

        // when
        sut.remove(before);
        sdkLogs = sut.find(before);

        // then
        assertThat(sdkLogs, hasSize(0));
    }
}