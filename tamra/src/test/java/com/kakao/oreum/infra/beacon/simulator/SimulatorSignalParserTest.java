package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.List;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
@RunWith(RobolectricTestRunner.class)
public class SimulatorSignalParserTest {

    JsonDataParser<SimulatorBeaconData> sut;

    @Before
    public void setUp() throws Exception {
        sut = new SimulatorSignalParser();
    }

    @Test
    public void testMap() throws Exception {
        // given
        long beaconId = 28;
        double accuracy = 3.0;
        String support = "RT9E7HKzSDwco47Vk+ZqFu3EAXeA/OXpM0UmuyqkQWyAwLSgMzEUKx5x8/PQIxjE6AyDFO9WAAYYhuxXQdeYzTlrLmEbQjGkxkqHXTYSWXA3usbB/SJNsq4wsDmveKW0";

        String json = String.format(Locale.US, "[ {\n" +
                "  \"beaconId\": %d,\n" +
                "  \"accuracy\": %f,\n" +
                "  \"support\": \"%s\"\n" +
                "} ]", beaconId, accuracy, support);

        // when
        List<? extends SimulatorBeaconData> beaconDatas = sut.parseArray(json);

        // then
        assertThat(beaconDatas.size(), is(1));
        for (SimulatorBeaconData beaconData: beaconDatas) {
            assertThat(beaconData.id(), is(beaconId));
            assertThat(beaconData.accuracy(), is(accuracy));
            assertNotNull(beaconData.uuid());
            assertNotNull(beaconData.uuid());
            assertTrue(beaconData.major() > 0);
            assertTrue(beaconData.minor() > 0);
        }
    }
}