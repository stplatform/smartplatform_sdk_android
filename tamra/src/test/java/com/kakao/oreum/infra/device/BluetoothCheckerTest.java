package com.kakao.oreum.infra.device;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class BluetoothCheckerTest {
    BluetoothChecker sut;

    @Before
    public void setUp() throws Exception {
        sut = new BluetoothChecker();
    }

    @Test
    public void testCheck() throws Exception {
        assertFalse(sut.check());
    }

    @Test (expected = DeviceCheckError.class)
    public void testAssertOk() throws Exception {
        // when
        // then
        sut.assertOk();
    }
}