package com.kakao.oreum.infra.beacon.altbeacon;

import android.content.Context;

import com.kakao.oreum.infra.beacon.BeaconService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class BeaconServiceFactoryTest {
    Context context;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
    }

    @Test
    public void testCreate() throws Exception {


        // when
        BeaconService service1 = BeaconServiceFactory.create(context);
        BeaconService service2 = BeaconServiceFactory.create(context);

        // then
        assertThat(service2, is(service1));
    }
}