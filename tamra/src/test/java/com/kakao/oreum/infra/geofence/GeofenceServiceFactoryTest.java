package com.kakao.oreum.infra.geofence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class GeofenceServiceFactoryTest {

    @Test
    public void testCreate() throws Exception {
        // when
        GeofenceService geofenceService = GeofenceServiceFactory.create(RuntimeEnvironment.application);

        // then
        assertThat(geofenceService, instanceOf(GeofenceServiceBasedGoogleAPI.class));
    }
}