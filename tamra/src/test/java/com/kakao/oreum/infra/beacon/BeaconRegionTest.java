package com.kakao.oreum.infra.beacon;

import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class BeaconRegionTest {

    @Test
    public void testInitWith() throws Exception {
        // given
        String identifier = "IDENTIFIER";
        UUID uuid = UUID.randomUUID();
        int major = 1;
        int minor = 2;

        // when
        BeaconRegion regionWithUUID = BeaconRegion.initWith(identifier, uuid);
        BeaconRegion regionWithMajor = BeaconRegion.initWith(identifier, uuid, major);
        BeaconRegion regionWithMinor = BeaconRegion.initWith(identifier, uuid, major, minor);

        // then
        assertThat(regionWithUUID.proximityUUID(), is(uuid));
        assertThat(null, is(regionWithUUID.major()));
        assertThat(null, is(regionWithUUID.minor()));
        assertThat(regionWithMajor.major(), is(major));
        assertThat(null, is(regionWithMajor.minor()));

        assertThat(regionWithMinor.minor(), is(minor));
    }

    @Test
    public void testEquals() throws Exception {
        // given
        String identifier = "IDENTIFIER";
        UUID uuid = UUID.randomUUID();
        int major = 1;

        // when
        BeaconRegion regionWithUUID = BeaconRegion.initWith(identifier, uuid);
        BeaconRegion regionWithMajor = BeaconRegion.initWith(identifier, uuid, major);

        // then
        assertThat(regionWithUUID, is(regionWithUUID));
        assertThat("ID", not(regionWithUUID));
        assertThat(null, not(regionWithUUID));
        assertThat(regionWithUUID, is(regionWithMajor));
    }
}