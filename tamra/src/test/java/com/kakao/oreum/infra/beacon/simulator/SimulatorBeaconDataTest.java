package com.kakao.oreum.infra.beacon.simulator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
@RunWith(RobolectricTestRunner.class)
public class SimulatorBeaconDataTest {

    @Test
    public void testEquals() throws Exception {
        // given
        String support = "RT9E7HKzSDwco47Vk+ZqFu3EAXeA/OXpM0UmuyqkQWyAwLSgMzEUKx5x8/PQIxjE6AyDFO9WAAYYhuxXQdeYzTlrLmEbQjGkxkqHXTYSWXA3usbB/SJNsq4wsDmveKW0";
        SimulatorBeaconData data1 =
                SimulatorBeaconData.builder().id(28).accuracy(3).support(support).build();
        SimulatorBeaconData data2 =
                SimulatorBeaconData.builder().id(28).accuracy(10).support(support).build();

        // then
        assertThat(data1, is(data1));
        assertThat(data1, is(data2));
        assertThat(null, not(data1));
    }
}