package com.kakao.oreum.infra.beacon.distance;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class FitCurveDistanceCaculatorTest {

    DistanceCalculator sut;

    @Before
    public void setUp() throws Exception {
        sut = new FitCurveDistanceCaculator();
    }

    @Test
    public void test_rssi_0() throws Exception {
        // Given

        // When
        Double distance = sut.distance(-12, 0);

        // Then
        assertThat(distance, is(-1.0)); 
    }

    @Test
    public void test_rssi가_클수록_거리는_가까움() throws Exception {
        // Given
        Double distance1 = sut.distance(-8, -10);
        Double distance2 = sut.distance(-8, -3);

        // When
        // Then
        assertThat(distance1, greaterThan(distance2));
    }

    @Test
    public void test_txPower가_클수록_거리는_멀다() throws Exception {
        // Given
        Double distance1 = sut.distance(-8, -10);
        Double distance2 = sut.distance(-12, -10);

        // When
        // Then
        assertThat(distance1, greaterThan(distance2));
    }
}