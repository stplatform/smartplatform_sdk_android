package com.kakao.oreum.infra.persistence.sqlite;

import com.kakao.oreum.ox.format.ISO8601DateFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class SyncDBRepositoryTest {
    SyncDBRepository sut;

    @Before
    public void setUp() throws Exception {
        sut = new SyncDBRepository(new TamraDBHelper(RuntimeEnvironment.application), new ISO8601DateFormat());
    }

    @Test
    public void testUpdateLastSyncDateTimeForRegion() throws Exception {
        // given
        Date last = new Date();

        // when
        sut.updateLastSyncDateTimeForRegion(last);
        Date fetched = sut.getLastSyncDateTimeForRegion();

        // then
        assertThat(fetched, is(last));
    }

    @Test
    public void testUpdateLastSyncDateTimeForBeacon() throws Exception {
        // given
        Date last = new Date();

        // when
        sut.updateLastSyncDateTimeForBeacon(last);
        Date fetched = sut.getLastSyncDateTimeForBeacon();

        // then
        assertThat(fetched, is(last));
    }

    @Test
    public void testUpdateLastSyncDateTimeForSDKLog() throws Exception {
        // given
        Date last = new Date();

        // when
        sut.updateLastSyncDateTimeForSDKLog(last);
        Date fetched = sut.getLastSyncDateTimeForSDKLog();

        // then
        assertThat(fetched, is(last));
    }
}