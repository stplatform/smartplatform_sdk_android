package com.kakao.oreum.infra.beacon.simulator;

import com.kakao.oreum.tamra.internal.data.parser.JsonDataParser;

import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class SimulatorBeaconParserTest {
    JsonDataParser<SimulatorBeaconData.BeaconSpec> sut;

    @Before
    public void setUp() throws Exception {
        sut = new SimulatorBeaconParser();
    }

    @Test
    public void testMap() throws Exception {
        // given
        String uuid = "74704ede-18fe-4c49-bd24-95616c1e0fe8";
        int major = 1;
        int minor = 2;
        int txPower= -10;
        String json = String.format(Locale.KOREA, "{\n" +
                "  \"uuid\": \"%s\",\n" +
                "  \"major\": %d,\n" +
                "  \"minor\": %d,\n" +
                "  \"txPower\": %d\n" +
                "}", uuid, major, minor, txPower);

        // when
        SimulatorBeaconData.BeaconSpec support = sut.parseObject(json);

        // then
        assertNotNull(support);
        assertEquals(uuid, support.uuid());
        assertEquals(major, support.major());
        assertEquals(minor, support.minor());
        assertEquals(txPower, support.txPower());
    }
}