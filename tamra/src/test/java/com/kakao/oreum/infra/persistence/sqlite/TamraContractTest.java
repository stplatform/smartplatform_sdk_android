package com.kakao.oreum.infra.persistence.sqlite;

import org.junit.Test;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class TamraContractTest {
    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        // when
        TamraContract.class.newInstance();
    }

}