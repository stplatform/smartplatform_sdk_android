package com.kakao.oreum.infra.persistence.sqlite;

import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;
import com.kakao.oreum.tamra.base.DateTime;
import com.kakao.oreum.tamra.base.Period;
import com.kakao.oreum.tamra.base.Proximity;
import com.kakao.oreum.tamra.internal.data.data.HistoryData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class HistoryDBRepositoryTest {
    HistoryDBRepository sut;
    private SystemTimeService timeService;

    @Before
    public void setUp() throws Exception {
        timeService = new SystemTimeService();
        sut = new HistoryDBRepository(new TamraDBHelper(RuntimeEnvironment.application), new ISO8601DateFormat());

        sut.insert(HistoryData.builder()
                .spotId(1)
                .proximity(Proximity.Near)
                .description("test")
                .issuedAt(new DateTime().agoInHours(3).getTime())
                .build());

        sut.insert(HistoryData.builder()
                .spotId(1)
                .proximity(Proximity.Near)
                .description("test1")
                .issuedAt(new DateTime().agoInMinutes(3).getTime())
                .build());

        sut.insert(HistoryData.builder()
                .spotId(1)
                .proximity(Proximity.Near)
                .description("test1")
                .issuedAt(new DateTime().agoInSeconds(3).getTime())
                .build());

        sut.insert(HistoryData.builder()
                .spotId(1)
                .proximity(Proximity.Near)
                .description("test1")
                .issuedAt(new DateTime().agoInMilliseconds(3).getTime())
                .build());
    }

    @Test
    public void testFind() throws Exception {
        // when
        List<HistoryData> dataList = sut.find(new Period(new DateTime().agoInHours(4), new DateTime().agoInHours(1)));

        // then
        assertThat(dataList, hasSize(1));

        // when
        dataList = sut.find(new Period(new DateTime().agoInHours(1), new DateTime().agoInSeconds(1)));

        // then
        assertThat(dataList, hasSize(2));
    }
}