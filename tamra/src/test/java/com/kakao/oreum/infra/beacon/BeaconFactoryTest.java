package com.kakao.oreum.infra.beacon;

import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class BeaconFactoryTest {

    @Test
    public void testProximity() throws Exception {
        // given
        int txPower = -10;
        int rssi = -5;

        // when
        Beacon beacon = BeaconFactory.getDefault().create(UUID.randomUUID(), 1, 1, txPower, rssi);

        // then
        assertThat(beacon.proximity(), is(Proximity.Immediate));

        // given
        rssi = -10;

        // when
        beacon = BeaconFactory.getDefault().create(UUID.randomUUID(), 1, 1, txPower, rssi);

        // then
        assertThat(beacon.proximity(), is(Proximity.Near));

        // given
        rssi = -20;

        // when
        beacon = BeaconFactory.getDefault().create(UUID.randomUUID(), 1, 1, txPower, rssi);

        // then
        assertThat(beacon.proximity(), is(Proximity.Far));

        // given
        rssi = 0;

        // when
        beacon = BeaconFactory.getDefault().create(UUID.randomUUID(), 1, 1, txPower, rssi);

        // then
        assertThat(beacon.proximity(), is(Proximity.Unknown));
    }
}