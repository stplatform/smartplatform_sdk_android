package com.kakao.oreum.infra.geofence;

import android.content.Intent;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.location.Geofence;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.util.ServiceController;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
@Ignore
public class GeofenceIntentProcessorTest {
    private GeofenceIntentProcessor sut;
    private ServiceController<GeofenceIntentProcessor> controller;
    private GeofenceEventHandler eventHandler;

    @Before
    public void setUp() throws Exception {
        controller = Robolectric.buildService(GeofenceIntentProcessor.class);
        sut = controller.attach().create().get();

        eventHandler = mock(GeofenceEventHandler.class);
        GeofenceEventHandlers.register(eventHandler);
    }

    @Test
    public void testOnHandleIntentEnter() throws Exception {
        // given
        Intent intent = new Intent(RuntimeEnvironment.application, GeofenceIntentProcessor.class);
        intent.putExtra("com.google.android.location.intent.extra.transition", Geofence.GEOFENCE_TRANSITION_ENTER);
        intent.putExtra("com.google.android.location.intent.extra.geofence_list", getGeofences());
        intent.putExtra("com.google.android.location.intent.extra.triggering_location", mock(Location.class));

        // when
        sut.onHandleIntent(intent);

        // then
        verify(eventHandler, atLeast(1)).onEnter(any(Geofence.class));
    }


    @Test
    public void testOnHandleIntentExit() throws Exception {
        // given
        Intent intent = new Intent(RuntimeEnvironment.application, GeofenceIntentProcessor.class);
        intent.putExtra("com.google.android.location.intent.extra.transition", Geofence.GEOFENCE_TRANSITION_EXIT);
        intent.putExtra("com.google.android.location.intent.extra.geofence_list", getGeofences());
        intent.putExtra("com.google.android.location.intent.extra.triggering_location", mock(Location.class));

        // when
        sut.onHandleIntent(intent);

        // then
        verify(eventHandler, atLeast(1)).onExit(any(Geofence.class));
    }


    @Test
    public void testOnHandleIntentDwell() throws Exception {
        // given
        Intent intent = new Intent(RuntimeEnvironment.application, GeofenceIntentProcessor.class);
        intent.putExtra("com.google.android.location.intent.extra.transition", Geofence.GEOFENCE_TRANSITION_DWELL);
        intent.putExtra("com.google.android.location.intent.extra.geofence_list", getGeofences());
        intent.putExtra("com.google.android.location.intent.extra.triggering_location", mock(Location.class));

        // when
        sut.onHandleIntent(intent);

        // then
        verify(eventHandler, atLeast(1)).onDwell(any(Geofence.class));
    }

    @After
    public void tearDown() {
        controller.destroy();
    }

    private ArrayList<byte[]> getGeofences() {
        ArrayList<byte[]> geofences = new ArrayList<>();
//        geofences.add(marshall(new ParcelableGeofence(1, "abc", 1, (short)1, 0.0, 0.0, (float)100.0, 1, 1, 1)));
        return geofences;
    }

    public byte[] marshall(Parcelable parceable) {
        Parcel parcel = Parcel.obtain();
        parceable.writeToParcel(parcel, 0);
        byte[] bytes = parcel.marshall();
        parcel.recycle();
        return bytes;
    }
}