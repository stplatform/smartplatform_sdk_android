package com.kakao.oreum.infra.beacon.altbeacon;

import com.kakao.oreum.testhelper.base.TestBase;

import org.altbeacon.beacon.BeaconManager;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.2.0
 */
public class AltBeaconConnectorTest extends TestBase {
    AltBeaconConnector sut;
    AltBeaconConnectListener mockListener;

    @Before
    public void setUp() throws Exception {
        sut = new AltBeaconConnector();
        mockListener = mock(AltBeaconConnectListener.class);
    }

    @Test
    public void connect되면_listener에게_알린다() throws Exception {
        // Given
        AltBeaconBridgeService mockBeaconBridgeService = mock(AltBeaconBridgeService.class);
        sut.setConnectListener(mockListener);

        // When
        sut.connect(mockBeaconBridgeService);

        // Then
        verify(mockListener, times(1)).connected(mockBeaconBridgeService);
    }

    @Test
    public void disconnect되면_listener에게_알린다() throws Exception {
        // Given
        AltBeaconBridgeService mockBeaconBridgeService = mock(AltBeaconBridgeService.class);
        sut.connect(mockBeaconBridgeService);
        sut.setConnectListener(mockListener);

        // When
        sut.disconnect();

        // Then
        verify(mockListener, times(1)).disconnected();
    }

    @Test
    public void listener를_set할때_disconnected상태이면_disconnect알림을_보낸다() throws Exception {
        // Given

        // When
        sut.setConnectListener(mockListener);

        // Then
        verify(mockListener, times(1)).disconnected();
    }

    @Test
    public void listener가_set될때_connect상태이면_connect알림을_보낸다() throws Exception {
        // Given
        AltBeaconBridgeService mockBeaconBridgeService = mock(AltBeaconBridgeService.class);
        sut.connect(mockBeaconBridgeService);

        // When
        sut.setConnectListener(mockListener);

        // Then
        verify(mockListener, times(1)).connected(mockBeaconBridgeService);

    }
}