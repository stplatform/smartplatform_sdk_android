package com.kakao.oreum.infra.beacon.simulator;

import android.content.Context;

import com.kakao.oreum.infra.beacon.BeaconService;
import com.kakao.oreum.ox.http.OxHttpClient;

import org.junit.Test;

import java.net.URL;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
public class SimulatorFactoryTest {
    protected Context getContext() {
        Context context = mock(Context.class);
        return context;
    }

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스를_생성할_수_없다() throws Exception {
        // when
        SimulatorFactory.class.newInstance();
    }

    @Test
    public void testCreate() throws Exception {
        // given
        Context context = getContext();
        OxHttpClient httpClient = OxHttpClient.forBaseUrl(new URL("http://localhost"));
        String deviceName = "abc";

        // when
        BeaconService simulator = SimulatorFactory.create(httpClient, deviceName);
        BeaconService simulator2 = SimulatorFactory.create(httpClient, deviceName);

        // then
        assertNotNull(simulator);
        assertTrue(simulator == simulator2);
    }
}