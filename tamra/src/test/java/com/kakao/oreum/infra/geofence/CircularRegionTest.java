package com.kakao.oreum.infra.geofence;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class CircularRegionTest {

    @Test
    public void testEquals() throws Exception {
        // given
        long id = 1;

        // when
        CircularRegion region1 = CircularRegion.builder()
                .id(id)
                .latitude(10.0)
                .longitude(10.0)
                .radius(100)
                .build();
        CircularRegion region2 = CircularRegion.builder()
                .id(id)
                .latitude(20.0)
                .longitude(20.0)
                .radius(200)
                .build();

        // then
        assertThat(null, not(region1));
        assertThat("", not(region1));
        assertThat(region1, is(region1));
        assertThat(region2, is(region1));

        assertThat(region2.id(), is(region1.id()));
        assertThat(region2.latitude(), not(region1.latitude()));
        assertThat(region2.longitude(), not(region1.longitude()));
        assertThat(region2.radius(), not(region1.radius()));
    }
}