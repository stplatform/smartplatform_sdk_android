package com.kakao.oreum.ox.http;

import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class StatusTest extends TestBase {

    @Test
    public void 코드200번대는_성공으로처리() throws Exception {
        // Then
        for (Status status : Status.values()) {
            if (status.code() < 200 && status.code() > 299) {
                assertThat(status.success(), is(true));
                assertThat(status.error(), is(false));
                assertThat(status.moved(), is(false));

            }
        }
    }

    @Test
    public void 코드300번대는_moved로처리() throws Exception {
        // Then
        for (Status status : Status.values()) {
            if (status.code() < 300 && status.code() > 399) {
                assertThat(status.success(), is(false));
                assertThat(status.error(), is(false));
                assertThat(status.moved(), is(true));

            }
        }
    }

    @Test
    public void 코드400번이상은_error로처리() throws Exception {
        // Then
        for (Status status : Status.values()) {
            if (status.code() >= 400) {
                assertThat(status.success(), is(false));
                assertThat(status.error(), is(true));
                assertThat(status.moved(), is(false));

            }
        }
    }

}