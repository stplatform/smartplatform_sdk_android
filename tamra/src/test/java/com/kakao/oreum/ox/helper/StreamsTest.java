package com.kakao.oreum.ox.helper;

import org.junit.Test;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class StreamsTest {
    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Streams.class.newInstance();
    }
}