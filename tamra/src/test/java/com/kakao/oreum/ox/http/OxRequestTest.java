package com.kakao.oreum.ox.http;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.testhelper.base.RestApiTest;

import org.junit.Test;

import java.io.ByteArrayInputStream;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class OxRequestTest extends RestApiTest {
    private static final Logger LOG = getLogger(OxRequestTest.class);

    @Test
    public void test_simple_case() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/hello"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello")
                ));
        // When
        Response response = OxRequest.forURL(urlFor("/hello"))
                .emptyBody()
                .request(Method.GET);

        // Then
        assertThat(response.status(), is(Status.OK));
        assertThat(response.bodyAsString(), is("hello"));
    }

    @Test
    public void parameter를_추가할_수_있다() throws Exception {
        // Given
        stubFor(get(urlPathEqualTo("/hello"))
                .withQueryParam("name", equalTo("john"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello, john")
                ));
        stubFor(get(urlPathEqualTo("/hello")) // parameter 없는 경우
                .withQueryParam("name", equalTo("jane"))
                .willReturn(aResponse()
                        .withStatus(400)
                        .withBody("U R NOT john")
                ));
        // When
        Response response = OxRequest
                .forURL(urlFor("/hello"))
                .params("name", "john")
                .emptyBody()
                .request(Method.GET);

        // Then
        assertThat(response.status(), is(Status.OK));
        assertThat(response.bodyAsString(), is("hello, john"));
    }

    @Test
    public void 원래_url에_파라메터도_유지되어야_한다() throws Exception {
        // Given
        stubFor(get(urlPathEqualTo("/hello"))
                .withQueryParam("name", equalTo("john"))
                .withQueryParam("greeting", equalTo("hello"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello, john")
                ));

        // When
        Response response = OxRequest
                .forURL(urlFor("/hello?greeting=hello"))
                .params("name", "john")
                .request(Method.GET);
        LOG.debug("response: {}", response);
        // Then
        assertThat(response.status(), is(Status.OK));
        assertThat(response.bodyAsString(), is("hello, john"));
    }

    @Test
    public void http_conn관련_옵션을_설정할_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/old"))
                .willReturn(aResponse()
                        .withStatus(301)
                        .withHeader("Location", urlFor("/new").toString())
                        .withBody("hello")
                ));
        stubFor(get(urlEqualTo("/new"))
                .willReturn(aResponse()
                        .withFixedDelay(200) // 강제 딜레이
                        .withStatus(200)
                        .withBody("Brave New World")
                ));
        // When
        Response response = OxRequest.forURL(urlFor("/old"))
                .withOpts(HttpOpts.create()
                        .readTimeout(500)
                        .followRedirects(true))
                .request(Method.GET);

        // Then
        assertThat(response.status(), is(Status.OK));
        assertThat(response.bodyAsString(), is("Brave New World"));
    }

    @Test
    public void header를_조작할_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/hello"))
                .withHeader("User-Agent", containing("Safari"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello nice guy.")
                ));

        stubFor(get(urlEqualTo("/hello"))
                .withHeader("User-Agent", containing("Internet Explorer"))
                .willReturn(aResponse()
                        .withStatus(400)
                        .withBody("DO NOT USE FUCKING IE!!!!")
                ));

        // When + Then
        assertThat(
                OxRequest.forURL(urlFor("/hello"))
                        .header("User-Agent", "Safari/1.0")
                        .request(Method.GET).status(),
                is(Status.OK));
        assertThat(
                OxRequest.forURL(urlFor("/hello"))
                        .header("User-Agent", "Internet Explorer")
                        .request(Method.GET).status(),
                is(Status.BadRequest));
    }

    @Test
    public void body를_stream으로_설정할_수_있다() throws Exception {
        // Given
        stubFor(post(urlEqualTo("/create"))
                .withRequestBody(matching("body:.*"))
                .willReturn(aResponse()
                        .withStatus(200)));

        // When
        Response response = OxRequest.forURL(urlFor("/create"))
                .body(new ByteArrayInputStream("body: hello".getBytes()))
                .request(Method.POST);

        // Then
        verify(postRequestedFor(urlEqualTo("/create"))
                .withRequestBody(equalTo("body: hello")));
        assertThat(response.status(), is(Status.OK));
    }

    @Test
    public void body를_string으로_설정할_수_있다() throws Exception {
        // Given
        stubFor(post(urlEqualTo("/create"))
                .withRequestBody(matching("body:.*"))
                .willReturn(aResponse()
                        .withStatus(200)));

        // When
        Response response = OxRequest.forURL(urlFor("/create"))
                .body("body: hello")
                .request(Method.POST);

        // Then
        verify(postRequestedFor(urlEqualTo("/create"))
                .withRequestBody(equalTo("body: hello")));
        assertThat(response.status(), is(Status.OK));
    }
}