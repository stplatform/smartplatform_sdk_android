package com.kakao.oreum.ox.logger;

import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public abstract class LoggerTest extends TestBase {
    private static final Logger LOG = getLogger(LoggerTest.class);
    protected Logger sut;
    private ByteArrayOutputStream loggingStream;

    /**
     * logger 객체 생성.
     *
     * @param level
     * @return
     */
    protected abstract Logger createLogger(Level level, PrintStream outstream);

    protected abstract String asString(Level level);

    protected final Logger createLogger(Level level) throws UnsupportedEncodingException {
        return createLogger(level, createLogStream());
    }

    /**
     * Logger에 의해서 출력된 정보들
     *
     * @return
     */
    protected String loggedText() throws UnsupportedEncodingException {
        if (loggingStream != null) {
            return new String(loggingStream.toByteArray(), "UTF-8");
        } else {
            return null;
        }
    }

    protected PrintStream createLogStream() throws UnsupportedEncodingException {
        loggingStream = new ByteArrayOutputStream();
        return new PrintStream(loggingStream, true, "UTF-8");
    }


    @Test
    public void accept_되지_않는_log는_남기지_않는다() throws Exception {
        // When
        sut = createLogger(Level.DEBUG);
        sut.trace("hello");

        // Then
        LOG.debug("logged{}", loggedText());
        String logged = loggedText();
        assertThat(logged, isEmptyString());
    }

    @Test
    public void accept되는_info_log는_로그레벨과_함께_메시지가_loggin된다() throws Exception {
        // When
        sut = createLogger(Level.DEBUG);
        sut.info("hello {}", "oreum");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.INFO)));
        assertThat(loggedText(), containsString("hello oreum"));
    }

    @Test
    public void 예외가_넘겨지면_로그메시지와_함께_예외정보가_logging된다() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);

        // When
        sut.warn("something wrong!", new IOException("fail to read."));

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("something wrong!"));
        assertThat(loggedText(), containsString("fail to read"));
        assertThat(loggedText(), containsString("IOException"));
    }

    @Test
    public void 예외가_넘겨지면_stack_trace정보가_출력된다() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);

        // When
        sut.warn("something wrong!", new IOException("fail to read."));

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("something wrong!"));
        assertThat(loggedText(), containsString("at " + Logger.class.getName()));
    }

    @Test
    public void 중첩예외가_넘겨지면_stack_trace정보에_caused_by부분이_출력된다() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);

        // When
        sut.warn("something wrong!", new IOException("fail to read.", new FileNotFoundException()));

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("something wrong!"));
        assertThat(loggedText(), containsString("at " + Logger.class.getName()));
        assertThat(loggedText(), containsString(IOException.class.getName()));
        assertThat(loggedText(), containsString("Caused by: " + FileNotFoundException.class.getName()));
    }

    //=================================================
    // 이하 테스트는 메소드들이 출력과 잘 연결되었는지 확인하는 용도
    //  copy & paste 코드라 일단 확인용
    //=================================================
    @Test
    public void trace_log_arg1() throws Exception {
        // Given
        sut = createLogger(Level.TRACE);

        // When
        sut.trace("1. {}", "oreum");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.TRACE)));
        assertThat(loggedText(), containsString("1. oreum"));
    }


    @Test
    public void trace_log_arg2() throws Exception {
        // Given
        sut = createLogger(Level.TRACE);
        // When
        sut.trace("2. {}, {}", "oreum", "tamra");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.TRACE)));
        assertThat(loggedText(), containsString("2. oreum, tamra"));
    }

    @Test
    public void trace_log_arg3() throws Exception {
        // Given
        sut = createLogger(Level.TRACE);
        // When
        sut.trace("3. {}, {}, {}", "oreum", "tamra", "jeju");

        // Then
        LOG.trace("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.TRACE)));
        assertThat(loggedText(), containsString("3. oreum, tamra, jeju"));
    }

    @Test
    public void trace_log_with_execption() throws Exception {
        // Given
        sut = createLogger(Level.TRACE);

        // When
        sut.trace("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.TRACE)));
        assertThat(loggedText(), containsString("oops!"));
        assertThat(loggedText(), containsString(NullPointerException.class.getName()));
    }

    @Test
    public void trace_log_skipped() throws Exception {
        // Given
        sut = createLogger(Level.INFO);

        // When
        sut.trace("1. {}", "oreum");
        sut.trace("2. {}, {}", "oreum", "tamra");
        sut.trace("3. {}, {}, {}", "oreum", "tamra", "jeju");
        sut.trace("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), isEmptyOrNullString());
    }

    @Test
    public void debug_log_arg1() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);

        // When
        sut.debug("1. {}", "oreum");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.DEBUG)));
        assertThat(loggedText(), containsString("1. oreum"));
    }


    @Test
    public void debug_log_arg2() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);
        // When
        sut.debug("2. {}, {}", "oreum", "tamra");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.DEBUG)));
        assertThat(loggedText(), containsString("2. oreum, tamra"));
    }

    @Test
    public void debug_log_arg3() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);
        // When
        sut.debug("3. {}, {}, {}", "oreum", "tamra", "jeju");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.DEBUG)));
        assertThat(loggedText(), containsString("3. oreum, tamra, jeju"));
    }

    @Test
    public void debug_log_with_execption() throws Exception {
        // Given
        sut = createLogger(Level.DEBUG);

        // When
        sut.debug("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.DEBUG)));
        assertThat(loggedText(), containsString("oops!"));
        assertThat(loggedText(), containsString(NullPointerException.class.getName()));
    }

    @Test
    public void debug_log_skipped() throws Exception {
        // Given
        sut = createLogger(Level.INFO);

        // When
        sut.debug("1. {}", "oreum");
        sut.debug("2. {}, {}", "oreum", "tamra");
        sut.debug("3. {}, {}, {}", "oreum", "tamra", "jeju");
        sut.debug("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), isEmptyOrNullString());
    }

    @Test
    public void info_log_arg1() throws Exception {
        // Given
        sut = createLogger(Level.INFO);

        // When
        sut.info("1. {}", "oreum");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.INFO)));
        assertThat(loggedText(), containsString("1. oreum"));
    }


    @Test
    public void info_log_arg2() throws Exception {
        // Given
        sut = createLogger(Level.INFO);
        // When
        sut.info("2. {}, {}", "oreum", "tamra");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.INFO)));
        assertThat(loggedText(), containsString("2. oreum, tamra"));
    }

    @Test
    public void info_log_arg3() throws Exception {
        // Given
        sut = createLogger(Level.INFO);
        // When
        sut.info("3. {}, {}, {}", "oreum", "tamra", "jeju");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.INFO)));
        assertThat(loggedText(), containsString("3. oreum, tamra, jeju"));
    }

    @Test
    public void info_log_with_execption() throws Exception {
        // Given
        sut = createLogger(Level.INFO);

        // When
        sut.info("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.INFO)));
        assertThat(loggedText(), containsString("oops!"));
        assertThat(loggedText(), containsString(NullPointerException.class.getName()));
    }

    @Test
    public void info_log_skipped() throws Exception {
        // Given
        sut = createLogger(Level.WARN);

        // When
        sut.info("1. {}", "oreum");
        sut.info("2. {}, {}", "oreum", "tamra");
        sut.info("3. {}, {}, {}", "oreum", "tamra", "jeju");
        sut.info("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), isEmptyOrNullString());
    }

    @Test
    public void warn_log_arg1() throws Exception {
        // Given
        sut = createLogger(Level.WARN);

        // When
        sut.warn("1. {}", "oreum");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("1. oreum"));
    }


    @Test
    public void warn_log_arg2() throws Exception {
        // Given
        sut = createLogger(Level.WARN);
        // When
        sut.warn("2. {}, {}", "oreum", "tamra");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("2. oreum, tamra"));
    }

    @Test
    public void warn_log_arg3() throws Exception {
        // Given
        sut = createLogger(Level.WARN);
        // When
        sut.warn("3. {}, {}, {}", "oreum", "tamra", "jeju");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("3. oreum, tamra, jeju"));
    }

    @Test
    public void warn_log_with_execption() throws Exception {
        // Given
        sut = createLogger(Level.WARN);

        // When
        sut.warn("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.WARN)));
        assertThat(loggedText(), containsString("oops!"));
        assertThat(loggedText(), containsString(NullPointerException.class.getName()));
    }

    @Test
    public void warn_log_skipped() throws Exception {
        // Given
        sut = createLogger(Level.ERROR);

        // When
        sut.warn("1. {}", "oreum");
        sut.warn("2. {}, {}", "oreum", "tamra");
        sut.warn("3. {}, {}, {}", "oreum", "tamra", "jeju");
        sut.warn("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), isEmptyOrNullString());
    }

    @Test
    public void error_log_arg1() throws Exception {
        // Given
        sut = createLogger(Level.ERROR);

        // When
        sut.error("1. {}", "oreum");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.ERROR)));
        assertThat(loggedText(), containsString("1. oreum"));
    }


    @Test
    public void error_log_arg2() throws Exception {
        // Given
        sut = createLogger(Level.ERROR);
        // When
        sut.error("2. {}, {}", "oreum", "tamra");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.ERROR)));
        assertThat(loggedText(), containsString("2. oreum, tamra"));
    }

    @Test
    public void error_log_arg3() throws Exception {
        // Given
        sut = createLogger(Level.ERROR);
        // When
        sut.error("3. {}, {}, {}", "oreum", "tamra", "jeju");

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.ERROR)));
        assertThat(loggedText(), containsString("3. oreum, tamra, jeju"));
    }

    @Test
    public void error_log_with_execption() throws Exception {
        // Given
        sut = createLogger(Level.ERROR);

        // When
        sut.error("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), containsString(asString(Level.ERROR)));
        assertThat(loggedText(), containsString("oops!"));
        assertThat(loggedText(), containsString(NullPointerException.class.getName()));
    }

    @Test
    public void error_log_skipped() throws Exception {
        // Given
        sut = createLogger(Level.NONE);

        // When
        sut.error("1. {}", "oreum");
        sut.error("2. {}, {}", "oreum", "tamra");
        sut.error("3. {}, {}, {}", "oreum", "tamra", "jeju");
        sut.error("oops!", new NullPointerException());

        // Then
        LOG.debug("logged{}", loggedText());
        assertThat(loggedText(), isEmptyOrNullString());
    }


    @Test
    public void test_isLoggable로_처리가능한_logLevel을_체크할_수_있다() throws Exception {
        // Given
        // When
        // Then
        assertThat(createLogger(Level.TRACE).isLoggable(Level.DEBUG), is(true));
        assertThat(createLogger(Level.TRACE).isLoggable(Level.INFO), is(true));
        assertThat(createLogger(Level.INFO).isLoggable(Level.DEBUG), is(false));
        assertThat(createLogger(Level.INFO).isLoggable(Level.TRACE), is(false));
    }
}
