package com.kakao.oreum.ox.http;

import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.testhelper.base.TestBase;
import com.kakao.oreum.ox.helper.Streams;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class ResponseTest extends TestBase {
    Response sut;

    @Before
    public void setUp() throws Exception {
        sut = new Response(Status.OK,
                OxIterable.from(
                        Header.of("User-Agent", "Tamra/0.6.0"),
                        Header.of("Content-Type", "text/plain")
                ).toList(),
                new ByteArrayInputStream("hello world".getBytes())
        );
    }

    @Test
    public void http_raw_message로_변경할_수_있다() throws Exception {
        // Given

        // When
        String raw = sut.rawMessage();

        // Then
        assertThat(raw, containsString("HTTP/1.1 200 OK"));
        assertThat(raw, containsString("User-Agent: Tamra/0.6.0"));
    }

    @Test
    public void message_body를_inputStream으로_얻을수_있다() throws Exception {
        // Given

        // When
        InputStream bodyStream = sut.body();

        // Then
        assertThat(Streams.readAsString(bodyStream), is("hello world"));
    }

    @Test
    public void header를_header객체로_얻을_수_있다() throws Exception {
        // Given

        // When
        List<Header> headers = sut.headers();

        // Then
        assertThat(headers, contains(
                Header.of("User-Agent", "Tamra/0.6.0"),
                Header.of("Content-Type", "text/plain")
        ));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void headers는_수정_불가능하다() throws Exception {
        // Given

        // When
        sut.headers().clear();

        // Then
        fail("should raise exception");
    }
}