package com.kakao.oreum.ox.http;

import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class MethodTest extends TestBase {

    @Test
    public void 이름과_rawName은_같다___적어도_난독화되기전에는() throws Exception {
        // 새로운 거 추가했을때 typo 체크를 위한 테스트
        for (Method method : Method.values()) {
            assertThat(method.name(), is(method.rawName()));
        }
    }
}