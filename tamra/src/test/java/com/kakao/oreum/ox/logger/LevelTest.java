package com.kakao.oreum.ox.logger;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class LevelTest {

    @Test
    public void test_accept_of_trace() throws Exception {
        assertThat(Level.TRACE.accept(Level.TRACE), is(true));
        assertThat(Level.TRACE.accept(Level.DEBUG), is(true));
        assertThat(Level.TRACE.accept(Level.INFO), is(true));
        assertThat(Level.TRACE.accept(Level.WARN), is(true));
        assertThat(Level.TRACE.accept(Level.ERROR), is(true));
    }

    @Test
    public void test_accept_of_debug() throws Exception {
        assertThat(Level.DEBUG.accept(Level.TRACE), is(false));
        assertThat(Level.DEBUG.accept(Level.DEBUG), is(true));
        assertThat(Level.DEBUG.accept(Level.INFO), is(true));
        assertThat(Level.DEBUG.accept(Level.WARN), is(true));
        assertThat(Level.DEBUG.accept(Level.ERROR), is(true));
    }

    @Test
    public void test_accept_of_info() throws Exception {
        assertThat(Level.INFO.accept(Level.TRACE), is(false));
        assertThat(Level.INFO.accept(Level.DEBUG), is(false));
        assertThat(Level.INFO.accept(Level.INFO), is(true));
        assertThat(Level.INFO.accept(Level.WARN), is(true));
        assertThat(Level.INFO.accept(Level.ERROR), is(true));
    }

@Test
    public void test_accept_of_warn() throws Exception {
        assertThat(Level.WARN.accept(Level.TRACE), is(false));
        assertThat(Level.WARN.accept(Level.DEBUG), is(false));
        assertThat(Level.WARN.accept(Level.INFO), is(false));
        assertThat(Level.WARN.accept(Level.WARN), is(true));
        assertThat(Level.WARN.accept(Level.ERROR), is(true));
    }

@Test
    public void test_accept_of_error() throws Exception {
        assertThat(Level.ERROR.accept(Level.TRACE), is(false));
        assertThat(Level.ERROR.accept(Level.DEBUG), is(false));
        assertThat(Level.ERROR.accept(Level.INFO), is(false));
        assertThat(Level.ERROR.accept(Level.WARN), is(false));
        assertThat(Level.ERROR.accept(Level.ERROR), is(true));
    }


}