package com.kakao.oreum.ox.collection;

import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class ListsTest extends TestBase {

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Lists.class.newInstance();
    }


    @Test
    public void 한개짜리_리스트_생성() throws Exception {
        // Given
        List<String> list = Lists.arrayList("hello");

        // When

        // Then
        assertThat(list, hasSize(1));
    }

    @Test
    public void 빈_리스트_생성() throws Exception {
        // Given
        List<String> list = Lists.arrayList();

        // When

        // Then
        assertThat(list, empty());
    }

    @Test
    public void null이_하나있는_list() throws Exception {
        // Given
        List<String> list = Lists.arrayList((String) null);

        // When

        // Then
        assertThat(list, hasSize(1));
        assertThat(list, hasItem((String) null));
    }

    @Test
    public void testArrayList() throws Exception {
        // given
        int[] arrayInts = new int[] { 1, 2 };

        // when
        List<Integer> integers = Lists.arrayList(arrayInts);

        // then
        assertThat(integers, hasSize(2));
    }

    @Test
    public void testLinkedList() throws Exception {
        // given
        Set<String> strings = new HashSet<>();
        strings.add("value1");
        strings.add("value2");

        // when
        List<String> integers = Lists.linkedList(strings);

        // then
        assertThat(integers, hasSize(strings.size()));
    }
}