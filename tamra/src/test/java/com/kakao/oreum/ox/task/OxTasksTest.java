package com.kakao.oreum.ox.task;

import com.kakao.oreum.common.function.Executable;
import com.kakao.oreum.testhelper.base.AndroidContextTest;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class OxTasksTest extends AndroidContextTest {

    @Test
    public void test_default_onUI() throws Exception {
        // Given

        // When
        // unittest에서 어떻게 나올까?
        RunOn runOn = OxTasks.currentRunOn();

        // Then
        // 확인!! : 이걸 베이스로
        assertThat(runOn, is(RunOn.UI));
        assertThat(OxTasks.onUiThread(), is(true));
    }

    @Test
    public void test_onBackground() throws Exception {
        // Given
        RunOn[] ref = new RunOn[1];

        // When
        doNWait(() -> {
            ref[0] = OxTasks.currentRunOn();
        });

        // Then
        assertThat(ref[0], is(RunOn.BACKGROUND));
    }

    @Test
    public void test_runOnBackground() throws Exception {
        // Given
        Runnable runnable = mock(Runnable.class);

        // When
        OxTasks.runOnBackground(runnable);

        // Then
        verify(runnable, never()).run(); // 아직 실행 안됨.
        backgroundLooper().idle(); // background looper 실행.
        verify(runnable, times(1)).run();
    }

    @Test
    public void test_runOn_Background() throws Exception {
        // Given
        Runnable runnable = mock(Runnable.class);

        // When
        OxTasks.runOn(RunOn.BACKGROUND, runnable);

        // Then
        verify(runnable, never()).run(); // 아직 실행 안됨.
        backgroundLooper().idle(); // background looper 실행.
        verify(runnable, times(1)).run();
    }

    @Test
    public void test_runOnUi() throws Exception {
        // Given
        Runnable runnable = mock(Runnable.class);

        // When
        OxTasks.runOnUi(runnable);

        // Then
        // main looper는 바로 실행되는듯. (현 쓰레드니깐)
        //        verify(runnable, never()).run();
        mainLooper().idle(); // main looper 실행.
        verify(runnable, times(1)).run();
    }

    @Test
    public void test_runOn_Ui() throws Exception {
        // Given
        Runnable runnable = mock(Runnable.class);

        // When
        OxTasks.runOn(RunOn.UI, runnable);

        // Then
        // main looper는 바로 실행되는듯. (현 쓰레드니깐)
        //        verify(runnable, never()).run();
        mainLooper().idle(); // main looper 실행.
        verify(runnable, times(1)).run();
    }

    @Test
    public void test_runRepeatly() throws Exception {
        // Given
        Executable runnable = mock(Executable.class);

        // When
        OxTasks.execRepeatly(Period.milliseconds(100), runnable);

        // Then
        waitMSec(250);
        verify(runnable, atLeast(3)).exec(); // 0ms, 100ms, 200ms. 최소 3번
    }


    private void waitMSec(long msec) throws InterruptedException {
        long now = System.currentTimeMillis();
        doNWait(() -> {
            while (now + msec > System.currentTimeMillis()) {
                Thread.yield();
            }
        });
    }

    private void doNWait(Runnable runnable) throws InterruptedException {
        Thread thread = new Thread(runnable);
        thread.start();
        thread.join();
    }


}