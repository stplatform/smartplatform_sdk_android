package com.kakao.oreum.ox.collection;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class UnmodifiableListIteratorTest {
    UnmodifiableListIterator<String> sut;
    @Before
    public void setUp() throws Exception {
        List<String> items = new ArrayList<>();
        items.add("string1");
        items.add("string2");
        sut = UnmodifiableListIterator.from(items.listIterator());
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testAdd() throws Exception {
        // when
        sut.add("string3");
    }

    @Test
    public void testHasNext() throws Exception {
        // then
        assertTrue(sut.hasNext());

        // when
        sut.next();
        sut.next();

        // then
        assertFalse(sut.hasNext());
    }

    @Test
    public void testHasPrevious() throws Exception {
        // then
        assertFalse(sut.hasPrevious());

        // when
        sut.next();

        // then
        assertTrue(sut.hasPrevious());
    }

    @Test
    public void testNextIndex() throws Exception {
        // then
        assertThat(sut.nextIndex(), is(0));

        // when
        sut.next();

        // then
        assertThat(sut.nextIndex(), is(1));
    }

    @Test
    public void testPrevious() throws Exception {
        // when
        sut.next();

        // then
        assertThat(sut.previous(), is("string1"));
    }

    @Test
    public void testPreviousIndex() throws Exception {
        // when
        sut.next();

        // then
        assertThat(sut.previousIndex(), is(0));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRemove() throws Exception {
        // when
        sut.remove();
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testSet() throws Exception {
        // when
        sut.set("string3");
    }
}