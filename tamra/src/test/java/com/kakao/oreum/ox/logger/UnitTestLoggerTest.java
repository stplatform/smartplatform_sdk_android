package com.kakao.oreum.ox.logger;

import org.junit.Test;

import java.io.PrintStream;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class UnitTestLoggerTest extends LoggerTest {

    @Override
    protected Logger createLogger(Level level, PrintStream outstream) {
        return new UnitTestLogger(this.getClass(), level, outstream, outstream);
    }

    @Override
    protected String asString(Level level) {
        return level.name();
    }

    @Test
    public void logger이름을_출력할때_package명이_길면_줄인다() throws Exception {
        // Given

        // When
        createLogger(Level.DEBUG).debug("long");

        // Then
        assertThat(loggedText(), containsString("c.k.o.o.l." + this.getClass().getSimpleName()));
    }

}