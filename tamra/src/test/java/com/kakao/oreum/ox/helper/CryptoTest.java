package com.kakao.oreum.ox.helper;

import org.junit.Test;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class CryptoTest {
    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Crypto.class.newInstance();
    }

    @Test (expected = CryptoException.class)
    public void 잘못된_정보를_처리할_때_exception_발생() throws Exception {
        // given
        byte[] encrypted = new byte[10];
        String key = "abc";

        // when
        Crypto.decrypt(encrypted, key);
    }
}