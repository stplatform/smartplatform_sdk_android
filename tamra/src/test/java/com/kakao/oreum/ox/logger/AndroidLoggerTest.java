package com.kakao.oreum.ox.logger;

import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import java.io.PrintStream;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class AndroidLoggerTest extends LoggerTest {

    @Override
    protected Logger createLogger(Level level, PrintStream outstream) {
        ShadowLog.stream = outstream;
        return new AndroidLogger("TEST", this.getClass(), level);
    }

    @Override
    protected String asString(Level level) {
        switch (level) {
            case TRACE:
                return "V/"; // verbose
            case DEBUG:
                return "D/"; // debug
            case INFO:
                return "I/"; // info
            case WARN:
                return "W/"; // warn
            case ERROR:
                return "E/"; // error
            default:
                return "";

        }
    }
}