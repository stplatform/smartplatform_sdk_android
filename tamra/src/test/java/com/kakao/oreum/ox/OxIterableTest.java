package com.kakao.oreum.ox;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.XPredicates;
import com.kakao.oreum.ox.collection.Lists;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class OxIterableTest extends TestBase {


    @Test
    public void list를_필터링할_수_있다() throws Exception {
        // Given
        List<String> list = Lists.arrayList("hello", "world", "설현짱");

        // When
        List<String> filtered = OxIterable.from(list)
                .filter(XPredicates.isEqual("hello"))
                .toList();

        // Then
        assertThat(filtered, contains("hello"));
        assertThat(filtered, hasSize(1));
    }

    @Test
    public void map_and_reduce를_수행할_수_있다() throws Exception {
        // Given
        List<String> input = Lists.arrayList("hello", "world", "1234");

        // When
        int strLength = OxIterable.from(input)
                .map(new CharCounter())
                .reduce((s, t) -> s + t, 0);

        // Then
        assertThat(strLength, is(14));
    }

    @Test
    public void consumer를_이용해서_forEach를_수행할수있다() throws Exception {
        // Given
        List<String> input = Lists.arrayList("hello", "world", "1234");
        final StringBuilder sb = new StringBuilder();

        // When
        OxIterable.from(input)
                .doEach(s -> sb.append(s).append("."));

        // Then
        assertThat(sb.toString(), is("hello.world.1234."));
    }

    @Test
    public void toString에_대한_join을_수행할_수_있다() throws Exception {
        // Given

        // When
        String joined = OxIterable.from(1, 2, 3, 5)
                .join(",");

        // Then
        assertThat(joined, is("1,2,3,5"));
    }

    @Test
    public void dotziterator객체도_iterable이다() throws Exception {
        // Given

        // When
        int sum = 0;
        for (int n : OxIterable.from(1, 2, 3, 4)) {
            sum += n;
        }

        // Then
        assertThat(sum, is(1 + 2 + 3 + 4));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void toSet은_immutable객체를_리턴한다() throws Exception {
        // Given

        // When
        Set<String> set = OxIterable.from("hello", "world")
                .toSet();

        set.add("other");

        // Then
        fail("should raise exception");
    }

    @Test
    public void 빈_iterable에_대한_toSet은_빈set을_리턴해야한다() throws Exception {
        // Given

        // When
        Set<String> set = OxIterable.<String>empty().toSet();

        // Then
        assertThat(set, notNullValue());
        assertThat(set, empty());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void toList은_immutable객체를_리턴한다() throws Exception {
        // Given

        // When
        List<String> list = OxIterable.from("hello", "world")
                .toList();

        list.add("other");

        // Then
        fail("should raise exception");
    }


    @Test
    public void 빈_iterable에_대한_toList은_빈list을_리턴해야한다() throws Exception {
        // Given

        // When
        List<String> list = OxIterable.<String>empty().toList();

        // Then
        assertThat(list, notNullValue());
        assertThat(list, empty());
    }

    @Test
    public void 첫_elem을_optional로_얻을_수있다() throws Exception {
        // Given

        // When
        Optional<String> str = OxIterable.from("hello", "world")
                .first();

        // Then
        assertThat(str.isPresent(), is(true));
        assertThat(str.get(), is("hello"));
    }

    @Test
    public void 빈_iterable이라면_first메소드는_empty_optional을_리턴한다() throws Exception {
        // Given

        // When
        Optional<String> str = OxIterable.<String>empty()
                .first();

        // Then
        assertThat(str, notNullValue());
        assertThat(str.isPresent(), is(false));
    }

    @Test
    public void 마지막_elem을_optional로_얻을_수있다() throws Exception {
        // Given

        // When
        Optional<String> str = OxIterable.from("hello", "world")
                .last();

        // Then
        assertThat(str.isPresent(), is(true));
        assertThat(str.get(), is("world"));
    }

    @Test
    public void 빈_iterable이라면_last메소드는_empty_optional을_리턴한다() throws Exception {
        // Given

        // When
        Optional<String> str = OxIterable.<String>empty()
                .last();

        // Then
        assertThat(str, notNullValue());
        assertThat(str.isPresent(), is(false));
    }

    @Test
    public void limit로_elm수를_줄일수_있다() throws Exception {
        // Given

        // When
        int sum = OxIterable.from(1, 2, 3, 4, 5, 6, 7)
                .limit(3)
                .reduce((s, t) -> s + t, 0);

        // Then
        assertThat(sum, is(6));
    }

    @Test
    public void 현재_사이즈보다_큰_수로_limit하면_사이즈가_변하지_않는다() throws Exception {
        // Given
        OxIterable<Integer> sut = OxIterable.from(1, 2, 3, 4, 5);
        int orgSize = sut.size();

        // When
        int size = sut.limit(100).size();

        // Then
        assertThat(size, is(orgSize));
    }

    @Test
    public void iterate순서를_정렬할수있다() throws Exception {
        // Given

        // When
        String value = OxIterable.from(8, 1, 5, 2, 4, 3)
                .sort((lhs, rhs) -> lhs - rhs)
                .join(",");

        // Then
        assertThat(value, is("1,2,3,4,5,8"));
    }

    @Test
    public void 특정_elm을_찾을_수_있다() throws Exception {
        // Given
        List<String> list = Lists.arrayList("hello", "world", "jun");

        // When
        Optional<String> found = OxIterable.from(list)
                .find(XPredicates.isEqual("world"));
        Optional<String> notFound = OxIterable.from(list)
                .find(XPredicates.of(XPredicates.isEqual(null)));

        // Then
        assertThat(found.isPresent(), is(true));
        assertThat(notFound.isPresent(), is(false));
    }

    static class CharCounter implements Function<String, Integer> {
        @Override
        public Integer apply(String s) {
            return s.length();
        }
    }

    ;
}