package com.kakao.oreum.ox.helper;

import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import static com.kakao.oreum.ox.format.MessageFormatter.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class MessageFormatterTest extends TestBase {

    @Test
    public void test_case1_of_slf4j() throws Exception {
        assertThat(format("Hi {}.", "there"), is("Hi there."));
    }

    @Test
    public void test_case2_of_slf4j() throws Exception {
        assertThat(format("Set {1,2,3} is not equal to {}.", "1,2"), is("Set {1,2,3} is not equal to 1,2."));
    }

    @Test
    public void test_case3_of_slf4j() throws Exception {
        assertThat(format("Set \\{} is not equal to {}.", "1,2"), is("Set {} is not equal to 1,2."));
    }

    @Test
    public void test_case4_of_slf4j() throws Exception {
        assertThat(format("File name is C:\\\\{}.", "file.zip"), is("File name is C:\\file.zip."));
    }

    @Test
    public void test_one_arg_on_middle() throws Exception {
        assertThat(format("hello {}. what's up", "john"), is("hello john. what's up"));
    }

    @Test
    public void test_one_arg_on_tail() throws Exception {
        assertThat(format("hello {}", "john"), is("hello john"));
    }

    @Test
    public void test_one_arg_on_head() throws Exception {
        assertThat(format("{}, thanx", "john"), is("john, thanx"));
    }

    @Test
    public void test_escape_arg() throws Exception {
        assertThat(format("hello \\{}", "john"), is("hello {}"));
    }

    @Test
    public void test_escape_and_arg() throws Exception {
        assertThat(format("hello \\{}...{}", "john"), is("hello {}...john"));
    }

    @Test
    public void test_null_format() throws Exception {
        assertThat(format(null, "john"), is(nullValue()));
    }

    @Test
    public void test_null_arg() throws Exception {
        assertThat(format("::{}::", (Object) null), is("::null::"));
    }

    @Test
    public void test_arg개수_부족시() throws Exception {
        assertThat(format("1.{}, 2.{}", "john"), is("1.john, 2.{}"));
    }

    @Test
    public void test_다수arg_포맷에_빈_arg을_넘길때() throws Exception {
        assertThat(format("1.{}, 2.{}"), is("1.{}, 2.{}"));
    }

    @Test
    public void test_다수arg_포맷에_null_arg을_넘길때() throws Exception {
        assertThat(format("1.{}, 2.{}", (Object[]) null), is("1.{}, 2.{}"));
    }

    @Test
    public void test_다수arg_포맷에_null_arg_list을_넘길때() throws Exception {
        assertThat(format("1.{}, 2.{}", (Object) null), is("1.null, 2.{}"));
    }

    @Test
    public void test_arg에_포맷문자열이_있는경우() throws Exception {
        assertThat(format("{} -> {}", "john{}", "jane{}"), is("john{} -> jane{}"));
    }

    @Test
    public void escape로_끝나는_경우() throws Exception {
        assertThat(format("{} -> \\", "john", "jane"), is("john -> "));
    }

    @Test
    public void brace로_끝나는_경우() throws Exception {
        assertThat(format("{} -> {", "john", "jane"), is("john -> {"));
    }
}