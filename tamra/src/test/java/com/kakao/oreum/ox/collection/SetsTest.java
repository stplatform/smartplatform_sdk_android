package com.kakao.oreum.ox.collection;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class SetsTest {

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Sets.class.newInstance();
    }

    @Test
    public void testHashSet() throws Exception {
        // given

        // when
        // then
        assertThat(Sets.hashSet(), any(Set.class));
    }

    @Test
    public void testHashSetWithIterable() throws Exception {
        // given
        Set<String> stringSet = new HashSet<>();
        stringSet.add("string1");
        stringSet.add("string2");

        // when
        Set<String> newSet = Sets.hashSet(stringSet);

        // then
        assertThat(newSet, containsInAnyOrder(stringSet.toArray()));
    }

    @Test
    public void testHashSetWithElements() throws Exception {
        // given

        // when
        Set<String> stringSet = Sets.hashSet("string1", "string2");

        // then
        assertThat(stringSet, containsInAnyOrder("string1", "string2"));
    }

    @Test
    public void testLinkedHashSetWithIterable() throws Exception {
        // given
        Set<String> iterable = new HashSet<>();
        iterable.add("string1");
        iterable.add("string2");

        // when
        Set<String> linkedHashSet = Sets.linkedHashSet(iterable);

        // then
        assertThat(linkedHashSet, containsInAnyOrder(iterable.toArray()));
    }

    @Test
    public void testLinkedHashSetWithElements() throws Exception {
        // given

        // when
        Set<String> linkedHashSet = Sets.linkedHashSet("string1", "string2");

        // then
        assertThat(linkedHashSet, containsInAnyOrder("string1", "string2"));
    }

    @Test
    public void testDifference() throws Exception {
        // given
        String value1 = "value1";
        String value2 = "value2";
        String value3 = "value3";
        Set<String> origin = new HashSet<>();
        origin.add(value1);
        origin.add(value2);
        Set<String> target = new HashSet<>();
        target.add(value2);
        target.add(value3);

        // when
        Set<String> diff = Sets.difference(origin, target);

        // then
        assertThat(diff, hasSize(1));
        assertThat(diff, contains(value1));
    }

    @Test
    public void testIntersection() throws Exception {
        // given
        String value1 = "value1";
        String value2 = "value2";
        String value3 = "value3";
        Set<String> origin = new HashSet<>();
        origin.add(value1);
        origin.add(value2);
        Set<String> target = new HashSet<>();
        target.add(value2);
        target.add(value3);

        // when
        Set<String> intersection = Sets.intersection(origin, target);

        // then
        assertThat(intersection, hasSize(1));
        assertThat(intersection, contains(value2));
    }

    @Test
    public void testUnion() throws Exception {
        // given
        String value1 = "value1";
        String value2 = "value2";
        String value3 = "value3";
        Set<String> origin = new HashSet<>();
        origin.add(value1);
        origin.add(value2);
        Set<String> target = new HashSet<>();
        target.add(value2);
        target.add(value3);

        // when
        Set<String> union = Sets.union(origin, target);

        // then
        assertThat(union, containsInAnyOrder(value1, value2, value3));
    }
}