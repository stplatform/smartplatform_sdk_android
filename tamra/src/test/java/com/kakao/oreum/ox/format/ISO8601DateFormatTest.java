package com.kakao.oreum.ox.format;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.testhelper.base.AndroidContextTest;

import org.junit.Test;

import java.text.DateFormat;
import java.util.Date;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class ISO8601DateFormatTest extends AndroidContextTest {
    private static final Logger LOG = getLogger(ISO8601DateFormatTest.class);
    DateFormat sut = new ISO8601DateFormat();

    @Test
    public void test_in_android() throws Exception {
        // android에서 'X' format을 못찾으신다.
        // Given
        Date date = new Date();

        // When
        String formatted = sut.format(date);
        LOG.debug("format :{}", formatted);
        Date parsed = sut.parse(formatted);

        // Then
        assertThat(parsed, is(date));
    }

    @Test
    public void test_another_case() throws Exception {
        // Given
        String dateStr = "2016-04-26T10:52:38.352Z";

        // When
        Date date = sut.parse(dateStr);
        String another = sut.format(date);

        // Then
        LOG.debug("date: {}", another);
        assertThat(date, notNullValue());
        assertThat(another, is("2016-04-26T19:52:38.352+09:00"));
    }

}