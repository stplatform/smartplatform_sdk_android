package com.kakao.oreum.ox.collection;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class ImmutableCollectionTest {
    ImmutableCollection<String> sut;

    @Before
    public void setUp() throws Exception {
        sut = new ImmutableCollection<String>() {
            @Override
            public boolean contains(Object object) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> collection) {
                return false;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public Iterator<String> iterator() {
                return null;
            }

            @Override
            public int size() {
                return 0;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] array) {
                return null;
            }
        };
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testAdd() throws Exception {
        // given
        // when
        sut.add("");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testAddAll() throws Exception {
        // given
        Set<String> toAdd = new HashSet<>();
        toAdd.add("a");
        toAdd.add("b");

        // when
        sut.addAll(toAdd);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testClear() throws Exception {
        sut.clear();
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRemove() throws Exception {
        sut.remove("abc");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRemoveAll() throws Exception {
        // given
        Set<String> toRemove = new HashSet<>();
        toRemove.add("a");
        toRemove.add("b");

        // when
        sut.removeAll(toRemove);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRetainAll() throws Exception {
        // given
        Set<String> toRetain = new HashSet<>();
        toRetain.add("a");
        toRetain.add("b");

        // when
        sut.retainAll(toRetain);
    }
}