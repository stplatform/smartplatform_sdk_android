package com.kakao.oreum.ox.collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class MapsTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Maps.class.newInstance();
    }

    @Test
    public void testMap() throws Exception {
        // given
        Maps.Builder builder = Maps.entry("key1", "value1");

        // when
        Map<String, String> returned = builder.map();

        // then
        assertThat(returned.get("key1"), is("value1"));

        // when
        returned.put("key2", "value2");

        // then
        assertThat(returned.keySet(), contains("key1", "key2"));
    }

    @Test
    public void testImmutableMap() throws Exception {
        // given
        Maps.Builder builder = Maps.linkedHashMap();
        builder.entry("key1", "value1");

        // when
        Map returned = builder.immutableMap();

        // then
        assertThat(returned.get("key1"), is("value1"));

        // given
        expectedException.expect(UnsupportedOperationException.class);

        // when
        returned.remove("key1");
    }
}