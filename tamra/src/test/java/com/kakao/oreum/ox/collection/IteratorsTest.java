package com.kakao.oreum.ox.collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class IteratorsTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Iterators.class.newInstance();
    }

    @Test
    public void test_concat_iterators() throws Exception {
        // Given
        List<String> list1 = Lists.arrayList("hello", "world");
        List<String> list2 = Lists.arrayList("john", "jane");

        // When
        Iterator<String> itr = Iterators.concat(list1.iterator(), list2.iterator());

        // Then
        List<String> rv = toList(itr);

        assertThat(rv, contains("hello", "world", "john", "jane"));
    }

    @Test
    public void test_forarray() throws Exception {
        // given
        String value1 = "James";
        String value2 = "Phillipa";

        // when
        UnmodifiableIterator<String> iterator = Iterators.forArray(value1, value2);

        // then
        assertTrue(iterator.hasNext());
        assertThat(iterator.next(), is(value1));
        assertTrue(iterator.hasNext());
        assertThat(iterator.next(), is(value2));
        assertFalse(iterator.hasNext());
    }

    @Test
    public void test_concat_empty_iterators() throws Exception {
        // Given

        // When
        Iterator<String> itr = Iterators.concat();

        // Then
        List<String> rv = toList(itr);

        assertThat(rv, empty());

        expectedException.expect(NoSuchElementException.class);
        itr.next();
        expectedException.expect(IllegalStateException.class);
        itr.remove();
    }

    @Test
    public void test_filter_iterator() throws Exception {
        // Given
        List<String> list = Lists.arrayList("john", "jane", "joker", "batman");

        // When
        Iterator<String> itr = Iterators.filter(list.iterator(), s-> s.startsWith("j"));

        // Then
        List<String> rv = toList(itr);
        assertThat(rv, not(contains("batman")));
        assertThat(rv, contains("john", "jane", "joker"));
        assertThat(rv, hasSize(3));
    }

    private <T> List<T> toList(Iterator<? extends T> itr) {
        List<T> rv = new ArrayList<>();
        while(itr.hasNext()) {
            rv.add(itr.next());
        }
        return rv;
    }
}