package com.kakao.oreum.ox.task;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class PeriodTest {


    @Test
    public void test_hour() throws Exception {
        // Given

        // When

        // Then
        assertThat(Period.hours(2).toSeconds(), is(2 * 60 * 60L));
        assertThat(Period.hours(2).toMillis(), is(2 * 60 * 60 * 1000L));
    }

    @Test
    public void test_min() throws Exception {
        // Given

        // When

        // Then
        assertThat(Period.minutes(12).toSeconds(), is(12* 60L));
        assertThat(Period.minutes(8).toMillis(), is(8 * 60 * 1000L));
    }

    @Test
    public void test_sec() throws Exception {
        // Given

        // When

        // Then
        assertThat(Period.seconds(31).toSeconds(), is(31L));
        assertThat(Period.seconds(120).toMillis(), is(120 * 1000L));
    }

    @Test
    public void test_millisec() throws Exception {
        // Given

        // When

        // Then
        assertThat(Period.milliseconds(31).toSeconds(), is(0L));
        assertThat(Period.milliseconds(1200).toSeconds(), is(1L));
        assertThat(Period.milliseconds(1200).toMillis(), is(1200L));
    }
}