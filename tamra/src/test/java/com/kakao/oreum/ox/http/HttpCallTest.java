package com.kakao.oreum.ox.http;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.logger.LoggerFactory;
import com.kakao.oreum.testhelper.base.RestApiTest;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class HttpCallTest extends RestApiTest {
    private static final Logger LOG = getLogger(HttpCallTest.class);
    @Before
    public void setUp() throws Exception {
        LoggerFactory.onUnitTest(true);
    }

    @Test
    public void test_happyCase() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("X-Sample", "dummy")
                        .withBody("Success")
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));
        LOG.debug("request:{}", request, new NullPointerException("hello"));

        // When
        Response response = HttpCall.call(request);
        LOG.debug("response:{}", response);

        // Then
        assertThat(response.status(), is(Status.OK));
        assertThat(response.bodyAsString(), is("Success"));
        assertThat(response.header("X-Sample").get(), is("dummy"));
    }

    @Test
    public void 한번_생성한_객체를_동일요청_처리에_재사용할_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("Success")
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));
        HttpCall sut = HttpCall.of(request);

        // When
        Response res1 = sut.call();
        Response res2 = sut.call();
        LOG.debug("response1:{}", res1);
        LOG.debug("response2:{}", res2);

        // Then
        assertThat(res1, not(sameInstance(res2)));
        assertThat(res1.status(), is(Status.OK));
        assertThat(res1.bodyAsString(), is("Success"));
        assertThat(res2.status(), is(Status.OK));
        assertThat(res2.bodyAsString(), is("Success"));
    }

    @Test
    public void 응답코드가_300이라도_예외없이_응답객체를_얻을_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(303)
                        .withHeader("Location", urlFor("/newDummy").toString())
                        .withBody("Moved")
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));

        // When
        Response response = HttpCall.of(request).call();

        // Then
        assertThat(response.status(), is(Status.SeeOther));
        assertThat(response.bodyAsString(), is("Moved"));
    }

    @Test
    public void 응답코드가_옵션을_설정하여_300번대_응답의_redirect를_따라갈_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(303)
                        .withHeader("Location", urlFor("/newDummy").toString())
                        .withBody("Moved")
                ));
        stubFor(get(urlEqualTo("/newDummy"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("Empty")
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));

        // When
        Response response = HttpCall.of(request)
                .withOpts(HttpOpts.create()
                        .followRedirects(true))
                .call();

        // Then
        assertThat(response.status(), is(Status.OK));
        assertThat(response.bodyAsString(), is("Empty"));
    }

    @Test
    public void body메시지가_정상적으로_server로_전송한다() throws Exception {
        // Given
        stubFor(post(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("OK")
                ));

        Request request = Request.create(Method.POST,
                urlFor("/dummy"),
                new ByteArrayInputStream("hello world".getBytes())
        );

        // When
        Response response = HttpCall.of(request).call();

        // Then
        assertThat(response.status(), is(Status.OK));
        verify(postRequestedFor(urlEqualTo("/dummy"))
                .withRequestBody(equalTo("hello world")));
    }

    @Test
    public void 응답코드가_400이라도_예외없이_응답객체를_얻을_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(404)
                        .withBody("ERROR")
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));

        // When
        Response response = HttpCall.call(request);

        // Then
        assertThat(response.status(), is(Status.NotFound));
        assertThat(response.bodyAsString(), is("ERROR"));
    }

    @Test
    public void 응답코드가_500이라도_예외없이_응답객체를_얻을_수_있다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withBody("ERROR")
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));

        // When
        Response response = HttpCall.call(request);

        // Then
        assertThat(response.status(), is(Status.InternalServerError));
        assertThat(response.bodyAsString(), is("ERROR"));
    }

    @Test(expected = HttpCallException.class)
    public void timeout이_발생하면_예외가_발생한다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/dummy"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("OK")
                        .withFixedDelay(500) // 강제 delay
                ));
        Request request = Request.create(Method.GET, urlFor("/dummy"));

        // When
        HttpCall.of(request)
                .withOpts(HttpOpts.create()
                        .readTimeout(10)
                        .connectTimeout(10))
                .call();

        // Then
        fail("should raise exception");
    }

}