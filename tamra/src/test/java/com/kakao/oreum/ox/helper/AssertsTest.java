package com.kakao.oreum.ox.helper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class AssertsTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void 인스턴스_생성_안됨() throws Exception {
        // when
        // then
        expectedException.expect(IllegalAccessException.class);
        Asserts.class.newInstance();
    }

    @Test
    public void testNotNull() throws Exception {
        // given
        String value = "value";

        // when
        // then
        Asserts.notNull(value);

        // given
        String nullValue = null;

        // when
        // then
        expectedException.expect(NullPointerException.class);
        Asserts.notNull(nullValue);
    }

    @Test
    public void testNotNullWithMessage() throws Exception {
        // given
        String message = "Null Pointer Exception";
        String notNullValue = "Not Null";

        // when
        // then
        Asserts.notNull(notNullValue, message);

        // given
        String nullValue = null;

        // when
        // then
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage(message);
        Asserts.notNull(nullValue, message);
    }

    @Test
    public void testShouldBe() throws Exception {
        // given
        boolean legalState = true;

        // when
        // then
        Asserts.shouldBe(legalState);

        // given
        boolean illegalState = false;

        // when
        // then
        expectedException.expect(IllegalStateException.class);
        Asserts.shouldBe(illegalState);
    }

    @Test
    public void testShouldBeWithMessage() throws Exception {
        // given
        String message = "Invalid";
        boolean legalState = true;

        // when
        // then
        Asserts.shouldBe(legalState, message);

        // given
        boolean illegalState = false;

        // when
        // then
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(message);
        Asserts.shouldBe(illegalState, message);
    }

    @Test
    public void testShouldNotBe() throws Exception {
        // given
        boolean legalState = false;

        // when
        // then
        Asserts.shouldNotBe(legalState);

        // given
        boolean illegalState = true;

        // when
        // then
        expectedException.expect(IllegalStateException.class);
        Asserts.shouldNotBe(illegalState);
    }

    @Test
    public void testShouldNotBeWithMessage() throws Exception {
        // given
        String message = "Invalid";
        boolean legalState = false;

        // when
        // then
        Asserts.shouldNotBe(legalState, message);

        // given
        boolean illegalState = true;

        // when
        // then
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(message);
        Asserts.shouldNotBe(illegalState, message);
    }
}