package com.kakao.oreum.ox.error;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Executable;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Supplier;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Null;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class ExceptionFreeTest {
    @Mock
    OxErrorObserver mockObserver;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        OxErrors.removeAllObservers();
        OxErrors.addObserver(mockObserver);
    }

    @Test
    public void test_wrap_executable() throws Exception {
        // Given
        Executable executable = () -> { /* nothing */ };

        // When
        Executable wrapped = ExceptionFree.of(executable);
        wrapped.exec();

        // Then
        verifyZeroInteractions(mockObserver);
    }

    @Test
    public void test_wrap_runtime_exception_executable() throws Exception {
        // Given
        Executable executable = ExceptionFree.of((Executable) () -> {
            throw new RuntimeException();
        });

        // When
        executable.exec();

        // Then
        verify(mockObserver, times(1)).notify(any(RuntimeException.class));
    }

    @Test
    public void test_runnable_throwing_ioexception() throws Exception {
        // Given
        Executable executable = ExceptionFree.executable(() -> {
            throw new IOException();
        });

        // When
        executable.exec();

        // Then
        verify(mockObserver, atLeast(1)).notify(any(IOException.class));
    }

    @Test
    public void test_run_throwing_runnable() throws Exception {
        // Given

        // When
        ExceptionFree.exec(() -> {
            throw new IOException();
        });

        // Then
        verify(mockObserver, atLeast(1)).notify(any(IOException.class));
    }

    @Test
    public void test_run_runnable_with_ex_handler() throws Exception {
        // Given
        Executable executable = mock(Executable.class);

        // When
        ExceptionFree.exec(() -> {
                    throw new IOException();
                }, OxErrors.notifyAndExec(executable)
        );

        // Then
        verify(mockObserver, atLeast(1)).notify(any(IOException.class));
        verify(executable, atLeast(1)).exec();
    }

    @Test
    public void test_function_normal() throws Exception {
        // Given
        Function<String, Integer> len = String::length;

        // When
        int length = ExceptionFree.of(len)
                .apply("hello");

        // Then
        assertThat(length, is(5));
        verifyZeroInteractions(mockObserver);
    }

    @Test
    public void test_function_unchecked() throws Exception {
        // Given
        Function<String, Integer> len = String::length;

        // When
        int length = ExceptionFree.of(len, 0)
                .apply(null);

        // Then
        assertThat(length, is(0));
    }

    @Test
    public void test_function_checked() throws Exception {
        // Given
        // 아래처럼 기본 func에서는 IOException 못던짐.
//        Function<File, String> func = (s) -> {
//            throw new FileNotFoundException();
//        };
        Function<File, String> func = ExceptionFree.<File, String>function(
                (s) -> {
                    throw new FileNotFoundException();
                });

        // When
        String content = func.apply(new File("/"));

        // Then
        assertThat(content, nullValue());
        verify(mockObserver, atLeast(1)).notify(any(FileNotFoundException.class));
    }

    @Test
    public void test_consumer_normal() throws Exception {
        // Given
        Consumer<String> consumer = mock(Consumer.class);

        // When
        ExceptionFree.of(consumer)
                .accept("hello");

        // Then
        verifyZeroInteractions(mockObserver);
        verify(consumer, atLeast(1)).accept(eq("hello"));
    }

    @Test
    public void test_consumer_unchecked() throws Exception {
        // Given
        Consumer<String> consumer = (s) -> {
            if (s == null) throw new NullPointerException();
        };

        // When
        ExceptionFree.of(consumer)
                .accept(null);

        // Then
        verify(mockObserver, atLeast(1)).notify(any(NullPointerException.class));
    }

    @Test
    public void test_consumer_checked() throws Exception {
        // Given
        Consumer<File> consumer = ExceptionFree.consumer((f) -> {
            if (f == null || !f.exists()) {
                throw new FileNotFoundException();
            }
            // do nothing
        });

        // When
        consumer.accept(null);

        // Then
        verify(mockObserver, atLeast(1)).notify(any(FileNotFoundException.class));
    }

    @Test
    public void test_throwing_supplier_without_exception() throws Exception {
        // Given
        Supplier<Optional<String>> supplier = ExceptionFree.supplier(() -> "hello");

        // When
        // Then
        assertThat(supplier.get().isPresent(), is(true));
        assertThat(supplier.get().get(), is("hello"));
    }

    @Test
    public void test_throwing_supplier_with_exception() throws Exception {
        // Given
        Supplier<Optional<String>> supplier = ExceptionFree.supplier(() -> {
            throw new IOException();
        });

        // When
        // Then
        assertThat(supplier.get(), is(Optional.empty()));
        verify(mockObserver, times(1)).notify(any(IOException.class));
    }

    @Test
    public void test_wrap_supplier_without_exception() throws Exception {
        // Given
        Supplier<String> supplier = () -> "hello";
        Supplier<Optional<String>> safeSupplier = ExceptionFree.of(supplier);

        // When
        // Then
        assertThat(safeSupplier.get().get(), is("hello"));
    }

    @Test
    public void test_wrap_supplier_with_exception() throws Exception {
        // Given
        Supplier<String> supplier = () -> {throw new NullPointerException();};
        Supplier<Optional<String>> safeSupplier = ExceptionFree.of(supplier);

        // When
        // Then
        assertThat(safeSupplier.get().isPresent(), is(false));
        verify(mockObserver, times(1)).notify(any(NullPointerException.class));
    }

    @Test
    public void test_get_without_exception() throws Exception {
        // Given
        Optional<String> data = ExceptionFree.get(() -> "hello");

        // When
        // Then
        assertThat(data.isPresent(), is(true));
        assertThat(data.get(), is("hello"));
    }

    @Test
    public void test_get_with_exception() throws Exception {
        // Given
        Optional<String> data = ExceptionFree.get(() -> {
            throw new FileNotFoundException();
        });

        // When
        // Then
        assertThat(data.isPresent(), is(false));
        verify(mockObserver, times(1)).notify(any(FileNotFoundException.class));
    }



}