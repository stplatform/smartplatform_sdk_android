package com.kakao.oreum.ox.collection;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class ImmutableSetTest {
    Set<String> target;

    @Before
    public void setUp() throws Exception {
        target = new HashSet<>();

        target.add("value1");
        target.add("value2");
    }

    @Test
    public void testOfElements() throws Exception {
        // given

        // when
        ImmutableSet immutableSet = ImmutableSet.of("value1", "value2");

        // then
        assertThat(immutableSet.size(), is(2));
    }

    @Test
    public void testOfSet() throws Exception {
        // given

        // when
        ImmutableSet immutableSet = ImmutableSet.of(target);
        target.add("value3");

        // then
        assertThat(immutableSet.size(), is(3));
    }

    @Test
    public void testCopyOf() throws Exception {
        // given

        // when
        ImmutableSet immutableSet = ImmutableSet.copyOf(target);
        target.add("value3");

        // then
        assertThat(immutableSet.size(), is(2));
    }

    @Test
    public void testContains() throws Exception {
        // given
        String value3 = "value3";

        // when
        ImmutableSet immutableSet = ImmutableSet.of(target);
        target.add(value3);

        // then
        assertTrue(immutableSet.contains(value3));
    }

    @Test
    public void testContainsAll() throws Exception {
        // given

        // when
        ImmutableSet immutableSet = ImmutableSet.of(target);

        // then
        assertTrue(immutableSet.containsAll(target));
    }

    @Test
    public void testIsEmpty() throws Exception {
        // given

        // when
        ImmutableSet immutableSet = ImmutableSet.of(target);

        // then
        assertFalse(immutableSet.isEmpty());
    }

    @Test
    public void testToArray() throws Exception {
        // given
        ImmutableSet immutableSet = ImmutableSet.of(target);

        // when
        Object[] arrays = immutableSet.toArray();

        // then
        assertThat(arrays.length, is(2));
    }

    @Test
    public void testToArrayParam() throws Exception {
        // given
        Object[] objects = new Object[target.size()];
        ImmutableSet immutableSet = ImmutableSet.of(target);

        // when
        Object[] arrays = immutableSet.toArray(objects);

        // then
        assertThat(arrays, is(objects));
    }
}