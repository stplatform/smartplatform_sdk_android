package com.kakao.oreum.ox.http;

import com.kakao.oreum.ox.collection.Lists;
import com.kakao.oreum.testhelper.base.TestBase;
import com.kakao.oreum.ox.helper.Streams;

import org.apache.tools.ant.filters.StringInputStream;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class RequestTest extends TestBase {

    private final String urlAsString = "http://localhost:8001";

    @Test(expected = NullPointerException.class)
    public void method없이_request를_생성할수_없다() throws Exception {
        // Given

        // When
        Request sut = Request.create(null, new URL(urlAsString));

        // Then
        fail("should raise exception");
    }

    @Test
    public void 생성하면_host헤더는_자동으로_추가된다_http_v1_1때문임() throws Exception {
        // Given
        Request request = Request.create(Method.GET, urlFor("/info"));

        // When

        // Then
        assertThat(request.headers(), contains(Header.of("Host", "localhost:8001")));
    }

    @Test
    public void port가_명시되지_않은_경우엔_Host헤더에도_포트_없이_생성된다() throws Exception {
        // Given
        Request request = Request.create(Method.GET, new URL("http://www.daum.net"));

        // When

        // Then
        assertThat(request.headers(), contains(Header.of("Host", "www.daum.net")));
    }

    @Test
    public void rawMessage는_HTTP_V1_1_기준의_message를_반환한다() throws Exception {
        // Given
        Request request = Request.create(Method.GET, new URL("http://www.daum.net/"));

        // When
        String rawMessage = request.rawMessage();

        // Then
        assertThat(rawMessage, startsWith("GET / HTTP/1.1"));
        assertThat(rawMessage, containsString("Host: www.daum.net"));
    }

    @Test
    public void toString은_rawMessage를_포함한다() throws Exception {
        // Given
        // Given
        Request request = Request.create(Method.POST,
                urlFor("/dummy"),
                Lists.arrayList(
                        Header.of("Custom-Header", "dummy1"),
                        Header.of("Custom-Header", "dummy2")
                ));

        // When
        String toString = request.toString();

        // Then
        assertThat(toString, containsString(request.rawMessage()));

    }

    @Test(expected = UnsupportedOperationException.class)
    public void headers메소드는_변경불가능한_list를_반환한다() throws Exception {
        // Given
        Request request = Request.create(Method.POST,
                urlFor("/dummy"),
                Lists.arrayList(
                        Header.of("Custom-Header", "dummy1"),
                        Header.of("Custom-Header", "dummy2")
                ));

        // When
        List<Header> headerList = request.headers();
        headerList.clear();
        ;

        // Then
        fail("should raise exception");
    }

    @Test
    public void body를_설정하지_않으면_hasBody는_false이어야한다() throws Exception {
        // Given
        Request request = Request.create(Method.GET, urlFor("/dummy"));

        // When

        // Then
        assertThat(request.hasBody(), is(false));
        assertThat(request.body(), nullValue());
    }

    @Test
    public void body에_대한_inputStream을_세팅하면_hasBody가_true가_된다() throws Exception {
        // Given
        Request request = Request.create(Method.GET, urlFor("/dummy"), new StringInputStream("hello"));

        // When

        // Then
        assertThat(request.hasBody(), is(true));
        assertThat(Streams.readAsString(request.body()), is("hello"));
    }

    private URL urlFor(String file) throws MalformedURLException {
        return new URL(urlAsString + (file.startsWith("/") ? file : "/" + file));
    }

}