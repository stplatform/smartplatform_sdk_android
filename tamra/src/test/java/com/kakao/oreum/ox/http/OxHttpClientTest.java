package com.kakao.oreum.ox.http;

import com.kakao.oreum.testhelper.base.RestApiTest;

import org.junit.Before;
import org.junit.Test;

import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class OxHttpClientTest extends RestApiTest {

    OxHttpClient sut;
    String dynamicHeader;

    @Before
    public void setUp() throws Exception {
        dynamicHeader = "first";
        sut = OxHttpClient.forBaseUrl(new URL(mockServerUrl))
                .defaultHeader("Common-Header", "hello")
                .defaultHeader(() -> Header.of("Dynamic-Header", dynamicHeader));
    }

    @Test
    public void 간단한_request수행_예() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/info"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello world")));
        // When
        Response response = sut.requestFor("/info")
                .request(Method.GET);

        // Then
        verify(getRequestedFor(urlEqualTo("/info")));
        assertThat(response.status(), is(Status.OK));
    }

    @Test
    public void 공통_헤더는_모든_request에_추가된다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/info"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello world")));

        // When
        Response response1 = sut.requestFor("/info")
                .header("Dummy", "dummy1")
                .request(Method.GET);

        Response response2 = sut.requestFor("/info")
                .header("Dummy", "dummy2")
                .request(Method.GET);


        // Then
        verify(getRequestedFor(urlEqualTo("/info"))
                .withHeader("Common-Header", equalTo("hello"))
                .withHeader("Dummy", equalTo("dummy1"))
        );
        verify(getRequestedFor(urlEqualTo("/info"))
                .withHeader("Common-Header", equalTo("hello"))
                .withHeader("Dummy", equalTo("dummy2"))
        );

        assertThat(response1.status(), is(Status.OK));
        assertThat(response2.status(), is(Status.OK));
    }

    @Test
    public void 동적_헤더는_requester생성_시점에_생성된다() throws Exception {
        // Given
        stubFor(get(urlEqualTo("/info"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody("hello world")));

        // When
        Response response1 = sut.requestFor("/info")
                .request(Method.GET);
        dynamicHeader = "second";
        Response response2 = sut.requestFor("/info")
                .request(Method.GET);


        // Then
        verify(getRequestedFor(urlEqualTo("/info"))
                .withHeader("Common-Header", equalTo("hello"))
                .withHeader("Dynamic-Header", equalTo("first"))
        );
        verify(getRequestedFor(urlEqualTo("/info"))
                .withHeader("Common-Header", equalTo("hello"))
                .withHeader("Dynamic-Header", equalTo("second"))
        );

        assertThat(response1.status(), is(Status.OK));
        assertThat(response2.status(), is(Status.OK));
    }
}