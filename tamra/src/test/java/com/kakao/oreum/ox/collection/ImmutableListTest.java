package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.annotation.Immutable;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class ImmutableListTest {
    @Test
    public void testOf() throws Exception {
        // given
        String item = "TEST";
        String item2 = "TEST2";
        String item3 = "TEST3";
        ArrayList<String> origin = new ArrayList<>();
        origin.add(item);
        origin.add(item2);
        origin.add(item3);

        // when
        ImmutableList<String> sut1 = ImmutableList.of(item, item2, item3);
        ImmutableList sut2 = ImmutableList.of(origin);

        // then
        assertThat(sut1.size(), is(origin.size()));
        assertThat(sut1.get(0), is(origin.get(0)));

        assertThat(sut2.size(), is(origin.size()));
        assertThat(sut2.get(0), is(origin.get(0)));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testAdd() {
        // given
        ImmutableList sut = ImmutableList.of(new ArrayList<String>());

        // when
        sut.add(0, "TEST");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testAddAll() {
        // given
        ImmutableList sut = ImmutableList.of(new ArrayList<String>());

        // when
        sut.addAll(0, new ArrayList<String>());
    }

    @Test
    public void testIndexOf() throws Exception {
        // given
        String item = "TEST";
        String item2 = "TEST2";
        ArrayList<String> origin = new ArrayList<>();
        origin.add(item);
        origin.add(item2);

        // when
        ImmutableList sut = ImmutableList.of(origin);

        // then
        assertThat(sut.size(), is(origin.size()));
        assertThat(sut.indexOf(item2), is(origin.indexOf(item2)));
        assertThat(sut.lastIndexOf(item), is(origin.lastIndexOf(item)));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRemove() {
        // given
        ArrayList<String> origin = new ArrayList<>();
        origin.add("TEST");

        ImmutableList sut = ImmutableList.of(origin);

        // when
        sut.remove(0);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testSet() {
        // given
        ArrayList<String> origin = new ArrayList<>();
        String item = "TEST";
        origin.add(item);

        ImmutableList sut = ImmutableList.of(origin);

        // when
        sut.set(0, item);
    }

    @Test
    public void testSubList() throws Exception {
        // given
        String item = "TEST";
        String item2 = "TEST2";
        ArrayList<String> origin = new ArrayList<>();
        origin.add(item);
        origin.add(item2);

        // when
        ImmutableList sut = ImmutableList.of(origin);

        // then
        assertThat(sut.subList(0, 1), is(origin.subList(0, 1)));
    }

    @Test
    public void testContains() throws Exception {
        // given
        String item = "TEST";
        String item2 = "TEST2";
        ArrayList<String> origin = new ArrayList<>();
        origin.add(item);
        origin.add(item2);

        // when
        ImmutableList sut = ImmutableList.of(origin);

        // then
        assertThat(sut.contains(item2), is(origin.contains(item2)));
        assertThat(sut.containsAll(origin), is(true));
    }

    @Test
    public void testToArray() throws Exception {
        // given
        String item = "TEST";
        String item2 = "TEST2";
        ArrayList<String> origin = new ArrayList<>();
        origin.add(item);
        origin.add(item2);

        String[] arrayOrigin = new String[3];
        String[] arraySut = new String[3];

        // when
        ImmutableList sut = ImmutableList.of(origin);

        // then
        assertThat(sut.toArray(), is(origin.toArray()));
        assertThat(sut.toArray(arraySut), is(origin.toArray(arrayOrigin)));
    }
}