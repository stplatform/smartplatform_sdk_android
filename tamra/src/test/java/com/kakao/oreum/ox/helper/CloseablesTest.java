package com.kakao.oreum.ox.helper;

import org.junit.Test;

import java.io.Closeable;
import java.io.IOException;

import static com.github.tomakehurst.wiremock.http.ResponseDefinition.ok;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class CloseablesTest {

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        Closeables.class.newInstance();
    }

    @Test
    public void should_close() throws Exception {
        // Given
        Closeable closeable = mock(Closeable.class);

        // When
        Closeables.closeQuietly(closeable);

        // Then
        verify(closeable, times(1)).close();
    }

    @Test
    public void should_do_nothing_for_null() throws Exception {
        // Given

        // When
        Closeables.closeQuietly(null);

        // Then
        ok();
    }

    @Test
    public void should_ok_when_throwing_exception() throws Exception {
        // Given
        Closeable closeable = mock(Closeable.class);
        doThrow(NullPointerException.class).when(closeable).close();

        // When
        Closeables.closeQuietly(closeable);

        // Then
        ok();
        verify(closeable, times(1)).close();
    }

    @Test
    public void should_ok_when_throwing_ioexception() throws Exception {
        // Given
        Closeable closeable = mock(Closeable.class);
        doThrow(IOException.class).when(closeable).close();

        // When
        Closeables.closeQuietly(closeable);

        // Then
        ok();
        verify(closeable, times(1)).close();
    }
}