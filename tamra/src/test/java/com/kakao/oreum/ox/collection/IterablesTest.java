package com.kakao.oreum.ox.collection;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.common.function.XPredicates;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class IterablesTest extends TestBase {

    private Function<String, Integer> charCounter = new Function<String, Integer>() {
        @Override
        public Integer apply(String s) {
            return s.length();
        }
    };

    @Test
    public void 다수의_iterable을_붙일수_있다() throws Exception {
        // Given
        List<String> list1 = Lists.arrayList("hello", "world");
        List<String> list2 = Lists.linkedList("123", "456", "789");

        // When
        Iterable<String> concated = Iterables.concat(list1, list2);

        // Then
        assertThat(concated, hasItems("hello", "world", "123", "456", "789"));
    }

    @Test
    public void 무한반복_iterable을_만들수있다() throws Exception {
        // Given
        List<String> list1 = Lists.arrayList("1", "2", "3");

        // When
        Iterable<String> cycle = Iterables.cycle(list1);

        // Then
        assertThat(cycle, hasItems("1", "2", "3", "1", "2"));
    }

    @Test
    public void 무한반복_cycle_iterable을_만들수있다() throws Exception {
        // Given

        // When
        Iterable<String> cycle = Iterables.cycle("1", "2");

        // Then
        assertThat(Iterables.get(cycle, 2).orElse(null), is("1"));
        assertThat(Iterables.get(cycle, 200).orElse(null), is("1"));
    }

    @Test
    public void 필터링된_iterable을_생성할수있다() throws Exception {
        // Given

        // When
        Iterable<String> filtered = Iterables.filter(
                Lists.arrayList("1", "2", "3", "4", "5"),
                XPredicates.isEqual("3")
        );

        // Then
        assertThat(filtered, hasItems("3"));
        assertThat(Iterables.size(filtered), is(1));
    }

    @Test
    public void 첫번째_값을_찾을수있다() throws Exception {
        // Given

        // When
        Optional<String> found = Iterables.find(
                Lists.arrayList("hello", "world", "holy"),
                startWith("h")
        );

        // Then
        assertThat(found.isPresent(), is(true));
        assertThat(found.get(), is("hello"));
    }

    @Test
    public void 값을_못찾으면_빈값을_리턴한다() throws Exception {
        // Given

        // When
        Optional<String> found = Iterables.find(
                Lists.arrayList("hello", "world", "holy"),
                startWith("x")
        );

        // Then
        assertThat(found.isPresent(), is(false));
    }

    @Test
    public void 갯수제한을_하는_iterable을_생성할수있다() throws Exception {
        // Given
        Iterable<Integer> iterables = Iterables.cycle(1, 2, 3, 4);

        // When
        Iterable<Integer> limited = Iterables.limit(iterables, 100);

        // Then
        assertThat(Iterables.size(limited), is(100));
    }

    @Test
    public void 개수제한이_실제_크기보다_큰값이면_기존_크기와동일한_iterable이_리턴된다() throws Exception {
        // Given
        List<Integer> list = Lists.arrayList(1, 2, 3, 4, 5);

        // When
        Iterable<Integer> limited = Iterables.limit(list, 100);

        // Then
        assertThat(Iterables.size(limited), is(list.size()));
    }

    @Test
    public void 다른_iterable로_변환할수_있다() throws Exception {
        // Given
        List<String> list = Lists.arrayList("hello", "world");

        // When
        Iterable<Integer> counts = Iterables.map(list, charCounter);

        // Then
        assertThat(counts, hasItems(5, 5));
        assertThat(Iterables.size(counts), is(2));
    }

    private Predicate<String> startWith(final String str) {
        return new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith(str);
            }
        };
    }
}