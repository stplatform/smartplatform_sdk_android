package com.kakao.oreum.ox.collection;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class ImmutableMapTest {
    private Map<String, String> target;

    @Before
    public void setUp() throws Exception {
        target = new HashMap<>();
        target.put("key1", "value1");
        target.put("key2", "value2");
    }

    @Test
    public void testOf() throws Exception {
        // given

        // when
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // then
        assertThat(sut.size(), is(2));

        // when
        target.put("key3", "value3");

        // then
        assertThat(sut.size(), is(3));
    }

    @Test
    public void testCopyOf() throws Exception {
        // given

        // when
        ImmutableMap<String, String> sut = ImmutableMap.copyOf(target);

        // then
        assertThat(sut.size(), is(2));

        // when
        target.put("key3", "value3");

        // then
        assertThat(sut.size(), is(2));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testClear() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        sut.clear();
    }

    @Test
    public void testContainsKey() throws Exception {
        // given
        String key3 = "key3";

        // when
        target.put(key3, "value3");
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // then
        assertTrue(sut.containsKey(key3));
    }

    @Test
    public void testContainsValue() throws Exception {
        // given
        String key3 = "key3";
        String value3 = "value3";

        // when
        target.put(key3, value3);
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // then
        assertTrue(sut.containsValue(value3));
    }

    @Test
    public void testEntrySet() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        Set set = sut.entrySet();

        // then
        assertThat(set.size(), is(2));
    }

    @Test
    public void testIsEmpty() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        // then
        assertFalse(sut.isEmpty());
    }

    @Test
    public void testKeySet() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        Set set = sut.keySet();

        // then
        assertThat(set.size(), is(2));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testPut() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        sut.put("key3", "value3");
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testPutAll() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);
        Map<String, String> toAddMap = new HashMap<>();
        toAddMap.put("key3", "value3");

        // when
        sut.putAll(toAddMap);
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testRemove() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        sut.remove("key1");
    }

    @Test
    public void testValues() throws Exception {
        // given
        ImmutableMap<String, String> sut = ImmutableMap.of(target);

        // when
        // then
        assertThat(sut.values().size(), is(2));
    }
}