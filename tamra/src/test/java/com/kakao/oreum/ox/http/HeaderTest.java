package com.kakao.oreum.ox.http;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class HeaderTest extends TestBase {
    private static final Logger LOG = getLogger(HeaderTest.class);

    @Test(expected = NullPointerException.class)
    public void name없이_header를_생성할_수_없다() throws Exception {
        // Given

        // When
        Header header = Header.of(null, "hello");
        LOG.debug("header:{}", header);

        // Then
        fail("should raise exception");
    }

    @Test
    public void value_없이_header를_생성하면_빈문자열_값을_갖는_header가_생성된다() throws Exception {
        // Given

        // When
        Header header = Header.of("Host", null);
        LOG.debug("header:{}", header);

        // Then
        assertThat(header.name(), is("Host"));
        assertThat(header.value(), is(""));
    }

    @Test
    public void rawMessage는_http_message_포맷의_header_문자열을_리턴한다() throws Exception {
        // Given
        Header header = Header.of("Host", "oreum.kakao.com");

        // When
        String rawMessage = header.rawMessage();

        // Then
        assertThat(rawMessage, is("Host: oreum.kakao.com"));
    }

    @Test
    public void header의_toString은_http_rawMessage포맷과_동일한_문자열을_리턴한다() throws Exception {
        // Given
        Header header = Header.of("Host", "oreum.kakao.com");

        // When

        // Then
        assertThat(header.toString(), is(header.rawMessage()));
    }

    @Test
    public void name과_value가_동일해야_동일한_header로_판단한다() throws Exception {
        // Given
        Header h1 = Header.of("Host", "test.service.com");
        Header h2 = Header.of("Host", "test.service.com");
        Header h3 = Header.of("Host", "dev.service.com");


        // When

        // Then
        assertThat(h1, is(h2));
        assertThat(h2, is(h1));
        assertThat(h1, is(not(h3)));
        assertThat(h2, is(not(h3)));
        assertThat(h1.hashCode(), is(h2.hashCode()));
    }

    @Test
    public void 이름이_동일하면_동일_필드의_header라_본다() throws Exception {
        // Given
        Header h1 = Header.of("field", "value1");
        Header h2 = Header.of("field", "value2");
        Header h3 = Header.of("otehrField", "value3");

        // When

        // Then
        assertThat(h1.sameField(h2), is(true));
        assertThat(h1.sameField(h3), is(false));
    }
}