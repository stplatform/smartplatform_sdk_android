package com.kakao.oreum.ox.logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class LoggerFactoryTest {

    @Before
    public void setUp() throws Exception {
        LoggerFactory.setDefaultLevel();
        LoggerFactory.onUnitTest(false);
    }

    @After
    public void tearDown() throws Exception {
        LoggerFactory.setDefaultLevel();
        LoggerFactory.onUnitTest(false); // 다른 테스트에 영향주지 않기.
    }

    @Test
    public void test_일반사용시_android_logger를_돌려준다() throws Exception {
        // Given
        Logger logger = LoggerFactory.getLogger(getClass());

        // When
        // Then
        assertThat(logger, instanceOf(AndroidLogger.class));
    }

    @Test
    public void testing설정시_console_logger를_돌려준다() throws Exception {
        // Given
        LoggerFactory.onUnitTest(true);
        Logger logger = LoggerFactory.getLogger(getClass());

        // When
        // Then
        assertThat(logger, instanceOf(UnitTestLogger.class));
    }

    @Test
    @Ignore // shell에서 돌릴경우 BuildConfig.DEBUG =true 인가보다.
    public void test_기본_생성시_debug_level로_생성된다() throws Exception {
        // Given
        Logger logger = LoggerFactory.getLogger(getClass());

        // When

        // Then
        assertThat(logger.isLoggable(Level.DEBUG), is(true));
        assertThat(logger.isLoggable(Level.TRACE), is(false));
    }

    @Test
    public void test_global하게_debug_level을_설정할수있다() throws Exception {
        // Given
        LoggerFactory.setLogLevel(Level.INFO);
        Logger logger = LoggerFactory.getLogger(getClass());

        // When

        // Then
        assertThat(logger.isLoggable(Level.DEBUG), is(false));
        assertThat(logger.isLoggable(Level.INFO), is(true));
    }
}