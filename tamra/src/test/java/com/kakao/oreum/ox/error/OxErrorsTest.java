package com.kakao.oreum.ox.error;

import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Executable;
import com.kakao.oreum.common.function.Function;
import com.kakao.oreum.ox.logger.LoggerFactory;
import com.kakao.oreum.tamra.error.TamraException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class OxErrorsTest {
    static {
        LoggerFactory.onUnitTest(true);
    }

    @Before
    public void setUp() throws Exception {
        OxErrors.removeAllObservers();
    }

    @Test
    public void 등록된_observer에게_모든_예외가_전달된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrorObserver observer2 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        OxErrors.addObserver(observer2);

        // When
        OxErrors.notifyAll(new IOException());

        // Then
        InOrder inOrder = inOrder(observer1, observer2);
        inOrder.verify(observer1, times(1)).notify(any(IOException.class));
        inOrder.verify(observer2, times(1)).notify(any(IOException.class));
    }

    @Test
    public void notifier는_등록된_observer에게_모든_예외를_전달된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrorObserver observer2 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        OxErrors.addObserver(observer2);

        // When
        OxErrors.NOTIFIER.notify(new IOException());

        // Then
        InOrder inOrder = inOrder(observer1, observer2);
        inOrder.verify(observer1, times(1)).notify(any(IOException.class));
        inOrder.verify(observer2, times(1)).notify(any(IOException.class));
    }

    @Test
    public void notifier는_notify시점에_등록된_observer에게_모든_예외가_전달된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrorObserver observer2 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        OxErrors.addObserver(observer2);

        // When
        OxErrorObserver notifier = OxErrors.NOTIFIER;
        OxErrors.removeObserver(observer2);
        notifier.notify(new IOException());

        // Then
        verify(observer1, times(1)).notify(any(IOException.class));
        verifyZeroInteractions(observer2);
    }

    @Test
    public void notify_중에_예외가_발생하더라도_무시된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrorObserver observer2 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        OxErrors.addObserver(observer2);
        doThrow(RuntimeException.class).when(observer1).notify(any(Exception.class));

        // When
        OxErrors.notifyAll(new IOException());

        // Then
        InOrder inOrder = inOrder(observer1, observer2);
        inOrder.verify(observer1, times(1)).notify(any(IOException.class));
        inOrder.verify(observer2, times(1)).notify(any(IOException.class));
    }

    @Test
    public void notifyAndExec는_다른_observer에게_예외를_전달한후_executable을_수행한다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrorObserver observer2 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        OxErrors.addObserver(observer2);
        Executable executable = mock(Executable.class);

        // When
        OxErrors.notifyAndExec(executable)
                .notify(new IOException());

        // Then
        InOrder inOrder = inOrder(observer1, observer2, executable);
        inOrder.verify(observer1, times(1)).notify(any(IOException.class));
        inOrder.verify(observer2, times(1)).notify(any(IOException.class));
        inOrder.verify(executable, times(1)).exec();
    }

    @Test
    public void notifyAndExec로_생성한_observer는_OxErrors에_등록이_안된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        Executable executable = mock(Executable.class);

        // When
        OxErrorObserver unregisterable = OxErrors.notifyAndExec(executable);
        OxErrors.addObserver(unregisterable);
        OxErrors.notifyAll(new IOException());

        // Then
        verify(observer1, times(1)).notify(any(IOException.class));
        verifyZeroInteractions(executable);
    }


    @Test
    public void notifyAndExec에서_execute중_발생한_예외도_다른_observer에게_예외가_전달된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrorObserver observer2 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        OxErrors.addObserver(observer2);
        Executable executable = mock(Executable.class);

        // When
        doThrow(RuntimeException.class)
                .when(executable)
                .exec();
        OxErrors.notifyAndExec(executable)
                .notify(new IOException());

        // Then
        InOrder inOrder = inOrder(observer1, observer2, executable);
        inOrder.verify(observer1, times(1)).notify(any(IOException.class));
        inOrder.verify(observer2, times(1)).notify(any(IOException.class));
        inOrder.verify(executable, times(1)).exec();
        inOrder.verify(observer1, times(1)).notify(any(RuntimeException.class));
        inOrder.verify(observer2, times(1)).notify(any(RuntimeException.class));
    }

    @Test
    public void notifyAndHandle은_다른_observer들에게_예외전달_후_handler를_수행한다() throws Exception {
        // Given
        OxErrorObserver observer = mock(OxErrorObserver.class);
        Consumer<Exception> handler = mock(Consumer.class);
        OxErrors.addObserver(observer);

        // When
        OxErrors.notifyAndHandle(handler)
                .notify(new IOException());

        // Then
        InOrder inOrder = inOrder(observer, handler);
        inOrder.verify(observer, times(1)).notify(any(IOException.class));
        inOrder.verify(handler, times(1)).accept(any(IOException.class));
    }

    @Test
    public void notifyAndHandle로_생성한_observer는_OxErrors에_등록이_안된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        Consumer<Exception> handler = mock(Consumer.class);

        // When
        OxErrorObserver unregisterable = OxErrors.notifyAndHandle(handler);
        OxErrors.addObserver(unregisterable);
        OxErrors.notifyAll(new IOException());

        // Then
        verify(observer1, times(1)).notify(any(IOException.class));
        verifyZeroInteractions(handler);
    }

    @Test
    public void notifyAndHandle에서_handler에_의해_발생한_예외도_다른_observer들에게_예외전달된다() throws Exception {
        // Given
        OxErrorObserver observer = mock(OxErrorObserver.class);
        Consumer<Exception> handler = mock(Consumer.class);
        OxErrors.addObserver(observer);

        // When
        doThrow(RuntimeException.class)
                .when(handler)
                .accept(any(IOException.class));
        OxErrors.notifyAndHandle(handler)
                .notify(new IOException());

        // Then
        InOrder inOrder = inOrder(observer, handler);
        inOrder.verify(observer, times(1)).notify(any(IOException.class));
        inOrder.verify(handler, times(1)).accept(any(IOException.class));
        inOrder.verify(observer, times(1)).notify(any(RuntimeException.class));
    }


    @Test
    public void wrapAndNotify는_예외를_wrap해서_전달한다() throws Exception {
        // Given
        OxErrorObserver observer = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer);

        // When
        OxErrors.wrapAndNotify(TamraException::new)
                .notify(new FileNotFoundException());

        // Then
        ArgumentCaptor<Exception> exceptionCaptor = ArgumentCaptor.forClass(Exception.class);
        verify(observer, atLeast(1)).notify(exceptionCaptor.capture());
        assertThat(exceptionCaptor.getValue(), instanceOf(TamraException.class));
        assertThat(exceptionCaptor.getValue(), not(instanceOf(FileNotFoundException.class)));
    }

    @Test
    public void wrapAndNotifier로_생성한_observer는_OxErrors에_등록이_안된다() throws Exception {
        // Given
        OxErrorObserver observer1 = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer1);
        Function<Exception, Exception> mapFunc = mock(Function.class);

        // When
        OxErrorObserver unregisterable = OxErrors.wrapAndNotify(mapFunc);
        OxErrors.addObserver(unregisterable);
        OxErrors.notifyAll(new IOException());

        // Then
        verify(observer1, times(1)).notify(any(IOException.class));
        verifyZeroInteractions(mapFunc);
    }

    @Test
    public void wrapAndNotify시에_func에서_예외가_발생하면_wrap하지_않고_원래_예외를_observer들에게_전달된다() throws Exception {
        // Given
        OxErrorObserver observer = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer);

        // When
        OxErrors.wrapAndNotify(e -> {throw new NullPointerException();})
                .notify(new FileNotFoundException());

        // Then
        verify(observer, atLeastOnce()).notify(any(FileNotFoundException.class));
    }

    @Test
    public void wrapAndNotify시에_func에서_예외가_발생하면_그_예외도_observer들에게_전달된다() throws Exception {
        // Given
        OxErrorObserver observer = mock(OxErrorObserver.class);
        OxErrors.addObserver(observer);

        // When
        OxErrors.wrapAndNotify(e -> {throw new NullPointerException();})
                .notify(new FileNotFoundException());

        // Then
        ArgumentCaptor<Exception> exceptionCaptor = ArgumentCaptor.forClass(Exception.class);
        verify(observer, atLeast(2)).notify(exceptionCaptor.capture());
        // 아래는 순서 중요
        assertThat(exceptionCaptor.getAllValues().get(0), instanceOf(FileNotFoundException.class));
        assertThat(exceptionCaptor.getAllValues().get(1), instanceOf(NullPointerException.class));
    }
}