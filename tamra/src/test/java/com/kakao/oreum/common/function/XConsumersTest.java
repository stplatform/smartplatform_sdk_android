package com.kakao.oreum.common.function;

import org.junit.Test;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XConsumersTest {

    @Test
    public void test_create() throws Exception {
        // Given
        Consumer<String> consumer = mock(Consumer.class);

        // When
        XConsumer<String> sut = XConsumers.of(consumer);
        sut.accept("hello");

        // Then
        verify(consumer, times(1)).accept(eq("hello"));
    }

}