package com.kakao.oreum.common;

import com.kakao.oreum.common.function.Consumer;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class OptionalTest {

    @Test(expected = IllegalArgumentException.class)
    public void null에_대해서_of를_호출하면_예외발생() throws Exception {
        // Given

        // When
        Optional.of(null);

        // Then
        fail("should raise exception");
    }

    @Test
    public void null일지도_모르는_상황에서는_ofNullable로_생성() throws Exception {
        // Given
        String arg = null;

        // When
        Optional<String> opt = Optional.ofNullable(arg);

        // Then
        assertThat(opt.isPresent(), is(false));
    }

    @Test
    public void test_empty는_빈_객체를_생성() throws Exception {
        // Given

        // When
        Optional opt = Optional.empty();

        // Then
        assertThat(opt.isPresent(), is(false));
    }

    @Test(expected = NoSuchElementException.class)
    public void empty에_대해_get을_하면_예외_발생() throws Exception {
        // Given

        // When
        Optional<String> opt = Optional.empty();
        String x = opt.get();

        // Then
        fail("should raise exception");
    }

    @Test
    public void test_filter() throws Exception {
        // Given
        Optional<String> o1 = Optional.of("hello");
        Optional<String> o2 = Optional.empty();

        // When
        // Then
        assertThat(o1.filter(s -> s.startsWith("h")).isPresent(), is(true));
        assertThat(o1.filter(s -> s.startsWith("x")).isPresent(), is(false));
        assertThat(o2.filter(s -> true).isPresent(), is(false));
    }

    @Test
    public void test_flatMap() throws Exception {
        // Given
        Optional<String> o1 = Optional.of("hello");
        Optional<String> o2 = Optional.empty();

        // When

        // Then
        assertThat(o1.flatMap(s -> Optional.of(s.length())).get(), is(5));
        assertThat(o1.flatMap(s -> null).isPresent(), is(false));
        assertThat(o2.flatMap(s -> Optional.of(0)).isPresent(), is(false));
    }

    @Test
    public void test_map() throws Exception {
        // Given
        Optional<String> o1 = Optional.of("hello");
        Optional<String> o2 = Optional.empty();

        // When
        // Then
        assertThat(o1.map(String::length).get(), is(5));
        assertThat(o2.map(String::length).isPresent(), is(false));
    }

    @Test
    public void test_orElse() throws Exception {
        // Given

        // When
        // Then
        assertThat(Optional.of("hello").orElse("world"), is("hello"));
        assertThat(Optional.empty().orElse("world"), is("world"));
    }

    @Test
    public void test_orElseGet() throws Exception {
        // Given

        // When

        // Then
        assertThat(Optional.of("hello").orElseGet(() -> "world"), is("hello"));
        assertThat(Optional.empty().orElseGet(() -> "world"), is("world"));
    }

    @Test(expected = NullPointerException.class)
    public void test_orElseThrow() throws Exception {
        // Given

        // When
        Optional.of("hello").orElseThrow(AssertionError::new); // not throw
        Optional.empty().orElseThrow(NullPointerException::new);
        // Then
        fail("should raise exception");
    }

    @Test
    public void test_ifPresent() throws Exception {
        // Given
        Consumer<String> consumer1 = mock(Consumer.class);
        Consumer<String> consumer2 = mock(Consumer.class);

        // When
        Optional.of("hello").ifPresent(consumer1);
        Optional.<String>empty().ifPresent(consumer2);
        // Then
        verify(consumer1, times(1)).accept("hello");
        verifyZeroInteractions(consumer2);
    }

    @Test
    public void test_hashCode() throws Exception {
        // Given
        // When
        // Then
        assertThat(Optional.empty().hashCode(), is(0));
        assertThat(Optional.of("hello").hashCode(), is("hello".hashCode()));
        assertThat(Optional.of(12.3).hashCode(), is(Double.valueOf(12.3).hashCode()));
    }

    @Test
    public void test_equals() throws Exception {
        // Given
        Optional<String> opt = Optional.of("hello");
        // When

        // Then
        assertThat(opt, is("hello"));
        assertThat(opt, is(opt));
        assertThat(opt, is(Optional.of("hello")));
        assertThat(Optional.empty(), is(Optional.ofNullable(null)));
        assertThat(Optional.ofNullable(null), is(Optional.empty()));
    }
}