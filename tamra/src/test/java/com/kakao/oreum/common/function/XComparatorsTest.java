package com.kakao.oreum.common.function;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XComparatorsTest {

    @Test(expected = NullPointerException.class)
    public void test_comparing_with_null() throws Exception {
        // Given

        // When
        XComparators.comparing(null);

        // Then
        fail("should raise exception");
    }

    @Test(expected = NullPointerException.class)
    public void test_comparing_with_null_null() throws Exception {
        // Given

        // When
        XComparators.comparing(null, null);

        // Then
        fail("should raise exception");
    }
}