package com.kakao.oreum.common.function;

import com.kakao.oreum.ox.collection.Lists;

import org.junit.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XComparatorTest {


    @Test
    public void wrap해도_기능은_동일하다() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        List<String> data = Lists.arrayList("01", "2", "003");

        // When
        Collections.sort(data, XComparators.of(len));

        // Then
        assertThat(data, contains("2", "01", "003"));
    }

    @Test
    public void test_naturalOrder() throws Exception {
        // Given
        List<String> data = Lists.arrayList("01", "2", "003");

        // When
        Collections.sort(data, XComparators.naturalOrder());

        // Then
        assertThat(data, contains("003", "01", "2"));
    }

    @Test
    public void test_comparing_with_keyExtractor() throws Exception {
        // Given
        Function<String, Character> lastCh = (s) -> s.charAt(s.length() - 1);
        List<String> data = Lists.arrayList("01", "2", "003");

        // When
        Collections.sort(data, XComparators.comparing(lastCh));

        // Then
        assertThat(data, contains("01", "2", "003"));
    }

    @Test
    public void test_comparing_with_keyExtractor_and_keyComparator() throws Exception {
        // Given
        Function<String, String> chomp = (s) -> s.replaceAll("\\s*$", "");
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        List<String> data = Lists.arrayList("111", "22  ", "3       ");

        // When
        Collections.sort(data, XComparators.comparing(chomp, len));

        // Then
        assertThat(data, contains("3       ", "22  ", "111"));
    }

    @Test
    public void test_nullsFirst() throws Exception {
        // Given
        List<String> data = Lists.arrayList("c", null, "a", null, null, "b");

        // When
        Collections.sort(data, XComparators.nullsFirst(XComparators.naturalOrder()));

        // Then
        assertThat(data, contains(null, null, null, "a", "b", "c"));
    }

    @Test
    public void test_nullsFirst_each() throws Exception {
        // Given
        Comparator<String> sut = XComparators.nullsFirst(XComparators.naturalOrder());

        // When

        // Then
        assertThat(sut.compare(null, null), is(0));
        assertThat(sut.compare(null, "a") < 0, is(true));
        assertThat(sut.compare("a", null) > 0, is(true));
    }

    @Test
    public void test_reversed() throws Exception {
        // Given
        XComparator<String> comp = XComparators.naturalOrder();
        List<String> data = Lists.arrayList("a", "c", "d", "b");

        // When
        Collections.sort(data, comp.reversed());

        // Then
        assertThat(data, contains("d", "c", "b", "a"));
    }

    @Test
    public void test_nullsLast() throws Exception {
        // Given
        List<String> data = Lists.arrayList("c", null, "a", null, null, "b");

        // When
        Collections.sort(data, XComparators.nullsLast(XComparators.naturalOrder()));

        // Then
        assertThat(data, contains("a", "b", "c", null, null, null));
    }

    @Test
    public void test_thenComparing_with_other() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        Comparator<String> natural = String::compareTo;
        List<String> data = Lists.arrayList("abc", "c", "d", "xb", "cd");

        // When
        Collections.sort(data, XComparators.of(len).thenComparing(natural));

        // Then
        assertThat(data, contains("c", "d", "cd", "xb", "abc"));
    }

    @Test
    public void test_thenComparing_with_keyExtractor() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        Function<String, Character> lastCh = (s) -> s.charAt(s.length() - 1);
        List<String> data = Lists.arrayList("xc", "a", "aac", "xya", "b", "da");

        // When
        Collections.sort(data, XComparators.of(len).thenComparing(lastCh));

        // Then
        assertThat(data, contains("a", "b", "da", "xc", "xya", "aac"));
    }

    @Test
    public void test_thenComparing_with_keyExtractor_keyComparator() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        Function<String, Character> lastCh = (s) -> s.charAt(s.length() - 1);
        List<String> data = Lists.arrayList("xc", "a", "aac", "xya", "b", "da");

        // When
        XComparator<Character> comp = XComparators.<Character>naturalOrder().reversed();
        Collections.sort(data, XComparators.of(len)
                .thenComparing(lastCh, comp));

        // Then
        assertThat(data, contains("b", "a", "xc", "da", "aac", "xya"));
    }

    @Test(expected = NullPointerException.class)
    public void test_thenComparing_with_null_comparator() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        // When
        XComparators.of(len)
                .thenComparing((Comparator<String>)null);

        // Then
        fail("should raise NPE");
    }

    @Test(expected = NullPointerException.class)
    public void test_thenComparing_with_null_function() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        // When
        XComparators.of(len)
                .thenComparing((Function<String, String>)null);

        // Then
        fail("should raise NPE");
    }


    @Test(expected = NullPointerException.class)
    public void test_thenComparing_with_null_null() throws Exception {
        // Given
        Comparator<String> len = (lhs, rhs) -> lhs.length() - rhs.length();
        // When
        XComparators.of(len)
                .thenComparing(null, null);

        // Then
        fail("should raise NPE");
    }

    @Test
    public void test_reverse_of_reversed_는_원래꺼와_동일() throws Exception {
        // Given
        Comparator<String> comp = String.CASE_INSENSITIVE_ORDER;
        List<String> data = Lists.arrayList("X", "c", "a", "B", "x");

        // When
        Collections.sort(data, XComparators.reverseOrder(comp)
                .reversed());

        // Then
        assertThat(data, contains("a", "B", "c", "X", "x"));
    }

}