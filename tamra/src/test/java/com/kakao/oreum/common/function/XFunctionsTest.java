package com.kakao.oreum.common.function;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XFunctionsTest {

    @Test
    public void identity_function은_input과_동일한_output을_리턴한다() throws Exception {
        // Given
        XFunction<String, String> sut = XFunctions.identity();

        // When
        // Then
        assertThat(sut.apply("hello"), is("hello"));
        assertThat(sut.apply(null), nullValue());
    }

    @Test
    public void 타입_무관하게_쓸수있다() throws Exception {
        // Given
        XFunction sut = XFunctions.identity();

        // When

        // Then
        assertThat(sut.apply("hello"), is("hello"));
        assertThat(sut.apply(null), nullValue());
        assertThat(sut.apply(1), is(1));
        assertThat(sut.apply(String.class), is((Object) String.class));
    }

    @Test
    public void function조합의_시작점으로_쓴다() throws Exception {
        // Given
        Function<Class, Integer> sut = XFunctions
                .<Class, String>
                        of(c -> c.getSimpleName())
                .andThen(s -> s.length());

        // When
        // Then
        assertThat(sut.apply(Function.class), is(8));
    }
}