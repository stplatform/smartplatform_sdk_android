package com.kakao.oreum.common.function;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XConsumerTest {

    @Test
    public void test_andThen() throws Exception {
        // Given
        Consumer<String> mock1 = mock(Consumer.class);
        Consumer<String> mock2 = mock(Consumer.class);
        Consumer<String> mock3 = mock(Consumer.class);

        // When
        XConsumers.of(mock1)
                .andThen(mock2)
                .andThen(mock3)
                .accept("hello");

        // Then
        InOrder inOrder = Mockito.inOrder(mock1, mock2, mock3);

        inOrder.verify(mock1, times(1)).accept(eq("hello"));
        inOrder.verify(mock2, times(1)).accept(eq("hello"));
        inOrder.verify(mock3, times(1)).accept(eq("hello"));
    }

}