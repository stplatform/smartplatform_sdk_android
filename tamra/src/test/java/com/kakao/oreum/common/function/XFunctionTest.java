package com.kakao.oreum.common.function;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XFunctionTest {

    @Test
    public void test_andThen() throws Exception {
        // Given
        Function<String, String> f1 = s -> s.substring(1); // 첫 문자는 버림
        Function<String, Integer> f2 = s -> s.length(); // 길이

        XFunction<String, String> sut = XFunctions.of(f1);

        // When
        XFunction<String, Integer> andThen = sut.andThen(f2);

        // Then
        assertThat(andThen.apply("hello"), is(4));
    }

    @Test(expected = NullPointerException.class)
    public void test_andThen_NPE_case() throws Exception {
        // Given
        Function<String, String> f1 = s -> s.substring(1); // 첫 문자는 버림

        XFunction<String, String> sut = XFunctions.of(f1);

        // When
        sut.andThen(null);

        // Then
        fail("should raise NPE");
    }


    @Test
    public void test_compose() throws Exception {
        // Given
        Function<String, Integer> strlen = s -> s.length(); // 길이
        Function<Class, String> className = c -> c.getSimpleName();

        XFunction<String, Integer> sut = XFunctions.of(strlen);

        // When
        XFunction<Class, Integer> andThen = sut.compose(className);

        // Then
        assertThat(andThen.apply(String.class), is(6));
    }

    @Test(expected = NullPointerException.class)
    public void test_compose_NPE_case() throws Exception {
        // Given
        Function<String, String> f1 = s -> s.substring(1); // 첫 문자는 버림

        XFunction<String, String> sut = XFunctions.of(f1);

        // When
        sut.compose(null);

        // Then
        fail("should raise NPE");
    }
}