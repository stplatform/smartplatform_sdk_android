package com.kakao.oreum.common.function;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XPredicateTest {

    @Test
    public void test_logical_and() throws Exception {
        // Given
        XPredicate<String> init = XPredicates.of(s -> s.startsWith("H"));

        // When
        Predicate<String> sut = init.and(s -> s.endsWith("O"));

        // Then
        assertTrue(sut.test("HELLO"));
        assertTrue(sut.test("HO"));
        assertFalse(sut.test("hO"));
        assertFalse(sut.test("Hello"));
    }

    @Test
    public void test_logical_or() throws Exception {
        // Given
        XPredicate<String> init = XPredicates.of(s -> s.length() == 5);

        // When
        XPredicate<String> sut = init.or(s -> false);

        // Then
        assertTrue(init.test("hello"));
        assertTrue(init.test("world"));
    }

    @Test
    public void test_negate() throws Exception {
        // Given
        XPredicate<String> eq = XPredicates.isEqual("hello");

        // When
        XPredicate<String> sut = eq.negate();

        // Then
        assertTrue(eq.test("hello"));
        assertFalse(sut.test("hello"));
    }
}