package com.kakao.oreum.common.function;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class XPredicatesTest {

    @Test
    public void test_isEqual() throws Exception {
        // Given
        XPredicate<String> sut = XPredicates.isEqual("hello");

        // When
        // Then
        assertTrue(sut.test("hello"));
        assertFalse(sut.test("halo"));
        assertFalse(sut.test(null));

        // given
        sut = XPredicates.isEqual(null);

        // when
        // then
        assertTrue(sut.test(null));
        assertFalse(sut.test(""));

    }

    @Test
    public void test_isEqual_for_othertype() throws Exception {
        // Given
        XPredicate<String> sut = XPredicates.isEqual('c');

        // When
        // Then
        assertFalse(sut.test("c"));
    }


    @Test
    public void test_create_xpredicate() throws Exception {
        // Given
        Predicate<String> predicate = s -> s.length() == 3;

        // When
        XPredicate<String> sut = XPredicates.of(predicate);

        // Then
        assertTrue(sut.test("123"));
        assertTrue(sut.test("sky"));
        assertFalse(sut.test("hello"));
    }

    @Test
    public void test_create_and_chain() throws Exception {
        // Given
        Predicate<String> sut = XPredicates.<String>of(s -> s.length() == 3)
                .and(s -> s.startsWith("s"));

        // When
        // Then
        assertTrue(sut.test("sky"));
        assertFalse(sut.test("should"));
    }
}