package com.kakao.oreum.tamra.props;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.logger.LoggerFactory;
import com.kakao.oreum.ox.http.OxRequest;
import com.kakao.oreum.ox.http.Method;
import com.kakao.oreum.ox.http.Response;
import com.kakao.oreum.ox.http.Status;
import com.kakao.oreum.tamra.base.Profile;
import com.kakao.oreum.tamra.internal.props.PropName;
import com.kakao.oreum.tamra.internal.props.PropValue;
import com.kakao.oreum.tamra.internal.props.TamraProperties;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URL;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * 기능 테스트용 아님. 설정 점검용.
 *
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class TamraPropertiesTest extends TestBase {
    private static final Logger LOG = getLogger(TamraPropertiesTest.class);

    @Before
    public void setUp() throws Exception {
        LoggerFactory.onUnitTest(true);
    }

    @Test
    public void production환경에서_api_url을_얻어서_사용할_수_있다() throws Exception {
        // Given
        TamraProperties sut = TamraProperties.load(Profile.PRODUCTION);

        // When
        PropValue apiUrl = sut.get(PropName.API_BASEURL);

        // Then
        LOG.debug("api : {}", apiUrl);
        assertThat(apiUrl.isPresent(), is(true));
        Response response = OxRequest.forURL(new URL(apiUrl.get() + "/_hcheck.hdn"))
                .request(Method.GET);
        assertThat(response.status(), is(Status.OK));
//        HttpResponse response = HTTP.GET(new URL(apiUrl.get() + "/_hcheck.hdn")).send();
//        assertThat(response.status(), is(Status.OK));
    }


    @Test
    public void test환경에서_api_url을_얻어서_사용할_수_있다() throws Exception {
        // Given
        TamraProperties sut = TamraProperties.load(Profile.TEST);

        // When
        PropValue apiUrl = sut.get(PropName.API_BASEURL);

        // Then
        LOG.debug("api : {}", apiUrl);
        assertThat(apiUrl.isPresent(), is(true));
        Response response = OxRequest.forURL(new URL(apiUrl.get() + "/_hcheck.hdn"))
                .request(Method.GET);
        assertThat(response.status(), is(Status.OK));
    }

    @Test
    public void dev환경에서_api_url을_얻어서_사용할_수_있다() throws Exception {
        // Given
        TamraProperties sut = TamraProperties.load(Profile.DEVELOP);

        // When
        PropValue apiUrl = sut.get(PropName.API_BASEURL);

        // Then
        LOG.debug("api : {}", apiUrl);
        assertThat(apiUrl.isPresent(), is(true));
    }

    @Ignore // 이유는 모르지만 ci 서버에서 깨진다. 잠시 막아두자
    @Test
    public void local환경에서_api_url을_얻어서_사용할_수_있다() throws Exception {
        // Given
        TamraProperties sut = TamraProperties.load(Profile.LOCAL);

        // When
        PropValue apiUrl = sut.get(PropName.API_BASEURL);

        // Then
        LOG.debug("api : {}", apiUrl);
        assertThat(apiUrl.isPresent(), is(true));
    }

}