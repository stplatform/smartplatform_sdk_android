package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.logger.LoggerFactory;
import com.kakao.oreum.tamra.base.Profile;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.internal.data.data.RegionData;
import com.kakao.oreum.tamra.internal.http.HttpClientFactory;
import com.kakao.oreum.testhelper.base.AndroidContextTest;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class RegionTransientRepositoryTest extends AndroidContextTest {
    private static final Logger LOG = getLogger(RegionTransientRepositoryTest.class);

    RegionTransientRepository sut;


    @Before
    public void setUp() throws Exception {
        LoggerFactory.onUnitTest(true);
        OxHttpClient httpClient = HttpClientFactory.create(getTamraConfig(Profile.TEST));
        sut = new RegionTransientRepository(httpClient);
    }

    @Test
    public void 모든_region을_조회할수있다() throws Exception {
        // Given

        // When
        Collection<RegionData> regions = sut.all();
        LOG.debug("data:{}", regions);

        // Then
        assertThat(regions, not(empty()));
    }

    @Test
    public void region으로_region데이터를_조회할_수_있다() throws Exception {
        // Given
        Region region = Regions.제주국제공항;

        // When
        Optional<RegionData> data = sut.find(region);
        LOG.debug("data:{}", data);
        // Then
        assertThat(data.isPresent(), is(true));
        assertThat(data.map(RegionData::id), is(region.id()));
    }

    @Test
    public void region_id로_region_데이터를_조회할_수_있다() throws Exception {
        // Given
        long regionId = 2;
        // When
        Optional<RegionData> data = sut.find(regionId);
        LOG.debug("data:{}", data);

        // Then
        assertThat(data.isPresent(), is(true));
        assertThat(data.map(RegionData::id), is(regionId));
        assertThat(data.map(RegionData::region), is(Region.of(2)));
    }

    @Test
    public void 신규지역확인용_테스트() throws Exception {
        Region[] regionsToTest = new Region[]{
                Regions.제주43길,
                Regions.플레이케이팝_박물관
        };
        for (Region region : regionsToTest) {
            Optional<RegionData> data = sut.find(region);

            LOG.debug("region: {} {}", region, data);
            assertThat(data.isPresent(), is(true));
            assertThat(data.get().id(), is(region.id()));
            assertThat(data.get().name(), is(not("N/A")));
        }
    }


}