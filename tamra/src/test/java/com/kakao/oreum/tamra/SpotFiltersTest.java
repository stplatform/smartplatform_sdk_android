/*
package com.kakao.oreum.tamra;

import com.kakao.oreum.common.function.XPredicate;
import com.kakao.oreum.ox.collection.Lists;
import com.kakao.oreum.tamra.base.Proximity;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.SpotFilters;
import com.kakao.oreum.testhelper.Mocks;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0


public class SpotFiltersTest extends TestBase {

    @Test @Ignore
    public void test_idfilter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().id(1L).build();
        Spot spot2 = Mocks.spot().id(2L).build();
        Spot spot3 = Mocks.spot().id(3L).build();

        XPredicate<Spot> filter = SpotFilters.oneOf(1L, 2L);

        // When
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(false));

        // Then
    }

    @Test @Ignore
    public void test_oneof() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().id(1L).build();
        Spot spot2 = Mocks.spot().id(2L).build();
        Spot spot3 = Mocks.spot().id(3L).build();

        XPredicate<Spot> filter = SpotFilters.oneOf(Lists.arrayList(spot1, spot2));

        // When
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(false));

        // Then
    }

    @Test @Ignore
    public void test_region_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().region(Regions.제주국제공항).build();
        Spot spot2 = Mocks.spot().region(Regions.제주창조경제혁신센터).build();

        XPredicate<Spot> filter = SpotFilters.belongsTo(Regions.제주국제공항);

        // When
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(false));
        // Then
    }

    @Test @Ignore
    public void test_proximity_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().proximity(Proximity.Immediate).build();
        Spot spot2 = Mocks.spot().proximity(Proximity.Near).build();
        Spot spot3 = Mocks.spot().proximity(Proximity.Far).build();

        XPredicate<Spot> filter = SpotFilters.proximity(Proximity.Near);

        // When
        // Then
        assertThat(filter.test(spot1), is(false));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(false));
    }

    @Test @Ignore
    public void test_min_proximity_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().proximity(Proximity.Immediate).build();
        Spot spot2 = Mocks.spot().proximity(Proximity.Near).build();
        Spot spot3 = Mocks.spot().proximity(Proximity.Far).build();

        XPredicate<Spot> filter = SpotFilters.minProximity(Proximity.Near);

        // When
        // Then
        assertThat(filter.test(spot1), is(false));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(true));
    }

    @Test @Ignore
    public void test_max_proximity_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().proximity(Proximity.Immediate).build();
        Spot spot2 = Mocks.spot().proximity(Proximity.Near).build();
        Spot spot3 = Mocks.spot().proximity(Proximity.Far).build();

        XPredicate<Spot> filter = SpotFilters.maxProximity(Proximity.Near);

        // When
        // Then
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(false));
    }

    @Test @Ignore
    public void test_min_accuracy_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().accuracy(1.0).build();
        Spot spot2 = Mocks.spot().accuracy(2.0).build();
        Spot spot3 = Mocks.spot().accuracy(3.0).build();

        XPredicate<Spot> filter = SpotFilters.minAccuracy(2.0);

        // When
        // Then
        assertThat(filter.test(spot1), is(false));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(true));
    }

    @Test @Ignore
    public void test_max_accuracy_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().accuracy(1.0).build();
        Spot spot2 = Mocks.spot().accuracy(2.0).build();
        Spot spot3 = Mocks.spot().accuracy(3.0).build();

        XPredicate<Spot> filter = SpotFilters.maxAccuracy(2.0);

        // When
        // Then
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(true));
        assertThat(filter.test(spot3), is(false));
    }

    @Test
    public void test_movable_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().movable(true).build();
        Spot spot2 = Mocks.spot().movable(false).build();

        XPredicate<Spot> filter = SpotFilters.movable(true);

        // When
        // Then
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(false));
    }

    @Test
    public void test_indoor_filter() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().indoor(true).build();
        Spot spot2 = Mocks.spot().indoor(false).build();

        XPredicate<Spot> filter = SpotFilters.indoor(true);

        // When
        // Then
        assertThat(filter.test(spot1), is(true));
        assertThat(filter.test(spot2), is(false));
    }


    @Test
    public void test_composite_case1() throws Exception {
        // Given
        Spot spot = Mocks.spot()
                .region(Regions.제주창조경제혁신센터)
                .proximity(Proximity.Immediate)
                .indoor(true)
                .build();

        XPredicate<Spot> filter1 = SpotFilters.belongsTo(Regions.제주국제공항)
                .or(SpotFilters.belongsTo(Regions.제주창조경제혁신센터));

        XPredicate<Spot> filter2 = SpotFilters.maxProximity(Proximity.Near);

        XPredicate<Spot> filter3 = SpotFilters.indoor(false);

        XPredicate<Spot> filter4 = filter1.and(filter2).and(filter3);

        XPredicate<Spot> filter5 = filter3.or(filter2).or(filter1);

        XPredicate<Spot> filter6 = filter4.negate();

        // When
        // Then
        assertThat(filter1.test(spot), is(true));
        assertThat(filter2.test(spot), is(true));
        assertThat(filter3.test(spot), is(false));
        assertThat(filter4.test(spot), is(false));
        assertThat(filter5.test(spot), is(true));
        assertThat(filter6.test(spot), is(true));
    }

    @Test
    public void test_notNull_filter() throws Exception {
        // Given
        Spot spot1 = null;
        Spot spot2 = Mocks.spot().build();

        // When
        XPredicate<Spot> sut = SpotFilters.notNull();

        // Then
        assertThat(sut.test(spot1), is(false));
        assertThat(sut.test(spot2), is(true));
    }


}*/
