package com.kakao.oreum.tamra.internal.data;

import android.content.Context;

import com.kakao.oreum.ox.http.OxHttpClient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class RegionRepositoryFactoryTest {

    Context context;
    OxHttpClient httpClient;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        httpClient = OxHttpClient.forBaseUrl(new URL("http://test"));
    }

    @Test
    public void testCreateTransient() throws Exception {
        // given
        // when
        RegionRepository regionRepository = RegionRepositoryFactory.create(context, httpClient, false);

        // then
        assertTrue(regionRepository instanceof RegionTransientRepository);
    }

    @Test
    public void testCreatePersistent() throws Exception {
        // given
        // when
        RegionRepository regionRepository = RegionRepositoryFactory.create(context, httpClient, true);

        // then
        assertTrue(regionRepository instanceof RegionPersistentRepository);
    }
}