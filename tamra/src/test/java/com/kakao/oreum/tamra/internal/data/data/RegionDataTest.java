package com.kakao.oreum.tamra.internal.data.data;

import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class RegionDataTest {

    @Test
    public void testBuild() throws Exception {
        // given
        long regionId = 1L;
        UUID uuid = UUID.randomUUID();
        String name = "Region 1";
        double longitude = 0.0;
        double latitude = 0.0;
        int radius = 100;
        String geohash = "abc";
        String description = "Region Description";
        String regionType = "Normal";

        // when
        RegionData regionData = RegionData.builder()
                .id(regionId)
                .uuid(uuid)
                .name(name)
                .longitude(longitude)
                .latitude(latitude)
                .radius(radius)
                .geohash(geohash)
                .description(description)
                .regionType(regionType)
                .build();

        // then
        assertThat(regionData.id(), is(regionId));
        assertThat(regionData.uuid(), is(uuid));
        assertThat(regionData.name(), is(name));
        assertThat(regionData.longitude(), is(longitude));
        assertThat(regionData.latitude(), is(latitude));
        assertThat(regionData.radius(), is(radius));
        assertThat(regionData.geohash(), is(geohash));
        assertThat(regionData.description(), is(description));
        assertThat(regionData.regionType(), is(regionType));
    }

    @Test
    public void testEquals() throws Exception {
        // given
        long regionId = 1L;

        // when
        RegionData regionData1 = RegionData.builder()
                .id(regionId)
                .uuid(UUID.randomUUID())
                .name("Region 1")
                .longitude(0.0)
                .latitude(0.0)
                .radius(100)
                .geohash("")
                .description("")
                .regionType("Normal")
                .build();

        RegionData regionData2 = RegionData.builder()
                .id(regionId)
                .uuid(UUID.randomUUID())
                .name("Region 2")
                .longitude(0.0)
                .latitude(0.0)
                .radius(10)
                .geohash("")
                .description("")
                .regionType("Normal")
                .build();

        // then
        assertThat(regionData1, is(regionData1));
        assertThat(regionData2, is(regionData1));
        assertThat(null, not(regionData1));
        assertThat("", not(regionData1));
    }
}