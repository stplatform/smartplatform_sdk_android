package com.kakao.oreum.tamra.internal.error;

import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.tamra.error.RecentTamraErrors;
import com.kakao.oreum.tamra.error.TamraErrorFilters;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class TamraErrorHolderTest {
    TamraErrorHolder sut;

    @Before
    public void setUp() throws Exception {
        sut = new TamraErrorHolder();
    }

    @Test
    public void 발생한_에러들을_제공한다() throws Exception {
        // Given

        // When
        sut.addError(new NullPointerException());


        // Then
        RecentTamraErrors errors = sut.recentErrors();
        assertThat(errors.retainCount(), is(1));
        assertThat(errors.filter(TamraErrorFilters.causedBy(NullPointerException.class)).retainCount(), is(1));
    }

    @Test
    public void oxErrors와의_연동을_위해_errorObserver를_제공한다() throws Exception {
        // Given
        OxErrors.addObserver(sut.errorObserver());

        // When
        ExceptionFree.exec(() -> {
            throw new NullPointerException();
        });

        // Then
        assertThat(sut.recentErrors().cumulativeCount(), greaterThanOrEqualTo(1));
    }

    @Test
    public void test_MAX_COUNT_이상의_오류가_발생하면_옛날건_지운다() throws Exception {
        // Given
        int maxRetainCount = RecentTamraErrors.MAX_RETAIN_COUNT;
        int occurring = maxRetainCount + 10;

        // When
        for(int i=0; i < occurring; i++) {
            sut.addError(new NullPointerException());
        }

        // Then
        assertThat(sut.recentErrors().retainCount(), is(maxRetainCount));
        assertThat(sut.recentErrors().cumulativeCount(), is(occurring));
    }
}