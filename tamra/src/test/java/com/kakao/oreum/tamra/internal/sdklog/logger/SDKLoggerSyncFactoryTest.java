package com.kakao.oreum.tamra.internal.sdklog.logger;

import android.content.Context;

import com.kakao.oreum.ox.http.OxHttpClient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class SDKLoggerSyncFactoryTest {
    Context context;

    com.kakao.oreum.tamra.base.Config config;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        config = com.kakao.oreum.tamra.base.Config.createFor(context, null, null);
    }

    @Test
    public void testGetSDKLogger() throws Exception {
        // when
        SDKLogger logger1 = SDKLoggerSyncFactory.getLogger(config);
        SDKLogger logger2 = SDKLoggerSyncFactory.getLogger(config);

        // then
        assertThat(logger1, is(logger2));
    }

    @Test
    public void testGetSDKLogSync() throws Exception {
        // when
        SDKLogSync logSync = SDKLoggerSyncFactory.getSDKLogSync(context,
                OxHttpClient.forBaseUrl(new URL("http://oreum-dev-api.krane.iwilab.com")), 1, false);


        // then
        assertNotNull(logSync);
    }
}