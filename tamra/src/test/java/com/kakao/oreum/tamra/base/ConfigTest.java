package com.kakao.oreum.tamra.base;

import android.content.Context;
import android.os.Build;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.9.0
 */
@RunWith(RobolectricTestRunner.class)
public class ConfigTest {
    Context context;
    String appKey = "APPKEY";
    String appId;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        appId = context.getPackageName();
    }

    @Test
    public void testForRelease() throws Exception {
        // when
        Config config = Config.forRelease(context, appKey);

        // then
        assertThat(config.context(), is(context));
        assertThat(config.appkey(), is(appKey));
        assertThat(config.profile(), is(Profile.PRODUCTION));
        assertThat(config.appId(), is(appId));
        assertThat(config.simulation(), is(false));
    }

    @Test
    public void testForTesting() throws Exception {
        // when
        Config config = Config.forTesting(context, appKey);

        // then
        assertThat(config.context(), is(context));
        assertThat(config.appkey(), is(appKey));
        assertThat(config.profile(), is(Profile.TEST));
        assertThat(config.appId(), is(appId));
        assertThat(config.simulation(), is(false));
    }

    @Test
    public void testCreateFor() throws Exception {
        // given
        Profile profile = Profile.DEVELOP;

        // when
        Config config = Config.createFor(context, appKey, profile);

        // then
        assertThat(config.context(), is(context));
        assertThat(config.appkey(), is(appKey));
        assertThat(config.profile(), is(profile));
        assertThat(config.appId(), is(appId));
        assertThat(config.simulation(), is(false));
    }

    @Test
    public void testSimulation() throws Exception {
        // given
        String deviceName = "deviceName";

        // when
        Config config = Config.forRelease(context, appKey).onSimulation(deviceName);

        // then
        assertThat(config.context(), is(context));
        assertThat(config.appkey(), is(appKey));
        assertThat(config.profile(), is(Profile.PRODUCTION));
        assertThat(config.appId(), is(appId));
        assertThat(config.simulation(), is(true));
        assertThat(config.deviceName(), is(deviceName));
    }

    @Test
    public void testSimulationWithoutDeviceName() {
        // when
        Config config = Config.forTesting(context, appKey).onSimulation();

        // then
        assertThat(config.deviceName(), is(Build.MODEL));

    }
}