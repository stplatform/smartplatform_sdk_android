package com.kakao.oreum.tamra.base;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.7.0
 */
public class RegionTest extends TestBase {
    private static final Logger LOG = getLogger(RegionTest.class);

    @Test
    public void test_equality() throws Exception {

        assertTrue(Region.of(2).equals(Regions.제주국제공항));
        assertTrue(Regions.제주국제공항.equals(Region.of(2)));
    }

    @Test
    public void test_all_regions() throws Exception {
        // Given

        // When

        // Then
        for (Region region : Regions.all()) {
            LOG.debug("region:{}", region);
            assertThat(region, not(Region.UNKNOWN));
        }
    }


}