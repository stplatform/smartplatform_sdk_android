package com.kakao.oreum.tamra.internal.http.common;

import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.http.Header;
import com.kakao.oreum.testhelper.base.AndroidContextTest;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class CommonHeadersTest extends AndroidContextTest {

    @Test
    public void user_agent헤더는_필수() throws Exception {
        // Given
        List<Header> headers = CommonHeaders.get(getContext());

        // When
        // Then
        assertTrue(OxIterable.from(headers)
                .any(h -> h.name().equals("User-Agent")));
    }

    @Test
    public void oreum_cid헤더는_필수() throws Exception {
        // Given
        List<Header> headers = CommonHeaders.get(getContext());

        // When
        // Then
        assertTrue(OxIterable.from(headers)
                .any(h -> h.name().equals("Oreum-CID")));
    }


    @Test
    public void content_type헤더는_json() throws Exception {
        // Given
        List<Header> headers = CommonHeaders.get(getContext());

        // When
        // Then
        assertTrue(OxIterable.from(headers)
                .any(h -> h.equals(Header.of("Content-Type", "application/json; charset=UTF-8"))));
    }
}