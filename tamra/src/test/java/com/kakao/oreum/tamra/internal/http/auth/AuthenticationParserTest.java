package com.kakao.oreum.tamra.internal.http.auth;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.logger.LoggerFactory;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static com.kakao.oreum.testhelper.json.JsonBuilder.iso8601Time;
import static com.kakao.oreum.testhelper.json.JsonBuilder.key;
import static com.kakao.oreum.testhelper.json.JsonBuilder.obj;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class AuthenticationParserTest {
    private static final Logger LOG = getLogger(AuthenticationParserTest.class);

    @Mock
    TimeService timeService;
    AuthenticationParser sut;

    @Before
    public void setUp() throws Exception {
        LoggerFactory.onUnitTest(true);
        MockitoAnnotations.initMocks(this);
        sut = new AuthenticationParser(() -> timeService);
        when(timeService.now()).thenReturn(new Date(100)); // 항상 100.
    }

    @Test
    public void 파싱이_실패하면_empty를_리턴한다() throws Exception {
        // Given
        String json = "{}";

        // When
        Optional<Authentication> auth = sut.parse(json);

        // Then
        assertThat(auth.isPresent(), is(false));
    }

    @Test
    public void 파싱에_성공하면_auth를_가진놈을_리턴한다() throws Exception {
        // Given
        String json = obj(
                key("token").val("dummy-token"),
                key("expireAt").val(iso8601Time(new Date()))
        ).toJson();

        LOG.debug("json:{}", json);

        // When
        Optional<Authentication> auth = sut.parse(json);

        // Then
        assertThat(auth.isPresent(), is(true));
        assertThat(auth.get().authToken(), is("dummy-token"));
        assertThat(auth.get().isValid(), is(true));
    }
}