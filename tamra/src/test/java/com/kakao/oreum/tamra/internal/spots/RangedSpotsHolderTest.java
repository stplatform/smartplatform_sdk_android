package com.kakao.oreum.tamra.internal.spots;

import com.kakao.oreum.tamra.base.NearbySpots;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class RangedSpotsHolderTest {

    @Test
    public void nearbySpots는_range_되기전이라도_null을_리턴하지_않는다() throws Exception {
        // Given
        RangedSpotsHolder sut = new RangedSpotsHolder();

        // When
        // Then
        assertThat(sut.nearbySpots(), notNullValue());
        assertThat(sut.nearbySpots().isEmpty(), is(true));
    }

    @Test
    public void 마지막_range된_nearbySpot을_리턴한다() throws Exception {
        // Given
        RangedSpotsHolder sut = new RangedSpotsHolder();
        NearbySpots spots1 = mock(NearbySpots.class);
        NearbySpots spots2 = mock(NearbySpots.class);

        // When
        sut.ranged(spots1);
        sut.ranged(spots2);

        // Then
        assertThat(sut.nearbySpots(), not(spots1));
        assertThat(sut.nearbySpots(), is(spots2));
    }

    @Test
    public void null값이_range되었더라도_이녀석은_null을_리턴하진않는다() throws Exception {
        // Given
        RangedSpotsHolder sut = new RangedSpotsHolder();

        // When
        sut.ranged(null);

        // Then
        assertThat(sut.nearbySpots(), notNullValue());
        assertThat(sut.nearbySpots().isEmpty(), is(true));
    }
}