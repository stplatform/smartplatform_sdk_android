package com.kakao.oreum.tamra.internal.props;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class PropValueTest {

    @Test
    public void testOr() throws Exception {
        // given
        String value = "PROP";

        // when
        PropValue notNullValue = PropValue.of(value);
        PropValue nullValue = PropValue.of(null);

        // then
        assertThat(notNullValue.or(""), is(value));
        assertThat(nullValue.or(""), is(""));
    }

    @Test
    public void testAsIntOr() throws Exception {
        // given
        String intValue = "123";
        String nonIntValue = "asdf";

        // when
        PropValue intValueProp = PropValue.of(intValue);
        PropValue nonIntValueProp = PropValue.of(nonIntValue);

        // then
        assertThat(intValueProp.asIntOr(0), is(123));
        assertThat(nonIntValueProp.asIntOr(0), is(0));
    }

    @Test
    public void testEquals() throws Exception {
        // given
        String value = "PROP";
        String intValue = "123";

        // when
        PropValue stringProp = PropValue.of(value);
        PropValue intProp = PropValue.of(intValue);
        PropValue intPropClone = PropValue.of(intValue);
        PropValue nullProp = PropValue.of(null);

        // then
        assertThat(stringProp, is(value));
        assertThat(stringProp, is(stringProp));
        assertThat(intProp, is(intPropClone));
        assertThat(intProp, not(123));
        assertThat(nullProp, is(nullProp));
    }
}