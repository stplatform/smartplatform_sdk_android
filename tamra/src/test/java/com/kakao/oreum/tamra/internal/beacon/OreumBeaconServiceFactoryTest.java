package com.kakao.oreum.tamra.internal.beacon;

import android.content.Context;

import com.kakao.oreum.tamra.internal.sdklog.logger.SDKLoggerSyncFactory;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.tamra.base.ObserverSensitivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.URL;

import static org.junit.Assert.assertNotNull;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class OreumBeaconServiceFactoryTest {
    Context context;
    OxHttpClient httpClient;
    com.kakao.oreum.tamra.base.Config config;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        config = com.kakao.oreum.tamra.base.Config.createFor(context, "", null);
        httpClient = OxHttpClient.forBaseUrl(new URL("http://oreum-dev-api.krane.iwilab.com"));
    }

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        OreumBeaconServiceFactory.class.newInstance();
    }

    @Test
    public void testCreate() throws Exception {
        // given
        // when
        AbstractBeaconService beaconService =
                OreumBeaconServiceFactory.create(config, httpClient, SDKLoggerSyncFactory.getDummySDKLogger());

        // then
        assertNotNull(beaconService);
    }

    @Test
    public void testCreateForSimulation() throws Exception {
        // given
        // when
        OreumBeaconService beaconService = OreumBeaconServiceFactory.createForSimulation(context, httpClient, "MY DEVICE");

        // then
        assertNotNull(beaconService);
    }
}