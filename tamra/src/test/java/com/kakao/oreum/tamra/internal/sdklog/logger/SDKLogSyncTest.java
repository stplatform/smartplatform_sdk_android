package com.kakao.oreum.tamra.internal.sdklog.logger;

import android.app.Application;

import com.kakao.oreum.infra.device.NetworkChecker;
import com.kakao.oreum.infra.persistence.sqlite.SDKLogDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.SyncDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.tamra.internal.sdklog.SDKLog;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;
import com.kakao.oreum.ox.http.OxHttpClient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class SDKLogSyncTest {
    private SyncDBRepository syncDBRepository;
    private TamraDBHelper dbHelper;
    private SystemTimeService timeService;
    private DateFormat dateFormat = new ISO8601DateFormat();
    private NetworkChecker networkChecker;

    @Before
    public void setUp() throws Exception {
        Application context = RuntimeEnvironment.application;
        dbHelper = new TamraDBHelper(context);
        syncDBRepository = new SyncDBRepository(dbHelper, dateFormat);
        timeService = new SystemTimeService();
        networkChecker = new NetworkChecker(context);
    }

    @Test
    public void testSync() throws Exception {
        // given
        Date current = timeService.now();

        // when
        Thread.sleep(100);
        new SDKLogSync(
                syncDBRepository,
                new SDKLogDBRepository(dbHelper, dateFormat),
                OxHttpClient.forBaseUrl(new URL("http://oreum-dev-api.krane.iwilab.com")),
                timeService,
                dateFormat,
                networkChecker,
                1,
                false
        );

        Thread.sleep(100);
        Date last = syncDBRepository.getLastSyncDateTimeForSDKLog();

        // then
        assertThat(last.getTime(), greaterThan(current.getTime()));
    }

    @Test
    public void testRequestSync() throws Exception {
        // given
        List<SDKLog> sdkLogs = generateSDKLogList();
        Date current = timeService.now();

        // when
        Thread.sleep(100);
        SDKLogSync sut = new SDKLogSync(
                syncDBRepository,
                new SDKLogDBRepository(dbHelper, dateFormat),
                OxHttpClient.forBaseUrl(new URL("http://oreum-dev-api.krane.iwilab.com")),
                timeService,
                dateFormat,
                networkChecker,
                1,
                false
        );

        Thread.sleep(100);
        sut.requestSync(sdkLogs, current); // 서버 전송 실패
        Date last = syncDBRepository.getLastSyncDateTimeForSDKLog();


        // then
        assertThat(last.getTime(), greaterThan(current.getTime()));
    }

    private List<SDKLog> generateSDKLogList() {
        List<SDKLog> sdkLogs = new ArrayList<>();
        sdkLogs.add(new SDKLog("testAction", "testParam", timeService.now()));
        return sdkLogs;
    }
}