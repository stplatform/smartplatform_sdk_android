package com.kakao.oreum.tamra.internal.data.data;

import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.logger.LoggerFactory;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.testhelper.Mocks;

import org.junit.Test;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class RegionUtilTest {
    static {
        LoggerFactory.onUnitTest(true);
    }
    private static final Logger LOG = getLogger(RegionUtilTest.class);

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        RegionUtil.class.newInstance();
    }

    @Test
    public void test_region_data에서_beacon_region을_생성할_수_있다() throws Exception {
        // Given
        RegionData regionData = Mocks.regionData().build();
        LOG.debug("regionData:{}", regionData);

        // When
        BeaconRegion beaconRegion = RegionUtil.toBeaconRegion(regionData);

        // Then
        LOG.debug("beaconRegion:{}", beaconRegion);

        assertThat(beaconRegion.proximityUUID(), is(regionData.uuid()));
    }

    @Test
    public void 생성된_beacon_region을_다시_region으로_환원할_수_있다() throws Exception {
        // Given
        RegionData regionData = Mocks.regionData().build();
        LOG.debug("regionData:{}", regionData);
        // When
        BeaconRegion beaconRegion = RegionUtil.toBeaconRegion(regionData);
        Region region = RegionUtil.toRegion(beaconRegion);

        // Then
        LOG.debug("beaconRegion:{}", beaconRegion);
        LOG.debug("region:{}", region);

        assertThat(region, is(regionData.region()));
    }


}