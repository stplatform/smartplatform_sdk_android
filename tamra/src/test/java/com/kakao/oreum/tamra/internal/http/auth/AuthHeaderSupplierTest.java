package com.kakao.oreum.tamra.internal.http.auth;

import com.kakao.oreum.ox.http.Header;
import com.kakao.oreum.tamra.internal.http.common.CommonHeaders;
import com.kakao.oreum.testhelper.base.RestApiTest;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.kakao.oreum.testhelper.json.JsonBuilder.key;
import static com.kakao.oreum.testhelper.json.JsonBuilder.obj;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class AuthHeaderSupplierTest extends RestApiTest {

    AuthHeaderSupplier sut;

    @Before
    public void setUp() throws Exception {
        sut = new AuthHeaderSupplier(
                urlFor("/auth/authenticate"),
                AppID,
                AppKey,
                new ArrayList<>()
        );
    }

    @Test
    public void test_success() throws Exception {
        // Given
        stubFor(post(urlEqualTo("/auth/authenticate"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withBody(obj(
                                key("token").val("dummy-token"),
                                key("expireAt").val("2100-12-31T01:00:00.000+09:00")
                        ).toJson())
                )
        );

        // When
        Header authHeader = sut.get();

        // Then
        assertThat(authHeader.name(), is("Oreum-Token"));
        assertThat(authHeader.value(), is("dummy-token"));
    }


    @Test
    public void test_failed() throws Exception {
        // Given
        stubFor(post(urlEqualTo("/auth/authenticate"))
                .willReturn(aResponse()
                        .withStatus(400)
                        .withBody(obj().toJson())
                )
        );

        // When
        Header authHeader = sut.get();

        // Then
        assertThat(authHeader.name(), is("Oreum-Token"));
        assertThat(authHeader.value(), is("N/A"));
    }

}