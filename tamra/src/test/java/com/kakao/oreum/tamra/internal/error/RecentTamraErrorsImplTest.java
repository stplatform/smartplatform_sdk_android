package com.kakao.oreum.tamra.internal.error;

import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.common.function.Predicate;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.tamra.error.RecentTamraErrors;
import com.kakao.oreum.tamra.error.TamraError;
import com.kakao.oreum.tamra.error.TamraErrorFilters;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class RecentTamraErrorsImplTest {

    @Test
    public void test_filter() throws Exception {
        // Given
        RecentTamraErrors sut = createFor(
                error(new NullPointerException()),
                error(new FileNotFoundException())
        );

        // When
        Predicate<TamraError> filter = TamraErrorFilters.causedBy(IOException.class);
        RecentTamraErrors errors = sut.filter(filter);

        // Then
        assertThat(errors.retainCount(), is(1));
    }

    @Test
    public void test_sorting() throws Exception {
        // Given
        RecentTamraErrors sut = createFor(
                new TamraError(new NullPointerException(), new Date(200)),
                new TamraError(new FileNotFoundException(), new Date(100)),
                new TamraError(new MalformedURLException(), new Date(300))
        );

        // When
        RecentTamraErrors errors = sut.orderBy(TamraError.RECENTLY_ORDER);

        // Then
        assertThat(errors.retainCount(), is(3));
        assertThat(errors.mostRecent().get().causedBy(), instanceOf(MalformedURLException.class));
    }

    @Test
    public void test_retainCounter는_현재_보관하는_오류개수를_리턴한다() throws Exception {
        // Given
        RecentTamraErrors sut = createFor(
                new TamraError(new NullPointerException(), new Date(200)),
                new TamraError(new FileNotFoundException(), new Date(100)),
                new TamraError(new MalformedURLException(), new Date(300))
        );
        RecentTamraErrors sut2 = createFor();

        // When
        // Then
        assertThat(sut.retainCount(), is(3));
        assertThat(sut.isEmpty(), is(false));
        assertThat(sut2.retainCount(), is(0));
        assertThat(sut2.isEmpty(), is(true));
    }

    @Test
    public void test_limit() throws Exception {
        // Given
        TamraError e1 = new TamraError(new NullPointerException(), new Date(100));
        TamraError e2 = new TamraError(new NullPointerException(), new Date(200));
        TamraError e3 = new TamraError(new NullPointerException(), new Date(300));
        RecentTamraErrors sut = createFor(e1, e2, e3);

        // When
        RecentTamraErrors limited = sut.orderBy(TamraError.RECENTLY_ORDER)
                .limit(1);

        // Then
        assertThat(limited.isEmpty(), is(false));
        assertThat(limited.retainCount(), is(1));
        assertThat(limited.mostRecent().get().occurredAt(), is(new Date(300)));
    }

    @Test
    public void test_foreach() throws Exception {
        // Given
        Consumer<TamraError> consumer = mock(Consumer.class);
        TamraError e1 = error(new NullPointerException());
        TamraError e2 = error(new FileNotFoundException());
        RecentTamraErrors sut = createFor(e1, e2);

        // When
        sut.foreach(consumer);

        // Then
        verify(consumer, times(sut.retainCount())).accept(any(TamraError.class));
    }

    @Test
    public void test_누적오류개수는_유지된다() throws Exception {
        // Given
        RecentTamraErrors sut = RecentTamraErrorsImpl.of(
                OxIterable.from(
                        error(new NullPointerException()),
                        error(new FileNotFoundException())),
                22
        );
        assertThat(sut.cumulativeCount(), is(22));

        // When
        RecentTamraErrors gen = sut.limit(1);

        // Then
        assertThat(gen.cumulativeCount(), is(22));
    }

    @Test
    public void test_for_loop에_직접_사용할_수_있다() throws Exception {
        // Given
        RecentTamraErrors sut = createFor(
                new TamraError(new NullPointerException(), new Date(200)),
                new TamraError(new FileNotFoundException(), new Date(100)),
                new TamraError(new MalformedURLException(), new Date(300))
        );

        // When
        // Then
        int count = 0;
        for (TamraError e : sut) {
            assertThat(e, notNullValue());
            count++;
        }
        assertThat(count, is(3));
    }

    @Test
    public void test_toList() throws Exception {
        // Given
        TamraError e1 = error(new NullPointerException());
        TamraError e2 = error(new FileNotFoundException());
        RecentTamraErrors sut = createFor(e1, e2);

        // When
        List<TamraError> list = sut.toList();

        // Then
        assertThat(list, hasSize(2));
        assertThat(list, contains(e1, e2));
    }

    private RecentTamraErrors createFor(TamraError... errors) {
        return RecentTamraErrorsImpl.of(OxIterable.from(errors), errors.length);
    }

    public TamraError error(Exception exception) {
        return new TamraError(exception, new Date());
    }

}