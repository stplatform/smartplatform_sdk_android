package com.kakao.oreum.tamra.internal.history;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class HistoryServiceFactoryTest {
    Context context;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
    }

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        HistoryServiceFactory.class.newInstance();
    }

    @Test
    public void create() throws Exception {
        // when
        HistoryService historyService = HistoryServiceFactory.create(context);

        // then
        assertNotNull(historyService);
    }

}