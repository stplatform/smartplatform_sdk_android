package com.kakao.oreum.tamra.internal.sdklog.logger;

import com.kakao.oreum.infra.persistence.sqlite.SDKLogDBRepository;
import com.kakao.oreum.infra.persistence.sqlite.TamraDBHelper;
import com.kakao.oreum.tamra.base.SDKLogParams;
import com.kakao.oreum.tamra.internal.sdklog.SDKLog;
import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.ox.format.ISO8601DateFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class PersistentSDKLoggerTest {
    SDKLogDBRepository repository;
    SDKLogger sut;
    private SystemTimeService timeService;

    @Before
    public void setUp() throws Exception {
        repository = new SDKLogDBRepository(new TamraDBHelper(RuntimeEnvironment.application), new ISO8601DateFormat());
        timeService = new SystemTimeService();
        sut = new PersistentSDKLogger(repository, timeService);
    }

    @Test
    public void testLog() throws Exception {
        // when
        sut.log("testAction", SDKLogParams.of("testKey", "testParam"));
        List<SDKLog> logList = repository.find(timeService.now());

        // then
        assertThat(logList, hasSize(1));
    }
}