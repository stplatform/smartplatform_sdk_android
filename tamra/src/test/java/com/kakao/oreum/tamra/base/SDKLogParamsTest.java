package com.kakao.oreum.tamra.base;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class SDKLogParamsTest {
    @Test
    public void of() throws Exception {
        // given
        String key = "key";
        String value = "value";

        // when
        SDKLogParams sut = SDKLogParams.of(key, value);

        // then
        assertThat(sut.params().size(), is(1));

        // when
        SDKLogParams sut2 = SDKLogParams.of(sut.params());

        // then
        assertThat(sut2.params().iterator().next(), is(sut.params().iterator().next()));
    }

    @Test
    public void addKeyValue() throws Exception {
        // given
        String key = "key";
        String value = "value";

        // when
        SDKLogParams sut = SDKLogParams.emptyParams();
        sut.add(key, value);

        // then
        assertThat(sut.params().size(), is(1));
    }

    @Test
    public void addSet() throws Exception {
        // given
        SDKLogParams target = SDKLogParams.of("key", "value");

        // when
        SDKLogParams sut = SDKLogParams.emptyParams();
        sut.add(target.params());

        // then
        assertThat(sut.params().iterator().next(), is(target.params().iterator().next()));
    }

    @Test
    public void addDuplicate() throws Exception {
        // given
        String key = "key";

        // when
        SDKLogParams sut = SDKLogParams.of(key, "origin");
        sut.add(key, "target");

        // then
        assertThat(sut.params().size(), is(1));
    }

    @Test
    public void string() throws Exception {
        // given
        String key = "key";

        // when
        SDKLogParams sut = SDKLogParams.of(key , "value");

        // then
        assertTrue(sut.toString().contains(key));
    }
}