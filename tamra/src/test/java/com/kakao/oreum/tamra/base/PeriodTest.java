package com.kakao.oreum.tamra.base;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.11.0
 */
public class PeriodTest {

    @Test
    public void testLastSeconds() throws Exception {
        // given
        int amount = 1;

        // when
        Period sut = Period.lastSeconds(amount);

        // then
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sut.from());
        calendar.add(Calendar.SECOND, amount);
        assertEquals(sut.to(), calendar.getTime());
    }

    @Test
    public void testLastMinutes() throws Exception {
        // given
        int amount = 1;

        // when
        Period sut = Period.lastMinutes(amount);

        // then
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sut.from());
        calendar.add(Calendar.MINUTE, amount);
        assertEquals(sut.to(), calendar.getTime());
    }

    @Test
    public void testLastHours() throws Exception {
        // given
        int amount = 1;

        // when
        Period sut = Period.lastHours(amount);

        // then
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sut.from());
        calendar.add(Calendar.HOUR, amount);
        assertEquals(sut.to(), calendar.getTime());
    }
}