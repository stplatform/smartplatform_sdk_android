package com.kakao.oreum.tamra.base;

import com.kakao.oreum.ox.collection.OxIterable;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class RegionsTest {

    @Test (expected = IllegalAccessException.class)
    public void 인스턴스_생성_안됨() throws Exception {
        // when
        Regions.class.newInstance();
    }

    @Test
    public void testAll() throws Exception {
        // given
        Region[] regions = Regions.all();

        // when
        boolean exist = OxIterable.from(regions).any(region -> region == Regions.동문재래시장);

        // then
        assertTrue(exist);
    }
}