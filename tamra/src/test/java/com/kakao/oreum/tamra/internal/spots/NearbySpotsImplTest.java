package com.kakao.oreum.tamra.internal.spots;

import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Proximity;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.tamra.base.SpotFilters;
import com.kakao.oreum.tamra.internal.spots.NearbySpotsImpl;
import com.kakao.oreum.testhelper.Mocks;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class NearbySpotsImplTest extends TestBase {


    @Test
    public void 일정_개수만_취할수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().build(),
                Mocks.spot().build(),
                Mocks.spot().build()
        );
        assertThat(sut.size(), is(3));

        // When
        NearbySpots spots = sut.limit(2);

        // Then
        assertThat(spots.size(), is(2));
    }

    @Test
    public void 특정_spot만_필터링할_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().id(1L).proximity(Proximity.Near).build(),
                Mocks.spot().id(2L).proximity(Proximity.Far).build(),
                Mocks.spot().id(3L).proximity(Proximity.Near).build()
        );

        // When
        NearbySpots spots = sut.filter(SpotFilters.proximity(Proximity.Near));

        // Then
        assertThat(spots.size(), is(2));
        assertTrue(spots.all(s -> s.proximity() == Proximity.Near));
    }

    @Test
    public void 다른_순서로_정렬할수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().id(1L).proximity(Proximity.Near).build(),
                Mocks.spot().id(2L).proximity(Proximity.Far).build(),
                Mocks.spot().id(3L).proximity(Proximity.Immediate).build()
        );

        // When
        NearbySpots spots = sut.orderBy(Spot.PROXIMITY_ORDER);

        // Then
        assertThat(spots.get(0).get().id(), is(3L));
        assertThat(spots.get(1).get().id(), is(1L));
        assertThat(spots.get(2).get().id(), is(2L));
    }

    @Test
    public void 첫번째_spot을_가져올_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().id(1L).proximity(Proximity.Near).build(),
                Mocks.spot().id(2L).proximity(Proximity.Far).build(),
                Mocks.spot().id(3L).proximity(Proximity.Immediate).build()
        );

        // When
        NearbySpots spots = sut.orderBy(Spot.PROXIMITY_ORDER);

        // Then
        assertThat(spots.first().get().id(), is(3L));
    }

    @Test
    public void 마지막_spot을_가져올_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().id(10L).build(),
                Mocks.spot().id(2L).build(),
                Mocks.spot().id(7L).build()
        );

        // When
        NearbySpots spots = sut.orderBy(Spot.ID_ORDER);

        // Then
        assertThat(spots.last().get().id(), is(10L));
    }

    @Test
    public void 빈_spots에_대해서는_first와_last모두_빈값이다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of();

        // When
        // Then
        assertThat(sut.first().isPresent(), is(false));
        assertThat(sut.last().isPresent(), is(false));
    }

    @Test
    public void 빈_spots에_대해서는_isEmpty는_true이다() throws Exception {
        assertThat(NearbySpotsImpl.of().isEmpty(), is(true));
        assertThat(NearbySpotsImpl.of(Mocks.spot().build()).isEmpty(), is(false));
    }

    @Test
    public void iterable로_직접_사용할_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().id(10L).build(),
                Mocks.spot().id(2L).build(),
                Mocks.spot().id(7L).build()
        );

        // When
        int count = 0;
        for (Spot it : sut) {
            count++;
        }

        // Then
        assertThat(count, is(sut.size()));
    }

    @Test
    public void index메소드를_이용하여_분류할_수있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Immediate).build()
        );

        // When
        Map<Proximity, Collection<Spot>> proxSpots = sut.index(Spot::proximity);

        // Then
        assertThat(proxSpots.size(), is(2));
        assertThat(proxSpots.get(Proximity.Near).size(), is(2));
        assertThat(proxSpots.get(Proximity.Immediate).size(), is(1));
    }

    @Test
    public void toMap을_이용하여_map으로_변환할_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().region(Regions.제주창조경제혁신센터).build(),
                Mocks.spot().region(Regions.제주국제공항).build(),
                Mocks.spot().region(Regions.중문관광단지).build()
        );

        // When
        Map<Spot, Region> regionMap = sut.toMap(Spot::region);

        // Then
        assertThat(regionMap.values(), containsInAnyOrder(
                Regions.제주창조경제혁신센터,
                Regions.제주국제공항,
                Regions.중문관광단지
        ));
    }

    @Test
    public void foreach를_이용하여_spot에_대해_iterate할_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Immediate).build()
        );

        // When
        List<Proximity> list = new ArrayList<>();
        sut.foreach(s -> list.add(s.proximity()));

        // Then
        assertThat(list.size(), is(3));
    }

    @Test
    public void foreach에_function을_조합을_할_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Immediate).build()
        );

        // When
        List<Proximity> list = new ArrayList<>();
        sut.foreach(s -> list.add(s.proximity()));

        // Then
        assertThat(list.size(), is(3));
    }

    @Test
    public void transform을_이용해_다른_객체로_변환할수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Near).build(),
                Mocks.spot().proximity(Proximity.Immediate).build()
        );

        // When
        Collection<Proximity> proximities = sut.transform(Spot::proximity);

        // Then
        assertThat(proximities, hasSize(3));
        assertThat(proximities, contains(Proximity.Near, Proximity.Near, Proximity.Immediate));
    }

    @Test
    public void java기본_collection으로_변환할_수_있다() throws Exception {
        // Given
        NearbySpots sut = NearbySpotsImpl.of(
                Mocks.spot().id(1L).build(),
                Mocks.spot().id(1L).build(),
                Mocks.spot().id(2L).build(),
                Mocks.spot().id(3L).build()
        );

        // When
        Set<Spot> set = sut.toSet();
        List<Spot> list = sut.toList();

        // Then
        assertThat(set.size(), is(sut.size() - 1));
        assertThat(list.size(), is(sut.size()));
    }
}