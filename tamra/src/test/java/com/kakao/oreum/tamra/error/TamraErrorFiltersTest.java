package com.kakao.oreum.tamra.error;

import com.kakao.oreum.common.function.Predicate;

import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class TamraErrorFiltersTest {

    @Test
    public void test_occurred_since() throws Exception {
        // Given
        TamraError error1 = new TamraError(new IOException(), new Date(100));
        TamraError error2 = new TamraError(new IOException(), new Date(200));

        // When
        Predicate<TamraError> occurredSince = TamraErrorFilters.occurredSince(new Date(150));

        // Then
        assertThat(occurredSince.test(error1), is(false));
        assertThat(occurredSince.test(error2), is(true));
    }

    @Test
    public void test_occurred_before() throws Exception {
        // Given
        TamraError error1 = new TamraError(new IOException(), new Date(100));
        TamraError error2 = new TamraError(new IOException(), new Date(200));

        // When
        Predicate<TamraError> occurredBefore = TamraErrorFilters.occurredBefore(new Date(150));

        // Then
        assertThat(occurredBefore.test(error1), is(true));
        assertThat(occurredBefore.test(error2), is(false));
    }

    @Test
    public void test_caused_by() throws Exception {
        // Given
        TamraError error1 = new TamraError(new IOException(), new Date(100));
        TamraError error2 = new TamraError(new FileNotFoundException(), new Date(200));
        TamraError error3 = new TamraError(new RuntimeException(), new Date(200));

        // When
        Predicate<TamraError> causedBy = TamraErrorFilters.causedBy(IOException.class);

        // Then
        assertThat(causedBy.test(error1), is(true));
        assertThat(causedBy.test(error2), is(true));
        assertThat(causedBy.test(error3), is(false));
    }
}