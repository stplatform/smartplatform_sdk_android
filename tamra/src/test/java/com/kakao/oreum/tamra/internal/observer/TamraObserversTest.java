package com.kakao.oreum.tamra.internal.observer;

import com.kakao.oreum.ox.error.OxErrorObserver;
import com.kakao.oreum.ox.error.OxErrors;
import com.kakao.oreum.ox.task.RunOn;
import com.kakao.oreum.tamra.base.NearbySpots;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.base.TamraObserver;
import com.kakao.oreum.tamra.internal.spots.NearbySpotsImpl;
import com.kakao.oreum.testhelper.base.AndroidContextTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class TamraObserversTest extends AndroidContextTest {

    TamraObservers sut;

    @Mock
    OxErrorObserver mockObserver;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        OxErrors.addObserver(mockObserver);
        sut = new TamraObservers();
    }

    @Test
    public void test_이벤트를_분배한다() throws Exception {
        // Given
        TamraObserver o1 = mock(TamraObserver.class);
        TamraObserver o2 = mock(TamraObserver.class);

        // When
        Region region = Regions.카카오스페이스닷투;
        sut.addObserver(RunOn.BACKGROUND, o1);
        sut.addObserver(RunOn.BACKGROUND, o2);
        sut.didEnter(region);

        // Then
        backgroundLooper().idle(); // post 된건 모두 수행.
        verify(o1, times(1)).didEnter(eq(region));
        verify(o2, times(1)).didEnter(eq(region));
    }


    @Test
    public void remove된_observer한테는_안알랴쥼() throws Exception {
        // Given
        TamraObserver o1 = mock(TamraObserver.class);
        TamraObserver o2 = mock(TamraObserver.class);

        // When
        Region region = Regions.카카오스페이스닷투;
        sut.addObserver(RunOn.BACKGROUND, o1);
        sut.addObserver(RunOn.BACKGROUND, o2);
        sut.didEnter(region);
        sut.removeObserver(o2);
        sut.didExit(region);

        // Then
        backgroundLooper().idle(); // post 된건 모두 수행.
        verify(o1, times(1)).didExit(eq(region));
        verify(o2, never()).didExit(eq(region));
    }

    @Test
    public void observer가_등록된_순서대로_이벤트가_분배된다() throws Exception {
        // Given
        TamraObserver o1 = mock(TamraObserver.class);
        TamraObserver o2 = mock(TamraObserver.class);

        // When
        NearbySpots spots = NearbySpotsImpl.empty();
        sut.addObserver(RunOn.BACKGROUND, o1);
        sut.addObserver(RunOn.BACKGROUND, o2);
        sut.ranged(spots);

        // Then
        backgroundLooper().idle(); // post 된건 모두 수행.
        InOrder inOrder = inOrder(o1, o2);
        inOrder.verify(o1, times(1)).ranged(eq(spots));
        inOrder.verify(o2, times(1)).ranged(eq(spots));
    }

    @Test
    public void test_observer에서_예외가_발생하면_errorObserver에_전달된다() throws Exception {
        // Given
        TamraObserver o1 = mock(TamraObserver.class);
        doThrow(NullPointerException.class)
                .when(o1).didExit(Region.UNKNOWN);

        // When
        sut.addObserver(RunOn.BACKGROUND, o1);
        sut.didExit(Region.UNKNOWN);

        // Then
        backgroundLooper().idle();
        verify(o1, times(1)).didExit(eq(Region.UNKNOWN));
        verify(mockObserver, atLeast(1)).notify(any(NullPointerException.class));
    }

    @Test
    public void test_등록된_observer중_하나에서_예외가_발생해도_나머지_observer들에게는_이벤트가_정상적으로_전달된다() throws Exception {
        // Given
        TamraObserver o1 = mock(TamraObserver.class);
        TamraObserver o2 = mock(TamraObserver.class);
        doThrow(NullPointerException.class)
                .when(o1).didEnter(Region.UNKNOWN);

        // When
        sut.addObserver(RunOn.BACKGROUND, o1);
        sut.addObserver(RunOn.BACKGROUND, o2);
        sut.didEnter(Region.UNKNOWN);

        // Then
        backgroundLooper().idle(); // post 된건 모두 수행.
        verify(o1, times(1)).didEnter(eq(Region.UNKNOWN));
        verify(o2, times(1)).didEnter(eq(Region.UNKNOWN));
        verify(mockObserver, times(1)).notify(any(NullPointerException.class));
    }
}