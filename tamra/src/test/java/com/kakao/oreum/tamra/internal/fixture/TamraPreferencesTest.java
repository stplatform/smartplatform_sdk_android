package com.kakao.oreum.tamra.internal.fixture;

import org.junit.Test;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
public class TamraPreferencesTest {
    @Test(expected = UnsupportedOperationException.class)
    public void 인스턴스_생성되지_않는다() {
        new TamraPreferences();
    }

}