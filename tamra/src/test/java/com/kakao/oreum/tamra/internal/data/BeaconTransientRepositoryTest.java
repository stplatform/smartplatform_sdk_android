package com.kakao.oreum.tamra.internal.data;

import com.kakao.oreum.common.Optional;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.http.OxHttpClient;
import com.kakao.oreum.ox.http.HttpCallException;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.tamra.base.Profile;
import com.kakao.oreum.tamra.base.Region;
import com.kakao.oreum.tamra.base.Regions;
import com.kakao.oreum.tamra.internal.data.data.BeaconData;
import com.kakao.oreum.tamra.internal.http.HttpClientFactory;
import com.kakao.oreum.testhelper.Mocks;
import com.kakao.oreum.testhelper.base.AndroidContextTest;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class BeaconTransientRepositoryTest extends AndroidContextTest {
    private static final Logger LOG = getLogger(BeaconTransientRepositoryTest.class);
    BeaconRepository sut;

    OxHttpClient httpClient;

    @Before
    public void setUp() throws Exception {
        OxHttpClient hc = HttpClientFactory.create(getTamraConfig(Profile.TEST));
        httpClient = spy(hc);
        sut = new BeaconTransientRepository(httpClient);
    }

    @Test
    public void 특정_region에_속한_모든_비콘데이터를_조회할_수_있다() throws Exception {
        // Given
        Region region = Regions.제주국제공항;

        // When
        Collection<BeaconData> beacons = sut.findBy(region);

        // Then
        LOG.debug("beacons: {}", beacons);
        assertThat(beacons, not(empty()));
        assertThat(OxIterable.from(beacons).all(b -> b.regionId() == region.id()), is(true));
    }

    @Test
    public void 특정_id로_beaconData를_조회할_수_있다() throws Exception {
        // Given

        // When
        Optional<BeaconData> data = sut.find(23);

        // Then
        LOG.debug("data:{}", data);
        assertThat(data.isPresent(), is(true));
    }

    @Test
    public void 유효하지_않은_id로_조회하면_빈값이_온다() throws Exception {
        // Given

        // When
        Optional<BeaconData> data = sut.find(0);

        // Then
        LOG.debug("data:{}", data);
        assertThat(data.isPresent(), is(false));
    }

    @Test
    public void beaconId로_spotdata를_조회할_수_있다() throws Exception {
        // Given

        // When
        Optional<String> spotData = sut.getSpotData(23);

        // Then
        LOG.debug("spots:{}", spotData);
        assertThat(spotData.isPresent(), is(true)); // 테스트 계정엔 데이터가 있다. 항상
    }

    @Test
    public void 감지된_beacon으로_beaconData를_찾을_수_있다() throws Exception {
        // Given
        BeaconData beaconData = sut.find(2).get();
        Beacon beacon = Mocks.beacon()
                .uuid(beaconData.uuid())
                .major(beaconData.major())
                .minor(beaconData.minor())
                .build();

        // When
        Optional<BeaconData> found = sut.findBy(beacon);

        // Then
        assertThat(found.isPresent(), is(true));
    }

    @Test
    public void spot_data조회는_캐시하지않고_매번_호출됨() throws Exception {
        // 서버에서 SDK에서의 접근이력통계를 취합하기 위해서 당분간 spot data api 호출은 캐시하지 않는다.
        // Given

        // When
        sut.getSpotData(1);
        sut.getSpotData(2);
        sut.getSpotData(3);
        sut.getSpotData(2);
        sut.getSpotData(3);
        sut.getSpotData(3);

        // Then
        verify(httpClient, times(1)).requestFor("/spots/1");
        verify(httpClient, times(2)).requestFor("/spots/2");
        verify(httpClient, times(3)).requestFor("/spots/3");
    }

    @Test
    public void spotData조회시_오류가_있으면_optional_empty를_리턴한다() throws Exception {
        // Given
        when(httpClient.requestFor("/spots/1"))
                .thenThrow(HttpCallException.class);
        // When

        Optional<String> data = sut.getSpotData(1);

        // Then
        assertThat(data.isPresent(), is(false));
    }
}