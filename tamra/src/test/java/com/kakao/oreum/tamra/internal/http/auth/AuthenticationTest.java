package com.kakao.oreum.tamra.internal.http.auth;

import com.kakao.oreum.infra.time.SystemTimeService;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.ox.logger.Logger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.8.0
 */
public class AuthenticationTest {
    private static final Logger LOG = getLogger(AuthenticationTest.class);
    @Mock
    TimeService timeService;

    String authToken;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        authToken = "dummy-auth-token";
        when(timeService.now())
                .thenReturn(
                        new Date(0), new Date(10),
                        new Date(20), new Date(30),
                        new Date(40), new Date(50),
                        new Date(60), new Date(70),
                        new Date(80), new Date(90),
                        new Date(100)
                );

    }

    @Test
    public void test_만료된_상태이면_authToken에_대해_Null을_제공한다() throws Exception {
        // Given
        Authentication auth = new Authentication(
                authToken,
                timeService.now(), // 이미 만료됨.
                timeService);

        // When
        String token = auth.authToken();

        // Then
        assertThat(token, nullValue());
    }

    @Test
    public void 유효한_인증상태이면_authToken을_제공한다() throws Exception {
        // Given
        Authentication auth = new Authentication(
                authToken,
                new Date(100), // 만료 안됌
                timeService);

        // When
        String token = auth.authToken();

        // Then
        assertThat(token, is(authToken));
    }

    @Test
    public void authtoken이_null이면_invalid이다() throws Exception {
        // Given
        Authentication auth = new Authentication(null, new Date(100), timeService);

        // When
        // Then
        LOG.debug("auth:{}", auth);
        assertThat(auth.isValid(), is(false));
    }

    @Test
    public void 만료시각이_null이면_만료된_auth가_생성된다() throws Exception {
        // Given
        Authentication auth = new Authentication(authToken, null, timeService);

        // When

        // Then
        LOG.debug("auth:{}", auth);
        assertThat(auth.isValid(), is(false));
    }

    @Test(expected = NullPointerException.class)
    public void timeservice없이_만들면_예외가_발생한다() throws Exception {
        // Given

        // When
        new Authentication(authToken, new Date(), null);

        // Then
        fail("should raise exception");
    }

    @Test
    public void 토큰과_만료_시각이_같아야_동일_객체로_취급한다() throws Exception {
        // Given
        Authentication sut1 = new Authentication(authToken, new Date(100), timeService);
        Authentication sut2 = new Authentication(authToken, new Date(100), new SystemTimeService());
        Authentication sut3 = new Authentication("differnt_token", new Date(100), timeService);
        Authentication sut4 = new Authentication(authToken, new Date(200), timeService);

        // When
        // Then
        assertThat(sut1, is(sut1));
        assertThat(sut1.equals(null), is(false));
        assertThat(sut1, is(sut2));
        assertThat(sut1.hashCode(), is(sut2.hashCode()));
        assertThat(sut1, not(sut3));
        assertThat(sut1.hashCode(), not(sut3.hashCode()));
        assertThat(sut1, not(sut4));
        assertThat(sut1.hashCode(), not(sut4.hashCode()));
    }


}