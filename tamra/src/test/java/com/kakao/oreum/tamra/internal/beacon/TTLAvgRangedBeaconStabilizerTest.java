package com.kakao.oreum.tamra.internal.beacon;

import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.ox.collection.OxIterable;
import com.kakao.oreum.ox.collection.Lists;
import com.kakao.oreum.infra.beacon.Beacon;
import com.kakao.oreum.infra.beacon.BeaconRegion;
import com.kakao.oreum.infra.time.TimeService;
import com.kakao.oreum.testhelper.Mocks;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.5.0
 */
public class TTLAvgRangedBeaconStabilizerTest extends TestBase {
    private static final Logger LOG = getLogger(TTLAvgRangedBeaconStabilizerTest.class);

    TTLAvgRangedBeaconStabilizer sut;

    @Mock
    TimeService timeService;
    long ttlMSec = 10;
    int minRangeCout = 3;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        sut = new TTLAvgRangedBeaconStabilizer(timeService, ttlMSec, minRangeCout);

        when(timeService.now()).thenReturn(
                new Date(1), new Date(2), new Date(3),
                new Date(4), new Date(5), new Date(6),
                new Date(7), new Date(8), new Date(9),
                new Date(10), new Date(11), new Date(12)
        );
    }

    @Test
    public void minRangeCount보다_적게_range된건_무시된다() throws Exception {
        // Given
        UUID uuid = UUID.randomUUID();
        BeaconRegion region = Mocks.beaconRegion().uuid(uuid).build();
        Beacon beacon1 = Mocks.beacon().uuid(uuid).build();
        Beacon beacon2 = Mocks.beacon().uuid(uuid).build();

        // When
        sut.ranged(Lists.arrayList(
                new RangedBeacon(region, beacon1, timeService.now()),
                new RangedBeacon(region, beacon2, timeService.now())
        ));
        sut.ranged(Lists.arrayList(
                new RangedBeacon(region, beacon2, timeService.now())
        ));
        sut.ranged(Lists.arrayList(
                new RangedBeacon(region, beacon2, timeService.now())
        ));

        // Then
        Collection<RangedBeacon> stable = sut.stabledBeacons();
        LOG.debug("beacon1: {}", beacon1);
        LOG.debug("beacon2: {}", beacon2);
        LOG.debug("stable set: {}", stable);
        assertThat(OxIterable.from(stable)
                .any(b -> b.beacon().equals(beacon2)), is(true));
        assertThat(OxIterable.from(stable)
                .any(b -> b.beacon().equals(beacon1)), is(false));
    }

    @Test
    public void ttl보다_오래된_것들은_무시된다() throws Exception {
        // Given
        UUID uuid = UUID.randomUUID();
        BeaconRegion region = Mocks.beaconRegion().uuid(uuid).build();
        Beacon beacon1 = Mocks.beacon().uuid(uuid).build();
        Beacon beacon2 = Mocks.beacon().uuid(uuid).build();

        // When
        sut.ranged(Lists.arrayList(
                new RangedBeacon(region, beacon1, timeService.now()),
                new RangedBeacon(region, beacon1, timeService.now()),
                new RangedBeacon(region, beacon1, timeService.now()),
                new RangedBeacon(region, beacon2, timeService.now())
        ));
        when(timeService.now()).thenReturn(new Date(12 + ttlMSec));
        sut.ranged(Lists.arrayList(
                new RangedBeacon(region, beacon2, timeService.now()),
                new RangedBeacon(region, beacon2, timeService.now()),
                new RangedBeacon(region, beacon2, timeService.now())
        ));

        // Then
        Collection<RangedBeacon> stable = sut.stabledBeacons();
        LOG.debug("beacon1: {}", beacon1);
        LOG.debug("beacon2: {}", beacon2);
        LOG.debug("stable set: {}", stable);
        assertThat(OxIterable.from(stable)
                .any(b -> b.beacon().equals(beacon2)), is(true));
        assertThat(OxIterable.from(stable)
                .any(b -> b.beacon().equals(beacon1)), is(false));
    }

    @Test
    public void 측정값의_평균값이_반환된다() throws Exception {
        // Given
        UUID uuid = UUID.randomUUID();
        int major = 111;
        int minor = 2312;
        BeaconRegion region = Mocks.beaconRegion().uuid(uuid).build();
        Beacon beacon_1st = Mocks.beacon()
                .uuid(uuid)
                .major(major)
                .minor(minor)
                .rssi(10)
                .build();
        Beacon beacon_2nd = Mocks.beacon()
                .uuid(uuid)
                .major(major)
                .minor(minor)
                .rssi(20)
                .build();
        Beacon beacon_3rd = Mocks.beacon()
                .uuid(uuid)
                .major(major)
                .minor(minor)
                .rssi(30)
                .build();
        Date rangedAt = timeService.now();
        // When
        sut.ranged(Lists.arrayList(
                new RangedBeacon(region, beacon_1st, timeService.now()),
                new RangedBeacon(region, beacon_2nd, timeService.now()),
                new RangedBeacon(region, beacon_3rd, timeService.now())
        ));

        // Then
        Collection<RangedBeacon> stable = sut.stabledBeacons();
        LOG.debug("stable set: {}", stable);

        Beacon beacon = OxIterable.from(stable).first().get().beacon();
        assertThat(beacon.rssi(), is(20));
    }
}