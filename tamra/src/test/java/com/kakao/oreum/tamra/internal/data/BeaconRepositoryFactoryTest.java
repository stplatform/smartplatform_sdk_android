package com.kakao.oreum.tamra.internal.data;

import android.content.Context;

import com.kakao.oreum.ox.error.ExceptionFree;
import com.kakao.oreum.ox.http.OxHttpClient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.10.0
 */
@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class BeaconRepositoryFactoryTest {

    Context context;
    OxHttpClient httpClient;

    @Before
    public void setUp() throws Exception {
        context = RuntimeEnvironment.application;
        httpClient = OxHttpClient.forBaseUrl(new URL("http://test"));
    }

    @Test
    public void testCreateTransient() throws Exception {
        // given
        // when
        BeaconRepository beaconRepository = BeaconRepositoryFactory.create(context, httpClient, false);

        // then
        assertTrue(beaconRepository instanceof BeaconTransientRepository);
    }

    @Test
    public void testCreatePersistent() throws Exception {
        // given
        // when
        BeaconRepository beaconRepository = BeaconRepositoryFactory.create(context, httpClient, true);

        // then
        assertTrue(beaconRepository instanceof BeaconPersistentRepository);
    }
}