package com.kakao.oreum.tamra;

import com.github.davidmoten.geo.GeoHash;
import com.kakao.oreum.common.Optional;
import com.kakao.oreum.common.function.Consumer;
import com.kakao.oreum.ox.logger.Logger;
import com.kakao.oreum.tamra.base.AsyncFetcher;
import com.kakao.oreum.tamra.base.Spot;
import com.kakao.oreum.testhelper.Mocks;
import com.kakao.oreum.testhelper.base.TestBase;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static com.kakao.oreum.ox.logger.LoggerFactory.getLogger;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * @author cocoon.tf@kakaocorp.com
 * @since 0.6.0
 */
public class SpotTest extends TestBase {
    private static final Logger LOG = getLogger(SpotTest.class);

    @Test
    public void spot은_id에_의해_식별된다() throws Exception {
        // Given
        Spot spot1 = Mocks.spot().id(2L).description("2번").build();
        Spot spot2 = Mocks.spot().id(2L).description("글쎄 나도 2번?").build();
        Spot spot3 = Mocks.spot().id(1L).description("난 1번").build();

        // When
        // Then
        assertThat(spot1, is(spot1));
        assertThat(spot1, is(spot2));
        assertThat(spot2, is(spot1));
        assertThat(spot1.hashCode(), is(spot2.hashCode()));
        assertThat(spot1, not(spot3));
        assertThat(spot1.hashCode(), not(spot3.hashCode()));
        assertThat(spot1.equals(null), is(false));
    }

    @Test
    public void spot_geohash는_유효하다() throws Exception {
        // 이 테스트는 spot 클래스 기능에 대한 테스트는 아니다.
        // Mocks가 생성하는 geohash 값에 대한 테스트이다.
        // Given
        double latitude = 23.75861571;
        double longitude = 59.8106812;
        Spot spot = Mocks.spot()
                .latitude(latitude)
                .longitude(longitude)
                .build();

        // When
        // Then
        LOG.debug("spot:{}", spot);
        assertThat(spot.latitude(), is(latitude));
        assertThat(spot.longitude(), is(longitude));
        assertThat(spot.geohash(), is(GeoHash.encodeHash(latitude, longitude)));
    }

    @Test
    public void async_data조회() throws Exception {
        // Given
        // 여기서 생성하는 AsyncFetcher는 sync로 작동하므로 별도의 sleep은 필요없다.
        String data = "hello world";
        AsyncFetcher<Optional<String>> patcher = c -> c.accept(Optional.of(data));
        Spot spot = Mocks.spot()
                .dataFetcher(patcher)
                .build();
        LOG.debug("spot:{}", spot);

        // When
        Consumer<Optional<String>> consumer = spy(Consumer.class);
        spot.data(consumer);

        // Then
        final ArgumentCaptor<Optional> captor = ArgumentCaptor.forClass(Optional.class);
        verify(consumer, times(1)).accept(captor.capture());
        assertThat(captor.getValue(), is(data));
    }

    @Test
    public void data_patcher가_지정되지_않았을때_data조회() throws Exception {
        // Given
        Spot spot = Mocks.spot()
                .dataFetcher(null)
                .build();

        // When
        Consumer<Optional<String>> consumer = spy(Consumer.class);
        spot.data(consumer);

        // Then
        final ArgumentCaptor<Optional> captor = ArgumentCaptor.forClass(Optional.class);
        verify(consumer, times(1)).accept(captor.capture());
        assertThat(captor.getValue(), is(Optional.empty()));
    }

    @Test
    public void deprecated_data조회는_항상_optional_empty를_리턴한다() throws Exception {
        // Given
        // 여기서 생성하는 AsyncPatcher는 sync로 작동하므로 별도의 sleep은 필요없다.
        AsyncFetcher<Optional<String>> patcher = mock(AsyncFetcher.class);
        Spot spot = Mocks.spot()
                .dataFetcher(patcher)
                .build();

        // When
        Optional<String> spotData = spot.data();

        // Then
        verifyZeroInteractions(patcher); // 사용되지 않음.
        assertThat(spotData, is(Optional.empty()));
    }
}